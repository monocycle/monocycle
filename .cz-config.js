'use strict';

const FS = require('fs')

const packageScopes = FS.readFileSync(__dirname + '/.scripts/RELEASABLE_PACKAGES', 'utf8')
  .trim()
  .split('\n')

// console.log('packages:', packages)
// process.exit(1)

// var packageScopes = [
//   'component',
// ];

const otherScopes = ['root'];

module.exports = {
  types: [
    { value: 'feat', name: 'feat:     Add a new feature' },
    { value: 'fix', name: 'fix:      Submit a bug fix' },
    { value: 'doc', name: 'doc:     Documentation only changes' },
    { value: 'test', name: 'test:     Adding missing tests or correcting existing tests' },
    { value: 'release', name: 'release:  Publish a new version of a package.' },
    {
      value: 'chore',
      name:
        'chore:    Any internal changes that do not affect the\n            ' +
        'the users of packages. Includes refactoring.',
    },
  ],

  scopes: packageScopes
    .concat(otherScopes)
    .sort()
    .map(name => ({ name })),

  scopeOverrides: {
    chore: packageScopes.concat(otherScopes),
    feat: packageScopes,
    fix: packageScopes,
    release: packageScopes,
    test: packageScopes,
  },

  allowCustomScopes: false,
  allowBreakingChanges: ['feat', 'fix', 'test'],
};
