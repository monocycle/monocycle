#!/usr/bin/env bash
# exit 0

# git stash -q -k --include-untracked

# git stash save -u 

# .scripts/run-tests.sh
pnpm run lint && pnpm run test && pnpm run build

retVal=$?


# git stash apply -q
git add -A

if [ $retVal -ne 0 ]; then
    echo "pre-commit: run-tests.sh failed with code $retVal"
    exit $retVal
fi
echo "pre-commit: run-tests.sh succeeded"

exit 0
