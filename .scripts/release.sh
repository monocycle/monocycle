#!/usr/bin/env bash
packageName=$1
branchName=$(git rev-parse --abbrev-ref HEAD)

if [[ "$branchName" != "master" ]]; then
  printf "release.sh is only allowed on master branch\n\n"
  exit 1
fi


if [[ -z $packageName ]]; then
  echo "Please run \"pnpm run release -- <packageName>\""
  exit 1
fi

versionType=""
needsRelease=0
exitstatus=0
node .scripts/check-release.js $packageName || needsRelease=$?;

# git stash -q --keep-index --include-untracked

if [ $needsRelease -eq 1 ]; then

  echo "$packageName will not release a patch, we are following ComVer"
  exitstatus=$needsRelease

elif [ $needsRelease -eq 2 ]; then

  echo "$packageName needs MINOR release"
  versionType="MINOR"
  .scripts/bump.js "$packageName/package.json" --minor || exitstatus=$?;
elif [ $needsRelease -eq 3 ]; then

  echo "$packageName needs MAJOR release"
  versionType="MAJOR"
  .scripts/bump.js "$packageName/package.json" --major || exitstatus=$?;
fi


if [[ $exitstatus != 0 ]]; then

  echo "$packageName failed with code [$exitstatus]"
  # git stash apply -q
  exit $exitstatus;
fi

if [ $needsRelease -eq 0 ]; then

  echo "$packageName has nothing to release"
  # git stash apply -q
  exit 0
fi


pnpm recursive --filter "@monocycle/$packageName" run test

retVal=$?
if [ $retVal -ne 0 ]; then
    echo "release: $packageName tests failed with code $retVal"
    # git stash apply -q
    exit $retVal
fi


# # pnpm run docs
touch "$packageName/CHANGELOG.md"
pnpm recursive --filter "@monocycle/$packageName" run changelog

git add "$packageName/CHANGELOG.md" "$packageName/package.json"
git commit --no-verify -m "release($packageName): $(cat $packageName/package.json | $(pnpm bin)/jase version)"

# git stash apply -q

git push origin master

pnpm recursive --filter "@monocycle/$packageName" exec -- pnpm publish --no-git-checks

echo "✓ Released new $versionType for $packageName"

