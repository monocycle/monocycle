#!/usr/bin/env node

const conventionalChangelog = require('conventional-changelog');
const addStream = require('add-stream');
const tempfile = require('tempfile');
const Fs = require('fs');

const theCommitThatStartedTheMonorepo = Fs
  .readFileSync(__dirname + '/SEED_COMMIT', 'utf8')
  .trim();

const packagesWithChangelog = Fs
  .readFileSync(__dirname + '/RELEASABLE_PACKAGES', 'utf8')
  .trim()
  .split('\n');

const argPackage = process.argv[2];

// Assume that for each package we will start iterating from
// theCommitThatStartedTheMonorepo onwards.
const startCommits = {};
packagesWithChangelog.forEach(package => {
  startCommits[package] = theCommitThatStartedTheMonorepo;
});

const runUpdateChangelogs = () => {

  packagesWithChangelog
    .filter(package => {
      if (typeof argPackage === 'string' && argPackage.length > 0) {
        return argPackage === package;
      } else {
        return true;
      }
    })
    .forEach(package => {

      console.log('updating changelog for package ' + package);

      const filename = __dirname + '/../' + package + '/CHANGELOG.md';
      const changelogOpts = {
        preset: 'angular',
        releaseCount: 0,
        pkg: {
          path: __dirname + '/../' + package + '/package.json',
        },
        transform: (commit, cb) => {
          if (commit.scope === package) {
            cb(null, commit);
          } else {
            cb();
          }
        },
      };
      const gitRawCommitsOpts = { from: startCommits[package] };

      const readStream = Fs.createReadStream(filename);
      const tmpFile = tempfile();

      conventionalChangelog(
        changelogOpts,
        { repository: 'gitlab:monocycle/monocycle' },
        gitRawCommitsOpts
      )
        .pipe(addStream(readStream))
        .pipe(Fs.createWriteStream(tmpFile))
        .on('finish', () => {
          Fs.createReadStream(tmpFile)
            .pipe(Fs.createWriteStream(filename));
        });
    })
}

// Update the startCommit for each package, looking for release commits
// for each package.
conventionalChangelog({
  preset: 'angular',
  append: true,
  transform: (commit, cb) => {
    if (commit.type === 'release') {
      startCommits[commit.scope] = commit.hash;
    }
    cb();
  }
}, {}, { from: theCommitThatStartedTheMonorepo, reverse: true })
  .on('end', runUpdateChangelogs).resume();

