# @monocycle

## Packages dependencies:


```graphviz
digraph G {
size="7,10"
	page="8.5,11"
	center=""
	node [color=lightblue2, style=filled];

	"Symbols";
  
	"After";

	"Isolated";

	"Listener";

	"State" -> "Listener";

	"Dynamic" -> "After";
	"Dynamic" -> "Listener";

	"Switch" -> "After";
	"Switch" -> "Listener";
	"Switch" -> "Dynamic";

	"Http" -> "After";
	"Http" -> "Listener";
	"Http" -> "State";

	"History" -> "Switch";
	"History" -> "Dynamic";
	"History" -> "Listener";
	"History" -> "Isolated";

	"Loader" -> "Http";
	"Loader" -> "State";
	"Loader" -> "Listener";

	"Dom" -> "After";
	"Dom" -> "Listener";
	"Dom" -> "Dynamic";
	"Dom" -> "Isolated";
	"Dom" -> "State";

	"Form" -> "After";
	"Form" -> "Listener";
	"Form" -> "Isolated";
	"Form" -> "Dom";
	"Form" -> "Http";
	"Form" -> "State";
	"Form" -> "Collection";

  "Image" -> "Dom";
	"Image" -> "Dynamic";
	"Image" -> "Isolated";
	"Image" -> "Loader";

  "Collection" -> "After";
	"Collection" -> "Listener";

  "Cropper" -> "Dom";
	"Cropper" -> "Isolated";
	"Cropper" -> "Listener";
	"Cropper" -> "State";
}

```