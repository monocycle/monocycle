import unless from 'ramda/src/unless.js';
import { Component } from '@monocycle/component';
import { assign } from '@monocycle/component/lib/utilities/assign.js';
import { coerce } from '@monocycle/component/lib/utilities/coerce.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import identity from 'ramda/src/internal/_identity.js';
import pipe from 'ramda/src/pipe.js';
import { makeEmptyObject } from '@monocycle/component/lib/utilities/empty.js';





export const WithAfter = pipe(
  coerce,
  ({ has }) => {

    return pipe(
      identity,
      unless(isFunction, makeEmptyObject),
      component => {

        return Component(assign(component)(sources => {

          return pipe(
            component,
            sinks => pipe(...has)(sinks, sources)
          )(sources);
        }));
      }
    );
  }
);