import test from 'ava';
import { WithAfter } from './After.js';
import values from 'ramda/src/values.js';
import keys from 'ramda/src/keys.js';
import { Stream as $ } from 'xstream';



test('create after', t => {

  t.plan(6);

  const _sources = {};
  const _sinks = {
    ga: $.of('sdjfhgfg'),
    meu: $.of('Hello'),
    zo: $.of(39),
  };

  const before = sources => {

    t.is(sources, _sources);

    return _sinks;
  };

  const component = WithAfter((sinks, sources) => {

    t.is(sinks, _sinks);
    t.is(sources, _sources);

    return {
      bu: sinks.zo.map(n => n + 3),
      ga: sinks.meu.map(s => s + ' world'),
    };
  })(before);

  const sinks = component(_sources);

  t.deepEqual(keys(sinks), ['bu', 'ga']);

  return $.combine(...values(sinks)).map(([bu, ga]) => {

    t.is(ga, 'Hello world');
    t.is(bu, 42);
  });
});