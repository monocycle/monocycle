import unless from 'ramda/src/unless.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import filter from 'ramda/src/filter.js';
import { Component } from '@monocycle/component';
import { assign } from '@monocycle/component/lib/utilities/assign.js';
import { coerce } from '@monocycle/component/lib/utilities/coerce.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import identity from 'ramda/src/internal/_identity.js';
import pipe from 'ramda/src/pipe.js';
import { makeEmptyObject } from '@monocycle/component/lib/utilities/empty.js';





export const WithBefore = pipe(
  coerce,
  over(lensProp('has'), filter(isFunction)),
  ({ has }) => {

    return pipe(
      identity,
      unless(isFunction, makeEmptyObject),
      component => {

        const before = Component(assign(component)(sources => {

          return pipe(
            sources => pipe(...has)(sources),
            component,
          )(sources);
        }));

        return before;
      }
    );
  }
);