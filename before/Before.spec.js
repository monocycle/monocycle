import test from 'ava';
import { WithBefore } from './Before.js';
import values from 'ramda/src/values.js';
import keys from 'ramda/src/keys.js';
import join from 'ramda/src/join.js';
import { Stream as $ } from 'xstream';





test('create before', t => {

  // t.plan(6);

  const _sources = {
    ga: $.of('world'),
    meu: $.of('Hello'),
  };

  const before = sources => {

    return {
      bu: $.combine(
        sources.meu,
        sources.ga
      ).map(join(' ')),
    };
  };

  const component = WithBefore(sources => {

    return {
      ...sources,

      // ga: $.of('bye'),
      meu: $.of('Bye'),
      // zo: $.of(42),
    };
  })(before);

  const sinks = component(_sources);

  t.deepEqual(keys(sinks), ['bu']);

  return $.combine(...values(sinks)).map(([bu]) => {

    t.is(bu, 'Bye world');
    // t.is(bu, 42);
  });

});