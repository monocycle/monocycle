# 5.0.0 (2020-11-09)


### Bug Fixes

* **before:** depends on component@10.1 ([6320983](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6320983961ba9eefd484be8f5b69b5a8d5a75c38))
* **before:** make package esm compatible ([9e612ad](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9e612ad62f3de8140ad2d52b9f1e2ae6eb4f512e))


### BREAKING CHANGES

* **before:** maybe



# 4.12.0 (2020-10-07)


### Bug Fixes

* **before:** depends on component@8.6 ([adbea54](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/adbea54dd032488a32558d91b60eae8fd5b79035))



# 4.10.0 (2020-05-19)


### Bug Fixes

* **before:** remove type:module ([14582fb](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/14582fbe9c8f2a2dc812c48038ee25f8d040c137))



# 4.9.0 (2020-02-18)


### Bug Fixes

* **before:** update dependencies ([cb66203](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/cb662032594216cb4540a64ad4a963e61948fbe1))



# 4.8.0 (2020-01-20)


### Bug Fixes

* **before:** always forward properties ([0898f92](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0898f924a3d80f9193623a78b422a7a1e7147e4d))



# 4.7.0 (2020-01-20)


### Bug Fixes

* **before:** depends on component@5.4 ([63524ee](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/63524eee01063e3991703de08107f72c0f5e0e3a))
* **before:** depends on listener@5.4 ([af23e5a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/af23e5a94ad0bc644ff202f6e625fc126c7ce66c))



# 4.6.0 (2020-01-15)


### Bug Fixes

* **before:** add tests ([8ed7ebb](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8ed7ebb1fa168b82ad07e0b62b89bfaeebda190e))



# 4.5.0 (2020-01-15)


### Features

* **before:** create package ([a4fece3](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/a4fece3801ecbdca74f072b5f8f1eec1beb57c90))



# 4.2.0 (2019-12-02)


### Bug Fixes

* **after:** update dependencies ([02c1279](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/02c127955f96762a0066e71df6db29beddb02fb1))



# 4.1.0 (2019-12-02)


### Bug Fixes

* **after:** add lock file ([3b8edff](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3b8edff5e8151eaafb85140071a3bcd2e22960e2))



# 4.0.0 (2019-11-11)


### Features

* **after:** hello es modules ([ac9e6ea](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ac9e6ea18380b01d9ba1c86d278fec255a6f4960))


### BREAKING CHANGES

* **after:** yes



# 3.13.0 (2019-11-11)


### Bug Fixes

* **after:** revert "hello es modules" ([639f85f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/639f85f704733735f64ba93ceaaf3961ed0f9899))



# 3.11.0 (2019-11-11)


### Features

* **after:** hello es modulesBREAKING CHANGE:yes ([ecc5b82](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ecc5b8242e58a04b50de8c2a767b4680c3057276))



## 3.10.0 (2019-05-11)

* fix(after): depend on component@4.1 ([0a7c270](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/0a7c270))



## 3.9.0 (2019-05-11)

* fix(after): declare as module ([2752e79](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/2752e79))



## 3.8.0 (2019-04-08)

* fix(after): depend on component@4.0 ([d7c0036](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/d7c0036))



## 3.7.0 (2019-04-02)

* fix(after): correct invalid main in package.json ([f6658e4](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/f6658e4))



## 3.6.0 (2019-03-23)

* fix(after): depend on component@3.6 ([e3fe4df](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/e3fe4df))



## 3.5.0 (2019-03-23)

* fix(after): depend on component@3.6 ([4c6c5e2](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/4c6c5e2))



## 3.4.0 (2019-03-23)

* fix(after): depend on component@3.5 ([ae7464f](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/ae7464f))



## 3.3.0 (2019-03-23)

* fix(after): depend on component@3.4 ([a0829bb](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/a0829bb))



## 3.2.0 (2019-03-10)

* fix(after): works without previous component ([c68c227](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/c68c227))
* test(after): add a test ([91bb2c0](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/91bb2c0))



## 3.1.0 (2019-03-09)

* fix(after): depend on component@3.2 ([6aa87ca](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/6aa87ca))



## 3.0.0 (2019-03-02)

* chore(after): add release npm script ([d042e98](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/d042e98))
* chore(after): echo exit code of check-release task ([84fe915](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/84fe915))
* chore(after): update npm scripts ([d0fe549](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/d0fe549))
* feat(after): update component dependency to 3.1 ([3edc226](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/3edc226))


### BREAKING CHANGE

* Now use Component factory


## 2.1.0 (2019-02-25)

* fix(after): add node_modules to .npmignore ([19804e8](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/19804e8))



## 2.0.0 (2019-02-03)

* fix(after): update @component ([cda1b9b](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/cda1b9b))


### BREAKING CHANGE

* y
