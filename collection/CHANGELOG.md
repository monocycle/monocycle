# 1.3.0 (2022-01-23)


### Bug Fixes

* **collection:** use cjs versions of cycles modules ([7fd3a78](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/7fd3a78cb63aa67c5307478d00d5f8d1256852c9))



# 1.0.0 (2020-11-09)


### Bug Fixes

* **collection:** make package esm compatible ([67fd51e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/67fd51e4d34329d0b54ab8c4b9da3609b79afdef))


### BREAKING CHANGES

* **collection:** maybe



# 0.4.0 (2020-10-07)


### Bug Fixes

* **collection:** update dependencies ([f4778f3](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f4778f33187f3ce8cac1e2054aa5346b453d2371))



# 0.3.0 (2020-08-15)


### Features

* **collection:** add DynamicCollection ([65076e5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/65076e512b19c3fa81e011deb24e4d47908a9650))



# 0.2.0 (2020-08-13)


### Features

* **collection:** pass sources to collectSinks ([e019cd5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/e019cd52ac02be34d13edd0600b791fd39f3c260))



# 0.1.0 (2020-08-02)


### Features

* **collection:** create package ([0394c5e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0394c5e3d2e215603305fe3f9e676476505ec3c0))



