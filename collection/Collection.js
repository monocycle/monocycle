import $ from 'xstream';
import { adapt } from '@cycle/run/lib/adapt.js';
import isolate from '@cycle/isolate';
import { pickMerge } from '@cycle/state/lib/cjs/pickMerge.js';
import { pickCombine } from '@cycle/state/lib/cjs/pickCombine.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import unless from 'ramda/src/unless.js';
import always from 'ramda/src/always.js';
import pipe from 'ramda/src/pipe.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { WithAfter } from '@monocycle/after';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { mergeSinks } from 'cyclejs-utils';
import isUndefined from 'ramda-adjunct/src/isUndefined.js';
import isNonEmptyArray from 'ramda-adjunct/src/isNonEmptyArray.js';





export class Instances {
  /**
   * An object representing all instances in a collection of components. Has the
   * methods pickCombine and pickMerge to get the combined sinks of all instances.
   */
  constructor(instances$) {
    this._instances$ = instances$;
  }
  /**
   * Like `merge` in xstream, this operator blends multiple streams together, but
   * picks those streams from a collection of component instances.
   *
   * Use the `selector` string to pick a stream from the sinks object of each
   * component instance, then pickMerge will merge all those picked streams.
   *
   * @param {String} selector a name of a channel in a sinks object belonging to
   * each component in the collection of components.
   * @return {Function} an operator to be used with xstream's `compose` method.
   */
  pickMerge(selector) {
    return adapt(this._instances$.compose(pickMerge(selector)));
  }
  /**
   * Like `combine` in xstream, this operator combines multiple streams together,
   * but picks those streams from a collection of component instances.
   *
   * Use the `selector` string to pick a stream from the sinks object of each
   * component instance, then pickCombine will combine all those picked streams.
   *
   * @param {String} selector a name of a channel in a sinks object belonging to
   * each component in the collection of components.
   * @return {Function} an operator to be used with xstream's `compose` method.
   */
  pickCombine(selector) {
    return adapt(this._instances$.compose(pickCombine(selector)));
  }
}

export const defaultItemScope = (/* key */) => ({ '*': null });

export const instanceLens = (itemKey, key) => {
  key = `${key}`;
  return {
    get: (array) => {

      if (isNonEmptyArray(array))
        for (let i = 0, n = array.length; i < n; ++i)
          if (`${itemKey(array[i], i)}` === key)
            return array[i];
    },
    set: (array, item) => {

      if (isUndefined(array))
        return [item];

      if (isUndefined(item))
        return array.filter((state, i) => `${itemKey(state, i)}` !== key);

      return array.map((state, i) => {

        return `${itemKey(state, i)}` === key
          ? item
          : state;
      });
    },
  };
};

export const identityLens = {
  get: (outer) => outer,
  set: (outer, inner) => inner,
};

export const collectSinks = instances => ({
  state: instances.pickMerge('state'),
  Style: instances.pickMerge('Style'),
  DOM: instances.pickCombine('DOM')
    .map(children => ({ sel: 'div', data: {}, children })),
});

const _Collection = options => {

  return sources => {

    const name = options.channel || 'state';
    const itemKey = options.itemKey;
    const itemScope = options.itemScope || defaultItemScope;
    const itemComp = options.item;
    const state$ = $.fromObservable(sources[name].stream);

    const instances$ = state$.fold(
      (acc, nextState) => {

        const { dict } = acc;

        if (Array.isArray(nextState)) {

          const nextInstArray = Array(nextState.length);
          const nextKeys = new Set();

          // add
          for (let i = 0, n = nextState.length; i < n; ++i) {

            const key = `${itemKey ? itemKey(nextState[i], i) : i}`;
            nextKeys.add(key);

            if (!dict.has(key)) {
              const stateScope = itemKey
                ? instanceLens(itemKey, key)
                : `${i}`;

              const otherScopes = itemScope(key);
              const scopes = typeof otherScopes === 'string'
                ? { '*': otherScopes, [name]: stateScope }
                : { ...otherScopes, [name]: stateScope };

              const sinks = isolate(itemComp, scopes)(sources);
              dict.set(key, sinks);

              nextInstArray[i] = sinks;
            } else {
              nextInstArray[i] = dict.get(key);
            }

            nextInstArray[i]._key = key;
          }

          // remove
          dict.forEach((_, key) => {
            if (!nextKeys.has(key)) {
              dict.delete(key);
            }
          });

          nextKeys.clear();

          return { dict: dict, arr: nextInstArray };
        }

        dict.clear();

        const key = `${itemKey ? itemKey(nextState, 0) : 'this'}`;
        const stateScope = identityLens;
        const otherScopes = itemScope(key);

        const scopes = typeof otherScopes === 'string'
          ? { '*': otherScopes, [name]: stateScope }
          : { ...otherScopes, [name]: stateScope };

        const sinks = isolate(itemComp, scopes)(sources);
        dict.set(key, sinks);

        return { dict: dict, arr: [sinks] };

      },
      { dict: new Map(), arr: [] }
    );

    return options.collectSinks(new Instances(instances$), sources);
  };
};

export const WithCollection = pipe(
  ensurePlainObj,
  over(lensProp('channel'), unless(isNonEmptyString, always('state'))),
  over(lensProp('itemScope'), unless(isFunction, always(defaultItemScope))),
  over(lensProp('collectSinks'), unless(isFunction, always(collectSinks))),
  ({
    collectSinks,
    channel,
    itemKey,
    itemScope,
    item,
    combiners,
  }) => {

    const collection = _Collection({
      collectSinks,
      channel,
      itemKey,
      itemScope,
      item,
    });

    return WithAfter((sinks, sources) => mergeSinks(
      [
        sinks,
        collection(sources),
      ],
      combiners,
    ));
  },
);

export const Collection = behaviorToFactory(WithCollection);
