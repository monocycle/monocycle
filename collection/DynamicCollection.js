import { Component } from '@monocycle/component';
import dropRepeats from 'xstream/extra/dropRepeats';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithCollection } from '@monocycle/collection';
import { extractSinks } from 'cyclejs-utils';
import { ensureString } from '@monocycle/component/lib/utilities/ensureString.js';
import unless from 'ramda/src/unless.js';
import lensProp from 'ramda/src/lensProp.js';
import over from 'ramda/src/over.js';
import call from 'ramda/src/call.js';
import prop from 'ramda/src/prop.js';
import pipe from 'ramda/src/pipe.js';
import filter from 'ramda/src/filter.js';
import always from 'ramda/src/always.js';
import assoc from 'ramda/src/assoc.js';
import has from 'ramda/src/has.js';
import __ from 'ramda/src/__.js';
import applyTo from 'ramda/src/applyTo.js';
import keys from 'ramda/src/keys.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';





export const WithDynamicCollection = pipe(
  ensurePlainObj,
  over(lensProp('SinksKeys'), unless(isFunction, always(keys))),
  over(lensProp('kinds'), pipe(
    ensurePlainObj,
    filter(isFunction),
    unless(has('default'), assoc('default', always(Component.empty)))
  )),
  ({
    kinds,
    itemKey,
    itemScope,
    SinksKeys,
    collectSinks,
  }) => pipe(
    WithCollection({
      itemKey,
      itemScope,
      item: sources => extractSinks(
        sources.state.stream
          .map(pipe(
            prop('kind'),
            ensureString,
            unless(has(__, kinds), always('default')),
          ))
          .compose(dropRepeats())
          .map(pipe(
            prop(__, kinds),
            call(),
            applyTo(sources),
          )).remember(),
        SinksKeys(sources),
      ),
      collectSinks,
    }),
  )
);

export const DynamicCollection = behaviorToFactory(WithDynamicCollection);

WithDynamicCollection.coerce = DynamicCollection.coerce = ensurePlainObj;