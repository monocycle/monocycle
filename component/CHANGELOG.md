# 10.3.0 (2022-01-24)


### Bug Fixes

* **component:** qdf ([2959566](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/2959566aace1b1ec3acf078efa7810bab5eb88b9))



# 10.2.0 (2022-01-23)


### Features

* **component:** relock ([401c13c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/401c13c99a2353106b9563e8e07cf3f24ba262b6))



# 10.1.0 (2020-11-09)


### Bug Fixes

* **component:** use conditional exports ([14c4cb0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/14c4cb0c572e86e442620e3130817990c365e38f))



# 10.0.0 (2020-11-04)


### Bug Fixes

* **component:** make package node esm compatible ([38137ee](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/38137ee4cfefdd7f6af84481f8d9cae4c75bb1dd))
* **component:** update ava ([5bc4165](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5bc41659859eb3d3de757ab372731ba983d9fdf1))


### BREAKING CHANGES

* **component:** maybe



# 8.6.0 (2020-10-07)


### Bug Fixes

* **component:** improve prop utility ([18f9621](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/18f9621743eae33fd5d1a02d380d38bbb5a37a53))



# 8.5.0 (2020-09-17)


### Features

* **component:** use ifElse utility ([350b39b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/350b39b40a1c1194ee7c599880c8aef220a64cef))



# 8.4.0 (2020-09-17)


### Features

* **component:** add ifElse utility ([f16e9b9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f16e9b96e96f13b266e3b6ecb8f1647fb71120bd))



# 8.3.0 (2020-05-19)


### Bug Fixes

* **component:** remove type:module ([357d1d1](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/357d1d17353e1d5077d0307d0387915c8caa76e5))



# 8.2.0 (2020-05-10)


### Features

* **component:** add flattenStream utility ([ab652b8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ab652b86f4ca94f2bff7a8b8d7c2926206675f8c))



# 8.1.0 (2020-05-04)


### Features

* **component:** pipe default to undefined when no args provided ([4366aeb](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4366aebc05d51e9cde2e0bd200ed73fa9e4ebeb5))



# 8.0.0 (2020-04-30)


### Features

* **component:** add and update utilities ([87a8561](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/87a85618921484fe3f69ca964a0d2d725caf56b1))


### BREAKING CHANGES

* **component:** yes



# 7.0.0 (2020-04-06)


### Features

* **component:** add PlainObject utility ([69f9653](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/69f96531b5536d687b627f02d01e71ebc530d4ba))
* **component:** add utility ([70722b0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/70722b0df72359e65bcae1caee1e1aba7bca3fd4))
* **component:** ensurePlainObj use PlainObject ([f70e63a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f70e63a9891859c0f50ca102ba6043127dc33225))


### BREAKING CHANGES

* **component:** yes



# 6.2.0 (2020-04-01)


### Bug Fixes

* **component:** remove assertFunction utility ([bdf69cc](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/bdf69cc1923fae2f8fb9559c996e3c1222570790))



# 6.1.0 (2020-03-26)


### Features

* **component:** include others utilities ([56cdda9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/56cdda95d6cb40eb588cdbf96608e464624996f9))



# 6.0.0 (2020-02-18)


### Features

* **component:** refactor Composite ([3e95668](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3e95668e30112d99d70616b64e085a82ccf5e8a4))


### BREAKING CHANGES

* **component:** yes



# 5.7.0 (2020-02-14)


### Bug Fixes

* **component:** repair fromEvent ([4fffd2c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4fffd2cb1d37e45cf2879b55b9898abc5c73fd13))



# 5.6.0 (2020-02-14)


### Features

* **component:** improve FromEvent ([cfe221c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/cfe221c9392c1bab165b4a3eb9b4828fce873e3f))



# 5.5.0 (2020-01-20)


### Bug Fixes

* **component:** always forward properties ([3017121](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/30171216554af8808ab32ca54294dac517e41cfa))



# 5.4.0 (2020-01-20)


### Bug Fixes

* **component:** always forward properties ([c7af762](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c7af7625ac6d703c88fe2532e10ce34e8625e347))



# 5.3.0 (2019-12-11)


### Features

* **component:** add ensureString and fix curring problems ([07de74d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/07de74d12aead47af54dd54204e41012bbeae4c2))



# 5.2.0 (2019-12-02)


### Bug Fixes

* **component:** update dependencies ([9adf2b3](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9adf2b3b23cf9769e252d8cbc4a9842065a302f7))



# 5.1.0 (2019-12-02)


### Bug Fixes

* **component:** add lock file ([41fb377](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/41fb377cd9991c82674e01afdb44696550af847c))



# 5.0.0 (2019-11-10)


### Features

* **component:** hello es modules ([e0f76b5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/e0f76b5670f72e7d5a643d19c8a4b388abedbd18))


### BREAKING CHANGES

* **component:** yes



## 4.1.0 (2019-05-11)

* chore(component): add a way to completely disable logging ([246db23](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/246db23))
* chore(component): update dependencies ([03f0997](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/03f0997))
* feat(component): add FromEvent utility ([ba7b6c1](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/ba7b6c1))
* feat(component): add WithComposite behavior ([7747331](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/7747331))
* test(component): refactor tests ([890c371](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/890c371))



## 4.0.0 (2019-04-08)

* feat(component): call mergeSinks even when 1 component ([49daeef](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/49daeef))


### BREAKING CHANGE

* Composite behavior has changed


## 3.10.0 (2019-03-31)

* feat(component): improve extend and add tests ([f3b1a22](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/f3b1a22))



## 3.9.0 (2019-03-28)

* fix(component): assign component properties the right way ([4ae9cc7](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/4ae9cc7)), closes [#2](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/issues/2)



## 3.8.0 (2019-03-27)

* feat(component): use behavior's coerce if specified ([098f96a](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/098f96a))
* chore(component): refactoring ([a914219](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/a914219))



## 3.7.0 (2019-03-24)

* feat(component): extract extend utility ([3fe3e0d](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/3fe3e0d))



## 3.6.0 (2019-03-23)

* fix(component): add missing apply call ([b3b9e2a](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/b3b9e2a))



## 3.5.0 (2019-03-23)

* fix(component): factory check the corrct index ([cdd9f53](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/cdd9f53))



## 3.4.0 (2019-03-23)

* fix(component): directly throw error in factory helpers ([5680b40](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/5680b40))



## 3.3.0 (2019-03-22)

* feat(component): add extendBehavior utility ([0c08324](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/0c08324))
* chore(component): remove default exports ([27f3b33](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/27f3b33))
* chore(component): use correct identity function ([dc63d26](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/dc63d26))



## 3.2.0 (2019-03-09)

* feat(component): coerce accept defaultOptions param ([26b317b](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/26b317b))



## 3.1.0 (2019-03-02)

* feat(component): add behaviorToFactory in utilities ([d331f32](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/d331f32))



## 3.0.0 (2019-03-01)

* feat(component): make Component and Composite singletons ([b04d529](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/b04d529))


### BREAKING CHANGE

* makeComponent has been removed


## 2.2.0 (2019-02-25)

* fix(component): add node_modules to .npmignore ([3d16bc9](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/3d16bc9))



## 2.1.0 (2019-02-25)

* fix(component): update dependencies ([8a44735](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/8a44735))



## 2.0.0 (2019-02-03)

* chore(component): add --fix flag to eslint and add watch-test script ([210247a](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/210247a))
* chore(component): add some utilities ([f96b605](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/f96b605))
* chore(component): remove unused scripts in package.json ([bb7f172](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/bb7f172))
* chore(component): uses mergeSinks from cyclejs-utils ([4d6eee3](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/4d6eee3))



## 1.0.0 (2019-02-02)




