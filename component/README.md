# @monocycle/component [WIP]

A tiny utility to Cycle no-handed !

The `Component` function is a monadic type constructor that encapsulates one or many [dataflow components](https://cycle.js.org)