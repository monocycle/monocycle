import { EmptyObject, makeEmptyObject } from './utilities/empty.js';
import { pipe } from './utilities/pipe.js';
import reject from 'ramda/src/reject.js';
import applyTo from 'ramda/src/applyTo.js';
import arrayOf from 'ramda/src/of.js';
import filter from 'ramda/src/filter.js';
import { ifElse } from './utilities/ifElse.js';
import unless from 'ramda/src/unless.js';
import propSatisfies from 'ramda/src/propSatisfies.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { mergeSinks } from 'cyclejs-utils';
import prop from 'ramda/src/prop.js';
import both from 'ramda/src/both.js';
import identical from 'ramda/src/identical.js';
import gt from 'ramda/src/gt.js';
import concat from 'ramda/src/concat.js';
import always from 'ramda/src/always.js';
import __ from 'ramda/src/__.js';
import apply from 'ramda/src/apply.js';
import isEmpty from 'ramda/src/isEmpty.js';
import ensureArray from 'ramda-adjunct/src/ensureArray.js';
import map from 'ramda/src/map.js';
import { behaviorToFactory } from './utilities/factory.js';
import { assign } from './utilities/assign.js';
import identity from 'ramda/src/internal/_identity.js';





/**
 * Creates a component from a function
 */
export const Component = pipe(
  unless(isFunction, makeEmptyObject),
  f => {

    const component = assign({
      ...f,
      isComponent: true,
      map: behavior => {

        return Component(assign({ ...component })(
          behavior(component)
        ));
      },
      concat: (others, options) => pipe(
        ensureArray,
        filter(isFunction),
        ifElse(isEmpty,
          always(component),
          pipe(
            concat([component]),
            components => Composite(components, options)
          ),
        ),
        Component
      )(others)
    })(sources => f(sources));

    return component;
  }
);

Component.empty = Component(EmptyObject);

/**
 * Creates a composite component from 0 or * components
 */
export const WithComposite = (components, combiners) => {

  return pipe(
    identity,
    unless(isFunction, always(Component.empty)),
    component => {

      return assign({ ...component, isComposite: true, })(pipe(
        ensureArray,
        concat([component]),
        reject(identical(Component.empty)),
        map(Component),
        ifElse(isEmpty,
          always(Component.empty),
          pipe(
            has => {

              return assign({ has })(sources => {

                return pipe(
                  map(applyTo(sources)),
                  ifElse(propSatisfies(gt(1), 'length'),
                    makeEmptyObject,
                    ifElse(propSatisfies(gt(2), 'length'),
                      prop(0),
                      pipe(
                        arrayOf,
                        concat(__, [combiners]),
                        apply(mergeSinks),
                      )
                    ),
                  ),
                )(has);
              });
            }
          )
        )
      )(components));
    },
    Component,
  );
};

export const Composite = behaviorToFactory(WithComposite);

export const isComponent = both(isFunction, prop('isComponent'));

export const isComposite = both(isFunction, prop('isComposite'));