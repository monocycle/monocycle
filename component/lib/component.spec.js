
import test from 'ava';
import concat from 'ramda/src/concat.js';
import identity from 'ramda/src/internal/_identity.js';
import map from 'ramda/src/map.js';
import prop from 'ramda/src/prop.js';
import Jsverify from 'jsverify';
import { Component } from './component.js';
import ensureArray from 'ramda-adjunct/src/ensureArray.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { pipe } from './utilities/pipe.js';
import when from 'ramda/src/when.js';
import filter from 'ramda/src/filter.js';
import keys from 'ramda/src/keys.js';
import uniq from 'ramda/src/uniq.js';
import always from 'ramda/src/always.js';
import flatten from 'ramda/src/flatten.js';
import { diagramArbitrary, withTime } from 'cyclejs-test-helpers';

const collectKinds = pipe(
  prop('kind'),
  ensureArray,
  filter(isFunction),
  map(pipe(
    when(
      prop('isComponent'),
      c => collectKinds(c)
    )
  )),
  flatten
);

const testsOptions = { tests: 100 };

export const ComponentMacro = test => (t, ...args) => test(t, Component, ...args);

const diagramsArb = Jsverify.dict(diagramArbitrary);

test('Monoid: right identity', t => {

  return withTime(Time => {

    const property = Jsverify.forall(diagramsArb, diagrams => {

      const component = pipe(
        map(Time.diagram),
        always,
        Component
      )(diagrams);

      const sinksA = component.concat(Component.empty)();
      const sinksB = component();

      uniq(concat(keys(sinksA), keys(sinksB)))
        .forEach(key => {
          Time.assertEqual(
            sinksA[key],
            sinksB[key],
            t.is.bind(t)
          );
        });

      return true;
    });

    Jsverify.assert(property, testsOptions);
  })();
});

test('Monoid: left identity', t => {

  return withTime(Time => {

    const property = Jsverify.forall(diagramsArb, diagrams => {

      const component = pipe(
        map(Time.diagram),
        always,
        Component
      )(diagrams);

      const composite = Component.empty.concat(component);

      const sinksA = composite();
      const sinksB = component();

      uniq(concat(keys(sinksA), keys(sinksB)))
        .forEach(key => {

          Time.assertEqual(
            sinksA[key],
            sinksB[key],
            t.is.bind(t)
          );
        });

      return true;
    });

    Jsverify.assert(property, testsOptions);

  })();
});

test('Monoid: associativity', t => {

  return withTime(Time => {

    const property = Jsverify.forall(diagramsArb, diagramsArb, diagramsArb, (a, b, c) => {

      const componentA = pipe(
        map(Time.diagram),
        always,
        Component
      )(a);

      const componentB = pipe(
        map(Time.diagram),
        always,
        Component
      )(b);

      const componentC = pipe(
        map(Time.diagram),
        always,
        Component
      )(c);

      const compositeA = componentA.concat(componentB).concat(componentC);
      const compositeB = componentA.concat(componentB.concat(componentC));

      const sinksA = compositeA();
      const sinksB = compositeB();

      concat(keys(sinksA), keys(sinksB))
        .forEach(key => {

          Time.assertEqual(
            sinksA[key],
            sinksB[key],
            t.is.bind(t)
          );
        });

      return true;
    });

    Jsverify.assert(property, testsOptions);

  })();
});

test('Functor: identity', t => {

  return withTime(Time => {

    const property = Jsverify.forall(diagramsArb, diagrams => {

      const component = pipe(
        map(Time.diagram),
        always,
        Component
      )(diagrams);

      const sinksA = component.map(identity)();
      const sinksB = component();

      uniq(concat(keys(sinksA), keys(sinksB)))
        .forEach(key => {

          Time.assertEqual(
            sinksA[key],
            sinksB[key],
            t.is.bind(t)
          );
        });

      return true;
    });

    Jsverify.assert(property, testsOptions);

  })();
});

test('Functor: composition', t => {

  return withTime(Time => {

    const property = Jsverify.forall(diagramsArb, diagramsArb, diagramsArb, (a, b, c) => {

      const component1 = pipe(
        map(Time.diagram),
        always,
        Component
      )(a);

      const component2 = pipe(
        map(Time.diagram),
        always,
        Component
      )(b);

      const component3 = pipe(
        map(Time.diagram),
        always,
        Component
      )(c);


      const f = component => sources => ({
        ...component(sources),
        ...component2(sources),
      });

      const g = component => sources => ({
        ...component(sources),
        ...component3(sources),
      });

      const composite1 = component1.map(f).map(g);
      const composite2 = component1.map(x => g(f((x))));

      const sinksA = composite1();
      const sinksB = composite2();

      uniq(concat(keys(sinksA), keys(sinksB)))
        .forEach(key => {

          Time.assertEqual(
            sinksA[key],
            sinksB[key],
            t.is.bind(t)
          );
        });

      return true;
    });

    Jsverify.assert(property, testsOptions);

  })();
});
