import isFunction from 'ramda-adjunct/src/isFunction.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import noop from 'ramda-adjunct/src/noop.js';
import { pipe } from './pipe.js';
import when from 'ramda/src/when.js';
import keys from 'ramda/src/keys.js';
import merge from 'ramda/src/merge.js';
import objOf from 'ramda/src/objOf.js';
import compose from 'ramda/src/compose.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import { ensurePlainObj } from './ensurePlainObj.js';
import { ensureArray } from './ensureArray.js';

export const WithFactoryMacro = factory => test => (t, args = [], ...others) => test(t, factory(...args), ...others);

export const withFunctionTest = (test = noop) => (t, ...rest) => {

  const testFunction = f => args => pipe(
    ensureArray,
    args => {

      t.true(isFunction(f));

      return f(...args);
    }
  )(args);

  testFunction.nbTests = 1;

  t.context.testFunction = testFunction;

  return test(t, ...rest);
};

export const WithComponentTest = makeBehavior => {

  const withComponentTest = compose(
    withFunctionTest,

    test => (t, options, ...rest) => {

      const { testFunction } = t.context;

      const testBehaviorFactory = testFunction(makeBehavior);

      const defaultOptions = pipe(
        when(isFunction, objOf('before')),
        ensurePlainObj,
        over(lensProp('before'), ensureArray),
        over(lensProp('expectedSinks'), ensureArray)
      )(options);

      const testComponent = (input, options) => {

        const {
          before = [],
          expectedSinks,
        } = merge(defaultOptions, ensurePlainObj(options));

        const behavior = testBehaviorFactory(input);

        const component = testFunction(behavior)(before);

        return sources => {

          const sinks = component(sources);

          t.true(isPlainObj(sinks));

          t.deepEqual(
            keys(sinks),
            expectedSinks
          );

          return sinks;
        };
      };

      testComponent.nbTests = (testFunction.nbTests * 2) + 2;

      t.context.testComponent = testComponent;

      return test(t, options, ...rest);
    },
  );

  return withComponentTest;
};