

export const assign = update => object => Object.assign(object, update);