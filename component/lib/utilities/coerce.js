import unless from 'ramda/src/unless.js';
import objOf from 'ramda/src/objOf.js';
import merge from 'ramda/src/merge.js';
import over from 'ramda/src/over.js';
import reject from 'ramda/src/reject.js';
import identity from 'ramda/src/internal/_identity.js';
import { pipe } from './pipe.js';
import lensProp from 'ramda/src/lensProp.js';
import ensureArray from 'ramda-adjunct/src/ensureArray.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import isUndefined from 'ramda-adjunct/src/isUndefined.js';
import { ensurePlainObj } from './ensurePlainObj.js';




export const Coerce = (key, defaultOptions) => pipe(
  identity,
  unless(isPlainObj, pipe(
    // when(isFalsy, EmptyObject),
    objOf(key),
    pipe(
      ensurePlainObj,
      merge
    )(defaultOptions)
  )),
  over(lensProp(key), pipe(ensureArray, reject(isUndefined))),
);

export const coerce = Coerce('has');