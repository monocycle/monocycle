import always from 'ramda/src/always.js';





export const EmptyObject = () => ({});

export const EmptyArray = () => [];

export const EmptyString = always('');

export const makeEmptyObject = always(EmptyObject);