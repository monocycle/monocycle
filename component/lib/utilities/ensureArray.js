import when from 'ramda/src/when.js';
import identity from 'ramda/src/internal/_identity.js';
import isUndefined from 'ramda-adjunct/src/isUndefined.js';
import _ensureArray from 'ramda-adjunct/src/ensureArray.js';
import { EmptyArray } from './empty.js';
import { pipe } from './pipe.js';





export const ensureArray = pipe(
  identity,
  when(isUndefined, EmptyArray),
  _ensureArray
);