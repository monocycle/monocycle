import unless from 'ramda/src/unless.js';
import isObjectLike from 'ramda-adjunct/src/isObjLike.js';
import { EmptyObject } from './empty.js';





export const ensureObject = unless(isObjectLike, EmptyObject);