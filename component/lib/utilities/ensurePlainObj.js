import unless from 'ramda/src/unless.js';
import { pipe } from './pipe.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import { PlainObject } from './object.js';
import identity from 'ramda/src/internal/_identity.js';





export const ensurePlainObj = pipe(
  identity,
  unless(isPlainObj, PlainObject)
);