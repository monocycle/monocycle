import unless from 'ramda/src/unless.js';
import { pipe } from './pipe.js';
import isString from 'ramda-adjunct/src/isString.js';
import { EmptyString } from './empty.js';
import identity from 'ramda/src/internal/_identity.js';





export const ensureString = pipe(
  identity,
  unless(isString, EmptyString),
);
