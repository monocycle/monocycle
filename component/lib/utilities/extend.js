import { pipe } from './pipe.js';
import apply from 'ramda/src/apply.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import unapply from 'ramda/src/unapply.js';
import { ifElse } from './ifElse.js';
import over from 'ramda/src/over.js';
import keys from 'ramda/src/keys.js';
import always from 'ramda/src/always.js';
import both from 'ramda/src/both.js';
import unless from 'ramda/src/unless.js';
import lensIndex from 'ramda/src/lensIndex.js';
import identity from 'ramda/src/identity.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import isNotEmpty from 'ramda-adjunct/src/isNotEmpty.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { assign } from './assign.js';
import { coerce } from './coerce.js';

/**
 * Create a factory function that extends a unary factory function
 * @param factory The factory to extend
 * @param defaultOptions The options that could be passed directly to the factory
 * @return another factory with default options set
 */
export const extend = pipe(
  unapply(identity),
  over(lensIndex(0), ifElse(isFunction,
    identity,
    provided => {
      throw new Error(`'factory' must be a component behavior factory (provided:${provided})`);
    },
  )),
  over(lensIndex(1), pipe(
    unless(
      both(
        isPlainObj,
        pipe(keys, isNotEmpty)
      ),
      provided => {
        throw new Error(`'defaultOptions' must be a non-empty plain object (provided:${provided})`);
      },
    )
  )),
  apply((factory, defaultOptions) => pipe(
    unless(isFunction, always(coerce))(factory.coerce),
    mergeDeepRight(defaultOptions),
    factory,
    assign(factory),
  ))
);