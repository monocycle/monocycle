import test from 'ava';
import { extend } from './extend.js';


test('extends a factory', t => {

  let factory = ({ ga = '', bu = '' }) => ({
    zo: '' + ga + (bu.text1 || '') + (bu.text2 || ''),
    bu: ga
  });

  factory = extend(factory, {
    ga: 42,
  });

  t.deepEqual(
    factory({
      bu: {
        text1: 'hello'
      }
    }),
    {
      zo: '42hello',
      bu: 42,
    }
  );

  factory = extend(factory, {
    ga: 43,
  });

  t.deepEqual(
    factory({
      bu: {
        text1: 'hi'
      }
    }),
    {
      zo: '43hi',
      bu: 43,
    }
  );

  factory = extend(factory, {
    bu: {
      text1: ' hi'
    },
    zo: 4
  });

  t.deepEqual(
    factory({
      ga: 44,
      bu: {
        text2: ' world!'
      }
    }),
    {
      zo: '44 hi world!',
      bu: 44,
    }
  );

});