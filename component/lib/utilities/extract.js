import { ifElse } from './ifElse.js';
import includes from 'ramda/src/includes.js';
import split from 'ramda/src/split.js';
import path from 'ramda/src/path.js';
import { pipe } from './pipe.js';
import { prop } from './prop.js';
import { ensureString } from './ensureString.js';





export const extract = pipe(
  ensureString,
  ifElse(includes('.'),
    pipe(
      split('.'),
      path,
    ),
    prop,
  ),
);