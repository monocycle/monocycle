import reduce from 'ramda/src/reduce.js';
import { pipe } from './pipe.js';
import { ensureArray } from './ensureArray.js';
import { ensureObject } from './ensureObject.js';
import { extract } from './extract.js';





export const extractAll = pipe(
  ensureArray,
  paths => pipe(
    ensureObject,
    object => {
      return reduce((before, path) => {
        return {
          ...before,
          [path]: extract(path)(object)
        };
      }, {})(paths);
    }
  )
);