import { pipe } from './pipe.js';
import apply from 'ramda/src/apply.js';
import { ifElse } from './ifElse.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import __ from 'ramda/src/__.js';





/** Transforms a component behavior factory into a component factory */
export const behaviorToFactory = ifElse(isFunction,
  Behavior => pipe(
    Behavior,
    apply(__, [])
  ),
  () => {
    throw new Error(`first parameter must be a component behavior factory`);
  },
);