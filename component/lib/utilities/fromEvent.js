import { Stream as $ } from 'xstream';
import { ensurePlainObj } from './ensurePlainObj.js';
import over from 'ramda/src/over.js';
import identity from 'ramda/src/internal/_identity.js';
import lensProp from 'ramda/src/lensProp.js';
import always from 'ramda/src/always.js';
import unless from 'ramda/src/unless.js';
import { pipe } from './pipe.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';





export const FromEvent = pipe(
  ensurePlainObj,
  over(lensProp('on'), unless(isNonEmptyString, always('addEventListener'))),
  over(lensProp('off'), unless(isNonEmptyString, always('removeEventListener'))),
  over(lensProp('extractEvent'), unless(isFunction, always(identity))),
  ({ on, off, extractEvent }) => eventType => node => {

    let listener;
    return $.create({
      start: observer => {

        listener = pipe(
          extractEvent,
          event => observer.next(event),
        );

        node[on](eventType, listener);
      },
      stop: () => node[off](eventType, listener)
    });
  }
);

export const fromOnOffEvent = FromEvent({
  on: 'on',
  off: 'off'
});