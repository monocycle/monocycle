




export const ifElse = (predicate, whenTrue, whenFalse) => {

  // eslint-disable-next-line no-undef
  if (global.ENV !== 'production') {

    if (typeof predicate !== 'function') {
      console.log('ifElse', { predicate });
      throw new Error(`ifElse: 'predicate' must be a function`);
    }

    if (typeof whenTrue !== 'function')
      throw new Error(`ifElse: 'whenTrue' must be a function`);

    if (typeof whenFalse !== 'function')
      throw new Error(`ifElse: 'whenFalse' must be a function`);
  }

  return value => {

    if (predicate(value))
      return whenTrue(value);

    return whenFalse(value);
  };
};