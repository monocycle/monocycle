
import both from 'ramda/src/both.js';
import isSafeInteger from 'ramda-adjunct/src/isSafeInteger.js';
import isNonNegative from 'ramda-adjunct/src/isNonNegative.js';





export const isNonNegativeInteger = both(
  isSafeInteger,
  isNonNegative
);