import both from 'ramda/src/both.js';
import isPositive from 'ramda-adjunct/src/isPositive.js';
import isInteger from 'ramda-adjunct/src/isInteger.js';





export const isPositiveInteger = both(isInteger, isPositive);
