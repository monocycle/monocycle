

import { pipe } from './pipe.js';
import unless from 'ramda/src/unless.js';
import concat from 'ramda/src/concat.js';
import when from 'ramda/src/when.js';
import join from 'ramda/src/join.js';
import arrayOf from 'ramda/src/of.js';
import objOf from 'ramda/src/objOf.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import always from 'ramda/src/always.js';
import { ensurePlainObj } from './ensurePlainObj.js';
import isString from 'ramda-adjunct/src/isString.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';



const coerce = pipe(
  when(isString, objOf('scope')),
  ensurePlainObj,
  over(lensProp('scope'), unless(isString, always('')))
);

export const Log = pipe(
  coerce,
  // over(lensProp('scope'), concat(scope)),
  over(lensProp('log'), unless(isFunction, always(console.log.bind(console)))), // eslint-disable-line
  over(lensProp('scope'), unless(isString, always(''))),
  ({ log: _log, scope }) => {

    if (!scope || !isString(scope))
      return _log;

    const log = (...args) => {

      if (Log.disabled)
        return;

      let prefix = scope;

      const isColored = isString(args[0]) && args[0].startsWith('%c');

      if (isColored) {
        prefix = '%c' + prefix + ' ' + args[0].slice(2);
        args.shift();
      }

      _log(
        prefix,
        ...args
      );
      return args[1];
    };

    log.partial = (...args) => {

      return x => {
        log(...args, x);
        return x;
      };
    };

    log.Log = pipe(
      coerce,
      over(lensProp('scope'), pipe(
        arrayOf,
        concat([scope]),
        join('.')
      )),
      Log,
    );

    return log;
  }
);