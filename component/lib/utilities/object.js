import { ifElse } from './ifElse.js';
import reduce from 'ramda/src/reduce.js';
import isObject from 'ramda-adjunct/src/isObj.js';
import { pipe } from './pipe.js';
import { EmptyObject } from './empty.js';





export const PlainObject = ifElse(isObject,
  pipe(
    Object.entries,
    reduce((before, [key, value]) => ({
      ...before,
      [key]: value,
    }), {}),
  ),
  EmptyObject
);

