import tail from 'ramda/src/tail.js';
import head from 'ramda/src/head.js';
import isEmpty from 'ramda/src/isEmpty.js';





export const pipe = (...functions) => {

  // eslint-disable-next-line no-undef
  if (global.ENV !== 'production') {

    if (isEmpty(functions))
      throw new Error(`pipe expects at least one argument`);

    for (let i = 0; i < functions.length; i++)
      if (typeof functions[i] !== 'function')
        throw new Error(`pipe only accepts function arguments`);

  }

  return (firstArg, ...args) => {

    return tail(functions).reduce((value, f) => {
      return f(value);
    }, head(functions)(firstArg, ...args));
  };
};