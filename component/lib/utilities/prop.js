import { ensureString } from './ensureString.js';
import { pipe } from './pipe.js';
import when from 'ramda/src/when.js';
import always from 'ramda/src/always.js';
import ifElse from 'ramda/src/ifElse.js';
import isEmpty from 'ramda/src/isEmpty.js';
import { isNonNegativeInteger } from './isNonNegativeInteger.js';
import { toString } from './toString.js';
import noop from 'ramda-adjunct/src/noop.js';
import isObjLike from 'ramda-adjunct/src/isObjLike.js';





export const prop = pipe(
  when(isNonNegativeInteger, toString),
  ensureString,
  when(isEmpty, always(noop)),
  key => ifElse(isObjLike,
    object => object[key],
    noop
  ),
);