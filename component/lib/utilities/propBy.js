import { prop } from './prop.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import isNil from 'ramda/src/isNil.js';





export const propBy = extractKey => {

  const mustAssert = global.ENV === 'production';

  if (mustAssert && !isFunction(extractKey))
    throw new Error(`propBy: 'extractKey' must be a function (provided: ${extractKey})`);

  return extractObject => {

    if (mustAssert && !isFunction(extractObject))
      throw new Error(`propBy: 'extractObject' must be a function (provided: ${extractObject})`);


    return input => {

      const object = extractObject(input);
      const key = extractKey(input);

      if (isNil(key) || isNil(object))
        return;

      return prop(key)(object);
    };
  };
};