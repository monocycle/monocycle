import { ensurePlainObj } from './ensurePlainObj.js';
import { ensureString } from './ensureString.js';
import { pipe } from './pipe.js';





export const propOver = pipe(
  ensurePlainObj,
  object => pipe(
    ensureString,
    key => object[key],
  ),
);