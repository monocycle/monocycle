import { Stream as $ } from 'xstream';
import { pipe } from './pipe.js';
import uniq from 'ramda/src/uniq.js';
import __ from 'ramda/src/__.js';
import reject from 'ramda/src/reject.js';
import contains from 'ramda/src/contains.js';
import filter from 'ramda/src/filter.js';
import map from 'ramda/src/map.js';
import has from 'ramda/src/has.js';
import concat from 'ramda/src/concat.js';
import reduce from 'ramda/src/reduce.js';



/**
 * Merges all sinks in the array
 * @param 'sinks' the sinks to be merged
 * @param 'exceptions' a dictionary of special channels, e.g. DOM
 * @return the new unified sinks 
 */
export const mergeSinks = (allSinks, exceptions = {}) => {

  const emptySinks = pipe(
    reduce((before, sinks) => before.concat(Object.keys(sinks)), []),
    uniq,
    map(key => ({ [key]: [] })),
    reduce(Object.assign, {})
  )(allSinks);

  const combinedSinks = allSinks.reduce((before, curr) => {
    return pipe(
      Object.keys,
      map(key => ({ [key]: !curr[key] ? before[key] : [...before[key], curr[key]] })),
      reduce(Object.assign, {})
    )(before);
  }, emptySinks);

  return pipe(

    //merge
    Object.keys,
    reject(contains(__, Object.keys(exceptions))),
    map(key => ({
      [key]: combinedSinks[key].length === 1 ? combinedSinks[key][0] : $.merge(...combinedSinks[key])
    })),

    //add specials
    concat(__,
      pipe(
        Object.keys,
        filter(has(__, combinedSinks)),
        map(key => ({ [key]: exceptions[key](combinedSinks[key]) })),
      )(exceptions)
    ),

    reduce(Object.assign, {}),
  )(combinedSinks);
};