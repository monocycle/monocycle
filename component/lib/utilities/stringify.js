import isUndefined from 'ramda-adjunct/src/isUndefined.js';
import isRegExp from 'ramda-adjunct/src/isRegExp.js';




export const stringify = state => {

  try {

    return JSON.stringify(
      state,
      (k, v) => isUndefined(v)
        ? 'undefined'
        : (
          isRegExp(v)
            ? v.toString()
            : v
        ),
      2
    );

  } catch (err) {
    throw new Error(`StringifyError: ${err.message}`);
  }
};