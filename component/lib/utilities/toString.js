import isFunction from 'ramda-adjunct/src/isFunction.js';

export const toString = x => x && isFunction(x.toString) && x.toString();