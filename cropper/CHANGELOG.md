# 1.0.0 (2020-11-09)


### Bug Fixes

* **cropper:** make package esm compatible ([7348fd3](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/7348fd3d4ab3588e84a2f341f4ac8e9de8b8caed))


### BREAKING CHANGES

* **cropper:** maybe



# 0.14.0 (2020-10-07)


### Bug Fixes

* **cropper:** update dependencies ([aee0607](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/aee060715c4d0a73274d5aba67e6a239f43c2163))



# 0.13.0 (2020-08-31)


### Bug Fixes

* **cropper:** make it responsive for firefox and chrome ([02cd782](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/02cd78293cbff8dafe11d07f3f705bbcf93933fd))



# 0.12.0 (2020-08-30)


### Bug Fixes

* **cropper:** add cropper container height:100% ([fe7d5d0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fe7d5d0ef7324de756a475830023ecb84b6a6bab))



# 0.11.0 (2020-05-19)


### Bug Fixes

* **cropper:** update deps ([9dd0559](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9dd05598bbde013b61872b5a182a2225983ead2c))



# 0.10.0 (2020-04-01)


### Bug Fixes

* **cropper:** use camelcase in css vars ([14d9604](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/14d96042fc72fea40415d8358b24bc1cd0b91ab7))



# 0.9.0 (2020-03-13)


### Features

* **cropper:** add mobile events ([84b7394](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/84b7394fec841ff73875f75bc802ab9d2bf5fe37))



# 0.7.0 (2020-03-05)


### Bug Fixes

* **cropper:** mouseLeave bug ([fb9d35d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fb9d35d5baea0bcc666d72ed7469808bdfaa7ab8))



# 0.6.0 (2020-03-05)


### Features

* **cropper:** add 'urlKey' option to Cropper ([c0d4e26](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c0d4e2633c8e797cf2a3b13010a4756b9094dcdc))
* **cropper:** Drawer use blur filter ([c60fa1a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c60fa1aa1a45d1b715e65986258a146ae39c49fc))
* **cropper:** remove all debounce in Cropper ([00e04c8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/00e04c876d1452ce6db5377ec925e58d7664f7de))
* **cropper:** use setImage$ to init drawer ([8fe1128](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8fe11289b6f49904f67c011d13386bac602cbed5))



# 0.5.0 (2020-02-29)


### Bug Fixes

* **cropper:** don't draw more than natural size ([17c5f71](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/17c5f71fe2dd637bfd73842176daef840930bdb3))



# 0.4.0 (2020-02-29)


### Bug Fixes

* **cropper:** don't resize more than natural size ([c799da9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c799da9f7bf1b9c9f7c4aad9994d64e4944e1f58))
* **cropper:** Drawer waits for image to be loaded ([c9dda71](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c9dda71a1886dd654ad8cd9c446616d358c99ad0))



# 0.3.0 (2020-02-29)


### Bug Fixes

* **cropper:** depends on dom@27.4 ([5a21f46](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5a21f4602f0ff8d06c20fcf1ecd3adf8e0245f6f))


### Features

* **cropper:** make it working ([a88a1f6](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/a88a1f6a94c65d497f025c534894de4a27bdd525))



# 0.2.0 (2020-02-27)


### Bug Fixes

* **cropper:** repair and split Cropper ([96cf32f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/96cf32fad1ba12dce475b6c5e31db315efec184f))



# 0.1.0 (2020-02-25)


### Features

* **cropper:** create package ([974fc3a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/974fc3ac2727be814bd4a537f08e4efaa7534d1f))



