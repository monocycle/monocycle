import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory'
import pipe from 'ramda/src/pipe.js'
import { coerceViewOptions } from '@monocycle/dom/src/View'
import { WithCropperIntents } from './CropperIntents.js'
import { WithCropperModel } from './CropperModel'
import { WithCropperView } from './CropperView.js'





export const WithCropper = pipe(
  coerceViewOptions,
  ({ when, urlKey, ...viewOptions }) => pipe(

    WithCropperView({
      ...viewOptions,
      urlKey
    }),

    WithCropperIntents({ when }),

    WithCropperModel({ urlKey }),
  ),
)

export const withCropper = WithCropper()

export const Cropper = behaviorToFactory(WithCropper)

export const cropper = Cropper()