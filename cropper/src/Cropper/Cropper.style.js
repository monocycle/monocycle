



export const CropperStyle = {
  overflowY: 'auto',
  overflowX: 'hidden',
  height: [
    '-moz-min-content',
    '-webkit-min-content',
    'min-content',
  ],
  margin: 'auto',
}

export const CropperContainerStyle = [
  {
    position: 'relative',
  },
]

export const CropperImageStyle = [
  {
    cursor: 'crosshair',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
    objectFit: 'contain',
    width: '100%'
  },
]

export const CropperCropStyle = [
  {
    border: 'var(--cropBorderSize, 0px) solid var(--cropBorderColor, #000)',
    boxShadow: '0 0 0 9999rem var(--cropShadowColor, rgba(0, 0, 0, 0.6))',
    cursor: 'move',
    display: 'block',
    position: 'absolute',
    zIndex: 1,
  },
]

export const CropperHandleStyle = ({ media, theme: { breakpoint } }) => {

  return [
    media({ minWidth: 0, maxWidth: breakpoint }, {
      '--handleSize': 'var(--handleSizeMobile, 2rem)',
    }),
    media({ minWidth: breakpoint }, {
      '--handleSize': 'var(--handleSizeDesktop, 1.5rem)',
    }),
    {
      background: 'var(--handleColor, #fff)',
      border: 'var(--handleBorderSize, 1px) solid var(--handleBorderColor, #000)',
      borderRadius: '50%',
      height: 'var(--handleSize)',
      width: 'var(--handleSize)',
      position: 'absolute',

      '&.n': { cursor: 'n-resize' },
      '&.ne': { cursor: 'ne-resize' },
      '&.e': { cursor: 'e-resize' },
      '&.se': { cursor: 'se-resize' },
      '&.s': { cursor: 's-resize' },
      '&.sw': { cursor: 'sw-resize' },
      '&.w': { cursor: 'w-resize' },
      '&.nw': { cursor: 'nw-resize' },

      '&.n, &.s': {
        left: 'calc(50% - var(--handleSize) / 2)',
        // top: 'var(--handleSize) / 2)',
      },
      '&.w, &.e': {
        top: 'calc(50% - var(--handleSize) / 2)',
      },

      '&.n, &.nw, &.ne': {
        top: 'calc(var(--handleSize) / -2 - var(--handleBorderSize, 1px))',
      },
      '&.e, &.ne, &.se': {
        right: 'calc(var(--handleSize) / -2 - var(--handleBorderSize, 1px))',
      },
      '&.s, &.se, &.sw': {
        bottom: 'calc(var(--handleSize) / -2 - var(--handleBorderSize, 1px))',
      },
      '&.w, &.sw, &.nw': {
        left: 'calc(var(--handleSize) / -2 - var(--handleBorderSize, 1px))',
      },

    },
  ]
}