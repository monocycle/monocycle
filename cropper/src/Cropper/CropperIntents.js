import { WithListener } from '@monocycle/listener'
import { WithAfter } from '@monocycle/after'
import pipe from 'ramda/src/pipe.js'
import path from 'ramda/src/path.js'
import omit from 'ramda/src/omit.js'
import applySpec from 'ramda/src/applySpec.js'
import $ from 'xstream'
import noop from 'ramda-adjunct/src/noop.js'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory'
import over from 'ramda/src/over.js'
import lensProp from 'ramda/src/lensProp.js'
import not from 'ramda/src/not.js'
import always from 'ramda/src/always.js'
import when from 'ramda/src/when.js'
import { parseFrom } from '@monocycle/listener'





const extractClientX = event => {
  return 'touches' in event
    ? event.touches[0].clientX
    : event.clientX
}

const extractClientY = event => {
  return 'touches' in event
    ? event.touches[0].clientY
    : event.clientY
}

export const WithCropperIntents = pipe(
  ensurePlainObj,
  over(lensProp('when'), pipe(
    when(not, always(pipe(noop, $.of))),
    parseFrom,
  )),
  ({ when }) => pipe(
    WithListener({ // startDraw
      to: 'startDraw$',
      combine: $.merge,
      from: ({ imageMouseDown$ }) => {
        return imageMouseDown$
          .map(event => {

            const clientX = extractClientX(event)
            const clientY = extractClientY(event)

            const { offsetTop, offsetLeft } = event.ownerTarget.parentNode // container
            const { scrollTop, scrollLeft } = event.ownerTarget.parentNode.parentNode // root
            const {
              naturalWidth, naturalHeight,
              clientWidth, clientHeight
            } = event.ownerTarget

            return ({
              naturalWidth, naturalHeight,
              x: (naturalWidth * (clientX - offsetLeft + scrollLeft)) / clientWidth,
              y: (naturalHeight * (clientY - offsetTop + scrollTop)) / clientHeight,
            })
          })
      },
    }),
    WithListener({ // updateDraw
      to: 'updateDraw$',
      combine: $.merge,
      from: ({ mouseMove$ }) => {
        return mouseMove$
          .map(event => {

            const clientX = extractClientX(event)
            const clientY = extractClientY(event)

            const { scrollTop, scrollLeft } = event.ownerTarget.parentNode // root
            const { offsetTop, offsetLeft } = event.ownerTarget // container
            const {
              naturalWidth, naturalHeight,
              clientWidth, clientHeight
            } = event.ownerTarget.querySelector('img')

            return {
              x: (naturalWidth * (clientX - offsetLeft + scrollLeft)) / clientWidth,
              y: (naturalHeight * (clientY - offsetTop + scrollTop)) / clientHeight,
            }
          })
      },
    }),
    WithListener({ // startMove
      to: 'startMove$',
      combine: $.merge,
      from: ({ cropMouseDown$ }) => {
        return cropMouseDown$
          .map(event => {

            const clientX = extractClientX(event)
            const clientY = extractClientY(event)

            const { scrollTop, scrollLeft } = event.ownerTarget.parentNode.parentNode // root
            const { offsetTop, offsetLeft } = event.ownerTarget.parentNode // container
            const {
              naturalWidth, naturalHeight,
              clientWidth, clientHeight,
            } = event.ownerTarget.parentNode.querySelector('img')

            return {
              naturalWidth, naturalHeight,
              x: (naturalWidth * (clientX - offsetLeft + scrollLeft)) / clientWidth,
              y: (naturalHeight * (clientY - offsetTop + scrollTop)) / clientHeight,
            }
          })
      },
    }),
    WithListener({ // updateMove
      to: 'updateMove$',
      combine: $.merge,
      from: ({ mouseMove$ }) => {
        return mouseMove$
          .map(event => {

            const { scrollTop, scrollLeft } = event.ownerTarget.parentNode // root
            const { offsetTop, offsetLeft } = event.ownerTarget // container
            const {
              naturalWidth, naturalHeight,
              clientWidth, clientHeight
            } = event.ownerTarget.querySelector('img')

            const clientX = extractClientX(event)
            const clientY = extractClientY(event)

            return {
              x: (naturalWidth * (clientX - offsetLeft + scrollLeft)) / clientWidth,
              y: (naturalHeight * (clientY - offsetTop + scrollTop)) / clientHeight,
            }
          })
      },
    }),
    WithListener({ // startResize
      to: 'startResize$',
      combine: $.merge,
      from: ({ handleMouseDown$ }) => {
        return handleMouseDown$
          .map(applySpec({
            direction: path(['ownerTarget', 'dataset', 'direction']),
          }))
      },
    }),
    WithListener({ // updateResize
      to: 'updateResize$',
      combine: $.merge,
      from: ({ mouseMove$ }) => {
        return mouseMove$
          .map(event => {

            const { scrollTop, scrollLeft } = event.ownerTarget.parentNode // root
            const { offsetTop, offsetLeft } = event.ownerTarget // container
            const {
              naturalWidth, naturalHeight,
              clientWidth, clientHeight
            } = event.ownerTarget.querySelector('img')

            const clientX = extractClientX(event)
            const clientY = extractClientY(event)

            return {
              x: (naturalWidth * (clientX - offsetLeft + scrollLeft)) / clientWidth,
              y: (naturalHeight * (clientY - offsetTop + scrollTop)) / clientHeight,
            }
          })
      },
    }),
    WithListener({ // setCropperImage
      to: 'setCropperImage$',
      combine: $.merge,
      from: pipe(
        when,
        cropperImage$ => {

          return cropperImage$
            .map(pipe(
              url => new Promise(resolve => {
                var image = new Image

                image.addEventListener('load', () => resolve({
                  url,
                  naturalWidth: image.naturalWidth,
                  naturalHeight: image.naturalHeight,
                }))

                image.src = url
              }),
              $.fromPromise
            )).flatten()
        }
      ),
    }),
    WithListener({ // unsetCroppingArea
      to: 'unsetCroppingArea$',
      from: ['setCropperImage$']
    }),
    WithListener({ // initCropper
      to: 'initCropper$',
      combine: $.merge,
      from: [pipe(noop, $.of), 'mouseUp$', 'mouseLeave$'],
    }),
    WithAfter(omit([
      'mouseMove$',
      'mouseUp$',
      'mouseLeave$',
      'imageMouseDown$',
      'cropMouseDown$',
      'handleMouseDown$',
    ]))
  ),
)

export const withCropperIntents = WithCropperIntents()

export const CropperIntents = behaviorToFactory(WithCropperIntents)

export const cropperIntents = CropperIntents()