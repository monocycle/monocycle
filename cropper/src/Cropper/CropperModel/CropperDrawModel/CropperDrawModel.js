import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory'
import pipe from 'ramda/src/pipe.js'
import { WithTransition } from '@monocycle/state'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'
import { StartDrawReducer } from './_reducers/startDraw.js'
import { UpdateDrawReducer } from './_reducers/updateDraw.js'





export const WithCropperDrawModel = pipe(
  ensurePlainObj,
  () => pipe(
    WithTransition({ // startDraw
      label: 'startDraw',
      from: 'startDraw$',
      reducer: StartDrawReducer,
    }),
    WithTransition({ // updateDraw
      label: 'updateDraw',
      from: 'updateDraw$',
      reducer: UpdateDrawReducer,
    }),
  )
)

export const withCropperDrawModel = WithCropperDrawModel()

export const CropperDrawModel = behaviorToFactory(WithCropperDrawModel)

export const cropperDrawModel = CropperDrawModel()