import pipe from 'ramda/src/pipe.js'
import when from 'ramda/src/when.js'
import propEq from 'ramda/src/propEq.js'
import lensProp from 'ramda/src/lensProp.js'
import always from 'ramda/src/always.js'
import over from 'ramda/src/over.js'
import merge from 'ramda/src/merge.js'
import __ from 'ramda/src/__.js'
import unless from 'ramda/src/unless.js'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'
import { restrictPosition } from '../../_utilities/restrictPosition.js'
import { preserveAspect } from '../../_utilities/preserveAspect.js'
import { isNonNegativeInteger } from '../../_utilities/isNonNegativeInteger.js'





export const StartDrawReducer = ({
  naturalWidth, naturalHeight,
  x, y,
}) => pipe(
  ensurePlainObj,
  when(propEq('status', 'none'), pipe(
    merge(__, {
      status: 'drawing',
      naturalWidth, naturalHeight,
      x0: x, y0: y,
    }),
    over(lensProp('minWidth'), unless(isNonNegativeInteger, always(1))),
    over(lensProp('minHeight'), unless(isNonNegativeInteger, always(1))),
    // over(lensProp('maxWidth'), unless(isNonNegativeInteger, always(naturalWidth))),
    // over(lensProp('maxHeight'), unless(isNonNegativeInteger, always(naturalHeight))),
    ({
      aspectRatio,
      minWidth, minHeight,
      ...state
    }) => {

      const maxWidth = Math.min(state.maxWidth, naturalWidth)
      const maxHeight = Math.min(state.maxHeight, naturalHeight)

      return ({
        ...state,
        aspectRatio,
        minWidth, minHeight,
        // maxWidth, maxHeight,
        value: pipe(
          preserveAspect({
            aspectRatio,
            minWidth, minHeight,
            maxWidth, maxHeight,
          }),
          restrictPosition({
            maxWidth: naturalWidth,
            maxHeight: naturalHeight
          }),
        )({
          x, y,
          width: minWidth,
          height: minHeight,
        }),
      })
    }
  )),
)