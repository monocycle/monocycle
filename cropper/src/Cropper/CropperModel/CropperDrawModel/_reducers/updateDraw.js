import pipe from 'ramda/src/pipe.js'
import when from 'ramda/src/when.js'
import propEq from 'ramda/src/propEq.js'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'
import { restrictPosition } from '../../_utilities/restrictPosition.js'
import { preserveAspect } from '../../_utilities/preserveAspect.js'
import { isPositiveInteger } from '../../_utilities/isPositiveInteger.js'





export const UpdateDrawReducer = ({ x, y }) => pipe(
  ensurePlainObj,
  when(propEq('status', 'drawing'), ({
    naturalWidth, naturalHeight,
    x0, y0,
    minWidth, minHeight,
    maxWidth, maxHeight,
    aspectRatio,
    ...state
  }) => ({
    ...state,
    naturalWidth, naturalHeight,
    x0, y0,
    minWidth, minHeight,
    maxHeight, maxWidth,
    aspectRatio,
    value: pipe(
      preserveAspect({
        aspectRatio,
        minWidth, minHeight,
        maxWidth: isPositiveInteger(maxWidth)
          ? Math.min(maxWidth, naturalWidth)
          : naturalWidth,
        maxHeight: isPositiveInteger(maxHeight)
          ? Math.min(maxHeight, naturalHeight)
          : naturalHeight,
      }),
      restrictPosition({
        maxWidth: naturalWidth,
        maxHeight: naturalHeight
      }),
    )({
      x: x0,
      y: y0,
      width: x - x0,
      height: y - y0,
    })
  }))
)