import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory'
import pipe from 'ramda/src/pipe.js'
import always from 'ramda/src/always.js'
import { WithTransition } from '@monocycle/state'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'
import { initCropperReducer } from './_reducers/initCropper.js'
import { unsetCroppingAreaReducer } from './_reducers/unsetCroppingArea.js'
import { makeSetCropperImageReducer } from './_reducers/setCropperImage'
import { withCropperDrawModel } from './CropperDrawModel'
import { withCropperMoveModel } from './CropperMoveModel'
import { withCropperResizeModel } from './CropperResizeModel'





export const WithCropperModel = pipe(
  ensurePlainObj,
  ({ urlKey }) => pipe(
    WithTransition({ // initCropper
      label: 'initCropper',
      from: 'initCropper$',
      reducer: always(initCropperReducer),
    }),
    WithTransition({ // initCropper
      label: 'setCropperImage',
      from: 'setCropperImage$',
      reducer: makeSetCropperImageReducer({ urlKey }),
    }),
    WithTransition({ // initCropper
      label: 'unsetCroppingArea',
      from: 'unsetCroppingArea$',
      reducer: always(unsetCroppingAreaReducer),
    }),
    withCropperDrawModel,
    withCropperMoveModel,
    withCropperResizeModel,
  )
)

export const withCropperModel = WithCropperModel()

export const CropperModel = behaviorToFactory(WithCropperModel)

export const cropperModel = CropperModel()