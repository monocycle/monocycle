import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory'
import pipe from 'ramda/src/pipe.js'
import { WithTransition } from '@monocycle/state'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'
import { StartMoveReducer } from './_reducers/startMove.js'
import { UpdateMoveReducer } from './_reducers/updateMove.js'





export const WithCropperMoveModel = pipe(
  ensurePlainObj,
  () => pipe(
    WithTransition({ // startMove
      label: 'startMove',
      from: 'startMove$',
      reducer: StartMoveReducer,
    }),
    WithTransition({ // updateMove
      label: 'updateMove',
      from: 'updateMove$',
      reducer: UpdateMoveReducer,
    }),
  )
)

export const withCropperMoveModel = WithCropperMoveModel()

export const CropperMoveModel = behaviorToFactory(WithCropperMoveModel)

export const cropperMoveModel = CropperMoveModel()