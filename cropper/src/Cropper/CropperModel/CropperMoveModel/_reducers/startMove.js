import pipe from 'ramda/src/pipe.js'
import when from 'ramda/src/when.js'
import propEq from 'ramda/src/propEq.js'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'





export const StartMoveReducer = ({
  naturalWidth, naturalHeight,
  x, y,
}) => pipe(
  ensurePlainObj,
  when(propEq('status', 'none'), ({
    value,
    ...state
  }) => ({
    ...state,
    value,
    status: 'movingByMouse',
    naturalWidth, naturalHeight,
    x0: x - value.x,
    y0: y - value.y,
  }))
)