import pipe from 'ramda/src/pipe.js'
import when from 'ramda/src/when.js'
import propEq from 'ramda/src/propEq.js'
import assoc from 'ramda/src/assoc.js'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'
import { restrictPosition } from '../../_utilities/restrictPosition.js'





export const UpdateMoveReducer = ({ x, y }) => pipe(
  ensurePlainObj,
  when(propEq('status', 'movingByMouse'), ({
    naturalWidth, naturalHeight,
    x0, y0,
    value,
    ...state
  }) => ({
    ...state,
    naturalWidth, naturalHeight,
    x0, y0,
    value: pipe(
      assoc('x', x - x0),
      assoc('y', y - y0),
      restrictPosition({
        maxWidth: naturalWidth,
        maxHeight: naturalHeight
      }),
    )(value),
  }))
)