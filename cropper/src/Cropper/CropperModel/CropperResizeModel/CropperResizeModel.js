import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory'
import pipe from 'ramda/src/pipe.js'
import { WithTransition } from '@monocycle/state'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'
import { StartResizeReducer } from './_reducers/startResize.js'
import { UpdateResizeReducer } from './_reducers/updateResize.js'





export const WithCropperResizeModel = pipe(
  ensurePlainObj,
  () => pipe(
    WithTransition({ // startResize
      label: 'startResize',
      from: 'startResize$',
      reducer: StartResizeReducer,
    }),
    WithTransition({ // updateResize
      label: 'updateResize',
      from: 'updateResize$',
      reducer: UpdateResizeReducer,
    }),
  )
)

export const withCropperResizeModel = WithCropperResizeModel()

export const CropperResizeModel = behaviorToFactory(WithCropperResizeModel)

export const cropperResizeModel = CropperResizeModel()