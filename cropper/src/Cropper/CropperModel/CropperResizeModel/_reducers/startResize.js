import pipe from 'ramda/src/pipe.js'
import when from 'ramda/src/when.js'
import propEq from 'ramda/src/propEq.js'
import includes from 'ramda/src/includes.js'
import prop from 'ramda/src/prop.js'
import lensProp from 'ramda/src/lensProp.js'
import over from 'ramda/src/over.js'
import __ from 'ramda/src/__.js'
import unless from 'ramda/src/unless.js'
import { ensureString } from '@monocycle/component/lib/utilities/ensureString'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'
import { directions, oppositePositionsFinders } from '../../../_utilities/directions.js'
import noop from 'ramda-adjunct/src/noop.js'





export const StartResizeReducer = pipe(
  ensurePlainObj,
  over(lensProp('direction'), pipe(
    ensureString,
    unless(includes(__, directions), noop),
  )),
  when(prop('direction'), ({ direction }) => pipe(
    ensurePlainObj,
    unless(propEq('status', 'resizing'), ({
      value,
      ...state
    }) => {

      const findOppositePosition = oppositePositionsFinders[direction]

      const { x, y } = findOppositePosition(value)

      return {
        ...state,
        value,
        status: 'resizing',
        direction,
        x0: x,
        y0: y,
      }
    }),
  )),
)