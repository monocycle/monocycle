import pipe from 'ramda/src/pipe.js'
import when from 'ramda/src/when.js'
import always from 'ramda/src/always.js'
import assoc from 'ramda/src/assoc.js'
import propEq from 'ramda/src/propEq.js'
import max from 'ramda/src/max.js'
import includes from 'ramda/src/includes.js'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'
import round from 'ramda-adjunct/src/round.js'
import { restrictPosition } from '../../_utilities/restrictPosition.js'
import { isPositiveInteger } from '../../_utilities/isPositiveInteger.js'
import { preserveAspect } from '../../_utilities/preserveAspect.js'





export const UpdateResizeReducer = ({ x, y }) => pipe(
  ensurePlainObj,
  when(propEq('status', 'resizing'), ({
    naturalWidth, naturalHeight,
    x0, y0,
    minWidth, minHeight,
    maxWidth, maxHeight,
    aspectRatio,
    direction,
    value,
    ...state
  }) => ({
    ...state,
    naturalWidth, naturalHeight,
    x0, y0,
    minWidth, minHeight,
    maxWidth, maxHeight,
    aspectRatio,
    direction,
    value: pipe(
      when(always(includes('e', direction)),
        assoc('width', max(x - x0, 1)),
      ),
      when(always(includes('w', direction)),
        assoc('width', max(x0 - x, 1)),
      ),
      when(always(includes('s', direction)),
        assoc('height', max(y - y0, 1)),
      ),
      when(always(includes('n', direction)),
        assoc('height', max(y0 - y, 1)),
      ),
      preserveAspect({
        aspectRatio,
        minWidth, minHeight,
        maxWidth: isPositiveInteger(maxWidth)
          ? Math.min(maxWidth, naturalWidth)
          : naturalWidth,
        maxHeight: isPositiveInteger(maxHeight)
          ? Math.min(maxHeight, naturalHeight)
          : naturalHeight,
        byWidth: includes(direction, ['w', 'e'])
      }),
      ({ x, y, width, height }) => ({ // reposition origin
        width,
        height,
        x: includes('w', direction)
          ? x0 - width
          : includes(direction, ['n', 's'])
            ? x0 - (width / 2)
            : x,
        y: includes('n', direction)
          ? y0 - height
          : includes(direction, ['w', 'e'])
            ? round(y0 - (height / 2))
            : y,
      }),
      restrictPosition({
        maxWidth: naturalWidth,
        maxHeight: naturalHeight
      }),
    )(value),
  }))
)