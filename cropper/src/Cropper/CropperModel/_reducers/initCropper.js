import pipe from 'ramda/src/pipe.js'
import merge from 'ramda/src/merge.js'
import __ from 'ramda/src/__.js'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'






export const initCropperReducer = pipe(
  ensurePlainObj,
  merge(__, {
    direction: null,
    pressedKeys: [],
    status: 'none',
    x0: 0,
    y0: 0,
  })
)