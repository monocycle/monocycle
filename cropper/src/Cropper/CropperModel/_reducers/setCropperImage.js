import pipe from 'ramda/src/pipe.js'
import unless from 'ramda/src/unless.js'
import over from 'ramda/src/over.js'
import always from 'ramda/src/always.js'
import lensProp from 'ramda/src/lensProp.js'
import allPass from 'ramda/src/allPass.js'
import assoc from 'ramda/src/assoc.js'
import dissoc from 'ramda/src/dissoc.js'
import propSatisfies from 'ramda/src/propSatisfies.js'
import when from 'ramda/src/when.js'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'
import isEmptyString from 'ramda-adjunct/src/isEmptyString.js'
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js'
import { isPositiveInteger } from '../_utilities/isPositiveInteger.js'





export const makeSetCropperImageReducer = pipe(
  ensurePlainObj,
  over(lensProp('urlKey'), unless(isNonEmptyString, always('url'))),
  ({ urlKey }) => pipe(
    ensurePlainObj,
    when(
      allPass([
        propSatisfies(isNonEmptyString, urlKey),
        propSatisfies(isPositiveInteger, 'naturalWidth'),
        propSatisfies(isPositiveInteger, 'naturalHeight'),
      ]),
      ({ url, naturalWidth, naturalHeight }) => pipe(
        ensurePlainObj,
        unless(always(isEmptyString(url)),
          pipe(
            dissoc('value'),
            assoc(urlKey, url),
            assoc('naturalWidth', naturalWidth),
            assoc('naturalHeight', naturalHeight),
          ),
        ),
      )
    ),
  )
)