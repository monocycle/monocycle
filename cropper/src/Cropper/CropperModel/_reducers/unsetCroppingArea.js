import dissoc from 'ramda/src/dissoc.js'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'
import pipe from 'ramda/src/pipe.js'





export const unsetCroppingAreaReducer = pipe(
  ensurePlainObj,
  dissoc('value'),
)