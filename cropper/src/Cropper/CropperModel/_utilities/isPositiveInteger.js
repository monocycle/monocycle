import allPass from 'ramda/src/allPass.js'
import isPositive from 'ramda-adjunct/src/isPositive.js'
import isSafeInteger from 'ramda-adjunct/src/isSafeInteger.js'




export const isPositiveInteger = allPass([isSafeInteger, isPositive])
