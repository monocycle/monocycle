import both from 'ramda/src/both.js'
import min from 'ramda/src/min.js'
import max from 'ramda/src/max.js'
import merge from 'ramda/src/merge.js'
import converge from 'ramda/src/converge.js'
import when from 'ramda/src/when.js'
import isPositive from 'ramda-adjunct/src/isPositive.js'
import isValidNumber from 'ramda-adjunct/src/isValidNumber.js'
import round from 'ramda-adjunct/src/round.js'
import isSafeInteger from 'ramda-adjunct/src/isSafeInteger.js'
import noop from 'ramda-adjunct/src/noop.js'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js'
import pipe from 'ramda/src/pipe.js'
import over from 'ramda/src/over.js'
import lensProp from 'ramda/src/lensProp.js'
import always from 'ramda/src/always.js'
import unless from 'ramda/src/unless.js'
import identity from 'ramda/src/internal/_identity.js'
import { isPositiveInteger } from './isPositiveInteger.js'





const isValidPositive = both(
  isValidNumber,
  isPositive,
)

export const preserveAspect = pipe(
  ensurePlainObj,
  over(lensProp('aspectRatio'), unless(isValidPositive, noop)), // missing test
  over(lensProp('byWidth'), Boolean), // missing test
  over(lensProp('minWidth'), unless(isPositiveInteger, always(1))),  // missing test
  over(lensProp('minHeight'), unless(isPositiveInteger, always(1))), // missing test
  over(lensProp('maxWidth'), unless(isPositiveInteger, noop)), // missing test
  over(lensProp('maxHeight'), unless(isPositiveInteger, noop)), // missing test
  ({
    aspectRatio,
    byWidth,
    minWidth, minHeight,
    maxWidth, maxHeight,
  } = {}) => {

    if (maxWidth && minWidth > maxWidth)
      throw new Error(`'minWidth' is greater than 'maxWidth'`)

    if (maxHeight && minHeight > maxHeight)
      throw new Error(`'minHeight' is greater than 'maxHeight'`)

    if (aspectRatio) {

      const aspectMinHeight = minWidth / aspectRatio

      minHeight = max(minHeight, aspectMinHeight) // missing test

      if (maxHeight && aspectMinHeight > maxHeight)
        throw new Error(`'maxHeight' is incompatible with aspect ratio`)

      const aspectMinWidth = minHeight * aspectRatio

      minWidth = max(minWidth, aspectMinWidth) // missing test

      if (maxWidth && aspectMinWidth > maxWidth)
        throw new Error(`'maxWidth' is incompatible with aspect ratio`)
    }

    return pipe(
      ensurePlainObj, // missing test
      over(lensProp('width'), pipe(
        unless(isValidPositive, always(1)), // missing test
        unless(isSafeInteger, round), // missing test
        max(minWidth),
        when(always(maxWidth), min(maxWidth)),
      )),
      over(lensProp('height'), pipe(
        unless(isValidPositive, always(1)), // missing test
        unless(isSafeInteger, round), // missing test
        max(minHeight),
        when(always(maxHeight), min(maxHeight)),
      )),
      when(always(aspectRatio), converge(merge, [
        identity,
        ({ width, height, ...rest }) => {


          if (byWidth) {

            const aspectHeight = round(width / aspectRatio)

            height = maxHeight && aspectHeight > maxHeight
              ? maxHeight
              : aspectHeight

            return {
              ...rest,
              width: round(height * aspectRatio),
              height,
            }
          }

          const aspectWidth = round(height * aspectRatio)

          width = maxWidth && aspectWidth > maxWidth
            ? maxWidth
            : aspectWidth

          return {
            ...rest,
            width,
            height: round(width / aspectRatio),
          }
        },
      ])),
    )
  },
)