
import test from 'ava'
import { preserveAspect } from './preserveAspect.js'
import isFunction from 'ramda-adjunct/src/isFunction.js'
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js'
import toString from 'ramda/src/toString.js'
import pipe from 'ramda/src/pipe.js'
import gte from 'ramda/src/gte.js'
import lte from 'ramda/src/lte.js'
import tap from 'ramda/src/tap.js'
import identical from 'ramda/src/identical.js'
import { isNonNegativeInteger } from './isNonNegativeInteger.js'





const ShouldDeepEqual = (f, prefix = '') => {


  const macro = (t, input, expected) => {

    t.deepEqual(f(t, input), expected)
  }

  macro.title = (title = '') => prefix + ' ' + title

  return macro
}

const ShouldPreserveAspect = (options, prefix = '') => {

  return ShouldDeepEqual((t, input) => {
    return pipe(
      preserveAspect(options),
      tap(value => {

        t.true(isPlainObj(value), `returned value should be a plain object`)

        const { width, height } = value

        t.true(
          isNonNegativeInteger(width),
          `returned 'width' should be a non-negative integer`
        )
        t.true(
          isNonNegativeInteger(height),
          `returned 'width' should be a non-negative integer`
        )

        if (options.aspectRatio)
          t.true(
            identical(width / height, options.aspectRatio),
            `returned ('width' / 'height') don't respect 'aspectRatio' (${width / height} !== ${options.aspectRatio}, ${{ width, height }}) `
          )

        if (options.minWidth)
          t.true(
            gte(width, options.minWidth),
            `returned 'width' don't respect 'minWidth'`
          )

        if (options.minHeight)
          t.true(
            gte(height, options.minHeight),
            `returned 'height' don't respect 'minHeight'`
          )

        if (options.maxWidth)
          t.true(
            lte(width, options.maxWidth),
            `returned 'height' don't respect 'maxWidth'`
          )

        if (options.maxHeight)
          t.true(
            lte(height, options.maxHeight),
            `returned 'height' don't respect 'maxHeight'`
          )

      }),
    )(input)
  }, prefix)
}

const ShouldFail = (f, prefix = '') => {

  const macro = (t, input, message) => {

    t.throws(() => {

      const returned = f(input)

      t.fail(toString({ returned: typeof returned }))

    }, { message })
  }

  macro.title = (title = '') => prefix + ' ' + title

  return macro
}



const aspectPreserverShouldFail = ShouldFail(preserveAspect, 'preserveAspect fails')


test('when minWidth > maxWidth', aspectPreserverShouldFail,
  { minWidth: 200, maxWidth: 100 },
  `'minWidth' is greater than 'maxWidth'`,
)
test('when minHeight > maxHeight', aspectPreserverShouldFail,
  { minHeight: 200, maxHeight: 100 },
  `'minHeight' is greater than 'maxHeight'`,
)
test('when maxHeight is incompatible with ratio', aspectPreserverShouldFail,
  { minWidth: 200, maxHeight: 99, aspectRatio: 2 },
  `'maxHeight' is incompatible with aspect ratio`,
)
test('when maxWidth is incompatible with ratio', aspectPreserverShouldFail,
  { maxWidth: 200, minHeight: 101, aspectRatio: 2 },
  `'maxWidth' is incompatible with aspect ratio`,
)
test('preserveAspect returns a function', t => {
  t.true(isFunction(preserveAspect()))
})

const shouldPreserveCrop = ShouldPreserveAspect(
  {
    minWidth: 100,
    minHeight: 50,
    maxWidth: 400,
    maxHeight: 300,
  },
  'Crop'
)

test('when width >= minWidth', shouldPreserveCrop,
  { width: 50, height: 50 },
  { width: 100, height: 50 },
)
test('when width <= maxWidth', shouldPreserveCrop,
  { width: 500, height: 50 },
  { width: 400, height: 50 },
)
test('when height >= minHeight', shouldPreserveCrop,
  { width: 100, height: 25 },
  { width: 100, height: 50 },
)
test('when height <= maxHeight', shouldPreserveCrop,
  { width: 100, height: 400 },
  { width: 100, height: 300 },
)

const shouldPreserveLandscapeRatio = ShouldPreserveAspect(
  {
    aspectRatio: 2,
    minWidth: 50,
    minHeight: 50,
    maxWidth: 400,
    maxHeight: 300,
  },
  'Preserve landscape ratio'
)

test('by height', shouldPreserveLandscapeRatio,
  { width: 200, height: 101 },
  { width: 202, height: 101 },
)
test('by height when width/height === aspectRatio', shouldPreserveLandscapeRatio,
  { width: 200, height: 100 },
  { width: 200, height: 100 },
)

test('Preserve landscape ratio by width',
  ShouldPreserveAspect({
    aspectRatio: 2,
    minWidth: 50,
    minHeight: 50,
    maxWidth: 400,
    maxHeight: 300,
    byWidth: true,
  }),
  { width: 201, height: 100 },
  { width: 202, height: 101 },
)

const shouldPreservePortraitRatio = ShouldPreserveAspect(
  {
    aspectRatio: 1 / 2,
    minWidth: 50,
    minHeight: 50,
    maxWidth: 400,
    maxHeight: 300,
  },
  'Preserve portrait ratio'
)

test('by height', shouldPreservePortraitRatio,
  { width: 100, height: 201 },
  { width: 101, height: 202 },
)

test('by height when width/height === aspectRatio', shouldPreservePortraitRatio,
  { width: 100, height: 200 },
  { width: 100, height: 200 },
)

test('Preserve portrait ratio by width',
  ShouldPreserveAspect({
    aspectRatio: 1 / 2,
    minWidth: 50,
    minHeight: 50,
    maxWidth: 400,
    maxHeight: 300,
    byWidth: true,
  }),
  { width: 101, height: 200 },
  { width: 101, height: 202 },
)


test('Contrains width then height when newWidth > maxWidth',
  ShouldPreserveAspect({ aspectRatio: 2, maxWidth: 42, maxHeight: 200 }),
  { width: 400, height: 300 },
  { width: 42, height: 21 },
)

test('Contrains height then width when newHeight > maxHeight',
  ShouldPreserveAspect({ aspectRatio: 2, maxWidth: 400, maxHeight: 42 }),
  { width: 300, height: 100 },
  { width: 84, height: 42 },
)
