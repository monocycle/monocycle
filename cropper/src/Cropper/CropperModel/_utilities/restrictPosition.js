




export const restrictPosition = ({ maxWidth, maxHeight }) => ({ x, y, width, height, ...rest }) => ({
  ...rest,
  x: x < 0
    ? 0
    : width + x > maxWidth
      ? maxWidth - width
      : x,
  y: y < 0
    ? 0
    : height + y > maxHeight
      ? maxHeight - height
      : y,
  width,
  height,
})