import { WithView, View, coerceViewOptions } from '@monocycle/dom/src/View'
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory'
import { CropperStyle, CropperContainerStyle, CropperCropStyle, CropperHandleStyle, CropperImageStyle } from './Cropper.style.js'
import { WithStyledView } from '@monocycle/dom/src/StyledView'
import dropRepeats from 'xstream/extra/dropRepeats'
import helpers from '@monocycle/dom/src/helpers.js'
import pipe from 'ramda/src/pipe.js'
import prop from 'ramda/src/prop.js'
import eqProps from 'ramda/src/eqProps.js'
import map from 'ramda/src/map.js'
import over from 'ramda/src/over.js'
import lensProp from 'ramda/src/lensProp.js'
import unless from 'ramda/src/unless.js'
import always from 'ramda/src/always.js'
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js'
import { WithIsolated } from '@monocycle/isolated'
import { directions } from './_utilities/directions.js'
const { Img } = helpers
import { WithEventListener } from '@monocycle/dom/src/EventListener'
import $ from 'xstream'





export const WithCropperView = pipe(
  coerceViewOptions,
  over(lensProp('urlKey'), unless(isNonEmptyString, always('url'))),
  ({ urlKey, ...viewOptions }) => pipe(
    WithStyledView({
      name: 'Cropper',
      styles: CropperStyle,
    }),
    WithView({
      style: {
        backgroundColor: 'var(--darkerColor)',
      },
      has: [
        View({
          has: [ // Container
            View({ // Crop
              sel: '.cropArea',
              from: (sinks, sources) => {
                return sources.state.stream
                  .compose(dropRepeats(eqProps('value')))
                  // .compose(sources.Time.debounce(0))
                  .map(pipe(
                    ({
                      value,
                      naturalWidth, naturalHeight
                    }) => ({
                      data: {
                        style: !value
                          ? { display: 'none' }
                          : {
                            height: `${(value.height / naturalHeight) * 100}%`,
                            left: `${(value.x * 100) / naturalWidth}%`,
                            top: `${(value.y * 100) / naturalHeight}%`,
                            width: `${(value.width / naturalWidth) * 100}%`,
                          },
                      },
                    })
                  )).startWith()
              },
              has: map((direction) => {
                return View({ // Handle
                  sel: `.cropHandle.${direction}`,
                  dataset: {
                    direction,
                  },
                }).map(pipe(

                  WithStyledView({
                    name: 'CropperHandle',
                    styles: CropperHandleStyle,
                  }),
                ))
              })(directions),
            }).map(pipe(
              WithStyledView({
                name: 'CropperCrop',
                styles: CropperCropStyle,
              }),
              WithEventListener({ // handleMouseDown
                type: [
                  'mousedown',
                  'touchstart',
                ],
                combine: $.merge,
                target: '.cropHandle',
                // target: ':root',
                options: { preventDefault: true },
                stopPropagation: true,
                to: 'handleMouseDown$',
              }),
              WithEventListener({ // cropMouseDown
                type: [
                  'mousedown',
                  'touchstart',
                ],
                combine: $.merge,
                target: '.cropArea',
                options: { preventDefault: true },
                to: 'cropMouseDown$',
              }),
            )),
            Img({
              from: (sinks, sources) => {
                return sources.state.stream
                  .map(prop(urlKey))
                  .compose(dropRepeats())
                  .map(src => ({
                    data: {
                      attrs: {
                        src,
                      },
                    },
                  }))
              },
            }).map(pipe(
              WithEventListener({ // imageMouseDown
                type: 'mousedown',
                combine: $.merge,
                target: 'img',
                options: { preventDefault: true },
                to: 'imageMouseDown$',
              }),
              WithEventListener({ // imageMouseDown
                type: 'touchstart',
                combine: $.merge,
                target: 'img',
                options: { preventDefault: true },
                to: 'imageMouseDown$',
              }),
              WithStyledView({
                name: 'CropperImage',
                styles: CropperImageStyle,
              }),
            )),
          ]
        }).map(pipe(
          WithStyledView({
            name: 'CropperContainer',
            styles: CropperContainerStyle,
          }),
          WithEventListener({ // mouseMove
            type: 'mousemove',
            combine: $.merge,
            options: { preventDefault: true },
            to: 'mouseMove$',
          }),
          WithEventListener({ // mouseMove
            type: 'touchmove',
            combine: $.merge,
            options: { preventDefault: true },
            to: 'mouseMove$',
          }),
          WithEventListener({ // mouseUp
            type: 'mouseup',
            combine: $.merge,
            to: 'mouseUp$',
          }),
          WithEventListener({ // mouseUp
            type: 'touchend',
            combine: $.merge,
            to: 'mouseUp$',
          }),
          WithEventListener({ // mouseUp
            type: 'mouseleave',
            combine: $.merge,
            to: 'mouseLeave$',
          }),
          WithEventListener({ // mouseUp
            type: 'touchleave',
            combine: $.merge,
            to: 'mouseLeave$',
          }),
          WithIsolated({
            DOM: 'CropperContainer',
            '*': null,
          })
        )),
      ],
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions
    }),
    WithIsolated({
      DOM: 'Cropper',
      '*': null,
    }),
  )
)

export const withCropperView = WithCropperView()

export const CropperView = behaviorToFactory(WithCropperView)

export const cropperView = CropperView()