import identity from 'ramda/src/internal/_identity.js'





export const directions = ['n', 'ne', 'e', 's', 'sw', 'w', 'nw', 'se']

export const oppositePositionsFinders = {
  n: ({ height, width, x, y }) => ({ x: x + width / 2, y: y + height }),
  ne: ({ height, x, y }) => ({ x, y: y + height }),
  e: ({ height, x, y }) => ({ x, y: y + height / 2 }),
  s: ({ width, x, y }) => ({ x: x + width / 2, y }),
  se: identity,
  sw: ({ width, x, y }) => ({ x: x + width, y }),
  w: ({ height, width, x, y }) => ({ x: x + width, y: y + height / 2 }),
  nw: ({ height, width, x, y }) => ({ x: x + width, y: y + height }),
}