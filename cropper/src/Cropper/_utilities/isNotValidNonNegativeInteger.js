

import either from 'ramda/src/either.js'
import complement from 'ramda/src/complement.js'
import isSafeInteger from 'ramda-adjunct/src/isSafeInteger.js'
import isNegative from 'ramda-adjunct/src/isNegative.js'





export const isNotValidNonNegativeInteger = either(
  complement(isSafeInteger),
  isNegative
)