import { coerceViewOptions } from '@monocycle/dom/src/View'
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory'
import pipe from 'ramda/src/pipe.js'
import { WithDrawerIntents } from './DrawerIntents.js'
import { withDrawerModel } from './DrawerModel.js'
import { WithDrawerView } from './DrawerView.js'





export const WithDrawer = pipe(
  coerceViewOptions,
  ({ fileType, ...viewOptions }) => pipe(
    WithDrawerIntents({ fileType }),
    withDrawerModel,
    WithDrawerView(viewOptions),
  )
)

export const withDrawer = WithDrawer()

export const Drawer = behaviorToFactory(WithDrawer)

export const drawer = Drawer()