import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory'
import { WithListener } from '@monocycle/listener'
import pipe from 'ramda/src/pipe.js'
import over from 'ramda/src/over.js'
import lensProp from 'ramda/src/lensProp.js'
import unless from 'ramda/src/unless.js'
import eqProps from 'ramda/src/eqProps.js'
import ifElse from 'ramda/src/ifElse.js'
import always from 'ramda/src/always.js'
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js'
import propSatisfies from 'ramda/src/propSatisfies.js'
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js'
import $ from 'xstream'
import dropRepeats from 'xstream/extra/dropRepeats'
import { EmptyString } from '@monocycle/component/lib/utilities/empty'





export const WithDrawerIntents = pipe(
  ensurePlainObj,
  over(lensProp('fileType'), unless(isNonEmptyString, always('image/jpeg'))),
  ({ fileType }) => pipe(
    WithListener({ // setImage
      from: (sinks, sources) => {

        const canvas = document.createElement('canvas')

        return sources.state.stream
          .compose(dropRepeats(eqProps('value')))
          .compose(sources.Time.debounce(10))
          .map(pipe(
            ifElse(propSatisfies(isPlainObj, 'value'),
              ({ url, value: { height, width, x, y }, maxWidth = width, maxHeight = height }) => {

                return new Promise(resolve => {

                  if (width < 1 || height < 1)
                    return resolve()
                  const context = canvas.getContext('2d')

                  canvas.width = maxWidth
                  canvas.height = maxHeight

                  const imgElement = new Image()
                  imgElement.src = url

                  imgElement.addEventListener('load', () => {

                    // blur using steps as radius
                    const oc = document.createElement('canvas')
                    const octx = oc.getContext('2d')
                    oc.width = imgElement.width
                    oc.height = imgElement.height
                    octx.filter = `blur(${(oc.width / canvas.width / 2) >> 1}px)`
                    octx.drawImage(imgElement, 0, 0)

                    // crop and resize
                    context.drawImage(
                      oc,
                      x, y,
                      width, height,
                      0, 0,
                      maxWidth, maxHeight,
                    )

                    resolve(canvas.toDataURL(fileType))
                  })
                })
              },
              pipe(
                EmptyString,
                Promise.resolve.bind(Promise),
              ),
            ),
            $.fromPromise,
          ))
          .flatten()
      },
      combine: $.merge,
      to: 'setImage$'
    }),
    WithListener({ // initDrawer
      from: pipe(EmptyString, $.of),
      combine: $.merge,
      to: 'setImage$',
    }),
  ),
)

export const withDrawerIntents = WithDrawerIntents()

export const DrawerIntents = behaviorToFactory(WithDrawerIntents)

export const drawerIntents = DrawerIntents()