import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory'
import pipe from 'ramda/src/pipe.js'
import { WithTransition } from '@monocycle/state'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'
import { SetImageReducer } from './_reducers/setImage.js'





export const WithDrawerModel = pipe(
  ensurePlainObj,
  () => pipe(
    WithTransition({ // initDrawer
      label: 'setImage',
      from: 'setImage$',
      reducer: SetImageReducer,
    }),
  )
)

export const withDrawerModel = WithDrawerModel()

export const DrawerModel = behaviorToFactory(WithDrawerModel)

export const drawerModel = DrawerModel()