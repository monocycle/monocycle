import { WithView, coerceViewOptions } from '@monocycle/dom/src/View'
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory'
import pipe from 'ramda/src/pipe.js'
import prop from 'ramda/src/prop.js'
import { WithIsolated } from '@monocycle/isolated'
import dropRepeats from 'xstream/extra/dropRepeats'
import helpers from '@monocycle/dom/src/helpers.js'
const { Img } = helpers
import { withFlex } from '@monocycle/dom/src/Flex'
import { Box } from '@monocycle/dom/src/Box/Box.js'
import { withJustified } from '@monocycle/dom/src/Justified.js'





export const WithDrawerView = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithView({
      style: {
        backgroundColor: 'var(--darkerColor)',
        position: 'relative',
        overflow: 'hidden',
      },
      has: [
        Box({
          style: {
            width: '100%',
            height: '100%',
          },
          has: [
            Img({
              style: {
                objectFit: 'contain',
                maxHeight: '100%',
                width: '100%',
              },
              from: (sinks, sources) => {
                return sources.state.stream
                  .map(prop('image'))
                  .compose(dropRepeats())
                  .map(src => ({
                    data: {
                      attrs: {
                        src,
                      },
                    },
                  }))
              },
            })
          ]
        }).map(pipe(
          withFlex,
          withJustified,
        ))
      ]
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions
    }),
    WithIsolated({
      DOM: 'image',
      '*': null,
    }),
  )
)

export const withDrawerView = WithDrawerView()

export const DrawerView = behaviorToFactory(WithDrawerView)

export const drawerView = DrawerView()