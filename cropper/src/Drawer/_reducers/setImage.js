import pipe from 'ramda/src/pipe.js'
import assoc from 'ramda/src/assoc.js'
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj'





export const SetImageReducer = image => pipe(
  ensurePlainObj,
  assoc('image', image)
)