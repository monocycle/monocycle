# 31.6.0 (2022-01-24)


### Bug Fixes

* **dom:** repair exports ([ea8223d](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/ea8223db7f28a48d78f8cecb199191eed63d6e57))



# 31.5.0 (2022-01-24)


### Bug Fixes

* **dom:** repair ([d1ae189](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/d1ae189428611a5bf965012f4e6e7cbe430e8b22))



# 31.4.0 (2022-01-24)


### Bug Fixes

* **dom:** use cjs modules from @cycle/dom ([a9b62e9](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/a9b62e9499cad0bf9b2efcd5736c4357dd977276))



# 31.3.0 (2021-01-23)


### Features

* **dom:** improve scrollDriver ([c588b60](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c588b60f4cee42294db69b1ea69e63037f96f115))



# 31.2.0 (2020-11-09)


### Bug Fixes

* **dom:** depends on component@10.1 + use conditional imports ([e83882b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/e83882be236bcef2890daa1fa073a64aee6254cd))



# 31.1.0 (2020-11-05)


### Bug Fixes

* **dom:** add @cycle/dom to dependencies ([b9f47d2](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b9f47d225ecaf8a2b9563eb303548d892e30350f))



# 31.0.0 (2020-11-05)


### Bug Fixes

* **dom:** make package esm compatible ([e6390ce](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/e6390cee0b9c22fce52beea00854435a524203cc))


### BREAKING CHANGES

* **dom:** maybe



# 30.1.0 (2020-10-07)


### Bug Fixes

* **dom:** exposes Card style ([813915a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/813915a74cae88f75c5ff3555872c7c9e3d2e758))
* **dom:** repair Modal ([f7431df](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f7431dfe8ab7df7689a44955d188e39166a02a92))
* **dom:** update dependencies ([87f2fcf](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/87f2fcf71274d4d8fa904c7106af49df7bc03b2a))



# 30.0.0 (2020-09-26)


### Features

* **dom:** lot of change ([48d14f6](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/48d14f61c04688279ca5afbde3301062d5aa2bba))


### BREAKING CHANGES

* **dom:** yess



# 29.6.0 (2020-09-05)


### Features

* **dom:** add DynamicView ([26cf0e2](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/26cf0e2a97e10be1617322494c6b3845cc78d10c))



# 29.5.0 (2020-09-05)


### Features

* **dom:** combine Ready events ([fd2fa8e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fd2fa8e18a370f3e87e2ee98beafb2fd3df45945))



# 29.4.0 (2020-08-30)


### Features

* **dom:** add scrollDriver ([af550af](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/af550af60c1c728fe709e0906d34d56481162410))



# 29.3.0 (2020-08-26)


### Bug Fixes

* **dom:** prevent nullpointererror ([1a2ced0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/1a2ced07a8416b70c869f48ecdeb7066d68944c9))



# 29.2.0 (2020-08-24)


### Bug Fixes

* **dom:** import missing components ([f8c5f7b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f8c5f7b356654369bf882ae42370c717241de91f))



# 29.1.0 (2020-08-24)


### Features

* **dom:** add Name ([264e515](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/264e5154a90c41e93d392c789919dd456c318d21))



# 29.0.0 (2020-08-14)


### Features

* **dom:** rewrite Overlay and Modal ([7aa1689](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/7aa16896193ddf6e5361e5d403096cdd67de18c4))


### BREAKING CHANGES

* **dom:** Modal and Overlay have drastically changed



# 28.6.0 (2020-08-07)


### Features

* **dom:** add NoScroll and ScrollY ([ae911d8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ae911d895789505c4308027fb70b92427a8c13e5))
* **dom:** improve modal animation ([364988a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/364988ae69d738ea695f87060740e9fea46b8763))



# 28.5.0 (2020-08-02)


### Features

* **dom:** add withView and view ([9e292d4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9e292d4e911b37ada04cf54abac7295e70e16884))



# 28.4.0 (2020-07-12)


### Features

* **dom:** add Spaced ([6891e66](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6891e662f073e8adc8e8d92c5aad77ce23be241b))



# 28.3.0 (2020-07-11)


### Features

* **dom:** extract HorizontallySpaced and VerticallySpaced ([338f4eb](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/338f4eb7b3ee09f09fb2858f9c21343276ead7a7))



# 28.1.0 (2020-07-11)


### Bug Fixes

* **dom:** export HorizontallyPadded and VerticallyPadded ([b8d26bb](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b8d26bb0e45a78e7343b8b1866d2babf4ba80598))



# 28.0.0 (2020-06-03)


### Features

* **dom:** rename SideNav to Sidenav ([42171c8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/42171c8629a10e46f3fafd37fae7d5e7f8ec6a43))


### BREAKING CHANGES

* **dom:** SideNav no longer exists, it has been renamed with Sidenav



# 27.16.0 (2020-05-19)


### Bug Fixes

* **dom:** update deps ([322cd01](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/322cd016a5f6542bd48dcf8752c6cab892d84cd6))



# 27.15.0 (2020-05-10)


### Features

* **dom:** EventListener: add transformWith option ([5e00308](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5e00308a0b48d3a81b315fdf2a16dc08b10e8134))



# 27.14.0 (2020-05-04)


### Bug Fixes

* **dom:** update dependencies ([f33586b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f33586b5b63952202f81dc86ef84153b152cc718))



# 27.13.0 (2020-04-01)


### Bug Fixes

* **dom:** update dependencies ([f552367](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f552367dc97ef7ae6d623f3c057023e7b1e1c093))



# 27.12.0 (2020-04-01)


### Bug Fixes

* **dom:** use camelcase for css vars ([91bb96f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/91bb96f798bffd3d6efc176369cfbf31699aad0c))



# 27.11.0 (2020-03-26)


### Bug Fixes

* **dom:** EventListener doesn't recreate stream ([6b85920](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6b859204f4deebffce5c19fd5a9fc9e3d8169a53))



# 27.10.0 (2020-03-26)


### Bug Fixes

* **dom:** parent view must have selector ([ea20423](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ea20423ff494d01c6740aa479e6491708efe7987))



# 27.9.0 (2020-03-16)


### Bug Fixes

* **dom:** repair Button ([0c36b83](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0c36b8311c3bc0580f0b1465e9f8761983df46c0))


### Features

* **dom:** EventListener accept multiple types ([5dbeca6](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5dbeca626215d21353fd30f168d2a0e3dd982b09))



# 27.8.0 (2020-03-10)


### Features

* **dom:** allow color for circle in SVGIcon ([2647d2a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/2647d2a71132aa41aa9e8122ff7ff60b2a42603c))



# 27.7.0 (2020-03-07)


### Bug Fixes

* **dom:** button disabled when disabled in props ([130a35b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/130a35bfec0966afc763e295f082d9d95966ac51))



# 27.6.0 (2020-03-06)


### Bug Fixes

* **dom:** allow to remove reactively SvgIcon ([d26ee43](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d26ee43151f3929cc60ae0fb1643ee54e78f8ecf))



# 27.5.0 (2020-03-05)


### Features

* **dom:** respect Button's disabled state ([d847d81](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d847d81ff086bc83b21817a5dd29a5bc5d165796))



# 27.4.0 (2020-02-29)


### Bug Fixes

* **dom:** depends on state@4.7 ([df87123](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/df87123a3c72bde194fa9fb3250ba4518e9fc62c))


### Features

* **dom:** add new components ([fe96e0f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fe96e0fa345ae19288cf83ea25bf5738b6ad595a))
* **dom:** EventListener can select multiple times ([da6f990](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/da6f99009225473aed77250f9315d6a6ce80f3dc))
* **dom:** View2 defaults to div when empty array ([84116fa](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/84116fa0b0644944af9b5bafb1a6ca81cfb17350))



# 27.3.0 (2020-02-21)


### Bug Fixes

* **dom:** repair View ([d1e9cb4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d1e9cb4673b3acffabe13b47b09947ba9d716413))



# 27.2.0 (2020-02-20)


### Bug Fixes

* **dom:** add empty lines ([75721d7](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/75721d78e7e35047d752e50bdc69fd86195ee66d))
* **dom:** CollapsibleView depends on view2 ([7d58a78](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/7d58a787a877355374f7cd00f63990c879529a96))
* **dom:** Overlay uses a MaybeView ([7cdd650](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/7cdd6501f02a3044fbb6b785721cc16c11b0e68b))
* **dom:** ProgressCircular depends on view2 ([9064dcd](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9064dcdc4bf447f7f0609a56602e08286b9063fe))
* **dom:** remove erroneous replace in Debug ([b6667cd](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b6667cd773e8e6cb2e6aec5856bcc5c0f38cf36e))


### Features

* **dom:** add delay option to EventListener ([46d4b5b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/46d4b5be7f1e8d2fff7d991a1c749a5515cae889))
* **dom:** add modalOpened$ listener ([746d36d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/746d36d90f987f714899230123012917f2a7cfa3))



# 27.1.0 (2020-02-18)


### Bug Fixes

* **dom:** rename LinkStyle file ([bc318df](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/bc318dfde35d90f873ab02cbf48389a2f868c4c1))



# 27.0.0 (2020-02-18)


### Bug Fixes

* **dom:** create a listener for each element ([1454d2a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/1454d2a6697267eb8ad9804b7f3a75a713e73c67))
* **dom:** improve Debug display ([5b4c262](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5b4c262fa23ed58abca5825fabfce34b81100b14))
* **dom:** update dependencies ([152a109](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/152a1093b757a9c80f3c70d83fa0981c93eec391))


### Features

* **dom:** export ButtonStyle ([13e33f5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/13e33f5aae10e6638c5db02b05a484c189b7364e))
* **dom:** remove MaybeLink and repair Link ([39b074d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/39b074d90b98279f1bbf5415d359551def2882c7))
* **dom:** rewrite Overlay and Modal ([8931529](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8931529a6a175221e4b5ac4c2d027addf727feac))
* **dom:** rewrite View ([487bf42](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/487bf4275b03705b18bc9dcd98132756ce541295))


### BREAKING CHANGES

* **dom:** yes
* **dom:** yes



# 25.0.0 (2020-02-14)


### Bug Fixes

* **dom:** add withNoFlex to SideNav ([ba4b312](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ba4b31218a025b059849a0284fb3f1b7e2b0b82e))
* **dom:** concat sel in helpers ([44e791b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/44e791b00b4a3270df36d5074285d91c439ea0ec))
* **dom:** tune Button style ([51e2a5a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/51e2a5ae6b95e5f358af6c960d45b2c8813cce60))


### Features

* **dom:** remove Focusable ([f693aa4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f693aa4e5a22a73ea303e1a6980040836482a878))
* **dom:** use css var iconSize in IconStyle ([7c0e0c4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/7c0e0c458444a49a5145618427c0f3fffa661d4f))


### BREAKING CHANGES

* **dom:** yes
* **dom:** yes



# 24.8.0 (2020-02-08)


### Bug Fixes

* **dom:** add Padded to Button ([4d07510](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4d0751036dd46c1fe53a1d839f79755637105c18))



# 24.7.0 (2020-02-08)


### Bug Fixes

* **dom:** adjust ([4b9a8fb](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4b9a8fb224f7af48be3586cce0d29b5f3e5359be))



# 24.6.0 (2020-01-20)


### Bug Fixes

* **dom:** depends on isolated@4.5 ([55e1c4f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/55e1c4f1307500bc8ad40f383373acbb5044431f))
* **dom:** repair SourceLogger ([952139e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/952139e266c63c3ce0da68d76e39bc5be9bc8793))



# 24.5.0 (2020-01-20)


### Bug Fixes

* **dom:** depends on component@5.5 ([ba6f96f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ba6f96fa699ad048a3a81763bdc4933e863e0595))



# 24.4.0 (2020-01-20)


### Bug Fixes

* **dom:** depends on listener@5.5 ([94eccde](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/94eccde5e3651afe9d439359fa38c547a4914d48))



# 24.3.0 (2020-01-20)


### Bug Fixes

* **dom:** depends on component@5.4 ([d6becaa](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d6becaa2c06476b0dc8ce7075013b3aca82c9887))



# 24.2.0 (2020-01-18)


### Bug Fixes

* **dom:** improve Modal and preload styles ([b0a464d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b0a464dd4227a9f1b7f68ce5f3e924e0b4ee4857))



# 24.1.0 (2020-01-15)


### Bug Fixes

* **dom:** repair loggers ([232df8e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/232df8e9c0e1bfd69977d76377b2e15b3a230f02))



# 24.0.0 (2020-01-12)


### Features

* **dom:** allow to customize SvgIcon color ([1187f23](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/1187f23403b53d47fbfbbbcb4696425129c58e8e))
* **dom:** extracts VerticallyPadded and HorizontallyPadded ([61967d3](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/61967d354db2372d37dd17bd4e15e3e7c2868e67))


### BREAKING CHANGES

* **dom:** yes



# 23.12.0 (2020-01-05)


### Features

* **dom:** Noflex reset flex ([2ae95c0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/2ae95c0d85d3e5fde6747db114d369e9ed8579ac))



# 23.11.0 (2019-12-10)


### Features

* **dom:** add withBox and box ([22dbd6c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/22dbd6c8dfe3020de1f4e77a77d4373c46cdfc0a))



# 23.10.0 (2019-12-02)


### Bug Fixes

* **dom:** update deps ([4d19de5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4d19de5996304322130829d1a5688a04899bcc05))



# 23.9.0 (2019-12-02)


### Bug Fixes

* **dom:** add lock file ([17c8f0f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/17c8f0fcba27795c17f0e8d7819b2113aa670de4))



# 23.8.0 (2019-12-01)


### Bug Fixes

* **dom:** remove horizontal from buttonStyle ([44bf964](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/44bf964b900bf8c01c7c8b7ea65edcfb67c488ef))
* **dom:** remove View has option when is undefined ([48df7e1](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/48df7e1785c77efc197fb0272fc97f55ad8bdffd))


### Features

* **dom:** define svgIcon coerce method ([030eb86](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/030eb86832c179f1285dc7db32012448f9376a3e))
* **dom:** refactor Link ([5b17cfe](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5b17cfe632c090e1f6e532d5ad31dda53fe3d5de))



# 23.7.0 (2019-11-30)


### Features

* **dom:** add card and withCard ([fb4b22b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fb4b22bb7dba5ff5ea14bd769cb0b2d9f3d983a2))
* **dom:** remove Loader ([b887be8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b887be8e5bbc61c78680dbc847ea104d9d0d78ff))



# 23.6.0 (2019-11-26)


### Features

* **dom:** make Image isomorphic ([fe78041](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fe78041f69e48678a938e3229a54bd30ebb46e30))



# 23.5.0 (2019-11-11)


### Bug Fixes

* **dom:** update dependencies ([c10d558](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c10d558c9eca56fa8de759b9cbef611fee52634d))



# 23.4.0 (2019-11-10)


### Bug Fixes

* **dom:** fallback to empty component in node ([2a440f9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/2a440f9ad2b7f71b17b669abc748aea357c25992))
* **dom:** use relative paths ([ae5bd89](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ae5bd897f83152fec7c68f4930db878619a74653))



# 23.3.0 (2019-11-10)


### Features

* **dom:** add Icon ([7c6e96e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/7c6e96ec419485740ef3646649a46d00873520b9))
* **dom:** add Image ([d46bd71](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d46bd71e0018ac90521fd30689a82f4913c9919a))
* **dom:** add Loader ([05f6682](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/05f6682bb3054b2dbe084f3cb977a5720312126f))
* **dom:** add ProgressCircular ([6e24471](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6e244718e13eae794d402bd26038ebd377929619))
* **dom:** add SvgIcon ([f943d0e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f943d0ec4d6c45551b4cad7ca84fc69c1fc1fe6d))



# 23.2.0 (2019-11-10)


### Bug Fixes

* **dom:** remove debugs ([ecc2a4d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ecc2a4deceb2c95e94ed5f952c11fb00098b1a80))



# 23.1.0 (2019-11-10)


### Features

* **dom:** add Image ([db03b30](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/db03b304e1984b2983ab993d0fe6be7e12bfd0ad))
* **dom:** add Layer and LayerParent ([c98b552](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c98b552379396f78d815c29ae53803b7269497e7))
* **dom:** define Overlay default theme ([48ccd4e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/48ccd4e3f4230d9929d1e78809e90eb570e489e3))



# 23.0.0 (2019-11-09)


### Bug Fixes

* **dom:** create empty div when empty array ([7128046](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/7128046a9873b705e5e084568e9f16738a6f671b))


### BREAKING CHANGES

* **dom:** yes



# 22.13.0 (2019-11-08)


### Bug Fixes

* **dom:** unset margin ([4a6c7f5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4a6c7f5ab992faa1d74f33a00cea83fd298d676e))



# 22.12.0 (2019-11-07)


### Features

* **dom:** improve prefixHref ([95e9345](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/95e9345d5ca6e37bb0e0d364043da9e1db189deb))



# 22.10.0 (2019-11-05)


### Bug Fixes

* **dom:** do not add / when path starts with ? ([29f6521](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/29f6521a88b4048cd96dc8044da7b0fa8cbba1c5))



# 22.9.0 (2019-10-30)


### Bug Fixes

* **dom:** change Button style ([de6e0a9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/de6e0a9537092f5959a9a99e74164bc16cab926b))



# 22.8.0 (2019-10-30)


### Bug Fixes

* **dom:** remove debounce 0 in Overlay ([ab9cfe4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ab9cfe4150985399b56f7072797a862106617d1e))



# 22.7.0 (2019-10-30)


### Bug Fixes

* **dom:** use a debounce 0 in Overlay ([8523ea0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8523ea04b1b86860c2095c6539f76acf098b95ac))



# 22.6.0 (2019-10-29)


### Bug Fixes

* **dom:** revert overlay buggy change ([fc48a88](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fc48a882914c3c9006b2d64f57fc72254cfb55f7))



# 22.5.0 (2019-10-29)


### Bug Fixes

* **dom:** OVERLAAAYYYYY!!! ([a10aee4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/a10aee46de98258c78aac386834016727eb282c4))



# 22.4.0 (2019-10-29)


### Bug Fixes

* **dom:** overlaaaaayyy ! ([520fcca](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/520fcca65fc4d850e1f9ee51e84b90885a6fed5a))



# 22.3.0 (2019-10-29)


### Bug Fixes

* **dom:** debounce overlay ([3933d29](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3933d2975a1aea989a0738b2cae1f9cbf5306663))



# 22.1.0 (2019-10-29)


### Bug Fixes

* **dom:** isolate FormModalBody ([a3b1b54](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/a3b1b54af51fa1ec736e439140c11e1e35cea6ef))



# 22.0.0 (2019-10-29)


### Bug Fixes

* **dom:** rewrite Modal and Overlay ([2af895f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/2af895fd0967665e6649888a6259d18f5055288e))


### BREAKING CHANGES

* **dom:** yup



# 21.1.0 (2019-10-28)


### Bug Fixes

* **dom:** repair Overlay ([c4c00e7](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c4c00e7975ff889c2125e316194dfe6dc8410608))



# 21.0.0 (2019-10-28)


### Features

* **dom:** rewrite Overlay ([b929332](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b9293326733cf6ec663bc9aca2761c4d6042bee2))


### BREAKING CHANGES

* **dom:** Overlay rewrited



# 20.5.0 (2019-10-27)


### Features

* **dom:** change Button transtion duration ([fb47911](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fb4791142ae17d21d6afdc00894d9a9918fcb971))



# 20.4.0 (2019-10-22)


### Bug Fixes

* **dom:** do not use self import ([f7fdc39](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f7fdc392f61849e303fc7f7f9a0a294e81b83a6e))


### Features

* **dom:** allow MaybeView to specify options ([7882394](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/788239456045a029ec1c6b0c3aa02afd8b28d9ef))



# 20.3.0 (2019-10-18)


### Bug Fixes

* **dom:** update Modal because Overlay changes ([7b2257d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/7b2257d567975e75e3c8184686b8d0827454a6d6))


### Features

* **dom:** add MaybeLink ([bf789b7](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/bf789b710d66b948a5e2f6c79cbec27d4ccde030))
* **dom:** add missing exports ([d307571](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d307571f31b4d189295c9cf7a23bf47be26755b6))



# 20.2.0 (2019-10-18)


### Bug Fixes

* **dom:** Overlay use position absolute ([ca2913d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ca2913dddd5573e21121dbc2f9fe16ea3a4692bf))



## 20.1.0 (2019-10-16)

* fix(dom): depends on dynamic@2.0 ([9f1a028](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9f1a028))



## 20.0.0 (2019-10-14)

* fix(dom): remove pointless exports ([a530e5c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/a530e5c))


### BREAKING CHANGE

* yes


## 19.2.0 (2019-10-14)

* fix(dom): renaming SpacedColStyle is a breaking change ([6c6f866](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6c6f866))



## 19.1.0 (2019-10-14)

* fix(dom): spaced use --gutter css variable ([4a204aa](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4a204aa))



## 19.0.0 (2019-10-13)

* feat(dom): rename Content to NoFlex to avoid name collision ([dc9d94b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/dc9d94b))


### BREAKING CHANGE

* yep


## 18.0.0 (2019-10-13)

* fix(dom): rewrite ButtonStyle using radial-gradient ([60dd2a3](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/60dd2a3))
* feat(dom): add Content ([325f2a8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/325f2a8))
* feat(dom): export flex component ([fcc20e7](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fcc20e7))


### BREAKING CHANGE

* ripple class is renamed active|buttonReleased listens on transitionend instead of animationend


## 17.5.0 (2019-10-09)

* fix(dom): revert "fix(dom): unset vnode data when empty" ([3c40321](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3c40321))



## 17.4.0 (2019-10-09)

* fix(dom): unset vnode data when empty ([b2dc74d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b2dc74d))



## 17.3.0 (2019-10-08)

* fix(dom): remove csstips dependency ([86fe43d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/86fe43d))



## 17.2.0 (2019-10-07)

* feat(dom): add debug helpers ([bf725fc](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/bf725fc))
* feat(dom): extracts ModalBodyStyle ([a0208c4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/a0208c4))
* fix(dom): use gutter/2 for Button padding ([aab8f0f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/aab8f0f))
* chore(dom): remove unused deps ([201e93b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/201e93b))



## 17.1.0 (2019-09-30)

* fix(dom): bump version ([624a6a9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/624a6a9))



## 17.0.0 (2019-09-30)

* feat(dom): big improvement ([8c490b1](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8c490b1))
* chore(dom): remove lib folder ([6c63b6f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6c63b6f))


### BREAKING CHANGE

* yep


## 16.2.0 (2019-09-26)

* fix(dom): depends on dynamic@1.2 ([4366ac2](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4366ac2))



## 16.1.0 (2019-09-26)

* fix(dom): modal animate works for all views ([7e4ea7a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/7e4ea7a))



## 16.0.0 (2019-09-24)

* fix(dom): update dependencies ([fe3df6b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fe3df6b))
* chore(dom): refactor ClickListener ([2e825bd](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/2e825bd))
* feat(dom): add MaybeView and MaybeButton ([4982a1e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4982a1e))
* feat(dom): add stopPropagation option to EventListener ([6caf3e3](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6caf3e3))
* feat(dom): rewrite Modal using MaybeView ([b80ca93](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b80ca93))


### BREAKING CHANGE

* maybe


## 15.0.0 (2019-09-16)

* fix(dom): link use StyledView instead of View ([9e8b234](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9e8b234))
* feat(dom): use custom snabbdom ([ce1927f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ce1927f))


### BREAKING CHANGE

* yes


## 14.0.0 (2019-09-15)

* feat(dom): add dom driver + remove sel:div defaults options ([87735b3](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/87735b3))


### BREAKING CHANGE

* yes


## 13.4.0 (2019-09-15)

* feat(dom): styleDriver expose classNames with dots ([3f81239](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3f81239))



## 13.3.0 (2019-09-15)

* feat(dom): makes styleDriver to pass classNames as an object ([8158661](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8158661))



## 13.2.0 (2019-09-14)

* fix(dom): style adjustements ([c9ed264](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c9ed264))



## 13.1.0 (2019-09-10)

* fix(dom): modal was displaying 'undefined' when hidden ([a5ffd2c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/a5ffd2c))



## 13.0.0 (2019-09-06)

* chore(dom): update dependencies ([9e827b0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9e827b0))
* feat(dom): export PaddedStyle ([cf96b3c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/cf96b3c))
* feat(dom): simpler modal ([b400481](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b400481))
* feat(dom): split spaced components and add Boxes ([948aaea](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/948aaea))
* fix(dom): cardStyle now uses theme ([6a83f6d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6a83f6d))


### BREAKING CHANGE

* yes


## 12.4.0 (2019-08-28)

* feat(dom): add SourceLogger ([9f1f6a8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9f1f6a8))
* chore(dom): remove mozilla button border ([c6bd428](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c6bd428))



## 12.3.0 (2019-07-10)

* feat(dom): add SinkLogger ([dcb637b](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/dcb637b))
* feat(dom): add Style component ([871c2c3](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/871c2c3))
* feat(dom): padded now uses --gutter css property ([eb89406](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/eb89406))
* feat(dom): styleDriver exposes a className helper ([757409e](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/757409e))
* fix(dom): link does not prefix ([24b8a2b](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/24b8a2b))
* chore(dom): remove useless files ([65b0450](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/65b0450))



## 12.2.0 (2019-06-18)

* fix(dom): remove unused files, comments, and logs ([ee8949b](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/ee8949b))



## 12.1.0 (2019-06-12)

* feat(dom): define a coerce method for Spaced and Padded ([e27f6f8](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/e27f6f8))
* feat(dom): gutter is expected to be a number ([0f62181](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/0f62181))
* feat(dom): modal: set zIndex to 100 and refactor ([aeff724](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/aeff724))



## 12.0.0 (2019-06-10)

* feat(dom): remove typestyle ([218701f](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/218701f))


### BREAKING CHANGE

* yep


## 11.3.0 (2019-06-09)

* feat(dom): add Modal ([17cd0fb](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/17cd0fb))
* feat(dom): add WithSinksLogger and WithSinkDebugger ([7b2af3a](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/7b2af3a))
* fix(dom): change Spaced behaviors order ([181af99](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/181af99))
* fix(dom): update typestyle import path ([5c9276a](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/5c9276a))
* fix(dom): upgrade SinkLogger ([055c27e](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/055c27e))
* chore(dom): remove comments and debug ([9ca453b](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/9ca453b))



## 11.2.0 (2019-05-30)

* fix(dom): set data to undefined on text vnodes ([812d030](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/812d030))



## 11.1.0 (2019-05-29)

* feat(dom): remove Map and material components ([beb6e41](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/beb6e41))



## 11.0.0 (2019-05-29)

* feat(dom): use a pure version of setupPage ([54e94b0](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/54e94b0))
* feat(dom): use es6 modules ([dd1ba5e](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/dd1ba5e))


### BREAKING CHANGE

* yes


## 10.0.0 (2019-05-20)

* feat(dom): improve options merging and add tests ([e3277df](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/e3277df))


### BREAKING CHANGE

* yes


## 9.6.0 (2019-05-18)

* fix(dom): invalid require path ([0da6383](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/0da6383))



## 9.5.0 (2019-05-18)

* fix(dom): fu=inally load marker image ([245f1b0](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/245f1b0))



## 9.4.0 (2019-05-17)

* fix(dom): invalid markerIcon include ([6bb9039](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/6bb9039))



## 9.3.0 (2019-05-17)

* fix(dom): include marker.svg image ([1928488](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/1928488))



## 9.2.0 (2019-05-17)

* fix(dom): and build sources ([205bbfe](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/205bbfe))



## 9.1.0 (2019-05-17)

* fix(dom): invalid import for ol/Map ([c62f850](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/c62f850))



## 9.0.0 (2019-05-17)

* feat(dom): use transpilify workflow ([42d0542](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/42d0542))


### BREAKING CHANGE

* sources moved to src folder


## 8.5.0 (2019-05-17)

* fix(dom): use __dirname ([b487451](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/b487451))



## 8.4.0 (2019-05-17)

* fix(dom): reverse 41906fe2d7bb063b7142756eee1d562aa3a691d1 ([eac56e0](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/eac56e0))



## 8.3.0 (2019-05-17)

* fix(dom): use Path.resolve instead of require.resolve ([41906fe](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/41906fe))



## 8.2.0 (2019-05-17)

* fix(dom): mapInstance$ was missing in nodejs ([f4bfc03](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/f4bfc03))



## 8.1.0 (2019-05-17)

* fix(dom): openlayers not required in nodejs ([eaae6b8](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/eaae6b8))



## 8.0.0 (2019-05-16)

* feat(dom): complete View rewrite ([7a201f3](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/7a201f3)), closes [snabbdom-merge#9f0c4d0093545244720d789fb4024202a251a01](https://gitlab.com/snabbdom-merge/issues/9f0c4d0093545244720d789fb4024202a251a01)


### BREAKING CHANGE

* 


## 7.3.0 (2019-05-16)

* fix(dom): fix snabbdom-merge commit id ([6783a63](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/6783a63))



## 7.2.0 (2019-05-11)

* fix(dom): expose prefixHref ([56fcc76](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/56fcc76))



## 7.1.0 (2019-05-11)

* fix(dom): fix typo in code and refactor ([dc7cd2e](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/dc7cd2e))



## 7.0.0 (2019-05-11)

* chore(dom): remove unused files and update lock file ([9474beb](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/9474beb))
* feat(dom): multiple rewrites of View and all components ([3b67e53](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/3b67e53))


### BREAKING CHANGE

* a lot


## 6.0.0 (2019-04-08)

* fix(dom): rewrite View and close #3 ([cfac78b](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/cfac78b)), closes [#3](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/issues/3) [#3](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/issues/3)


### BREAKING CHANGE

* View full rewrite


## 5.3.0 (2019-04-02)

* fix(dom): depend on validable@3.4 ([cbf5a06](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/cbf5a06))
* fix(dom): update deps ([52b3d7e](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/52b3d7e))



## 5.2.0 (2019-04-01)

* feat(dom): add EventListener ([ed1b0bb](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/ed1b0bb))
* feat(dom): add material components ([bb4cde7](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/bb4cde7))
* fix(dom): rename Clickable to ClickableView ([c4be62a](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/c4be62a))
* chore(dom): switch to snabbdom-merge@master ([48dfb27](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/48dfb27))



## 5.1.0 (2019-03-29)

* feat(dom): extract rendering logic from View's compositeView ([964a3bf](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/964a3bf))
* fix(dom): uncomment test ([7605f95](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/7605f95))



## 5.0.0 (2019-03-28)

* chore(dom): remove logs ([70afea1](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/70afea1))
* feat(dom): allowing to merge "from" options with options ([587bc9b](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/587bc9b))
* fix(dom): remove Form from dom ([e7e58d6](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/e7e58d6))


### BREAKING CHANGE

* Form is removed
* probably


## 4.9.0 (2019-03-28)

* fix(dom): depend on component@3.9 ([9811638](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/9811638))



## 4.8.0 (2019-03-28)

* fix(dom): freeze versions due to #2 ([1fb6b0d](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/1fb6b0d)), closes [#2](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/issues/2)



## 4.7.0 (2019-03-24)

* fix(dom): depend on component@3.7 ([2ea159a](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/2ea159a))
* fix(dom): remove prefixer from Link ([248a2ac](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/248a2ac))
* fix(dom): set minHeight on rowStyle ([8a22656](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/8a22656))



## 4.6.0 (2019-03-23)

* fix(dom): depend on component@3.6 ([abc647a](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/abc647a))



## 4.5.0 (2019-03-23)

* fix(dom): depend on component@3.5 ([e1b67c0](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/e1b67c0))



## 4.4.0 (2019-03-22)

* fix(dom): depend on component@3.3 ([7a00b2a](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/7a00b2a))
* fix(dom): flex accepts "sel" option ([93a4f27](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/93a4f27))
* chore(dom): depend on validable@3.1 ([17836ce](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/17836ce))



## 4.3.0 (2019-03-17)

* fix(dom): remove minHeight and ass a padding ([2e5bd98](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/2e5bd98))



## 4.2.0 (2019-03-17)

* fix(dom): update Button style ([1e9f092](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/1e9f092))



## 4.1.0 (2019-03-17)

* fix(dom): update Button style ([e2b00d1](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/e2b00d1))



## 4.0.0 (2019-03-17)

* fix(dom): change selector order in Button ([d3bdf0c](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/d3bdf0c))
* feat(dom): add Content component ([79fc5b0](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/79fc5b0))


### BREAKING CHANGE

* Button({ sel: '.ga' }) produces .Button.ga instead of .ga.Button


## 3.15.0 (2019-03-17)

* fix(dom): add address to helpers ([9e38e17](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/9e38e17))



## 3.14.0 (2019-03-17)

* fix(dom): forward Flex options to View ([bea6388](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/bea6388))
* chore(dom): remove dom dependency ([10284b6](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/10284b6))



## 3.13.0 (2019-03-16)

* feat(dom): remove Field,StyledView and extract styles ([d97435f](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/d97435f))



## 3.12.0 (2019-03-11)

* feat(dom): accept view options in Fill ([d28ddf0](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/d28ddf0))



## 3.11.0 (2019-03-10)

* fix(dom): view default to div ([93ee743](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/93ee743))



## 3.10.0 (2019-03-09)

* fix(dom): repair dependencies ([4be6735](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/4be6735))



## 3.9.0 (2019-03-09)

* feat(dom): expose csstips and csx ([687fc67](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/687fc67))



## 3.8.0 (2019-03-09)

* fix(dom): invalid requires ([d3c7af6](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/d3c7af6))



## 3.7.0 (2019-03-08)

* feat(dom): split Box into Row and Col ([74958a4](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/74958a4))



## 3.6.0 (2019-03-05)

* fix(dom): code refactoring ([d82c8cf](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/d82c8cf))
* fix(dom): depend on validable@3.0 ([9e39786](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/9e39786))
* fix(dom): remove Layout ([99e53a9](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/99e53a9))
* fix(dom): repair Button and Clickable ([5e6936e](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/5e6936e))
* chore(dom): extract CompositeView from View ([67a1cbf](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/67a1cbf))
* feat(dom): add view helpers ([e43a804](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/e43a804))
* feat(dom): update components ([88bf553](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/88bf553))



## 3.5.0 (2019-03-05)

* fix(dom): code refactoring ([d82c8cf](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/d82c8cf))
* fix(dom): depend on validable@3.0 ([9e39786](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/9e39786))
* fix(dom): remove Layout ([99e53a9](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/99e53a9))
* fix(dom): repair Button and Clickable ([5e6936e](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/5e6936e))
* chore(dom): extract CompositeView from View ([67a1cbf](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/67a1cbf))
* feat(dom): add view helpers ([e43a804](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/e43a804))
* feat(dom): update components ([88bf553](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/88bf553))



## 3.4.0 (2019-03-03)

* feat(dom): add SinkLogger helper ([d83160a](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/d83160a))
* chore(dom): re-export typestyle default instance ([c9ab87e](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/c9ab87e))



## 3.3.0 (2019-03-03)

* feat(dom): add SinkLogger helper ([d83160a](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/d83160a))
* chore(dom): re-export typestyle default instance ([c9ab87e](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/c9ab87e))



## 3.2.0 (2019-03-03)

* fix(dom): depend on isolated@3.0 ([309b34b](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/309b34b))



## 3.1.0 (2019-03-03)

* fix(dom): depend on after@3.0 ([64db054](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/64db054))
* chore(dom): change test title ([5643374](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/5643374))



## 3.0.0 (2019-03-02)

* feat(dom): improve View and Button ([a19c279](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/a19c279))
* feat(dom): upgrade Button to component@3.0 ([ca503c7](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/ca503c7))
* feat(dom): upgrade component to 3.0 ([11bc27d](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/11bc27d))
* fix(dom): remove ViewCombiner from utilities exports ([daec91f](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/daec91f))


### BREAKING CHANGE

* lots


## 2.6.0 (2019-02-25)

* fix(dom): add node_modules to .npmignore ([8a20654](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/8a20654))



## 2.5.0 (2019-02-25)

* fix(dom): update @symbols ([1967479](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/1967479))
* feat(dom): let mergeViewOptions concat "has" options ([206a8dc](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/206a8dc))



## 2.4.0 (2019-02-24)

* feat(dom): add Card component ([14383a6](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/14383a6))
* chore(dom): clean comments ([3bd43c5](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/3bd43c5))



## 2.3.0 (2019-02-24)

* feat(dom): add Box components ([642c2ef](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/642c2ef))
* feat(dom): add StyledView component ([06fd049](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/06fd049))
* chore(dom): fix lint warnings ([d399cfa](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/d399cfa))



## 2.2.0 (2019-02-05)

* fix(dom): don't expose renderVnode because snabbdom-to-html is a devDep ([1773976](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/1773976))



## 2.0.0 (2019-02-04)

* fix(dom): move 'Field' debounce and remove useless debounces ([c3824db](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/c3824db))
* fix(dom): update @component ([06222eb](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/06222eb))
* chore(dom): add files ([81dcc67](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/81dcc67))


### BREAKING CHANGE

* yes


