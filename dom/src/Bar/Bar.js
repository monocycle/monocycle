import pipe from 'ramda/src/pipe.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { coerceViewOptions } from '../View/View.js';
import { WithStyledView } from '../StyledView/StyledView.js';
import { WithRow } from '../Box/Row.js';
import { BarStyle } from './Bar.style.js';





export const WithBar = pipe(
  coerceViewOptions,
  rowOptions => {

    return pipe(
      WithRow({
        ...rowOptions
      }),
      WithStyledView({
        styles: BarStyle,
        name: 'Bar',
      }),
    );
  }
);

export const Bar = behaviorToFactory(WithBar);