import { center } from 'monostyle/src/flex.js';
import { padding } from 'monostyle/src/box.js';
import { content } from 'monostyle/src/flex.js';
import { rem } from 'monostyle/src/units.js';





export const BarStyle = ({
  size = rem(6.4),
  smallHeight = rem(3.2),
  bigHeight = rem(9.6),
  colors = {},
} = {}) => ([
  content,
  center,
  {
    $debugName: 'Bar',
    backgroundColor: colors.background,
    '& > *': {
      height: '100%',
    },
    '&:not(.col)': {
      // '&.spaced': padding(0, rem(theme.gutter)),
      height: size,
      minHeight: size,
      '&.small': {
        height: smallHeight,
        minHeight: smallHeight,
      },
      '&.big': {
        height: bigHeight,
        minHeight: bigHeight,
      }
    },
    '&.col': {
      // '&.spaced': padding(rem(theme.gutter), 0),
      width: size,
      minWidth: size,
      '&.small': {
        width: smallHeight,
        minWidth: smallHeight,
      },
      '&.big': {
        width: bigHeight,
        minWidth: bigHeight,
      }
    },
    ['& > &']: {
      ...padding(0),
      width: 'inherit',
      height: 'inherit',
    },
  }
]);