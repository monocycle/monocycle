import { WithEventListener } from '../EventListener';
import { extend } from '@monocycle/component/lib/utilities/extend.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';


const WithBlurListener = extend(WithEventListener, {
  type: 'blur',
  to: 'blur$',
});

const BlurListener = behaviorToFactory(WithBlurListener);

export {
  WithBlurListener,
  BlurListener,
};