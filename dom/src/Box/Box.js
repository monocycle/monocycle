import pipe from 'ramda/src/pipe.js';
import { WithView, coerceViewOptions }  from '../View/View.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView }  from '../StyledView/StyledView.js';
import { vertical, horizontal } from 'monostyle/src/flex.js';





export const WithBox = pipe(
  coerceViewOptions,
  ({
    reverse,
    ...viewOptions
  }) => pipe(
    reverse
      ? WithStyledView({
        name: 'ReverseBox',
        styles: ({ media, theme: { variables: { boxBreakpoint = 500 } } }) => [
          media({ maxWidth: boxBreakpoint }, horizontal),
          media({ minWidth: boxBreakpoint }, vertical),
        ]
      })
      : WithStyledView({
        name: 'Box',
        styles: ({ media, theme: { variables: { boxBreakpoint = 500 } } }) => [
          media({ maxWidth: boxBreakpoint }, vertical),
          media({ minWidth: boxBreakpoint }, horizontal),
        ]
      }),

    WithView({
      sel: '.',
      children: [],
      ...viewOptions
    }),
  )
);

export const withBox = WithBox();

export const Box = behaviorToFactory(WithBox);

export const box = Box();

WithBox.coerce = Box.coerce = coerceViewOptions;