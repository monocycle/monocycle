import pipe from 'ramda/src/pipe.js';
import { vertical as colStyle } from 'monostyle/src/flex.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithView, coerceViewOptions, ensureParentHasSelector }  from '../View/View.js';
import { WithStyledView }  from '../StyledView/StyledView.js';





export const WithCol = pipe(
  coerceViewOptions,
  ensureParentHasSelector,
  viewOptions => pipe(
    WithStyledView({
      name: 'Col',
      styles: colStyle
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions
    }),
  )
);

export const withCol = WithCol();

export const Col = behaviorToFactory(WithCol);

export const col = Col();

WithCol.coerce = Col.coerce = coerceViewOptions;