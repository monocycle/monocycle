import pipe from 'ramda/src/pipe.js';
import { WithView, coerceViewOptions }  from '../View/View.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView }  from '../StyledView/StyledView.js';





export const HorizontallyPaddedStyle = [
  {
    paddingLeft: 'var(--gutter)',
    paddingRight: 'var(--gutter)',
  }
];

export const WithHorizontallyPadded = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'HorizontallyPadded',
      styles: HorizontallyPaddedStyle
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
    })
  )
);

export const withHorizontallyPadded = WithHorizontallyPadded();

export const HorizontallyPadded = behaviorToFactory(WithHorizontallyPadded);

export const horizontallyPadded = HorizontallyPadded();

HorizontallyPadded.coerce = WithHorizontallyPadded.coerce = coerceViewOptions;
