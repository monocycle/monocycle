import pipe from 'ramda/src/pipe.js';
import always from 'ramda/src/always.js';
import { WithView, coerceViewOptions } from '../View/View.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from '../StyledView/index.js';





export const HorizontallySpacedStyle = always({
  '& > *': {
    marginRight: 'var(--gapX, var(--gutter))'
  },
  '& > *:last-child': {
    marginRight: '0px'
  }
});

export const WithHorizontallySpaced = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'horizontallySpaced',
      styles: HorizontallySpacedStyle
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
    })
  )
);

export const withHorizontallySpaced = WithHorizontallySpaced();

export const HorizontallySpaced = behaviorToFactory(WithHorizontallySpaced);

export const horizontallySpaced = HorizontallySpaced();

WithHorizontallySpaced.coerce = HorizontallySpaced.coerce = coerceViewOptions;