import pipe from 'ramda/src/pipe.js';
import { horizontal as rowStyle } from 'monostyle/src/flex.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithView, coerceViewOptions, ensureParentHasSelector }  from '../View/View.js';
import { WithStyledView }  from '../StyledView/StyledView.js';





export const WithRow = pipe(
  coerceViewOptions,
  ensureParentHasSelector,
  viewOptions => pipe(
    WithStyledView({
      name: 'Row',
      styles: rowStyle
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions
    }),
  )
);

export const withRow = WithRow();

export const Row = behaviorToFactory(WithRow);

export const row = Row();

WithRow.coerce = Row.coerce = coerceViewOptions;