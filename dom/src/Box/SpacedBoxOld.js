import pipe from 'ramda/src/pipe.js';
import { coerceViewOptions }  from '../View/View.js';
import { WithBox } from './Box';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithSpaced } from './Spaced.js';





export const WithSpacedBox = pipe(
  coerceViewOptions,
  ({
    reverse,
    ...spacedOptions
  }) => pipe(
    WithSpaced({
      reverse,
      ...spacedOptions,
    }),
    WithBox({
      reverse
    }),
  )
);

// export const withSpacedBox = WithSpacedBox();

export const SpacedBox = behaviorToFactory(WithSpacedBox);

// export const spacedBox = SpacedBox();

WithSpacedBox.coerce = SpacedBox.coerce = coerceViewOptions;