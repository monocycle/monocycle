import pipe from 'ramda/src/pipe.js';
import { coerceViewOptions, ensureParentHasSelector } from '../View/index.js';
import { WithCol } from './Col.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { withVerticallySpaced } from '../Box/VerticallySpaced.js';





export const WithSpacedCol = pipe(
  coerceViewOptions,
  ensureParentHasSelector,
  colOptions => pipe(
    withVerticallySpaced,
    WithCol({
      sel: '.',
      children: [],
      ...colOptions
    }),
  )
);

export const withSpacedCol = WithSpacedCol();

export const SpacedCol = behaviorToFactory(WithSpacedCol);

export const spacedCol = SpacedCol();

WithSpacedCol.coerce = SpacedCol.coerce = coerceViewOptions;