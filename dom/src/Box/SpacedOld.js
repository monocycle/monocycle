import pipe from 'ramda/src/pipe.js';
import { coerceViewOptions, WithView }  from '../View/View.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView }  from '../StyledView/StyledView.js';
import { HorizontallySpacedStyle } from './HorizontallySpaced.js';
import { VerticallySpacedStyle } from './VerticallySpaced.js';





export const WithSpaced = pipe(
  coerceViewOptions,
  ({
    reverse,
    ...viewOptions
  }) => pipe(
    reverse
      ? WithStyledView({
        name: 'ReverseSpaced',
        styles: ({ media, theme: { breakpoint = 700, ...theme } }) => [
          media({ maxWidth: breakpoint }, HorizontallySpacedStyle({ theme })),
          media({ minWidth: breakpoint }, VerticallySpacedStyle({ theme })),
        ]
      })
      : WithStyledView({
        name: 'Spaced',
        styles: ({ media, theme: { breakpoint = 700, ...theme } }) => [
          media({ maxWidth: breakpoint }, VerticallySpacedStyle({ theme })),
          media({ minWidth: breakpoint }, HorizontallySpacedStyle({ theme })),
        ]
      }),
    WithView({
      sel: '',
      children: [],
      ...viewOptions,
    }),
  )
);

// export const withSpaced = WithSpaced();

export const Spaced = behaviorToFactory(WithSpaced);

// export const spaced = Spaced();

WithSpaced.coerce = Spaced.coerce = coerceViewOptions;