import pipe from 'ramda/src/pipe.js';
import { coerceViewOptions, ensureParentHasSelector } from '../View/index.js';
import { WithRow } from './Row.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { withHorizontallySpaced } from '../Box/HorizontallySpaced.js';





export const WithSpacedRow = pipe(
  coerceViewOptions,
  ensureParentHasSelector,
  rowOptions => pipe(
    withHorizontallySpaced,
    WithRow({
      sel: '.',
      children: [],
      ...rowOptions,
    }),
  )
);

export const withSpacedRow = WithSpacedRow();

export const SpacedRow = behaviorToFactory(WithSpacedRow);

export const spacedCol = SpacedRow();

WithSpacedRow.coerce = SpacedRow.coerce = coerceViewOptions;