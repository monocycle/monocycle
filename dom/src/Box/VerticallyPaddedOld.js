import pipe from 'ramda/src/pipe.js';
import { WithView, coerceViewOptions }  from '../View/View.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView }  from '../StyledView/StyledView.js';





export const VerticallyPaddedStyle = [
  {
    paddingTop: 'var(--gapY, var(--gutter))',
    paddingBottom: 'var(--gapY, var(--gutter))',
  }
];

export const WithVerticallyPadded = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'VerticallyPadded',
      styles: VerticallyPaddedStyle
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
    })
  )
);

export const withVerticallyPadded = WithVerticallyPadded();

export const VerticallyPadded = behaviorToFactory(WithVerticallyPadded);

export const verticallyPadded = VerticallyPadded();

VerticallyPadded.coerce = WithVerticallyPadded.coerce = coerceViewOptions;
