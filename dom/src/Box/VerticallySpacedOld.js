import pipe from 'ramda/src/pipe.js';
import always from 'ramda/src/always.js';
import { WithView, coerceViewOptions } from '../View/View.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from '../StyledView/index.js';





export const VerticallySpacedStyle = always({
  '& > *': {
    marginBottom: 'var(--gapY, var(--gutter))'
  },
  '& > *:last-child': {
    marginBottom: '0px'
  },
});

export const WithVerticallySpaced = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'verticallySpaced',
      styles: VerticallySpacedStyle
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
    })
  )
);


export const withVerticallySpaced = WithVerticallySpaced();

export const VerticallySpaced = behaviorToFactory(WithVerticallySpaced);

export const verticallySpaced = VerticallySpaced();

WithVerticallySpaced.coerce = VerticallySpaced.coerce = coerceViewOptions;