import { rem } from 'monostyle/src/units.js';
import pipe from 'ramda/src/pipe.js';
import { ensureArray } from '@monocycle/component/lib/utilities/ensureArray.js';
import map from 'ramda/src/map.js';
import { ifElse } from '@monocycle/component/lib/utilities/ifElse.js';
import unless from 'ramda/src/unless.js';
import isNumber from 'ramda-adjunct/src/isNumber.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import noop from 'ramda-adjunct/src/noop.js';





const parseRemValue = unless(isNonEmptyString,
  ifElse(isNumber,
    rem,
    noop
  )
);

const parseRemValues = pipe(
  ensureArray,
  map(parseRemValue),
);

export {
  parseRemValues,
  parseRemValue,
};