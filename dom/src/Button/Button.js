import { Stream as $ } from 'xstream';
import pipe from 'ramda/src/pipe.js';
import pathSatisfies from 'ramda/src/pathSatisfies.js';
import filter from 'ramda/src/filter.js';
import when from 'ramda/src/when.js';
import hasPath from 'ramda/src/hasPath.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { ButtonStyle } from './Button.style.js';
import { WithStyledView } from '../StyledView/StyledView.js';
import { WithView, coerceViewOptions } from '../View/View.js';
import { WithEventListener } from '../EventListener/EventListener.js';
import identity from 'ramda/src/internal/_identity.js';
import { WithIsolated } from '@monocycle/isolated';
import isFalse from 'ramda-adjunct/src/isFalse.js';





export const WithButton = pipe(
  coerceViewOptions,
  mergeDeepRight({
    sel: '.',
    children: [],
    attrs: {
      type: 'button',
    },
  }),
  ({ scope, ...options }) => pipe(
    WithStyledView({
      name: 'Button',
      sel: 'button',
      styles: ButtonStyle,
    }),
    WithView({
      sel: '',
      children: [],
      ...options
    }),
    WithEventListener({ // buttonPressed
      type: 'mousedown',
      combine: $.merge,
      transform: filter(when(
        hasPath(['ownerTarget', 'disabled']),
        pathSatisfies(isFalse, ['target', 'disabled'])
      )),
      to: 'buttonPressed$'
    }),
    WithEventListener({ // buttonReleased
      type: 'transitionend',
      combine: $.merge,
      transform: filter(when(
        hasPath(['ownerTarget', 'disabled']),
        pathSatisfies(isFalse, ['target', 'disabled'])
      )),
      to: 'buttonReleased$'
    }),
    WithView({
      sel: '',
      children: [],
      from: ({ buttonPressed$, buttonReleased$ }) => {

        return $.merge(
          buttonPressed$.mapTo(true),
          buttonReleased$.mapTo(false),
        )
          .startWith(false)
          .map(pipe(
            Boolean,
            active => ({
              sel: '.',
              children: [],
              data: {
                class: {
                  active
                }
              }
            })
          ));
      }
    }),
    !scope ? identity : WithIsolated(scope),
  )
);

export const withButton = WithButton();

export const Button = behaviorToFactory(WithButton);

export const button = Button();
