import test from 'ava';
import { WithButton } from './Button';
import { viewTest } from '../utilities/testView';

viewTest({
  test,
  behavior: WithButton,
  sel: 'button.Padded.Button',
  data: {
    attrs: {
      type: 'button'
    },
    class: {
      active: false
    },
  },
});