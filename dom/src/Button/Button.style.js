import pipe from 'ramda/src/pipe.js';
import { rgba } from 'monostyle/src/color.js';
import over from 'ramda/src/over.js';
import lensPath from 'ramda/src/lensPath.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import { rem } from 'monostyle/src/units.js';
import { centerCenter, flexRoot } from 'monostyle/src/flex.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { toString } from '@monocycle/component/lib/utilities/toString.js';



export const ButtonStyle = pipe(
  ensurePlainObj,
  over(lensPath(['theme', 'button']), pipe(
    ensurePlainObj,
    mergeDeepRight({
      minHeight: rem(6.4),
      colors: {
        background: 'transparent',
        text: '#3e3e3e',
        ripple: rgba(0, 0, 0, 0.1),
        primary: {
          text: '#009688',
          background: '#009688',
          hover: {
            text: '#009688',
            background: 'green',
          }
        },
        invalid: {
          text: '#e93f3b',
          background: '#e93f3b',
        },
        valid: {
          text: '#1bc876',
          background: '#1bc876',
        },
        hover: {
          background: 'transparent',
          text: '#009688',
        },
        disabled: {
          text: '#000',
          background: 'gray',
        }
      },
    })
  )),
  ({ cssRule, theme: { button: theme } }) => {

    cssRule('button::-moz-focus-inner', {
      border: '0'
    });

    cssRule('button:focus', {
      outline: 'none'
    });

    return [
      flexRoot,
      // horizontal,
      centerCenter,
      // padding(rem(gutter / 2)),
      {
        color: toString(theme.colors.text),
        backgroundColor: toString(theme.colors.background),
        border: 'none',
        position: 'relative',
        inlineSize: 'inherit',
        cursor: 'pointer',
        overflow: 'hidden',
        transform: 'translate3d(0, 0, 0)',
        '&:disabled': {
          opacity: '.33',
          // color: toString(theme.colors.disabled.text),
          // backgroundColor: toString(theme.colors.disabled.background),
        },
        '&, &:hover, &:active, &:focus': {
          outline: 'none',
        },
        '&::after': {
          content: '""',
          display: 'block',
          position: 'absolute',
          width: '100%',
          height: '100%',
          top: 0,
          left: 0,
          backgroundImage: `radial-gradient(circle, ${toString(theme.colors.ripple)} 10%, transparent 10%)`,
          backgroundRepeat: 'no-repeat',
          backgroundPosition: '50%',
          opacity: 0,
          transform: 'scale(0, 0)',
        },
        '&.active:not(.stay):not(:disabled)::after': {
          transition: 'all .5s ease-out',
        },
        '&.active:not(:disabled)::after': {
          opacity: 1,
          transform: 'scale(11, 11)',
        },
        '&:hover': {
          color: toString(theme.colors.hover.text),
          backgroundColor: toString(theme.colors.hover.background),
        },
        '&.primary': {
          color: toString(theme.colors.primary.text),
          backgroundColor: toString(theme.colors.primary.background),
          '&:hover': {
            color: toString(theme.colors.primary.hover.text),
            backgroundColor: toString(theme.colors.primary.hover.background),
          },
        },
        // '&:disabled': {

        // }
      }
    ];
  }
);