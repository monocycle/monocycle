import { Stream as $ } from 'xstream';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import pipe from 'ramda/src/pipe.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { Button } from '../Button';
import { WithView }  from '../View/View.js';

const WithApp = pipe(
  ensurePlainObj,
  () => {

    return pipe(
      WithView({
        has: [
          Button({
            from: () => $.of({
              sel: '.pwet',
              children: 'hi!',
            })
          }),
        ]
      })
    );
  }
);

const App = behaviorToFactory(WithApp);

export {
  WithApp,
  App
};