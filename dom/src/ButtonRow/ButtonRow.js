import pipe from 'ramda/src/pipe.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { ButtonRowStyle } from './ButtonRow.style.js';
import { ButtonStyle } from '../Button/Button.style.js';
import { WithRow } from '../Box/Row.js';
import { WithStyledView }  from '../StyledView/StyledView.js';
import { WithStyle } from '../Style/Style.js';





export const WithButtonRow = pipe(
  WithRow.coerce,
  rowOptions => pipe(
    WithRow(rowOptions),
    WithStyle({
      name: 'Button',
      styles: ButtonStyle,
    }),
    WithStyledView({
      name: 'ButtonRow',
      styles: ButtonRowStyle,
    }),
  )
);

export const withButtonRow = WithButtonRow();

export const ButtonRow = behaviorToFactory(WithButtonRow);

export const buttonRow = ButtonRow();

WithButtonRow.coerce = ButtonRow.coerce = WithRow.coerce;