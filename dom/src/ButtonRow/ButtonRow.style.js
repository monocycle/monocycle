import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import pipe from 'ramda/src/pipe.js';





export const ButtonRowStyle = pipe(
  ensurePlainObj,
  ({ classNames: { Button } }) => [
    {
      [`& > ${Button}:first-child`]: {
        paddingLeft: 'calc(var(--padX, var(--pad, 1em)) / 2)',
      },
      [`& > ${Button}:last-child`]: {
        paddingRight: 'calc(var(--padX, var(--pad, 1em)) / 2)',
      },
      [`& > ${Button}:not(:first-child)`]: {
        paddingLeft: 'calc(var(--gapX, var(--gap, 1em)) / 2)',
      },
      [`& > ${Button}:not(:last-child)`]: {
        paddingRight: 'calc(var(--gapX, var(--gap, 1em)) / 2)',
      },
    },
  ]
);