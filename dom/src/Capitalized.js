import pipe from 'ramda/src/pipe.js';
import { coerceViewOptions, WithView } from './View/View.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from './StyledView/StyledView.js';





export const WithCapitalized = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'Capitalized',
      styles: [
        {
          textTransform: 'capitalize',
        },
      ],
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
    }),
  ),
);
export const withCapitalized = WithCapitalized();

export const Capitalized = behaviorToFactory(WithCapitalized);

export const capitalized = Capitalized();