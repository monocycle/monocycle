import pipe from 'ramda/src/pipe.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithView, coerceViewOptions, ensureParentHasSelector }  from '../View/View.js';
import { WithStyledView }  from '../StyledView/StyledView.js';
import { cardStyle } from './Card.style.js';





export const WithCard = pipe(
  coerceViewOptions,
  ensureParentHasSelector,
  viewOptions => pipe(
    WithStyledView({
      name: 'Card',
      styles: cardStyle
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions
    }),
  )
);

export const Card = behaviorToFactory(WithCard);

export const withCard = WithCard();

export const card = Card();