import test from 'ava';
import { WithCard } from './Card';
import { viewTest } from '../utilities/testView';

viewTest({
  test,
  behavior: WithCard,
  sel: '.Card',
});