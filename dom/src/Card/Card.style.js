import { rem } from 'monostyle/src/units.js';





export const cardStyle = [{
  $debugName: 'Card',
  overflow: 'hidden',
  borderRadius: rem(.4),
  boxShadow:
    '0 1px 3px 0 rgba(0, 0, 0, 0.2), ' +
    '0 1px 1px 0 rgba(0, 0, 0, 0.14), ' +
    '0 2px 1px -1px rgba(0, 0, 0, 0.12)',
  backgroundColor: 'var(--cardBgColor, var(--lighterColor, white))',
}];
