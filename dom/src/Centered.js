import pipe from 'ramda/src/pipe.js';
import { coerceViewOptions, WithView } from './View/View.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from './StyledView/StyledView.js';
import { center, flexRoot } from 'monostyle/src/flex.js';



export const centeredStyle = [
  flexRoot,
  center,
];

export const WithCentered = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'Centered',
      styles: centeredStyle,
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
    }),
  ),
);

export const withCentered = WithCentered();

export const Centered = behaviorToFactory(WithCentered);

export const centered = Centered();