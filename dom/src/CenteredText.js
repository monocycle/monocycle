import pipe from 'ramda/src/pipe.js';
import { coerceViewOptions, WithView } from './View/View.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from './StyledView/StyledView.js';





export const centeredTextStyle = {
  textAlign: 'center',
};

export const WithCenteredText = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'CenteredText',
      styles: centeredTextStyle,
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
    }),
  ),
);
export const withCenteredText = WithCenteredText();

export const CenteredText = behaviorToFactory(WithCenteredText);

export const centeredText = CenteredText();