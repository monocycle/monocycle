import { WithEventListener } from '../EventListener';
import { extend } from '@monocycle/component/lib/utilities/extend.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';


export const WithClickListener = extend(WithEventListener, {
  type: 'click',
  options: {
    preventDefault: true,
  },
  to: 'click$',
});

export const ClickListener = behaviorToFactory(WithClickListener);