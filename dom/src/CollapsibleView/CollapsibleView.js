import { Stream as $ } from 'xstream';
import { WithView, coerceViewOptions }  from '../View/View.js';
import always from 'ramda/src/always.js';
import pipe from 'ramda/src/pipe.js';
import lensProp from 'ramda/src/lensProp.js';
import over from 'ramda/src/over.js';
import identical from 'ramda/src/identical.js';
import prop from 'ramda/src/prop.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import unless from 'ramda/src/unless.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';





export const WithCollapsibleView = pipe(
  coerceViewOptions,
  over(lensProp('size'), unless(isNonEmptyString,
    always('200px')
  )),
  over(lensProp('size'), unless(isNonEmptyString,
    always('200px')
  )),
  over(lensProp('duration'), unless(isNonEmptyString,
    always('.25s')
  )),
  over(lensProp('property'), unless(identical('width'),
    always('height')
  )),
  over(lensProp('sel'), unless(isNonEmptyString,
    always('div')
  )),
  over(lensProp('from'), unless(isFunction,
    always((sinks, sources) => {
      return sources.state.stream
        .map(prop('collapsed'));
    })
  )),
  over(lensProp('style'), ensurePlainObj),
  ({ from, property, duration, max, size, style, ...viewOptions }) => {

    const transitionProperty = max
      ? `max-${property}`
      : property;

    return WithView({
      ...viewOptions,
      style: mergeDeepRight({
        overflow: 'hidden',
        transition: `${transitionProperty} ${duration}`,
        ...(max
          ? {
            [property]: 'auto',
          }
          : {}),
        remove: {
          [`${transitionProperty}`]: '0'
        },
      }, style),
      from: pipe(
        from,
        collapsed$ => {

          return $.merge(
            collapsed$.take(1).map(vnodeIsCollapsed => {
              return ({
                sel: '.',
                children: [],
                data: {
                  style: {
                    [`${transitionProperty}`]: vnodeIsCollapsed
                      ? '0'
                      : size,
                  }
                }
              });
            }),
            collapsed$.drop(1).map(vnodeIsCollapsed => {
              return vnodeIsCollapsed
                ? undefined
                : {
                  sel: '.',
                  children: [],
                  data: {
                    style: {
                      [`${transitionProperty}`]: '0',
                      delayed: {
                        [`${transitionProperty}`]: size,
                      },
                    }
                  }
                };
            }),
          );
        }
      ),
    });
  }
);

export const CollapsibleView = behaviorToFactory(WithCollapsibleView);