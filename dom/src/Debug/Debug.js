import pipe from 'ramda/src/pipe.js';
import assoc from 'ramda/src/assoc.js';
import replace from 'ramda/src/replace.js';
import when from 'ramda/src/when.js';
import tap from 'ramda/src/tap.js';
import path from 'ramda/src/path.js';
import slice from 'ramda/src/slice.js';
import assocPath from 'ramda/src/assocPath.js';
import startsWith from 'ramda/src/startsWith.js';
import { Component } from '@monocycle/component';
import { assign } from '@monocycle/component/lib/utilities/assign.js';
import { coerce } from '@monocycle/component/lib/utilities/coerce.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from '../StyledView/StyledView.js';
import { WithView } from '../View/View.js';
import { stringify } from '@monocycle/component/lib/utilities/stringify.js';
import { DebugStyle } from './Debug.style.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import isString from 'ramda-adjunct/src/isString.js';
import isNotUndefined from 'ramda-adjunct/src/isNotUndefined.js';
import { WithAfter } from '@monocycle/after';





export const WithDebug = pipe(
  coerce,
  viewOptions => {

    return pipe(
      WithStyledView({
        name: 'Debug',
        styles: DebugStyle
      }),
      WithView({
        sel: 'pre',
        ...viewOptions
      }),
    );
  }
);

export const log = (...args) => {
  return tap(console.log.bind(console, ...args));
};
export const warn = (...args) => {
  return tap(console.warn.bind(console, ...args));
};
export const error = (...args) => {
  return tap(console.error.bind(console, ...args));
};
export const debug = (...args) => {
  return tap(console.debug.bind(console, ...args));
};


export const WithStateDebug = pipe(
  coerce,
  assoc('from', (sinks, sources) => {
    return sources.state.stream.debug('state')
      .map(pipe(
        stringify,
        replace(/},\n +{/g, '__ARRAY_SEPARATOR__'),
        replace(/["{}]/g, ''),
        replace(/\n +,\n/g, '\n'),
        replace(/__ARRAY_SEPARATOR__/g, ','),
        replace(/\n[ ]+\n/g, '\n'),
        replace(/,\n/g, '\n'),
        replace(/\n /g, '\n'),
        when(startsWith('\n'), slice(1, Infinity))
      ))
      // .map(slice(1, Infinity))
      .replaceError(err => console.error(err))
      .startWith();
  }),
  WithDebug
);

export const Debug = behaviorToFactory(WithDebug);

export const StateDebug = behaviorToFactory(WithStateDebug);


export const WithSinksLogger = (key = 'sinks') => WithAfter(sinks => {

  console.debug(key, Object.keys(sinks));

  return sinks;
});

export const SinksLogger = behaviorToFactory(WithSinksLogger);

export const WithSinkLogger = (sinkKey, prefix) => {
  return WithAfter(sinks => {

    if (!sinks[sinkKey] || !isFunction(sinks[sinkKey].map))
      throw new Error(`No '${sinkKey}' sink`);

    const args = [
      sinkKey,
      prefix,
    ].filter(isNotUndefined);

    return {
      ...sinks,
      [sinkKey]: sinks[sinkKey].map(debug(...args))
    };
  });
};

export const SinkLogger = behaviorToFactory(WithSinkLogger);


export const WithSourceLogger = (sourcePath, prefix) => {

  return component => {

    return Component(assign(component)(sources => {

      let source = path(sourcePath)(sources);

      let key = prefix;
      if (isString(sourcePath))
        key + '.' + sourcePath.join('.');

      if (!source || !isFunction(source.map))
        throw new Error(`No '${key}' source`);

      source = source.map(value => {

        console.debug(...[
          key,
          prefix,
          value
        ].filter(isNotUndefined));

        return value;
      });

      const sinks = pipe(
        assocPath(sourcePath, source),
        component,
      )(sources);

      return {
        ...sinks,
      };
    }));
  };
};

export const SourceLogger = behaviorToFactory(WithSourceLogger);


export const WithSinkDebugger = key => {
  return WithAfter(sinks => {

    if (!sinks[key] || !isFunction(sinks[key].map))
      throw new Error(`No '${key}' sink`);

    sinks[key] = sinks[key].map(value => {

      key;
      // eslint-disable-next-line no-debugger
      debugger;

      return value;
    });

    return sinks;
  });
};

export const SinkDebugger = behaviorToFactory(WithSinkDebugger);
