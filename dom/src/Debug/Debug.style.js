import { rem } from 'monostyle/src/units.js';
import { scroll } from 'monostyle/src/scroll.js';
import { toString } from '@monocycle/component/lib/utilities/toString.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import over from 'ramda/src/over.js';
import pipe from 'ramda/src/pipe.js';
import concat from 'ramda/src/concat.js';
import unless from 'ramda/src/unless.js';
import lensPath from 'ramda/src/lensPath.js';
import always from 'ramda/src/always.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';





const defaultDebugStyle = [{
  $debugName: 'Debug',
  fontSize: rem(1.2),
  lineHeight: rem(1.6),
  fontFamily: '"Source Sans Pro", sans-serif',
  padding: 'var(--pad, 1em)'
}];

const DebugStyle = pipe(
  ensurePlainObj,
  over(lensPath(['background', 'color']),
    unless(isNonEmptyString, always('#3e3e3e'))
  ),
  over(lensPath(['text', 'color']),
    unless(isNonEmptyString, always('#fefefe'))
  ),
  theme => [
    scroll,
    {
      backgroundColor: toString(theme.background.color),
      color: toString(theme.text.color),
    }
  ],
  concat(defaultDebugStyle)
);

export {
  DebugStyle
};

