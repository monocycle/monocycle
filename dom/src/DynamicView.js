import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithDynamic } from '@monocycle/dynamic';
import { extend } from '@monocycle/component/lib/utilities/extend.js';
import apply from 'ramda/src/apply.js';
import { mergeViewStreams } from './View/View.js';





export const WithDynamicView = extend(WithDynamic, {
  mergeExceptions: {
    DOM: apply(mergeViewStreams)
  },
});

export const DynamicView = behaviorToFactory(WithDynamicView);