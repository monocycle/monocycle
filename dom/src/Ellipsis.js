import pipe from 'ramda/src/pipe.js';
import { coerceViewOptions, WithView } from './View/View.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from './StyledView/StyledView.js';





export const ellipsisStyle = {
  overflow: 'hidden',
  textOverflow: 'ellipsis',
};

export const WithEllipsis = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'Ellipsis',
      styles: ellipsisStyle,
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
    }),
  ),
);
export const withEllipsis = WithEllipsis();

export const Ellipsis = behaviorToFactory(WithEllipsis);

export const ellipsis = Ellipsis();