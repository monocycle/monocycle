import over from 'ramda/src/over.js';
import tap from 'ramda/src/tap.js';
import map from 'ramda/src/map.js';
import unless from 'ramda/src/unless.js';
import dissoc from 'ramda/src/dissoc.js';
import assoc from 'ramda/src/assoc.js';
import not from 'ramda/src/not.js';
import when from 'ramda/src/when.js';
import reduce from 'ramda/src/reduce.js';
import both from 'ramda/src/both.js';
import { ifElse } from '@monocycle/component/lib/utilities/ifElse.js';
import prop from 'ramda/src/prop.js';
import merge from 'ramda/src/merge.js';
import includes from 'ramda/src/includes.js';
import __ from 'ramda/src/__.js';
import lte from 'ramda/src/lte.js';
import apply from 'ramda/src/apply.js';
import lensProp from 'ramda/src/lensProp.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import castArray from 'ramda-adjunct/src/ensureArray.js';
import isInteger from 'ramda-adjunct/src/isInteger.js';
import noop from 'ramda-adjunct/src/noop.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { WithListener, parseFrom } from '@monocycle/listener';
import { pipe } from '@monocycle/component/lib/utilities/pipe.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { flattenStream } from '@monocycle/component/lib/utilities/flattenStream.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import always from 'ramda/src/always.js';
import identity from 'ramda/src/internal/_identity.js';
import { Stream as $ } from 'xstream';





const isNonNegativeInteger = both(
  isInteger,
  lte(0),
);

export const stopEventPropagation = tap(event => event.stopPropagation());

const select = source => reduce((source, target) => {

  return !target
    ? source
    : source.select(target);
}, source);

export const WithEventListener = pipe(
  ensurePlainObj,
  over(lensProp('from'), pipe(
    when(not, always(pipe(noop, $.of))),
    parseFrom,
  )),
  over(lensProp('type'), pipe(
    castArray,
    map(unless(isNonEmptyString, () => {
      throw new Error(`'type' must be a non empty string`);
    }))
  )),
  over(lensProp('options'), ensurePlainObj),
  over(lensProp('transform'), unless(isFunction, always(identity))),
  over(lensProp('transformWith'), unless(isFunction, noop)),
  when(prop('transformWith'), assoc('transform', identity)),
  over(lensProp('delay'), unless(isNonNegativeInteger, noop)),
  ifElse(prop('delay'),
    over(lensProp('delayType'), unless(
      includes(__, ['debounce', 'throttle', 'delay']),
      always('delay')
    )),
    dissoc('delayType')
  ),
  ({
    type,
    stopPropagation,
    options,
    target,
    transform,
    transformWith,
    delay,
    delayType,
    from: optionsFrom,
    ...listenerOptions
  }) => {

    const from = pipe(
      (sinks, sources) => {

        const selectSource = select(sources.DOM);

        return pipe(
          optionsFrom,
          map(pipe(
            ensurePlainObj,
            merge({ target }),
            over(lensProp('target'), castArray),
            ({ target }) => {

              const targetSource = selectSource(target);

              return pipe(
                map(type => targetSource.events(type, options)),
                apply($.merge),
              )(type);
            }
          )),
          flattenStream,
          transformWith
            ? transformWith(sinks, sources)
            : identity,
        )(sinks, sources);
      },
      transform,
    );

    return WithListener({
      ...listenerOptions,
      from: (sinks, sources) => {

        let event$ = from(sinks, sources);

        if (delay)
          event$ = event$.compose(sources.Time[delayType](delay));

        return !stopPropagation
          ? event$
          : map(stopEventPropagation)(event$);
      }
    });
  }
);

export const EventListener = behaviorToFactory(WithEventListener);
