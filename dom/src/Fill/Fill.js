import { WithView, coerceViewOptions }  from '../View/View.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView }  from '../StyledView/StyledView.js';
import { fillParent } from 'monostyle/src/box.js';
import pipe from 'ramda/src/pipe.js';

export const WithFill = pipe(
  coerceViewOptions,
  viewOptions => {

    return pipe(
      WithStyledView({
        name: 'Fill',
        styles: [
          fillParent,
        ],
      }),
      WithView({
        sel: '.',
        children: [],
        ...viewOptions
      }),
    );
  }
);
export const withFill = WithFill();

export const Fill = behaviorToFactory(WithFill);

export const fill = Fill();
