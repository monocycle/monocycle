import test from 'ava';
import { WithFill } from './';
import { viewTest } from '../utilities/testView';

viewTest({
  test,
  behavior: WithFill,
  sel: '.Fill',
});