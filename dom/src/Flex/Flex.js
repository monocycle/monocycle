import pipe from 'ramda/src/pipe.js';
import { flex as flexStyle } from 'monostyle/src/flex.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithView, coerceViewOptions, ensureParentHasSelector }  from '../View/View.js';
import { WithStyledView }  from '../StyledView/StyledView.js';





export const WithFlex = pipe(
  coerceViewOptions,
  ensureParentHasSelector,
  viewOptions => pipe(
    WithStyledView({
      name: 'Flex',
      styles: flexStyle
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions
    }),
  )
);

export const withFlex = WithFlex();

export const Flex = behaviorToFactory(WithFlex);

export const flex = Flex();