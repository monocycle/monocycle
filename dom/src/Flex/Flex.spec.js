import test from 'ava';
import { WithFlex } from './Flex';
import { viewTest } from '../utilities/testView';

viewTest({
  test,
  behavior: WithFlex,
  sel: '.Flex',
});