import { flex } from 'monostyle/src/flex.js';


const FlexibleStyle = () => [
  flex,
  {
    $debugName: 'Flexible',
  }
];

export {
  FlexibleStyle
};