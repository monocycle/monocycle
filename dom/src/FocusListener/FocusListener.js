import { WithEventListener } from '../EventListener';
import { extend } from '@monocycle/component/lib/utilities/extend.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';


const WithFocusListener = extend(WithEventListener, {
  type: 'focus',
  to: 'focus$',
  options: {
    useCapture: true
  },
});

const FocusListener = behaviorToFactory(WithFocusListener);

export {
  WithFocusListener,
  FocusListener,
};