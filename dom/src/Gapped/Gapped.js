import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from '../StyledView/StyledView.js';
import { WithView, coerceViewOptions } from '../View/View.js';
import { pipe } from '@monocycle/component/lib/utilities/pipe.js';
import { gappedStyle } from './Gapped.style.js';
import isEmpty from 'ramda/src/isEmpty.js';





export const WithGapped = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'Gapped',
      styles: gappedStyle,
    }),
    ...isEmpty(viewOptions) ? [] : [WithView({
      sel: '',
      children: [],
      ...viewOptions,
    })],
  ),
);

export const Gapped = behaviorToFactory(WithGapped);

export const withGapped = WithGapped();

export const gapped = Gapped();

WithGapped.coerce = Gapped.coerce = coerceViewOptions;

export default Gapped;