import { gappedXStyle } from '../GappedX/GappedX.style.js';
import { gappedYStyle } from '../GappedY/GappedY.style.js';





export const gappedStyle = [
  gappedXStyle,
  gappedYStyle,
];

export default gappedStyle;




// export const gappedStyle = {
//   rowGap: 'var(--gapY, var(--gap, 1em))',
//   columnGap: 'var(--gapX, var(--gap, 1em))',
// };

// export default gappedStyle;