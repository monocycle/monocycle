import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from '../StyledView/StyledView.js';
import { pipe } from '@monocycle/component/lib/utilities/pipe.js';
import { gappedXStyle } from './GappedX.style.js';
import isEmpty from 'ramda/src/isEmpty.js';
import { WithView, coerceViewOptions } from '../View/View.js';





export const WithGappedX = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'GappedX',
      styles: gappedXStyle,
    }),
    ...isEmpty(viewOptions) ? [] : [WithView({
      sel: '',
      children: [],
      ...viewOptions,
    })],
  ),
);

export const GappedX = behaviorToFactory(WithGappedX);

export const withGappedX = WithGappedX();

export const gappedX = GappedX();

WithGappedX.coerce = GappedX.coerce = coerceViewOptions;

export default GappedX;