import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from '../StyledView/StyledView.js';
import { pipe } from '@monocycle/component/lib/utilities/pipe.js';
import { gappedYStyle } from './GappedY.style.js';
import isEmpty from 'ramda/src/isEmpty.js';
import { WithView, coerceViewOptions } from '../View/View.js';





export const WithGappedY = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'GappedY',
      styles: gappedYStyle,
    }),
    ...isEmpty(viewOptions) ? [] : [WithView({
      sel: '',
      children: [],
      ...viewOptions,
    })],
  ),
);

export const GappedY = behaviorToFactory(WithGappedY);

export const withGappedY = WithGappedY();

export const gappedY = GappedY();

WithGappedY.coerce = GappedY.coerce = coerceViewOptions;

export default GappedY;