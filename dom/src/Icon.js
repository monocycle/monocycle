import pipe from 'ramda/src/pipe.js';
import { coerceViewOptions } from './View/View.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from './StyledView/StyledView.js';
import { rem } from 'monostyle/src/units.js';
import helpers from './helpers.js';
const { Img, WithI } = helpers;





export const IconStyle = ({ theme: { iconSize = 3.2 } }) => {

  const iconSizeInRem = rem(iconSize);

  return [
    {
      display: 'block',
      width: `var(--iconSize, ${iconSizeInRem})`,
      height: `var(--iconSize, ${iconSizeInRem})`,
      padding: '0',
      cursor: 'pointer',
      img: {
        width: '100%',
        height: '100%',
      }
    }
  ];
};

export const WithIcon = pipe(
  coerceViewOptions,
  ({ src, ...viewOptions }) => pipe(
    WithI({
      ...viewOptions,
      has: Img({ attrs: { src } })
    }),
    WithStyledView({
      name: 'Icon',
      styles: IconStyle
    }),
  )
);

export const Icon = behaviorToFactory(WithIcon);