import pipe from 'ramda/src/pipe.js';
import { coerceViewOptions, WithView } from './View/View.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from './StyledView/StyledView.js';
import { centerJustified, flexRoot } from 'monostyle/src/flex.js';



export const justifiedStyle = [
  flexRoot,
  centerJustified,
];

export const WithJustified = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'Justified',
      styles: justifiedStyle,
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
    }),
  ),
);
export const withJustified = WithJustified();

export const Justified = behaviorToFactory(WithJustified);

export const justified = Justified();