import pipe from 'ramda/src/pipe.js';
import { newLayer as layerStyle } from 'monostyle/src/layer.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithView, coerceViewOptions }  from '../View/View.js';
import { WithStyledView }  from '../StyledView/StyledView.js';


export const WithLayer = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'Layer',
      styles: layerStyle
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions
    }),
  )
);

export const withLayer = WithLayer();

export const Layer = behaviorToFactory(WithLayer);

export const layer = Layer();