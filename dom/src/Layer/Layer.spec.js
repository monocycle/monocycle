import test from 'ava';
import { WithLayer } from './Layer';
import { viewTest } from '../utilities/testView';

viewTest({
  test,
  behavior: WithLayer,
  sel: '.Layer',
});