import pipe from 'ramda/src/pipe.js';
import { layerParent as layerParentStyle } from 'monostyle/src/layer.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithView, coerceViewOptions }  from '../View/View.js';
import { WithStyledView }  from '../StyledView/StyledView.js';





export const WithLayerParent = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'LayerParent',
      styles: layerParentStyle
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions
    }),
  )
);

export const withLayerParent = WithLayerParent();

export const LayerParent = behaviorToFactory(WithLayerParent);

export const layerParent = LayerParent();