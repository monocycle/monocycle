import test from 'ava';
import { WithLayerParent } from './LayerParent';
import { viewTest } from '../utilities/testView';

viewTest({
  test,
  behavior: WithLayerParent,
  sel: '.LayerParent',
});