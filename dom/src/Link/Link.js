import pipe from 'ramda/src/pipe.js';
import when from 'ramda/src/when.js';
import unless from 'ramda/src/unless.js';
import always from 'ramda/src/always.js';
import identical from 'ramda/src/identical.js';
import concat from 'ramda/src/concat.js';
import startsWith from 'ramda/src/startsWith.js';
import anyPass from 'ramda/src/anyPass.js';
import prop from 'ramda/src/prop.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { LinkStyle } from './Link.style.js';
import { coerceViewOptions, WithView }  from '../View/View.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import isString from 'ramda-adjunct/src/isString.js';
import { WithStyledView }  from '../StyledView/StyledView.js';
import identity from 'ramda/src/internal/_identity.js';
import noop from 'ramda-adjunct/src/noop.js';





const ensureString = pipe(
  identity,
  unless(isString, always(''))
);

export const prefixHref = prefix => pipe(
  ensureString,
  !prefix ? identity : unless(
    anyPass([
      startsWith('http:'),
      startsWith('mailto:'),
      startsWith('callto:'),
      // startsWith('/'),
    ]),
    pipe(
      href => {

        return href && !href.startsWith('/') && !href.startsWith('?')
          ? '/' + href
          : href;
      },
      when(identical('/'), always('')),
      concat(prefix)
    )
  ),
);

export const WithLink = pipe(
  coerceViewOptions,
  over(lensProp('Style'), unless(isFunction, always(LinkStyle))),
  over(lensProp('from'), pipe(
    when(isString, prop),
    unless(isFunction, noop),
  )),
  ({
    href = '',
    Style,
    ...viewOptions
  }) => pipe(
    WithStyledView({
      name: 'Link',
      styles: Style,
      sel: 'a',
    }),
    WithView({
      sel: '.',
      children: [],
      ...(!href ? {} : { attrs: { href } }),
      ...viewOptions
    }),
  )
);

export const withLink = WithLink();

export const Link = behaviorToFactory(WithLink);

export const link = Link();