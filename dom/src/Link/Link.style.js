import { important } from 'monostyle/src/strings.js';
import { toString } from '@monocycle/component/lib/utilities/toString.js';





export const LinkStyle = ({
  colors = {},
} = {}) => {

  return [{
    $debugName: 'Link',
    color: important(toString(colors.linkText)),
  }];
};