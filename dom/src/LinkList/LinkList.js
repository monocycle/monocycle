import { coerce } from '@monocycle/component/lib/utilities/coerce.js';
import { Link } from '../Link';
import { WithUl } from '../';
import pipe from 'ramda/src/pipe.js';
import map from 'ramda/src/map.js';


const WithLinkList = pipe(
  coerce,
  ({ has, ...listOptions }) => {

    return WithUl({
      ...listOptions,
      has: map(Link)(has)
    });
  }
);

const LinkList = options => WithLinkList(options)();

export {
  LinkList,
  WithLinkList
};