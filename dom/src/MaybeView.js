import { Stream as $ } from 'xstream';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import always from 'ramda/src/always.js';
import when from 'ramda/src/when.js';
import objOf from 'ramda/src/objOf.js';
import identity from 'ramda/src/internal/_identity.js';
import False from 'ramda/src/F.js';
import unless from 'ramda/src/unless.js';
import { WithView } from './View/View.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import dropRepeats from 'xstream/extra/dropRepeats.js';





export const WithMaybeView = pipe(
  identity,
  when(isFunction, objOf('from')),
  WithView.coerce,
  over(lensProp('from'), unless(isFunction, always(pipe(
    False,
    $.of,
  )))),
  ({ from, ...viewOptions }) => pipe(
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
      from: (sinks, sources) => {

        return from(sinks, sources)
          .compose(dropRepeats())
          .map(pipe(
            visible => {
              return visible
                ? { sel: '.', children: [] }
                : undefined;
            },
          ));
      },
    }),
  )
);

export const MaybeView = behaviorToFactory(WithMaybeView);