import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView }  from '../StyledView/StyledView.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { WithView } from '../View/View';
import { withCol } from '../Box/Col';
import { Stream as $ } from 'xstream';
import { WithEventListener } from '../EventListener';
import { Card, cardStyle } from '../Card';
import pipe from 'ramda/src/pipe.js';
import lensProp from 'ramda/src/lensProp.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import over from 'ramda/src/over.js';
import propEq from 'ramda/src/propEq.js';
import filter from 'ramda/src/filter.js';
import always from 'ramda/src/always.js';
import unless from 'ramda/src/unless.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { WithOverlay, whenVisibleState } from '../Overlay/Overlay';
import { WithStyle } from '../Style/Style.js';
import { WithIsolated } from '@monocycle/isolated';
import { WithListener } from '@monocycle/listener';
import { ModalStyle } from './Modal.style.js';






export const WithModal = pipe(
  WithView.coerce,
  over(lensProp('overlay'), ensurePlainObj),
  over(lensProp('style'), pipe(
    ensurePlainObj,
    mergeDeepRight({
      transition: '.6s transform',
      transform: 'scale(0.6)',
      delayed: {
        transform: 'scale(1)',
      },
      remove: {
        transform: 'scale(0.6)',
      },
      destroy: {
        transform: 'scale(0.6)',
      },
    }),
  )),
  over(lensProp('visibleWhen'), unless(isFunction, always(whenVisibleState))),
  ({ visibleWhen, overlay: overlayOptions, ...viewOptions }) => pipe(
    WithStyle({
      name: 'Card',
      styles: cardStyle
    }),
    WithOverlay({
      sel: 'div',
      ...overlayOptions,
      visibleWhen,
      // extractVisible: prop(visibleKey),
      has: Card(viewOptions)
        .map(pipe(
          withCol,
          WithStyledView({
            name: 'Modal',
            styles: ModalStyle,
          }),
          WithIsolated({
            DOM: 'modal',
            '*': null,
          }),
        )),
    }),
    WithEventListener({
      type: 'mousedown',
      options: {
        preventDefault: true,
      },
      combine: $.merge,
      to: 'hideModal$',
    }),
    WithListener({
      from: $.empty,
      to: 'showModal$',
    }),
    WithEventListener({
      type: 'keydown',
      target: 'document',
      transform: filter(propEq('key', 'Escape')),
      combine: $.merge,
      to: 'hideModal$',
    }),
    WithEventListener({
      type: 'transitionend',
      combine: $.merge,
      to: 'modalOpened$',
    }),
  ),
);

export const withModal = WithModal();

export const Modal = behaviorToFactory(WithModal);

export const modal = Modal();

WithModal.coerce = Modal.coerce = WithView.coerce;