import { someChildWillScroll } from 'monostyle/src/scroll.js';





export const ModalStyle = [
  someChildWillScroll,
  {
    maxHeight: 'calc(100vh - (var(--padY, var(--pad, 1em)) * 4))',
    maxWidth: 'calc(100vw - (var(--padX, var(--pad, 1em)) * 2))',
  },
];