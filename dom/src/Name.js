import pipe from 'ramda/src/pipe.js';
import prop from 'ramda/src/prop.js';
import either from 'ramda/src/either.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import unless from 'ramda/src/unless.js';
import always from 'ramda/src/always.js';
import { coerceViewOptions, WithView } from './View/View.js';
import { withPadded } from './Padded';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { ensureString } from '@monocycle/component/lib/utilities/ensureString.js';
import { withOneLineText } from './OneLineText.js';
import { withEllipsis } from './Ellipsis.js';
import dropRepeats from 'xstream/extra/dropRepeats.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';





export const WithName = pipe(
  coerceViewOptions,
  over(lensProp('extractName'), unless(isFunction, always(prop('name')))),
  over(lensProp('extractShortName'), unless(isFunction, always(prop('shortName')))),
  ({
    noShort,
    extractShortName,
    extractName,
    ...viewOptions
  }) => pipe(
    WithView({
      from: (sinks, sources) => {
        return sources.state.stream
          .map(pipe(
            noShort
              ? extractName
              : either(extractShortName, extractName),
            ensureString,
          ))
          .compose(dropRepeats());
      },
      ...viewOptions,
    }),
    withEllipsis,
    withOneLineText,
    withPadded,
  ),
);

export const withName = WithName();

export const Name = behaviorToFactory(WithName);

export const name = Name();

WithName.coerce = Name.coerce = coerceViewOptions;