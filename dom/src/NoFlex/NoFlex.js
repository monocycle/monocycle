import pipe from 'ramda/src/pipe.js';
import { content } from 'monostyle/src/flex.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithView, coerceViewOptions } from '../View/View.js';
import { WithStyledView } from '../StyledView/StyledView.js';


export const WithNoFlex = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'NoFlex',
      styles: [
        content,
        { flex: 'initial' }
      ]
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions
    }),
  )
);

export const withNoFlex = WithNoFlex();

export const NoFlex = behaviorToFactory(WithNoFlex);

export const noFlex = NoFlex();