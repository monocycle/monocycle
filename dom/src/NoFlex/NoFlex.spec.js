import test from 'ava';
import { WithNoFlex } from './NoFlex.js';
import { viewTest } from '../utilities/testView';

viewTest({
  test,
  behavior: WithNoFlex,
  sel: '.NoFlex',
});