import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from './StyledView/index.js';
import { coerceViewOptions, WithView } from './View/View.js';
import pipe from 'ramda/src/pipe.js';
import { someChildWillScroll } from 'monostyle/src/scroll.js';





export const WithNoScroll = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'NoScroll',
      styles: someChildWillScroll,
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
    }),
  ),
);

export const withNoScroll = WithNoScroll();

export const NoScroll = behaviorToFactory(WithNoScroll);

export const noScroll = NoScroll();