import pipe from 'ramda/src/pipe.js';
import { coerceViewOptions, WithView } from './View/View.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from './StyledView/StyledView.js';





export const oneLineTextStyle = {
  whiteSpace: 'nowrap',
};

export const WithOneLineText = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'OneLineText',
      styles: oneLineTextStyle,
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
    }),
  ),
);
export const withOneLineText = WithOneLineText();

export const OneLineText = behaviorToFactory(WithOneLineText);

export const oneLineText = OneLineText();