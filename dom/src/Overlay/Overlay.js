
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView }  from '../StyledView/StyledView.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { WithView }  from '../View/View.js';
import { WithMaybeView } from '../MaybeView';
import pipe from 'ramda/src/pipe.js';
import lensProp from 'ramda/src/lensProp.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import always from 'ramda/src/always.js';
import over from 'ramda/src/over.js';
import prop from 'ramda/src/prop.js';
import unless from 'ramda/src/unless.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { OverlayStyle } from './Overlay.style.js';
import dropRepeats from 'xstream/extra/dropRepeats.js';





export const whenVisibleState = (sinks, sources) => {
  return sources.state.stream
    .map(pipe(prop('visible'), Boolean))
    .compose(dropRepeats());
};

export const WithOverlay = pipe(
  WithView.coerce,
  over(lensProp('visibleWhen'), unless(isFunction, always(whenVisibleState))),
  over(lensProp('style'), pipe(
    ensurePlainObj,
    mergeDeepRight({
      opacity: '0',
      delayed: { opacity: '1' },
      remove: { opacity: '0' },
      destroy: { opacity: '0' },
    }),
  )),
  ({ visibleWhen, ...viewOptions }) => pipe(
    WithStyledView({
      name: 'Overlay',
      styles: OverlayStyle,
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
    }),
    WithMaybeView({
      from: visibleWhen,
    }),
  ),
);

export const withOverlay = WithOverlay();

export const Overlay = behaviorToFactory(WithOverlay);

export const overlay = Overlay();
