
import { centerCenter } from 'monostyle/src/flex.js';
import { newLayer } from 'monostyle/src/layer.js';
import { hsla } from 'monostyle/src/color.js';
import { toString } from '@monocycle/component/lib/utilities/toString.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import lte from 'ramda/src/lte.js';
import prop from 'ramda/src/prop.js';
import allPass from 'ramda/src/allPass.js';
import gte from 'ramda/src/gte.js';
import always from 'ramda/src/always.js';
import unless from 'ramda/src/unless.js';
import pipe from 'ramda/src/pipe.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import isNumber from 'ramda-adjunct/src/isNumber.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';





export const OverlayStyle = pipe(
  ensurePlainObj,
  over(lensProp('theme'), pipe(
    prop('overlay'),
    ensurePlainObj,
    over(lensProp('zIndex'), unless(isNumber, always(100))),
    over(lensProp('opacity'), unless(
      allPass([
        isNumber,
        lte(0),
        gte(1),
      ]),
      always(.4)
    )),
    over(lensProp('transition'), pipe(
      ensurePlainObj,
      over(lensProp('duration'), unless(isNonEmptyString, always('1s'))),
      over(lensProp('property'), unless(isNonEmptyString, always('opacity'))),
    )),
  )),
  ({
    theme: {
      zIndex,
      opacity,
      transition: {
        property,
        duration,
      },
    }
  }) => ([
    newLayer,
    centerCenter,
    {
      display: 'flex',
      backgroundColor: toString(hsla(0, 0, 0, opacity)),
      zIndex,
      transition: `${duration} ${property}`,
      margin: '0',
    }
  ])
);