import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from '../StyledView/StyledView.js';
import { pipe } from '@monocycle/component/lib/utilities/pipe.js';
import { paddedStyle } from './Padded.style.js';
import isEmpty from 'ramda/src/isEmpty.js';
import { WithView, coerceViewOptions } from '../View/View.js';





export const WithPadded = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'Padded',
      styles: paddedStyle,
    }),
    ...isEmpty(viewOptions) ? [] : [WithView({
      sel: '',
      children: [],
      ...viewOptions,
    })],
  ),
);

export const Padded = behaviorToFactory(WithPadded);

export const withPadded = WithPadded();

export const padded = Padded();

WithPadded.coerce = Padded.coerce = coerceViewOptions;

export default Padded;