import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from '../StyledView/StyledView.js';
import { pipe } from '@monocycle/component/lib/utilities/pipe.js';
import { paddedXStyle } from './PaddedX.style.js';
import isEmpty from 'ramda/src/isEmpty.js';
import { WithView, coerceViewOptions } from '../View/View.js';





export const WithPaddedX = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'PaddedX',
      styles: paddedXStyle,
    }),
    ...isEmpty(viewOptions) ? [] : [WithView({
      sel: '',
      children: [],
      ...viewOptions,
    })],
  ),
);

export const PaddedX = behaviorToFactory(WithPaddedX);

export const withPaddedX = WithPaddedX();

export const paddedX = PaddedX();

WithPaddedX.coerce = PaddedX.coerce = coerceViewOptions;

export default PaddedX;