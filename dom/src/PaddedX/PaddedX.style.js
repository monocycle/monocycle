




export const paddedXStyle = {
  paddingLeft: 'var(--padX, var(--pad, 1em))',
  paddingRight: 'var(--padX, var(--pad, 1em))',
};

export default paddedXStyle;