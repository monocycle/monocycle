import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from '../StyledView/StyledView.js';
import { pipe } from '@monocycle/component/lib/utilities/pipe.js';
import { paddedYStyle } from './PaddedY.style.js';
import isEmpty from 'ramda/src/isEmpty.js';
import { WithView, coerceViewOptions } from '../View/View.js';





export const WithPaddedY = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'PaddedY',
      styles: paddedYStyle,
    }),
    ...isEmpty(viewOptions) ? [] : [WithView({
      sel: '',
      children: [],
      ...viewOptions,
    })],
  ),
);

export const PaddedY = behaviorToFactory(WithPaddedY);

export const withPaddedY = WithPaddedY();

export const paddedY = PaddedY();

WithPaddedY.coerce = PaddedY.coerce = coerceViewOptions;

export default PaddedY;