




export const paddedYStyle = {
  paddingTop: 'var(--padY, var(--pad, 1em))',
  paddingBottom: 'var(--padY, var(--pad, 1em))',
};

export default paddedYStyle;