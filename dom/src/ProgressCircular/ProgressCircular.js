import pipe from 'ramda/src/pipe.js';
import always from 'ramda/src/always.js';
import unless from 'ramda/src/unless.js';
import isNumber from 'ramda-adjunct/src/isNumber.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { coerceViewOptions, WithView }  from '../View/View.js';
import { WithStyledView }  from '../StyledView/StyledView.js';
import { svg } from '@cycle/dom';
import helpers from '../helpers.js';
import { progressCircularStyle } from './ProgressCircular.style.js';
const { WithI } = helpers;
import identity from 'ramda/src/internal/_identity.js';
import { Component } from '@monocycle/component';





const isBrowser = typeof window !== 'undefined';

export const WithProgressCircular = !isBrowser
  ? always(
    pipe(
      identity,
      unless(isFunction, always(Component.empty)),
    )
  )
  : pipe(
    coerceViewOptions,
    ({
      thickness = .25,
      size = 64,
      backgroundColor = 'var(--glassColor, #666)',
      color = 'var(--firstColor, #0083ff)',
      ...viewOptions
    }) => pipe(
      WithStyledView({
        name: 'ProgressCircular',
        styles: progressCircularStyle
      }),
      WithView({
        sel: '.',
        children: [],
        ...viewOptions,
      }),
      WithI({
        sel: '.',
        children: [],
        style: {
          opacity: '0',
          transition: 'opacity .3s',
          delayed: {
            opacity: '1',
          },
          remove: {
            opacity: '0',
          },
        },
        from: (sinks, sources) => {

          const radius = (size / 2);
          const strokeWidth = radius - (radius * (1 - thickness));
          const insideRadius = radius - (strokeWidth / 2);
          const perimeter = Math.round((2 * Math.PI * insideRadius) * 100) / 100;
          const halfSize = size / 2;

          return sources.state.stream
            .map(progress => {

              return !isNumber(progress) || progress < 0 || progress >= 100
                ? undefined
                : {
                  children: svg(
                    {
                      attrs: {
                        width: size,
                        height: size,
                        viewport: `0 0 ${size} ${size}`,
                        version: '1.1',
                        xmlns: 'http://www.w3.org/2000/svg'
                      },
                    },
                    [
                      svg.circle({
                        attrs: {
                          r: insideRadius,
                          cx: halfSize,
                          cy: halfSize,
                          stroke: backgroundColor,
                          'stroke-dasharray': perimeter,
                          'stroke-dashoffset': 0,
                          'stroke-width': strokeWidth - 1,
                        }
                      }),
                      svg.circle({
                        style: {
                          'stroke-dashoffset': Math.round((((100 - progress) / 100) * perimeter) * 100) / 100,
                        },
                        attrs: {
                          r: insideRadius,
                          cx: halfSize,
                          cy: halfSize,
                          stroke: color,
                          'stroke-dasharray': perimeter,
                          'stroke-width': strokeWidth,
                        }
                      }),
                    ]
                  )
                };
            });
        }
      })
    )
  );

export const withProgressCircular = WithProgressCircular();

export const ProgressCircular = behaviorToFactory(WithProgressCircular);

export const progressCircular = ProgressCircular();
