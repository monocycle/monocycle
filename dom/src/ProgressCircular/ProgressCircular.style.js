


export const progressCircularStyle = [
  {
    '& circle': {
      transition: 'stroke-dashoffset .3s linear',
      fill: 'transparent'
    }
  }
];

