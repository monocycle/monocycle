import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from '../StyledView/StyledView.js';
import { WithView, coerceViewOptions } from '../View/View.js';
import { pipe } from '@monocycle/component/lib/utilities/pipe.js';
import { RootStyle } from './Root.style.js';
import isEmpty from 'ramda/src/isEmpty.js';





export const WithRoot = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    ...isEmpty(viewOptions) ? [] : [WithView({
      sel: 'div',
      children: [],
      ...viewOptions,
    })],
    WithStyledView({
      name: 'Root',
      styles: RootStyle,
    }),
  ),
);

export const Root = behaviorToFactory(WithRoot);

export const withRoot = WithRoot();

WithRoot.coerce = Root.coerce = coerceViewOptions;

export default Root;