import { setupPage } from '../utilities/page.js';
import reduce from 'ramda/src/reduce.js';
import always from 'ramda/src/always.js';
import sortBy from 'ramda/src/sortBy.js';
import over from 'ramda/src/over.js';
import when from 'ramda/src/when.js';
import allPass from 'ramda/src/allPass.js';
import isEmpty from 'ramda/src/isEmpty.js';
import converge from 'ramda/src/converge.js';
import gte from 'ramda/src/gte.js';
import map from 'ramda/src/map.js';
import lensProp from 'ramda/src/lensProp.js';
import either from 'ramda/src/either.js';
import unless from 'ramda/src/unless.js';
import type from 'ramda/src/type.js';
import { pipe } from '@monocycle/component/lib/utilities/pipe.js';
import { prop } from '@monocycle/component/lib/utilities/prop.js';
import { propOver } from '@monocycle/component/lib/utilities/propOver.js';
import { rem } from 'monostyle/src/units.js';
import { fillParent } from 'monostyle/src/box.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { ensureArray } from '@monocycle/component/lib/utilities/ensureArray.js';
import { isPositiveInteger } from '@monocycle/component/lib/utilities/isPositiveInteger.js';
import noop from 'ramda-adjunct/src/noop.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';






export const prefixCssVariables = pipe(
  Object.entries,
  reduce((variables, [name, value]) => ({
    ...variables,
    [`--${name}`]: value,
  }), {}),
);

export const RootStyle = pipe(
  ensurePlainObj,
  over(lensProp('theme'), pipe(
    ensurePlainObj,
    over(lensProp('properties'), ensurePlainObj),
    over(lensProp('variables'), ensurePlainObj),
    over(lensProp('breakpoints'), pipe(
      ensureArray,
      map(pipe(
        over(lensProp('prefix'), unless(isNonEmptyString, pipe(type, type => {
          throw new Error(`RootStyle: 'prefix' must be a non-empty string (provided: ${type})`);
        }))),
        over(lensProp('from'), unless(isPositiveInteger, always(0))),
        over(lensProp('to'), unless(isPositiveInteger, noop)),
        when(allPass([
          prop('from'),
          prop('to'),
          converge(gte, [
            prop('from'),
            prop('to'),
          ]),
        ]), ({ prefix }) => {
          throw new Error(`RootStyle: Invalid '${prefix}' breakpoint: 'to' must be greater than 'from'`);
        }),
        unless(
          either(
            prop('from'),
            prop('to'),
          ),
          ({ prefix }) => {
            throw new Error(`RootStyle: Invalid '${prefix}' breakpoint: 'from' or 'to' is required`);
          },
        ),
      )),
      sortBy(prop('from')),
    )),
  )),
  ({
    cssRule,
    media,
    theme: {
      breakpoints,
      variables,
      properties,
    },
  }) => {

    breakpoints = reduce((before, { from, to, prefix }) => {

      if (!variables[prefix] && !properties[prefix])
        return before;

      return [
        ...before,
        media(
          {
            minWidth: from,
            maxWidth: to && to - 0.02,
          },
          {
            ...pipe(
              propOver(properties),
              ensurePlainObj,
            )(prefix),

            ...pipe(
              propOver(variables),
              ensurePlainObj,
              unless(isEmpty, prefixCssVariables),
            )(prefix),

          },
        ),
      ];
    }, [])(breakpoints);

    // normalize
    // cssRaw('\nbutton,hr,input{overflow:visible}audio,canvas,progress,video{display:inline-block}progress,sub,sup{vertical-align:baseline}html{font-family:sans-serif;line-height:1.15;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0} menu,article,aside,details,footer,header,nav,section{display:block}h1{font-size:2em;margin:.67em 0}figcaption,figure,main{display:block}figure{margin:1em 40px}hr{box-sizing:content-box;height:0}code,kbd,pre,samp{font-family:monospace,monospace;font-size:1em}a{background-color:transparent;-webkit-text-decoration-skip:objects}a:active,a:hover{outline-width:0}abbr[title]{border-bottom:none;text-decoration:underline;text-decoration:underline dotted}b,strong{font-weight:bolder}dfn{font-style:italic}mark{background-color:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative}sub{bottom:-.25em}sup{top:-.5em}audio:not([controls]){display:none;height:0}img{border-style:none}svg:not(:root){overflow:hidden}button,input,optgroup,select,textarea{font-family:sans-serif;font-size:100%;line-height:1.15;margin:0}button,input{}button,select{text-transform:none}[type=submit], [type=reset],button,html [type=button]{-webkit-appearance:button}[type=button]::-moz-focus-inner,[type=reset]::-moz-focus-inner,[type=submit]::-moz-focus-inner,button::-moz-focus-inner{border-style:none;padding:0}[type=button]:-moz-focusring,[type=reset]:-moz-focusring,[type=submit]:-moz-focusring,button:-moz-focusring{outline:ButtonText dotted 1px}fieldset{border:1px solid silver;margin:0 2px;padding:.35em .625em .75em}legend{box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}progress{}textarea{overflow:auto}[type=checkbox],[type=radio]{box-sizing:border-box;padding:0}[type=number]::-webkit-inner-spin-button,[type=number]::-webkit-outer-spin-button{height:auto}[type=search]{-webkit-appearance:textfield;outline-offset:-2px}[type=search]::-webkit-search-cancel-button,[type=search]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}summary{display:list-item}[hidden],template{display:none}\n'.trim());

    cssRule('*', {
      margin: 0,
    });

    setupPage(cssRule)('body > :first-child');

    cssRule('html', {
      fontSize: '62.5%',
    });

    cssRule('*', {
      '-moz-box-sizing': 'border-box',
      '-webkit-box-sizing': 'border-box',
      boxSizing: 'border-box',
      // margin: 0,
    });

    cssRule('*,*:before,*:after', {
      boxSizing: 'inherit',
    });

    cssRule('h1,h2,h3,h4,h5,h6', {
      color: 'var(--firstColor)',
      lineHeight: 1,
    });

    cssRule('img', {
      maxWidth: '100%',
      height: 'auto',
    });

    cssRule('a', {
      color: 'var(--linkColor, var(--firstColor))',
      textDecoration: 'none',
      '&:hover': {
        textDecoration: 'underline',
      },
    });

    return [
      fillParent,
      ...breakpoints,
      {
        ...properties,
        ...prefixCssVariables(variables),
        fontSize: `var(--textSize, ${rem(2)})`,
        color: 'var(--textColor, var(--darkerColor, black))',
      },
    ];
  },
);

export default RootStyle;