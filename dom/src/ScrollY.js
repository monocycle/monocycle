import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from './StyledView/index.js';
import { coerceViewOptions, WithView } from './View/View.js';
import pipe from 'ramda/src/pipe.js';
import { scrollY } from 'monostyle/src/scroll.js';





export const WithScrollY = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithStyledView({
      name: 'ScrollY',
      styles: scrollY,
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
    }),
  ),
);

export const withScrollY = WithScrollY();

export const ScrollY = behaviorToFactory(WithScrollY);

export const noScrollY = ScrollY();