import { coerceViewOptions } from '../View/View.js';
import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import unless from 'ramda/src/unless.js';
import lensProp from 'ramda/src/lensProp.js';
import always from 'ramda/src/always.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyledView } from '../StyledView/StyledView.js';
import { WithCollapsibleView } from '../CollapsibleView/index.js';
import { Col } from '../Box/Col.js';
import { WithFill } from '../Fill/index.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import isString from 'ramda-adjunct/src/isString.js';
import { withNoFlex } from '../NoFlex';
import { SidenavStyle } from './Sidenav.style.js';
import { rem } from 'monostyle/src/units.js';





export const WithSidenav = pipe(
  coerceViewOptions,
  over(lensProp('slots'), pipe(
    ensurePlainObj,
    over(lensProp('Body'), unless(isFunction, always(Col))),
  )),
  over(lensProp('width'), unless(isString, always(rem(30)))),
  ({ has, slots, width, ...viewOptions }) => pipe(
    WithStyledView({
      name: 'Sidenav',
      styles: SidenavStyle,
    }),
    withNoFlex,
    WithCollapsibleView({
      property: 'width',
      duration: '.3s',
      size: width,
      ...viewOptions,
      has: slots.Body(has).map(WithFill())
    }),
  )
);

export const Sidenav = behaviorToFactory(WithSidenav);

WithSidenav.coerce = Sidenav.coerce = coerceViewOptions;
