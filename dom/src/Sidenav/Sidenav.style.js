

export const SidenavStyle = {
  position: 'relative',
  transition: 'width 0.3s',
  overflow: 'hidden',
};