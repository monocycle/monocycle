import { Stream as $ } from 'xstream';
import pipe from 'ramda/src/pipe.js';
import unless from 'ramda/src/unless.js';
import always from 'ramda/src/always.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import { Component } from '@monocycle/component';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { ensureArray } from '@monocycle/component/lib/utilities/ensureArray.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';

const WithSinkLogger = pipe(
  ensurePlainObj,
  over(lensProp('sinkName'), unless(isNonEmptyString, () => {
    throw new Error('\'sinkName\' must be a string');
  })),
  over(lensProp('log'), unless(isFunction,
    always(console.log.bind(console)) // eslint-disable-line
  )),
  over(lensProp('args'), ensureArray),
  ({ log, sinkName, args }) => component => {

    return Component(sources => {

      const sinks = component(sources);

      return {
        ...sinks,
        [sinkName]: (sinks[sinkName] || $.empty()).map(view => {

          return log(...args, view) || view;
        })
      };
    });
  }
);


export {
  WithSinkLogger
};