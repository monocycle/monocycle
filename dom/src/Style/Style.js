import { Stream as $ } from 'xstream';
import pipe from 'ramda/src/pipe.js';
import { WithListener } from '@monocycle/listener';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import unless from 'ramda/src/unless.js';
import over from 'ramda/src/over.js';
import always from 'ramda/src/always.js';
import lensProp from 'ramda/src/lensProp.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { parseName, parseStyles } from '../drivers/style.js';





const WithStyle = pipe(
  ensurePlainObj,
  over(lensProp('name'), parseName),
  over(lensProp('styles'), parseStyles),
  over(lensProp('combine'), unless(isFunction, pipe(
    always($.merge)
  ))),
  ({ name, styles, combine }) => pipe(
    WithListener({
      from: pipe(
        always({
          name,
          styles,
        }),
        $.of
      ),
      combine,
      to: 'Style'
    }),
  )
);

const Style = behaviorToFactory(WithStyle);

export {
  WithStyle,
  Style,
};