import pipe from 'ramda/src/pipe.js';
import concat from 'ramda/src/concat.js';
import objOf from 'ramda/src/objOf.js';
import defaultTo from 'ramda/src/defaultTo.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithStyle } from '../Style/Style.js';
import { WithView } from '../View/View.js';





export const WithStyledView = pipe(
  WithView.coerce,
  ({
    name,
    styles,
    combine,
    ...viewOptions
  }) => pipe(
    WithStyle({
      name,
      styles,
      combine,
    }),
    WithView({
      sel: '.',
      children: [],
      from: (sinks, sources) => {
        return sources.Style.class(name)
          .map(pipe(
            defaultTo(''),
            concat('.'),
            objOf('sel'),
          ));
      },
    }),
    WithView({
      sel: '.',
      children: [],
      ...viewOptions,
    }),
  ),
);

export const StyledView = behaviorToFactory(WithStyledView);

WithStyledView.coerce = StyledView.coerce = WithView.coerce;