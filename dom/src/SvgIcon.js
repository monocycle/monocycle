import { Stream as $ } from 'xstream';
import pipe from 'ramda/src/pipe.js';
import propSatisfies from 'ramda/src/propSatisfies.js';
import assoc from 'ramda/src/assoc.js';
import prop from 'ramda/src/prop.js';
import lensProp from 'ramda/src/lensProp.js';
import map from 'ramda/src/map.js';
import over from 'ramda/src/over.js';
import unless from 'ramda/src/unless.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import { ifElse } from '@monocycle/component/lib/utilities/ifElse.js';
import converge from 'ramda/src/converge.js';
import assocPath from 'ramda/src/assocPath.js';
import always from 'ramda/src/always.js';
import isEmpty from 'ramda/src/isEmpty.js';
import dissoc from 'ramda/src/dissoc.js';
import isString from 'ramda-adjunct/src/isString.js';
import isUndefined from 'ramda-adjunct/src/isUndefined.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { EmptyObject } from '@monocycle/component/lib/utilities/empty.js';
import { coerceViewOptions } from './View/View.js';
import { IconStyle } from './Icon.js';
import { WithStyledView } from './StyledView/StyledView.js';
import { fillParent } from 'monostyle/src/box.js';
import identity from 'ramda/src/internal/_identity.js';
import { parseFrom } from '@monocycle/listener';
import helpers from './helpers.js';
const { WithI } = helpers;





export const SvgIconStyle = options => [
  ...IconStyle(options),
  {
    svg: {
      ...fillParent,
      'path, circle': {
        fill: 'var(--iconColor)'
      }
    }
  }
];

export const coerceSvgIconOptions = pipe(
  coerceViewOptions,
  ifElse(propSatisfies(isString, 'svg'),
    converge(assocPath(['props', 'innerHTML']), [
      prop('svg'),
      identity
    ]),
    converge(assoc('has'), [
      prop('svg'),
      identity
    ]),
  ),
  dissoc('svg'),
  assocPath(['hook', 'prepatch'], (oldVnode, vnode) => {
    if (oldVnode && oldVnode.children && !isEmpty(oldVnode.children)) {
      Object.assign(vnode, oldVnode);
      return vnode;
    }
  }),
);

export const WithSvgIcon = pipe(
  coerceSvgIconOptions,
  over(lensProp('from'), pipe(
    unless(isFunction, always(pipe(
      EmptyObject,
      $.of,
    ))),
    parseFrom,
  )),
  ({ from, ...viewOptions }) => pipe(
    WithStyledView({
      name: 'SvgIcon',
      styles: SvgIconStyle
    }),
    WithI({
      ...viewOptions,
      from: pipe(
        from,
        map(unless(isUndefined, pipe(
          coerceSvgIconOptions,
          mergeDeepRight(viewOptions)
        )))
      )
    }),
  )
);

export const SvgIcon = behaviorToFactory(WithSvgIcon);

SvgIcon.coerce = WithSvgIcon.coerce = coerceSvgIconOptions;
