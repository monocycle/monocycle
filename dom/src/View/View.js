import { Stream as $ } from 'xstream';
import pipe from 'ramda/src/pipe.js';
import omit from 'ramda/src/omit.js';
import lensProp from 'ramda/src/lensProp.js';
import propSatisfies from 'ramda/src/propSatisfies.js';
import { ifElse } from '@monocycle/component/lib/utilities/ifElse.js';
import when from 'ramda/src/when.js';
import over from 'ramda/src/over.js';
import assoc from 'ramda/src/assoc.js';
import hasProp from 'ramda/src/has.js';
import map from 'ramda/src/map.js';
import apply from 'ramda/src/apply.js';
import prop from 'ramda/src/prop.js';
import filter from 'ramda/src/filter.js';
import identity from 'ramda/src/internal/_identity.js';
import objOf from 'ramda/src/objOf.js';
import merge from 'ramda/src/merge.js';
import both from 'ramda/src/both.js';
import any from 'ramda/src/any.js';
import either from 'ramda/src/either.js';
import unless from 'ramda/src/unless.js';
import lensIndex from 'ramda/src/lensIndex.js';
import always from 'ramda/src/always.js';
import dissoc from 'ramda/src/dissoc.js';
import pick from 'ramda/src/pick.js';
import isEmpty from 'ramda/src/isEmpty.js';
import reject from 'ramda/src/reject.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import isUndefined from 'ramda-adjunct/src/isUndefined.js';
import isNotUndefined from 'ramda-adjunct/src/isNotUndefined.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import isString from 'ramda-adjunct/src/isString.js';
import castArray from 'ramda-adjunct/src/ensureArray.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import isEmptyArray from 'ramda-adjunct/src/isEmptyArray.js';
import noop from 'ramda-adjunct/src/noop.js';
import { ensureArray } from '@monocycle/component/lib/utilities/ensureArray.js';
import { ensureString } from '@monocycle/component/lib/utilities/ensureString.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { Composite, WithComposite } from '@monocycle/component';
import { WithListener } from '@monocycle/listener';
import { WithAfter } from '@monocycle/after';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { EmptyArray, EmptyObject } from '@monocycle/component/lib/utilities/empty.js';
import { mergeViews } from './mergeViews.js';





const vnodeKeys = ['sel', 'children', 'text', 'key', 'elm'];

export const ensureParentHasSelector = when(
  both(
    propSatisfies(isUndefined, 'sel'),
    either(
      propSatisfies(isNotUndefined, 'has'),
      propSatisfies(isNotUndefined, 'children'),
    ),
  ),
  assoc('sel', 'div'),
);

export const parseSelector = unless(isNonEmptyString, noop);
export const parseFrom = unless(isFunction, noop);
export const parseChildren = over(lensProp('children'),
  unless(isUndefined, pipe(
    ensureArray,
    map(pipe(
      when(isString, pipe(
        objOf('text'),
      )),
      over(lensProp('data'), ensurePlainObj),
    )),
    filter(isPlainObj),
  ))
);

export const coerceViewOptions = pipe(
  identity,
  when(isString, objOf('text')),
  unless(isPlainObj, pipe(
    ensureArray,
    reject(isUndefined),
    unless(isEmpty, ifElse(
      any(isFunction),
      objOf('has'),
      objOf('children'),
    )),
  )),
  ensurePlainObj,
  over(lensProp('has'), pipe(
    ensureArray,
    reject(isUndefined),
    map(unless(isFunction, pipe(
      ensureString,
      s => View(s)
    ))),
    when(isEmpty, noop),
  )),
  when(propSatisfies(isUndefined, 'has'), dissoc('has')),
);

export const mergeViewStreams = pipe(
  $.combine,
  map(mergeViews),
);

export const WithView = pipe(
  identity,
  when(isEmptyArray, pipe(
    always('div'),
    objOf('sel'),
    over(lensProp('children'), EmptyArray),
    over(lensProp('data'), EmptyObject),
  )),
  coerceViewOptions,
  ensureParentHasSelector,
  over(lensProp('children'), unless(isUndefined, pipe(
    ensureArray,
    reject(isUndefined),
    map(pipe(
      when(isString, objOf('text')),
      over(lensProp('data'), ensurePlainObj),
    )),
    filter(isPlainObj),
  ))),
  unless(prop('children'), dissoc('children')),
  over(lensProp('from'), parseFrom),
  ({ from, has, ...viewOptions }) => pipe(
    WithListener({
      from: () => $.of(
        isEmpty(viewOptions)
          ? undefined
          : {
            ...pick(vnodeKeys)(viewOptions),
            data: omit(vnodeKeys)(viewOptions),
          }
      ),
      combine: mergeViewStreams,
      to: 'DOM',
    }),
    !has ? identity : WithComposite(
      [
        Composite(has, {
          Ready: apply($.combine),
          DOM: apply($.combine),
        })
      ],
      {
        Ready: apply($.combine),
        DOM: pipe(
          apply($.combine),
          map(pipe(
            over(lensIndex(1), pipe(
              castArray,
              objOf('children'),
              reject(isUndefined),
              merge({ sel: '.' }),
            )),
            mergeViews,
          )),
        ),
      },
    ),
    !from ? identity : WithListener({
      from: pipe(
        from,
        map(unless(isUndefined, pipe(
          coerceViewOptions,
          omit(['has']),
          ifElse(hasProp('sel'),
            pipe(
              over(lensProp('sel'), parseSelector),
              ensureParentHasSelector,
            ),
            assoc('sel', '.'),
          ),
          ifElse(hasProp('children'),
            parseChildren,
            assoc('children', []),
          ),
        )))
      ),
      combine: mergeViewStreams,
      to: 'DOM',
    }),
    WithAfter(over(lensProp('DOM'), map(pipe(
      ensurePlainObj,
      when(prop('text'), assoc('children', undefined)),
      ifElse(isEmpty, noop, over(lensProp('data'), ensurePlainObj)),
    ))))
  ),
);

export const withView = WithView();

export const View = behaviorToFactory(WithView);

export const view = View();

export const EmptyView = always(view);

WithView.coerce = View.coerce = coerceViewOptions;