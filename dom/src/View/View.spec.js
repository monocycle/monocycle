import { Stream as $ } from 'xstream';
import test from 'ava';
import { View } from './View.js';
// import { ViewMacro } from '../utilities/testView.js';
// const viewMacro = ViewMacro(WithView);
// const reactiveViewMacro = ReactiveViewMacro(WithView);

// const viewMacro = ViewMacro(WithView);


// test('temporary test', t => {

//   t.true(true);
// });

test('reactive: string', t => {

  t.plan(1);

  const view = View({
    from: () => {

      return $.of('ga');
    }
  });

  const sinks = view({});

  return sinks.DOM.map(actual => {

    const expected = { text: 'ga', sel: '.', data: {}, children: undefined };

    t.deepEqual(actual, expected);
  });
});



// test(`empty view (when no options)`, viewMacro, t => (t.plan(1), {
//   input: undefined,
//   expected: {
//     sel: 'div',
//     children: [],
//     data: {},
//     elm: undefined,
//     key: undefined,
//     text: undefined,
//   },
// }));