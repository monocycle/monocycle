import __ from 'ramda/src/__.js';
import tail from 'ramda/src/tail.js';
import reduce from 'ramda/src/reduce.js';
import pipe from 'ramda/src/pipe.js';
import apply from 'ramda/src/apply.js';
import unapply from 'ramda/src/unapply.js';
import head from 'ramda/src/head.js';
import juxt from 'ramda/src/juxt.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import gt from 'ramda/src/gt.js';
import { ifElse } from '@monocycle/component/lib/utilities/ifElse.js';
import isEmpty from 'ramda/src/isEmpty.js';
import path from 'ramda/src/path.js';
import concat from 'ramda/src/concat.js';
import prop from 'ramda/src/prop.js';
import propSatisfies from 'ramda/src/propSatisfies.js';
import identity from 'ramda/src/internal/_identity.js';
import mergeWith from 'ramda/src/mergeWith.js';
import isUndefined from 'ramda-adjunct/src/isUndefined.js';
import isString from 'ramda-adjunct/src/isString.js';
import noop from 'ramda-adjunct/src/noop.js';
import mergeSelectors from 'snabbdom-merge/merge-selectors.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { ensureArray } from '@monocycle/component/lib/utilities/ensureArray.js';





export const extractHooks = pipe(
  path(['data', 'hook']),
  ensurePlainObj,
);

export const mergeViews = views => {

  return pipe(
    ensureArray,
    ifElse(propSatisfies(gt(1), 'length'),
      noop,
      ifElse(propSatisfies(gt(2), 'length'),
        prop(0),
        pipe(
          juxt([head, tail]),
          apply(reduce((view1, view2) => {

            if (isUndefined(view1))
              return view2;

            if (isUndefined(view2))
              return undefined;

            const children = 'children' in view2 & view2.children === undefined
              ? []
              : concat(view1.children || [], view2.children || []);

            const hasChildren = !isEmpty(children);

            return pipe(
              mergeDeepRight,
              mergeDeepRight(__, {
                sel: isUndefined(view2.sel)
                  ? view2.sel
                  : mergeSelectors(view1.sel, view2.sel),
                children: hasChildren ? children : undefined,
                text: hasChildren ? undefined : isString(view2.text)
                  ? view2.text
                  : (isString(view1.text)
                    ? view1.text
                    : undefined),
                data: {
                  hook: mergeWith(
                    pipe(unapply(identity), juxt),
                    extractHooks(view1),
                    extractHooks(view2),
                  ),
                },
              }),
            )(view1, view2);
          })),
        ),
      ),
    ),
  )(views);
};