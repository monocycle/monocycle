
import { EventDelegator } from '@cycle/dom/lib/cjs/EventDelegator';
import { VNodeWrapper } from './VNodeWrapper.js';
import { IsolateModule } from '@cycle/dom/lib/cjs/IsolateModule';
import { MainDOMSource } from '@cycle/dom/lib/cjs/MainDOMSource';
import { getValidNode, checkValidContainer } from '@cycle/dom/lib/cjs/utils';
import { init } from './_utilities/snabbdom';
import { htmlDomApi } from 'snabbdom/htmldomapi.js';
import { Stream as $ } from 'xstream';
import concat from 'xstream/extra/concat.js';
import sampleCombine from 'xstream/extra/sampleCombine.js';
import { toVNode } from 'snabbdom/tovnode';
import {
  StyleModule,
  ClassModule,
  PropsModule,
  AttrsModule,
  DatasetModule,
} from '@cycle/dom/lib/cjs/modules';





const defaultModules = {
  StyleModule,
  ClassModule,
  PropsModule,
  AttrsModule,
  DatasetModule,
};

const makeDOMDriverInputGuard = modules => {
  if (!Array.isArray(modules))
    throw new Error(`Optional modules option must be an array for snabbdom modules`);
};

const dropCompletion = input => {
  return $.merge(input, $.never());
};

const unwrapElementFromVNode = vnode => {
  return vnode.elm;
};

const reportSnabbdomError = err => {
  (console.error || console.log)(err);
};

const addRootScope = vnode => {
  vnode.data = vnode.data || {};
  vnode.data.isolate = [];
  return vnode;
};

const makeDOMReady$ = () => {
  return $.create({
    start(listener) {
      if (document.readyState === 'loading') {
        document.addEventListener('readystatechange', () => {
          const state = document.readyState;
          if (state === 'interactive' || state === 'complete') {
            listener.next(null);
            listener.complete();
          }
        });
      } else {
        listener.next(null);
        listener.complete();
      }
    },
    stop() { },
  });
};

// temporary extract makeDOMDriver
export const makeDOMDriver = (container, options) => {

  if (!options)
    options = {};

  checkValidContainer(container);

  const modules = options.modules || defaultModules;

  makeDOMDriverInputGuard(modules);

  const isolateModule = new IsolateModule();
  const patch = init(
    [isolateModule.createModule()].concat(modules),
    options.domApi
  );

  const domReady$ = makeDOMReady$();

  let wrapVnode;
  let mutationObserver;

  const mutationConfirmed$ = $.create({
    start(listener) {
      mutationObserver = new MutationObserver(() => listener.next(null));
    },
    stop() {
      mutationObserver.disconnect();
    },
  });

  const DOMDriver = (vnode$, name = 'DOM') => {

    if (
      !vnode$ ||
      typeof vnode$.addListener !== `function` ||
      typeof vnode$.fold !== `function`
    ) {
      throw new Error(
        `The DOM driver function expects as input a Stream of ` +
        `virtual DOM elements`
      );
    }

    const sanitation$ = $.create();

    const firstRoot$ = domReady$.map(() => {

      const firstRoot = getValidNode(container) || document.body;

      wrapVnode = VNodeWrapper(firstRoot);

      return firstRoot;
    });

    // We need to subscribe to the sink (i.e. vnode$) synchronously inside this
    // driver, and not later in the map().flatten() because this sink is in
    // reality a SinkProxy from @cycle/run, and we don't want to miss the first
    // emission when the main() is connected to the drivers.
    // Read more in issue #739.
    const rememberedVNode$ = vnode$.remember();
    rememberedVNode$.addListener({});

    // The mutation observer internal to mutationConfirmed$ should
    // exist before elementAfterPatch$ calls mutationObserver.observe()
    mutationConfirmed$.addListener({});

    const elementAfterPatch$ = firstRoot$
      .map(firstRoot => {

        return $.merge(
          rememberedVNode$.endWhen(sanitation$),
          sanitation$
        )
          .map(wrapVnode)
          .startWith(addRootScope(toVNode(firstRoot)))
          .fold(patch, toVNode(firstRoot))
          .drop(1)
          .map(unwrapElementFromVNode)
          .startWith(firstRoot)
          .map(element => {
            mutationObserver.observe(element, {
              childList: true,
              attributes: true,
              characterData: true,
              subtree: true,
              attributeOldValue: true,
              characterDataOldValue: true,
            });
            return element;
          })
          .compose(dropCompletion); // don't complete this stream
      })
      .flatten();

    const rootElement$ = concat(domReady$, mutationConfirmed$)
      .endWhen(sanitation$)
      .compose(sampleCombine(elementAfterPatch$))
      .map(arr => arr[1])
      .remember();

    // Start the snabbdom patching, over time
    rootElement$.addListener({
      error: reportSnabbdomError
    });

    const delegator = new EventDelegator(rootElement$, isolateModule);

    return new MainDOMSource(
      rootElement$,
      sanitation$,
      [],
      isolateModule,
      delegator,
      name
    );
  };

  return DOMDriver;
};


export const DOMDriver = (container, options = {}) => makeDOMDriver(container, {
  ...options,
  domApi: {
    ...htmlDomApi,
    // createElement: pipe(
    //   tagName => {
    //     // console.warn('createElement', tagName)

    //     return tagName.startsWith('.')
    //       ? 'div' + tagName
    //       : tagName;
    //     // 
    //   },
    //   htmlDomApi.createElement
    // ),
  },
  modules: options.modules || [
    AttrsModule,
    ClassModule,
    PropsModule,
    DatasetModule,
    StyleModule,
  ]
});

