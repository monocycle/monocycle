import { vnode as vnodeFn } from 'snabbdom/vnode';
import { h } from 'snabbdom/h';
import { classNameFromVNode, selectorParser } from 'snabbdom-selector';
import { isDocFrag } from '@cycle/dom/lib/cjs/utils';





export const VNodeWrapper = rootElement => {

  const wrapDocFrag = children => vnodeFn(
    '',
    { isolate: [] },
    children,
    undefined,
    rootElement
  );

  const wrap = children => {

    const { tagName, id, className } = rootElement;
    const selId = id
      ? `#${id}`
      : '';

    const selClass = className
      ? `.${className.split(` `).join(`.`)}`
      : '';

    const vnode = h(
      `${tagName.toLowerCase()}${selId}${selClass}`,
      {},
      children
    );

    vnode.data = vnode.data || {};
    vnode.data.isolate = vnode.data.isolate || [];

    return vnode;
  };

  return vnode => {

    if (isDocFrag(rootElement))
      return wrapDocFrag(vnode === null ? [] : [vnode]);

    if (vnode === null/*  || vnode === undefined */)
      return wrap([]);

    const { tagName: selTagName, id: selId } = selectorParser(vnode);
    const vNodeClassName = classNameFromVNode(vnode);
    const vNodeData = vnode.data || {};
    const vNodeDataProps = vNodeData.props || {};
    const { id: vNodeId = selId } = vNodeDataProps;

    const isVNodeAndRootElementIdentical =
      typeof vNodeId === 'string' &&
      vNodeId.toUpperCase() === rootElement.id.toUpperCase() &&
      selTagName.toUpperCase() === rootElement.tagName.toUpperCase() &&
      vNodeClassName.toUpperCase() === rootElement.className.toUpperCase();

    if (isVNodeAndRootElementIdentical)
      return vnode;

    return wrap([vnode]);
  };
};

