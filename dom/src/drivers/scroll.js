import pipe from 'ramda/src/pipe.js';
import prop from 'ramda/src/prop.js';
import over from 'ramda/src/over.js';
import unless from 'ramda/src/unless.js';
import lensProp from 'ramda/src/lensProp.js';
import always from 'ramda/src/always.js';
import pick from 'ramda/src/pick.js';
import fromEvent from 'xstream/extra/fromEvent.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { ensureArray } from '@monocycle/component/lib/utilities/ensureArray.js';
import isObj from 'ramda-adjunct/src/isObj.js';
import { coerceScrollOptions } from 'components/ScrollTo';





// eslint-disable-next-line no-undef
const getWindow = always(window);

export const scrollDriver = sink => {

  sink
    .map(pipe(
      coerceScrollOptions,
      over(lensProp('parent'), unless(isObj, getWindow)),
      ({ parent, value }) => {

        parent.scrollTo(...ensureArray(value));
      },
    ))
    .addListener({});

  const select = pipe(
    ensurePlainObj,
    over(lensProp('parent'), unless(isObj, getWindow)),
    ({ parent }) => {
      return fromEvent(parent, 'scroll')
        .map(prop('currentTarget'))
        .startWith(parent)
        .map(pipe(
          pick(['scrollLeft', 'scrollTop', 'scrollX', 'scrollY']),
        ))
        .remember();
    },
  );

  return {
    select,
  };
};
