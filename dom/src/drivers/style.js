import { Stream as $ } from 'xstream';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import ensureArray from 'ramda-adjunct/src/ensureArray.js';
import isNonNegative from 'ramda-adjunct/src/isNonNegative.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import isArray from 'ramda-adjunct/src/isArray.js';
import find from 'ramda/src/find.js';
import applyTo from 'ramda/src/applyTo.js';
import map from 'ramda/src/map.js';
import { ifElse } from '@monocycle/component/lib/utilities/ifElse.js';
import equals from 'ramda/src/equals.js';
import pipe from 'ramda/src/pipe.js';
import reduce from 'ramda/src/reduce.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import propEq from 'ramda/src/propEq.js';
import findIndex from 'ramda/src/findIndex.js';
import prop from 'ramda/src/prop.js';
import filter from 'ramda/src/filter.js';
import unless from 'ramda/src/unless.js';
import always from 'ramda/src/always.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import dropRepeats from 'xstream/extra/dropRepeats.js';
import concat from 'xstream/extra/concat.js';
import { EmptyArray } from '@monocycle/component/lib/utilities/empty.js';
import noop from 'ramda-adjunct/src/noop.js';





const parseName = unless(isNonEmptyString, noop);

const parseStyles = unless(isFunction, pipe(
  ensureArray,
  map(ensurePlainObj),
  always,
));

const StyleDriver = pipe(
  ensurePlainObj,
  over(lensProp('theme'), ensurePlainObj),
  over(lensProp('classNames'), unless(isArray, EmptyArray)),
  ({ theme, style, classNames, ...options }) => {

    return sink => {

      const defineStyle$ = $.create();
      const stream$ = concat(
        $.fromObservable(sink),
        $.never(),
      );

      stream$.subscribe({
        next: i => defineStyle$._n(i),
        error: err => defineStyle$._e(err),
        complete: () => defineStyle$._c(),
      });

      const allStyles$ = defineStyle$
        .map(pipe(
          over(lensProp('styles'), parseStyles),
          over(lensProp('name'), parseName),
        ))
        .fold((classNames, { name, styles: generateStyles }) => {

          const index = findIndex(propEq('name', name))(classNames);

          const retrieveRealClassName = pipe(
            propEq('name'),
            find,
            applyTo(classNames),
            prop('class')
          );

          if (isNonNegative(index))
            return classNames;

          const styles = pipe(
            generateStyles,
            ensureArray,
            filter(isPlainObj)
          )({
            ...options,
            theme,
            style,
            classNames: reduce((classNames, { name, class: className }) => ({
              ...classNames,
              [name]: '.' + className
            }), {})(classNames),
            className: retrieveRealClassName,
          });

          // log('defineStyle', { name, styles, allStyles });

          return [
            ...classNames,
            {
              name,
              class: style(...styles, {
                $debugName: name
              }),
            }
          ];
        }, classNames)
        .drop(1)
        .remember();

      const select = ifElse(
        equals('*'),
        always(allStyles$),
        name => {
          return allStyles$
            .map(find(propEq('name', name)))
            .filter(Boolean)
            .compose(dropRepeats());
        }
      );

      return Object.assign(allStyles$, {
        select,
        class: pipe(
          select,
          map(prop('class')),
        )
      });
    };
  }
);


export {
  StyleDriver,
  parseName,
  parseStyles,
};