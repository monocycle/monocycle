import pipe from 'ramda/src/pipe.js';
import slice from 'ramda/src/slice.js';
import head from 'ramda/src/head.js';
import toUpper from 'ramda/src/toUpper.js';
import concat from 'ramda/src/concat.js';
import converge from 'ramda/src/converge.js';
import lensProp from 'ramda/src/lensProp.js';
import over from 'ramda/src/over.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithView } from './View/View.js';
import htmlTags from 'html-tags';
import { ensureString } from '@monocycle/component/lib/utilities/ensureString.js';





const capitalize = converge(concat, [
  pipe(head, toUpper),
  slice(1, Infinity)
]);

export default htmlTags.reduce((before, tagName) => {

  const name = capitalize(tagName);

  const ViewBehavior = pipe(
    WithView.coerce,
    over(lensProp('sel'), ensureString),
    ({ sel, ...viewOptions }) => pipe(
      WithView({
        ...viewOptions,
        sel: tagName + sel
      }),
    )
  );

  const factory = behaviorToFactory(ViewBehavior);

  return {
    ...before,
    [`With${name}`]: ViewBehavior,
    [`with${name}`]: ViewBehavior(),
    [name]: factory,
    [tagName]: factory(),
  };
}, {});