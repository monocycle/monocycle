import { Stream as $ } from 'xstream';
import pipe from 'ramda/src/pipe.js';
import always from 'ramda/src/always.js';
import objOf from 'ramda/src/objOf.js';
import when from 'ramda/src/when.js';
import isFalsy from 'ramda-adjunct/src/isFalsy.js';

const DefaultView = pipe(
  when(isFalsy, always('')),
  $.of,
  objOf('DOM'),
  always
);

export {
  DefaultView
};