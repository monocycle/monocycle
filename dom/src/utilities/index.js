import { DefaultView } from './DefaultView';
import { mergeViewOptions } from './mergeViewOptions';

export {
  DefaultView,
  mergeViewOptions,
};