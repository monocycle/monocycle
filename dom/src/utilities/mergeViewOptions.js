
import mergeSelectors from 'snabbdom-merge/merge-selectors';
import concat from 'ramda/src/concat.js';
import curry from 'ramda/src/curry.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import isArray from 'ramda-adjunct/src/isArray.js';

const mergeViewOptions = curry((defaultOptions, options) => {

  const merged = mergeDeepRight(defaultOptions, options);

  const has = !isArray(defaultOptions.has) || !isArray(options.has)
    ? merged.has
    : concat(
      defaultOptions.has,
      options.has,
    );

  return {
    ...merged,
    ...(!defaultOptions.sel && !options.sel
      ? {
        has
      }
      : {
        sel: mergeSelectors(
          defaultOptions.sel,
          options.sel
        ),
        has
      })
  };
});

export {
  mergeViewOptions
};