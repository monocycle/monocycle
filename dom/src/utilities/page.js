import { fillParent } from 'monostyle/src/box.js';





export const setupPage = cssRule => selector => {

  // Use full window size for application
  cssRule('html, body', {
    height: '100%',
    width: '100%',
    padding: 0,
    margin: 0
  });

  // Use border box
  cssRule('html', {
    '-moz-box-sizing': 'border-box',
    '-webkit-box-sizing': 'border-box',
    boxSizing: 'border-box'
  });

  cssRule('*,*:before,*:after', {
    boxSizing: 'inherit'
  });

  // Also root should fill parent
  cssRule(selector, fillParent);
};