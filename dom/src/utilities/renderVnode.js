import modules from 'snabbdom-to-html/modules';
import init from 'snabbdom-to-html/init';

const renderVnode = init([
  modules.class,
  modules.props,
  modules.attributes,
  modules.style
]);

export {
  renderVnode
};