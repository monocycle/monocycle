import { mockTimeSource } from '@cycle/time';
import pipe from 'ramda/src/pipe.js';
import reduce from 'ramda/src/reduce.js';
import head from 'ramda/src/head.js';
import tail from 'ramda/src/tail.js';
import { renderVnode } from './renderVnode';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import htmlLooksLike from 'html-looks-like';
import { Stream as $ } from 'xstream';
import { mockDOMSource, ul, li, strong } from '@cycle/dom';
import isString from 'ramda-adjunct/src/isString.js';
import mergeSelectors from 'snabbdom-merge/merge-selectors';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { WithView, View }  from '../View/View.js';


const mergeAllSelectors = (...selectors) => reduce(mergeSelectors, head(selectors))(tail(selectors));
const withTime = test => {

  return (...args) => {
    return new Promise(resolve => {

      const Time = mockTimeSource();

      test(...args, Time);

      Time.run(resolve);
    });
  };
};

const viewTest = pipe(
  ensurePlainObj,
  ({
    test,
    sel,
    behavior,
    data = {}
  }) => {


    const factory = behaviorToFactory(behavior);
    const viewMacro = ViewMacro(behavior);
    const reactiveViewMacro = ReactiveViewMacro(behavior);


    /** Definition */

    test(`empty view (when no options)`, viewMacro, t => (t.plan(1), {
      input: undefined,
      expected: {
        sel: behavior === WithView ? 'div' : sel,
        children: behavior === WithView ? [] : undefined,

        // sel: undefined,
        data,
        elm: undefined,
        key: undefined,
        // children: undefined,
        text: undefined,
      },
    }));

    test(`empty view (empty options)`, viewMacro, t => (t.plan(1), {
      input: {},
      expected: {
        sel: behavior === WithView ? undefined : sel,

        // sel: undefined,
        data,
        elm: undefined,
        key: undefined,
        children: undefined,
        text: undefined,
      },
    }));

    test(`view (from string)`, viewMacro, t => (t.plan(1), {
      input: 'ga',
      expected: {
        sel: behavior === WithView ? undefined : sel,
        data,
        elm: undefined,
        key: undefined,
        children: undefined,
        text: 'ga',
      },
    }));

    test(`view (from text option)`, viewMacro, t => (t.plan(1), {
      input: {
        text: 'ga'
      },
      expected: {
        sel: behavior === WithView ? undefined : sel,
        data,
        elm: undefined,
        key: undefined,
        children: undefined,
        text: 'ga',
      },
    }));

    test(`view (custom selector)`, viewMacro, t => (t.plan(1), {
      input: {
        sel: '.ga'
      },
      expected: {
        sel: sel === 'div' ? '.ga' : mergeSelectors(sel, '.ga'),
        data,
        elm: undefined,
        key: undefined,
        children: undefined,
        text: undefined,
      },
    }));

    test(`view (from array)`, viewMacro, t => (t.plan(1), {
      input: ['ga'],
      expected: {
        sel,
        data,
        elm: undefined,
        key: undefined,
        text: undefined,
        children: [
          { data: {}, text: 'ga' }
        ]
      },
    }));

    test(`view from 'children' option (string)`, viewMacro, t => (t.plan(1), {
      input: {
        children: 'ga'
      },
      expected: {
        sel,
        data,
        elm: undefined,
        key: undefined,
        text: undefined,
        children: [
          { data: {}, text: 'ga', }
        ],
      },
    }));

    test(`view from 'children' option (array of string)`, viewMacro, t => (t.plan(1), {
      input: {
        children: ['ga']
      },
      expected: {
        sel,
        data,
        elm: undefined,
        key: undefined,
        text: undefined,
        children: [
          { data: {}, text: 'ga', }
        ],
      },
    }));

    test(`view from 'children' option (view)`, viewMacro, t => (t.plan(1), {
      input: {
        children: strong('ga')
      },
      expected: {
        sel,
        data,
        elm: undefined,
        key: undefined,
        text: undefined,
        children: [
          {
            sel: 'strong',
            data: {},
            elm: undefined,
            key: undefined,
            children: undefined,
            text: 'ga',
          }
        ]
      },
    }));

    test(`view from 'children' option (array of views)`, viewMacro, t => (t.plan(1), {
      input: {
        children: [
          ul([
            li('hello'),
            li('world'),
            '!'
          ])
        ]
      },
      expected: {
        sel,
        data,
        elm: undefined,
        key: undefined,
        text: undefined,
        children: [
          {
            sel: 'ul',
            data: {},
            elm: undefined,
            key: undefined,
            text: undefined,
            children: [
              {
                sel: 'li',
                elm: undefined,
                key: undefined,
                data: {},
                text: 'hello',
                children: undefined,
              },
              {
                sel: 'li',
                elm: undefined,
                key: undefined,
                data: {},
                text: 'world',
                children: undefined,
              },
              {
                sel: undefined,
                elm: undefined,
                key: undefined,
                data: undefined,
                children: undefined,
                text: '!',
              }
            ]
          }
        ]
      },
    }));

    test(`view from view`, viewMacro, t => (t.plan(1), {
      input: View('ga'),
      expected: {
        sel,
        data,
        elm: undefined,
        key: undefined,
        text: undefined,
        children: [
          {
            sel: undefined,
            data: {},
            elm: undefined,
            key: undefined,
            text: 'ga',
            children: undefined,
          }
        ]
      },
    }));

    test(`view from 'has' option (view)`, viewMacro, t => (t.plan(1), {
      input: {
        has: View('ga')
      },
      expected: {
        sel,
        data,
        elm: undefined,
        key: undefined,
        text: undefined,
        children: [
          {
            sel: undefined,
            data: {},
            elm: undefined,
            key: undefined,
            text: 'ga',
            children: undefined,
          }
        ]
      },
    }));

    test(`view from array of view`, viewMacro, t => (t.plan(1), {
      input: [
        View('ga'),
        View('bu')
      ],
      expected: {
        sel,
        data,
        elm: undefined,
        key: undefined,
        text: undefined,
        children: [
          {
            sel: undefined,
            data: {},
            elm: undefined,
            key: undefined,
            text: 'ga',
            children: undefined,
          },
          {
            sel: undefined,
            data: {},
            elm: undefined,
            key: undefined,
            text: 'bu',
            children: undefined,
          },
        ]
      },
    }));

    test(`view from 'has' option (array of view)`, viewMacro, t => (t.plan(1), {
      input: {
        has: [
          View('ga'),
          View('bu')
        ]
      },
      expected: {
        sel,
        data,
        elm: undefined,
        key: undefined,
        text: undefined,
        children: [
          {
            sel: undefined,
            data: {},
            elm: undefined,
            key: undefined,
            text: 'ga',
            children: undefined,
          },
          {
            sel: undefined,
            data: {},
            elm: undefined,
            key: undefined,
            text: 'bu',
            children: undefined,
          },
        ]
      },
    }));

    test(`nested views`, viewMacro, t => (t.plan(1), {
      input: factory(factory('ga')),
      expected: {
        sel,
        data,
        elm: undefined,
        key: undefined,
        text: undefined,
        children: [
          {
            sel,
            data,
            elm: undefined,
            key: undefined,
            text: undefined,
            children: [
              {
                sel: behavior === WithView ? undefined : sel,
                data,
                elm: undefined,
                key: undefined,
                text: 'ga',
                children: undefined,
              }
            ]
          },
        ]
      },
    }));



    // /** Composition */


    test(`override upstream view (no options)`, viewMacro, t => (t.plan(1), {
      input: undefined,
      upstream: View({
        children: 'zo'
      }),
      expected: {
        sel: mergeSelectors('div', sel),
        data,
        elm: undefined,
        key: undefined,
        text: undefined,
        children: [
          { data: {}, text: 'zo' }
        ],
      },
    }));

    test(`override upstream view (empty options)`, viewMacro, t => (t.plan(1), {
      input: {},
      upstream: View({
        children: 'zo'
      }),
      expected: {
        sel: behavior === WithView ? undefined : mergeSelectors('div', sel),
        data,
        elm: undefined,
        key: undefined,
        text: behavior === WithView ? '' : undefined,
        children: behavior === WithView ? undefined : [
          { data: {}, text: 'zo' }
        ],
      },
    }));

    test(`override upstream view (text option)`, viewMacro, t => (t.plan(1), {
      input: 'ga',
      upstream: View({
        sel: 'address',
        text: 'zo'
      }),
      expected: {
        sel: behavior === WithView ? undefined : mergeSelectors('address', sel),
        data,
        elm: undefined,
        key: undefined,
        text: 'ga',
        children: undefined
      },
    }));

    test(`override upstream (remove children)`, viewMacro, t => (t.plan(1), {
      input: {
        sel: '.ga',
        children: undefined
      },
      upstream: View({
        sel: 'span',
        children: 'zo'
      }),
      expected: {
        sel: behavior === WithView ? 'span.ga' : mergeAllSelectors('span', sel, '.ga'),
        data,
        elm: undefined,
        key: undefined,
        text: undefined,
        children: undefined
      },
    }));

    test(`override upstream (keep children)`, viewMacro, t => (t.plan(1), {
      input: {
        sel: '.ga',
        children: []
      },
      upstream: View({
        sel: 'span',
        children: 'zo'
      }),
      expected: {
        sel: behavior === WithView ? 'span.ga' : mergeAllSelectors('span', sel, '.ga'),

        // sel: mergeAllSelectors('span', sel, '.ga'),
        data,
        elm: undefined,
        key: undefined,
        text: undefined,
        children: [{ data: {}, text: 'zo' }]
      },
    }));



    // /** Reactive view */

    test(`reactive: preserve initial options`, viewMacro, t => (t.plan(1), {
      input: {
        sel: '#ga.bu',
        style: {
          color: 'red'
        },
        has: [View('zo'), View('ga')],
        from: () => $.of({
          sel: '.',
        })
      },
      expected: {
        sel: behavior === WithView ? '#ga.bu' : mergeSelectors(sel, '#ga.bu'),
        data: mergeDeepRight(data, {
          style: {
            color: 'red'
          }
        }),
        elm: undefined,
        key: undefined,
        text: undefined,
        children: [
          {
            sel: undefined,
            data: {},
            elm: undefined,
            key: undefined,
            text: 'zo',
            children: undefined,
          },
          {
            sel: undefined,
            data: {},
            elm: undefined,
            key: undefined,
            text: 'ga',
            children: undefined,
          },
        ]
      },
    }));

    test(`reactive: update initial options`, viewMacro, t => (t.plan(1), {
      input: {
        sel: '.bu',
        style: {
          color: 'white'
        },
        children: 'zo',
        from: () => $.of({
          sel: '.ga',
          children: 'ga',
          style: {
            background: 'black',
          },
        })
      },
      expected: {
        sel: behavior === WithView ? '.bu.ga' : mergeSelectors(sel, '.bu.ga'),
        data: mergeDeepRight(data, {
          style: {
            color: 'white',
            background: 'black',
          }
        }),
        elm: undefined,
        key: undefined,
        text: undefined,
        children: [
          { data: {}, text: 'zo' },
          { data: {}, text: 'ga' },
        ]
      }
    }));

    test(`reactive: destroy vnode`, reactiveViewMacro, (t, Time) => (t.plan(3), {
      input: {
        sel: '.ga',
        children: 'hello',
        from: () => {
          return Time.diagram(
            'a---b-----c|',
            {
              a: true,
              b: false,
              c: true,
            }
          )
            .map(isVnodeConnected => isVnodeConnected ? {} : ({
              sel: undefined,
              children: undefined,
            }));
        }
      },
      expected$: Time.diagram(
        'a---b-----c|',
        {
          a: {
            sel: behavior === WithView ? '.ga' : mergeSelectors(sel, '.ga'),
            data,
            elm: undefined,
            key: undefined,
            text: undefined,
            children: [
              { data: {}, text: 'hello' },
            ]
          },
          b: ``,
          c: {
            sel: behavior === WithView ? '.ga' : mergeSelectors(sel, '.ga'),
            data,
            elm: undefined,
            key: undefined,
            text: undefined,
            children: [
              { data: {}, text: 'hello' },
            ]
          },
        }
      )
    }));

    test(`reactive: destroy composite view`, reactiveViewMacro, (t, Time) => (t.plan(3), {
      input: {
        sel: '.ga',
        has: View('hello'),
        from: () => {
          return Time.diagram(
            'a---b-----c|',
            {
              a: true,
              b: false,
              c: true,
            }
          )
            .map(isVnodeConnected => isVnodeConnected ? {

            } : ({
              sel: undefined,
              children: undefined,
            }));
        }
      },
      expected$: Time.diagram(
        'a---b-----c|',
        {
          a: {
            sel: behavior === WithView ? '.ga' : mergeSelectors(sel, '.ga'),
            data,
            elm: undefined,
            key: undefined,
            text: undefined,
            children: [
              {
                sel: undefined,
                elm: undefined,
                key: undefined,
                data: {},
                text: 'hello',
                children: undefined,
              },
            ]
          },
          b: ``,
          c: {
            sel: behavior === WithView ? '.ga' : mergeSelectors(sel, '.ga'),
            data,
            elm: undefined,
            key: undefined,
            text: undefined,
            children: [
              {
                sel: undefined,
                elm: undefined,
                key: undefined,
                data: {},
                text: 'hello',
                children: undefined,
              },
            ]
          }
        }
      )
    }));

  }
);

const testView = (t, actual, expected) => {

  isString(expected)
    ? t.notThrows(htmlLooksLike.bind(undefined,
      renderVnode(actual),
      expected
    ))
    : t.deepEqual(actual, expected);
};

const ViewMacro = behavior => (t, Spec) => {

  const {
    input = [],
    expected,
    upstream,
    sources = {
      DOM: mockDOMSource({}),
      Style: { class: $.of }
    }
  } = Spec(t);

  const view = behavior(input)(upstream);

  const sinks = view(sources);

  return sinks.DOM.map(actual => {

    testView(t, actual, expected);
  });
};



const ReactiveViewMacro = behavior => withTime((t, Spec, Time) => {

  const {
    input = [],
    expected$,
    upstream,
    sources = {
      DOM: mockDOMSource({}),
      Style: { class: $.of }
    }
  } = Spec(t, Time);

  const view = behavior(input)(upstream);

  const sinks = view(sources);

  Time.assertEqual(
    sinks.DOM,
    expected$,
    testView.bind(undefined, t)
  );
});


export {
  testView,
  viewTest,
  ViewMacro,
  ReactiveViewMacro,
  withTime,
};
