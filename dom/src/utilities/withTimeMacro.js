import { withTime } from 'cyclejs-test-helpers';

const withTimeMacro = test => (t, ...others) => withTime(Time => test(t, Time, ...others))();

export {
  withTimeMacro
};