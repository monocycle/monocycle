# 4.2.0 (2022-01-24)


### Bug Fixes

* **dynamic:** depends on component@10.2 ([31d5527](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/31d552714f858ac496c1e2c636cd3b3575a1d044))



# 4.1.0 (2020-11-09)


### Bug Fixes

* **dynamic:** depends on component@10.1 ([9918230](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9918230d385687a7566577b60a3fb69a89d703b6))



# 4.0.0 (2020-11-05)


### Bug Fixes

* **dynamic:** make package esm compatible ([d1e9e9a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d1e9e9a028e792b6ec2e68eac3f78de4007908e4))


### BREAKING CHANGES

* **dynamic:** maybe



# 3.1.0 (2020-10-07)


### Bug Fixes

* **dynamic:** update dependencies ([78ad13e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/78ad13e6159ab5717475649d65e381fdecb290ca))



# 3.0.0 (2020-09-05)


### Features

* **dynamic:** rename to Dynamic + support Ready sink ([fcdeabd](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fcdeabd94a53dd526c1307b1927471748bc18058))


### BREAKING CHANGES

* **dynamic:** Dynamic3 become Dynamic and old Dynamic is removed



# 2.14.0 (2020-05-19)


### Bug Fixes

* **dynamic:** remove type:module ([8cad3fa](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8cad3fa7823f435c5bfdd73eda7e04eb85841161))



# 2.13.0 (2020-05-02)


### Bug Fixes

* **dynamic:** repair Dynamic3 ([58936d4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/58936d46c2ce6d82486a5bb589de0154dbfdad9a))



# 2.12.0 (2020-03-26)


### Bug Fixes

* **dynamic:** don't momoize sinks ([3355227](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/335522727967a8c73bf9f3a8af0c85e3c33de22d))



# 2.11.0 (2020-02-21)


### Bug Fixes

* **dynamic:** repair Dynamic3 ([acb3e92](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/acb3e92d9842e6c5fd721c367ee40cf4ddadb175))



# 2.10.0 (2020-02-20)


### Features

* **dynamic:** add Dynamic3 ([f80fd48](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f80fd48237f03108a8519a3600da5defc1ef5fa1))



# 2.9.0 (2020-01-20)


### Bug Fixes

* **dynamic:** depends on component@5.5 ([bdbaafe](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/bdbaafef3549be0c1d9e81b9e2f580cf5c47fab6))



# 2.8.0 (2020-01-20)


### Bug Fixes

* **dynamic:** depends on listener@5.5 ([3404d58](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3404d5891c509212b1a2bccb132a1005790f9c9a))



# 2.7.0 (2020-01-20)


### Bug Fixes

* **dynamic:** depends on component@5.4 ([32279f6](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/32279f636f68f2568b3b7e5b1c268613bb2129e7))



# 2.6.0 (2019-12-02)


### Bug Fixes

* **dynamic:** update deps ([4389306](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4389306a0a43abf50f7ca1252a2957b1115038d8))



# 2.5.0 (2019-12-02)


### Bug Fixes

* **dynamic:** add lock file ([f73bf06](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f73bf06043c696033422f825a489b721994f8d37))



# 2.4.0 (2019-11-11)


### Features

* **dynamic:** update dependencies ([f420d37](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f420d370991d24f4a71c1e61a0e2f40cb6cc6937))



# 2.3.0 (2019-10-28)


### Bug Fixes

* **dynamic:** dynamic2's mergeExceptions is not a function ([3a43366](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3a433665510b6dd13b0a626c5169754e4b0b0690))



# 2.2.0 (2019-10-27)


### Features

* **dynamic:** add repl ([27d57ed](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/27d57ed1005cae4a2ef99eb65eeb998b4b52eda6))



# 2.1.0 (2019-10-18)


### Bug Fixes

* **dynamic:** add missing dependency ([ccd5b97](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ccd5b9712c77aca73baae298b445b3797119f742))



## 2.0.0 (2019-10-16)

* feat(dynamic): add sinks as 1st param of Dynamic2 listener ([f52ec9c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f52ec9c))


### BREAKING CHANGE

* yes


## 1.3.0 (2019-10-07)

* feat(dynamic): try simpler a Dynamic ([7bed224](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/7bed224))



## 1.2.0 (2019-09-26)

* fix(dynamic): sinks defaults to  upstream sinks ([5b788c4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5b788c4))



## 1.1.0 (2019-06-08)

* feat(dynamic): default to identity and refactor code ([422e845](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/422e845))



## 1.0.0 (2019-05-11)

* fix(dynamic): depend on component@4.1 after@3.10 listener@3.8 ([9a97948](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/9a97948))
* feat(dynamic): from expect a behavior stream ([64cef58](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/64cef58))
* chore(dynamic): remove unused dependency ([8545cc3](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/8545cc3))


### BREAKING CHANGE

* from expect a behavior stream


## 0.3.0 (2019-04-08)

* fix(dynamic): depend on component@4.0 ([2597e80](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/2597e80))



## 0.2.0 (2019-04-02)

* fix(dynamic): depend on after@3.7 ([156b7a3](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/156b7a3))
* feat(dynamic): create module and add Dynamic ([71abe38](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/71abe38))



