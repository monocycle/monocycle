import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import __ from 'ramda/src/__.js';
import map from 'ramda/src/map.js';
import lensProp from 'ramda/src/lensProp.js';
import { parseFrom } from '@monocycle/listener';
import unless from 'ramda/src/unless.js';
import always from 'ramda/src/always.js';
import when from 'ramda/src/when.js';
import uniq from 'ramda/src/uniq.js';
import not from 'ramda/src/not.js';
import concat from 'ramda/src/concat.js';
import keys from 'ramda/src/keys.js';
import applyTo from 'ramda/src/applyTo.js';
import { WithAfter } from '@monocycle/after';
import { Stream as $ } from 'xstream';
import prop from 'ramda/src/prop.js';
import apply from 'ramda/src/apply.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { Component } from '@monocycle/component';
import { mergeSinks, extractSinks } from 'cyclejs-utils';
import dropRepeatsModule from 'xstream/extra/dropRepeats.js';




const dropRepeats = dropRepeatsModule.default || dropRepeatsModule;

export const WithDynamic = pipe(
  ensurePlainObj,
  over(lensProp('from'), pipe(
    when(not, always(pipe(always(undefined), $.of))),
    parseFrom,
  )),
  over(lensProp('has'), pipe(
    ensurePlainObj,
    map(Component)
  )),
  over(lensProp('SinksKeys'), pipe(
    unless(isFunction, always(keys)),
    SinksKeys => pipe(
      SinksKeys,
      concat(['Ready']),
      uniq
    )
  )),
  over(lensProp('mergeExceptions'), ensurePlainObj),
  ({ from, has, SinksKeys, mergeExceptions }) => {

    return WithAfter((sinks, sources) => {

      return mergeSinks(
        [
          sinks,
          extractSinks(
            from(sinks, sources)
              .compose(dropRepeats())
              .map(prop(__, has))
              .compose(dropRepeats())
              .filter(isFunction)
              .map(applyTo(sources))
              .remember(),
            uniq(SinksKeys(sources))
          )
        ],
        {
          Ready: apply($.combine),
          ...mergeExceptions
        }
      );
    });
  }
);

export const Dynamic = behaviorToFactory(WithDynamic);

export const WithDynamic2 = pipe(
  ensurePlainObj,
  over(lensProp('from'), pipe(
    when(not, always(pipe(always(undefined), $.of))),
    parseFrom,
  )),
  over(lensProp('SinksKeys'), unless(isFunction, always(keys))),
  over(lensProp('mergeExceptions'), ensurePlainObj),
  ({ from, SinksKeys, mergeExceptions }) => {

    return WithAfter((sinks, sources) => {

      return mergeSinks([
        sinks,
        extractSinks(
          from(sinks, sources),
          SinksKeys(sources)
        )
      ], mergeExceptions);
    });
  }
);

export const Dynamic2 = behaviorToFactory(WithDynamic2);