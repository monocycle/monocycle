import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { View } from '@monocycle/dom/src/View';
import { WithSpacedBox } from '@monocycle/dom/src/Box/Spaced';
import { Flex } from '@monocycle/dom/src/Flex';
import { WithStyledView } from '@monocycle/dom/src/StyledView/StyledView.js';
import { WithTransition } from '@monocycle/state';
import { WithListener } from '@monocycle/listener';
import { WithIsolated } from '@monocycle/isolated';
import { Component, isComponent } from '@monocycle/component';
import { WithDynamic2 } from '@monocycle/dynamic';
import pipe from 'ramda/src/pipe.js';
import path from 'ramda/src/path.js';
import always from 'ramda/src/always.js';
import mergeAll from 'ramda/src/mergeAll.js';
import unless from 'ramda/src/unless.js';
import applyTo from 'ramda/src/applyTo.js';
import keys from 'ramda/src/keys.js';
import values from 'ramda/src/values.js';
import prop from 'ramda/src/prop.js';
import tryCatch from 'ramda/src/tryCatch.js';
import assoc from 'ramda/src/assoc.js';
import map from 'ramda/src/map.js';
import { Stream as $ } from 'xstream';
import { AppStyle } from './App.style.js';
import dropRepeats from 'xstream/extra/dropRepeats.js';





const remember = stream => stream.remember();

export const WithApp = () => {


  return pipe(
    WithStyledView({
      name: 'AesWebsite',
      styles: AppStyle
    }),
    WithStyledView({
      styles: ({ media, theme: { breakpoint } }) => [
        media({ minWidth: 0, maxWidth: breakpoint }, {
          // '& > *': {
          //   minHeight: '50%'
          // }
        }),
        media({ minWidth: breakpoint }, {
          // '& > *': {
          //   minWidth: '50%'
          // }
        }),
        {
          '&, & textarea': {
            backgroundColor: '#3c3c3c',
            color: '#fefefe'
          },
        }
      ]
    }),
    WithSpacedBox([
      Flex({
        sel: 'textarea'
      }).map(pipe(
        WithListener({
          from: (sinks, sources) => {
            return sources.DOM.events('input')
              .map(path(['target', 'value']))
              .compose(sources.Time.debounce(500))
              .compose(dropRepeats())
              .debug('evaluate$');
          },
          to: 'evaluate$',
        }),
        WithIsolated({
          DOM: 'textarea',
          '*': null,
        }),
      )),
      Flex([
        pipe(
          WithListener({
            from: pipe(
              always([
                // import('@monocycle/component/component.js'),
                // import('@monocycle/dom/src/View/View.js'),
                // import('@monocycle/dom/src/Button/Button.js'),
              ]),
              Promise.all.bind(Promise),
              $.fromPromise,
              map(mergeAll),
              remember
            ),
            to: 'context$',
          }),
          WithDynamic2({
            from: ({/*  input$,  */context$ }, sources) => {

              return $.combine(
                sources.state.stream
                  .map(prop('value'))
                  .debug('value'),
                context$
              )
                .map(pipe(
                  tryCatch(
                    pipe(
                      ([value, context]) => {

                        const buildComponent = new Function(
                          '$',
                          ...keys(context),
                          `return ${value}`,
                        );

                        const component = buildComponent(
                          $,
                          ...values(context)
                        );

                        return component;
                      },
                    ),
                    always(Component.empty),
                  ),
                  unless(isComponent, always(View(''))),
                  applyTo(sources),
                ))
                .replaceError(error => {
                  console.error(error.stack);
                  return $.empty();
                });
            },
          }),
        )(undefined)
      ]),
    ]),
    // WithListener({
    //   from: ({ input$ }, sources) => {
    //     return sources.state.stream
    //       .map(pipe(
    //         prop('factories'),
    //         map(factory => {
    //           // debugger
    //           return import(factory)
    //         }),
    //         $.fromPromise
    //       ))
    //       .flatten()
    //       .remember()
    //   },
    //   to: 'factories$'
    // }),
    // WithSourceLogger(['state', 'stream']),
    WithTransition({
      label: 'initRepl',
      // from: 'initRepl$',
      reducer: always(pipe(
        ensurePlainObj,
        assoc('value', '')
      ))
    }),
    WithTransition({
      label: 'evaluate',
      from: 'evaluate$',
      reducer: input => pipe(
        ensurePlainObj,
        assoc('value', input)
      )
    }),
  );
};

export const App = behaviorToFactory(WithApp);


/* const textarea = document.getElementById('input')
console.log('textarea', textarea)

textarea.addEventListener('input', event => {
  console.log('input', event.target.value)

  try {
    // eslint-disable-next-line
    const component = new Function(
      'a',
      'b',
      `
      return ${event.target.value}
    `,
    )

    const result = component(2, 6)
    console.debug('result:', result)
  } catch (error) {
    //console.error('error', error.stack)
  }
}) */