import pipe from 'ramda/src/pipe.js';
import { WithStyledView } from '@monocycle/dom/src/StyledView/StyledView.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory';
import { WithField } from '../Field';
import { Component } from '@monocycle/component';
import { BooleanFieldStyle } from './BooleanField.style.js';
import { BooleanFieldInput } from './BooleanFieldInput';





export const WithBooleanField = options => pipe(
  WithField({
    slots: {
      message: Component.empty,
      Input: BooleanFieldInput,
    },
    ...options,
  }),
  WithStyledView({ // BooleanField
    name: 'BooleanField',
    styles: BooleanFieldStyle,
  }),
);

export const withBooleanField = WithBooleanField();

export const BooleanField = behaviorToFactory(WithBooleanField);

export const booleanField = BooleanField();