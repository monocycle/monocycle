import { em } from 'csx/lib/units';





export const BooleanFieldStyle = ({ classNames: { BooleanFieldInput } }) => [
  {
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    [BooleanFieldInput]: {
      fontSize: em(.8),
    },
  },
];
