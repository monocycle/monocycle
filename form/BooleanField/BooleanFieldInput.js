import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import path from 'ramda/src/path.js';
import applySpec from 'ramda/src/applySpec.js';
import map from 'ramda/src/map.js';
import prop from 'ramda/src/prop.js';
import lensPath from 'ramda/src/lensPath.js';
import merge from 'ramda/src/merge.js';
import { View } from '@monocycle/dom/src/View/View.js';
import { WithStyledView } from '@monocycle/dom/src/StyledView/StyledView.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory';
import { WithFieldInput } from '../Field/FieldInput.js';
import helpers from '@monocycle/dom/src/helpers.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj';
import { em } from 'csx/lib/units';
import { toString } from '@monocycle/component/lib/utilities/toString';
import dropRepeats from 'xstream/extra/dropRepeats';
const { Input } = helpers;
import combineObj from 'xs-combine-obj';




export const BooleanFieldInputStyle = pipe(
  ensurePlainObj,
  over(lensPath(['theme', 'booleanFieldInput', 'colors']), pipe(
    ensurePlainObj,
    merge({
      background: 'var(--firstColor, #2196F3)',
      focus: 'gray',
      text: 'black',
    }),
    map(toString),
  )),
  ({ theme: { booleanFieldInput: { colors } } }) => [
    {
      position: 'relative',
      display: 'inline-block',
      width: em(3),
      height: em(1.7),
      'input': {
        opacity: 0,
        width: 0,
        height: 0,
        '&:checked': {
          backgroundColor: colors.background,
          '& + .slider': {
            backgroundColor: colors.background,
            '&:before': {
              transform: `translateX(100%)`,
            },
          },
        },
        '&:focus + .slider': {
          boxShadow: `0 0 1px ${colors.background}`,
        },
      },
      '.slider': {
        position: 'absolute',
        cursor: 'pointer',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: '#ccc',
        transition: '.4s',
        '&.round': {
          borderRadius: em(1.7),
          '&:before': {
            borderRadius: '50%',
          },
        },
        '&:before': {
          position: 'absolute',
          content: '""',
          height: em(1.3),
          width: em(1.3),
          left: em(.2),
          bottom: em(.2),
          backgroundColor: 'white',
          transition: '.4s',
        },
      },
    },
  ],
);

export const WithBooleanFieldInput = pipe(
  ensurePlainObj,
  options => pipe(
    WithFieldInput({
      sel: 'div',
      inputEvent: {
        transform: map(path(['target', 'checked'])),
      },
      has: [
        Input({
          sel: '.input',
          attrs: { type: 'checkbox' },
          from: (sinks, sources) => {
            return combineObj({
              checked: sources.state.stream
                .filter(prop('isField'))
                .map(pipe(prop('value'), Boolean))
                .compose(dropRepeats()),
              disabled: sources.state.stream
                .map(pipe(prop('disabled'), Boolean))
                .compose(dropRepeats()),
            })
              .map(applySpec({
                data: {
                  attrs: {
                    disabled: prop('disabled'),
                  },
                  props: {
                    checked: prop('checked')
                  },
                }
              }));
          },
        }),
        View({
          sel: 'div.slider.round',
        }),
      ],
      ...options,
    }),
    WithStyledView({
      name: 'BooleanFieldInput',
      styles: BooleanFieldInputStyle,
    }),
  )
);

export const withBooleanFieldInput = WithBooleanFieldInput();

export const BooleanFieldInput = behaviorToFactory(WithBooleanFieldInput);

export const booleanFieldInput = BooleanFieldInput();