# 16.2.0 (2022-01-24)


### Bug Fixes

* **form:** repuublish ([1b9098d](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/1b9098d1d59dd2440e63cb8eea4d709d1fb0d418))



# 16.1.0 (2020-11-09)


### Bug Fixes

* **form:** update deps + import with js ext ([9b73984](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9b7398423f43d8439bb0e909078f43614e3c5f26))



# 16.0.0 (2020-11-09)


### Bug Fixes

* **form:** make package esm compatible ([f333245](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f33324519ea2f2c3f0c09259e6cb919bb3191108))


### BREAKING CHANGES

* **form:** maybe



# 15.3.0 (2020-10-07)


### Bug Fixes

* **form:** improve styles ([325e373](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/325e37395b30d05c9319422d888a4bce0a6b5cd5))
* **form:** update dependencies ([efc090d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/efc090d0bd603428e81cda7e54b0aa4935693716))



# 15.2.0 (2020-09-26)


### Features

* **form:** depends on form@30.0 and http@7.0 ([a02fe52](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/a02fe52eb492f8c071f064ab5e26cd67bebbd883))



# 15.1.0 (2020-08-25)


### Bug Fixes

* **form:** repair FromModal ([1e5d2e8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/1e5d2e893f1243d9baf3ad85ea68afe0bceba57f))



# 15.0.0 (2020-08-20)


### Bug Fixes

* **form:** remove old FormModal ([b0d4a2d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b0d4a2d686d95392d9258cb8c0ca7ed627ccf5fa))


### BREAKING CHANGES

* **form:** remove old FormModal



# 14.4.0 (2020-08-20)


### Bug Fixes

* **form:** Form: define default category for request ([49ff10a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/49ff10a2deab1be7354d6775e8edd10c9dea0c86))
* **form:** FormView displays 'before' slot ([44d85fe](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/44d85fe7b619cc9edc0d148a1131f3f0000f0937))


### Features

* **form:** add FormModal ([fc614b3](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fc614b39ae428d33c66798dfd4e54db5ec039c12))



# 14.3.0 (2020-08-17)


### Bug Fixes

* **form:** repair validate ([ae5dc2d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ae5dc2d6e7d64923cc336c306e7e7521082543a4))



# 14.2.0 (2020-08-15)


### Bug Fixes

* **form:** repair index file ([4ec116c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4ec116cad5c8f1f8c7cffda279696d4804442aa4))



# 14.1.0 (2020-08-15)


### Features

* **form:** Form can  have nested fields ([917e4ea](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/917e4ea5f87be2a7efeda0f5558dc19a6303ce53))



# 14.0.0 (2020-08-14)


### Features

* **form:** rewrite FIeld FIeldGroup and Form ([5dcf9a8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5dcf9a8e68d3e64f17b9d7ace3cb685dfa207c33))


### BREAKING CHANGES

* **form:** rewrite



# 13.0.0 (2020-08-08)


### Features

* **form:** rewrite Form ([bff575e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/bff575ebeb829a0043edba0ac2256e3c7a30ec71))


### BREAKING CHANGES

* **form:** not compatible with previous version



# 12.0.0 (2020-08-03)


### Features

* **form:** rewrite Form using Fields ([f81ddbb](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f81ddbbb2cad0c0aa39069cc34040b99c106b04f))


### BREAKING CHANGES

* **form:** yes



# 11.15.0 (2020-06-03)


### Bug Fixes

* **form:** refactor fields ([807ec46](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/807ec466e83a53ebb42290c8087f6a6b0466a78f))



# 11.14.0 (2020-05-19)


### Bug Fixes

* **form:** update deps ([ba6a5c5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ba6a5c516c6d729b80b2ef33798f7e66b1f13d0a))



# 11.13.0 (2020-05-11)


### Bug Fixes

* **form:** update dependencies ([c7f0daa](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c7f0daad6f707da3f50ed9c1d430fb6b437d2816))



# 11.12.0 (2020-05-11)


### Features

* **form:** Field: let customize FieldInput events ([d8e3ee5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d8e3ee5f25ce4aec4eaa6e269aec8ee38db5890d))



# 11.11.0 (2020-05-10)


### Features

* **form:** Field: adjust for TagsField ([ecc401b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ecc401bcaa58cebf18aa311970df5d0d6a71ec71))



# 11.10.0 (2020-04-27)


### Features

* **form:** expose resetFormReducer ([5b24990](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5b2499066bf99dc153ecd7eff058f9b80b97c55e))



# 11.9.0 (2020-04-26)


### Bug Fixes

* **form:** dont use default transparent ([6b17bfa](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6b17bfa1b829846f1245528738eb237e3a46fbc6))



# 11.8.0 (2020-04-26)


### Features

* **form:** set default fieldBgColor to transparent ([db9afca](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/db9afca9a42998835775ce82cc5cadc8b6ad5599))



# 11.7.0 (2020-04-26)


### Bug Fixes

* **form:** depends on component@7.0 ([2128193](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/2128193e6681991cd98c2a50f314866e29e0e174))


### Features

* **form:** use css variables for formStyle ([2fa1975](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/2fa1975b9dbdc19ad89f975b79ea5fed444bd45a))



# 11.6.0 (2020-04-01)


### Bug Fixes

* **form:** use camelcase in css vars ([51f9191](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/51f9191a674a8c9760597fe1ac13c1fa96ea254f))



# 11.5.0 (2020-03-26)


### Bug Fixes

* **form:** BooleanField missed a div selector ([b466b31](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b466b31efba714c58c51051951511a10a504d7c2))
* **form:** update dependencies ([6196c07](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6196c07664d4ef75da82f4c4547b23945a036788))



# 11.4.0 (2020-03-26)


### Bug Fixes

* **form:** Field explicitly defines selectors ([8089e96](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8089e965f218f9f05bd346efe49fa57aef47bef3))


### Features

* **form:** add support for dot paths in Form ([99a670b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/99a670b9c99ba120b49a2bef298900009b1c7d18))
* **form:** fieldStyle uses css variables ([ae8749d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ae8749ddfb1caa97c0ccdda23ae733888cfc94a9))



# 11.3.0 (2020-03-06)


### Bug Fixes

* **form:** use EventListener for reset ([a60931a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/a60931ac0d80a4eb43d4f5b686796c63654a0ef8))



# 11.2.0 (2020-02-21)


### Bug Fixes

* **form:** depends on dom@27.3 ([a5b81eb](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/a5b81ebc1fa7d10040e6fb8aad1f20b46792ec88))
* **form:** improve Field and TextareaField style ([6e7846e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6e7846ee1375389f6a427262d96a8671b90f30aa))



# 11.1.0 (2020-02-20)


### Bug Fixes

* **form:** depends on dom@27.2 ([83094b0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/83094b0a1218773c3682fef29ec7f8bcff484db0))



# 11.0.0 (2020-02-18)


### Bug Fixes

* **form:** update dependencies ([f6b5a3a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f6b5a3add47a4f93f1aed272fadaf5265627fb09))


### Features

* **form:** adapt Field and Form to dom@27 ([95f0f17](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/95f0f17d40447c9319683659209824d129dd7695))
* **form:** update styles ([fa6c99c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fa6c99c7ada8872fe689c63059e3eb3340ef2ce3))


### BREAKING CHANGES

* **form:** yes



# 10.1.0 (2020-02-14)


### Bug Fixes

* **form:** add Flex to form body ([cb24bab](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/cb24bab66994453f2b4fb5e26b691c126540d0ca))



# 10.0.0 (2020-02-14)


### Features

* **form:** improve Forn and Fields ([190e2e8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/190e2e8026aa89b80bc9b0d8f75736dc31ecbf2e))


### BREAKING CHANGES

* **form:** yes



# 9.0.0 (2020-02-10)


### Bug Fixes

* **form:** big improvement ([dc5adbb](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/dc5adbb8b826f378044e3084b34ce8a904a8afde))


### BREAKING CHANGES

* **form:** yes



# 8.17.0 (2020-02-08)


### Bug Fixes

* **form:** depends on dom@24.8 ([d7f4e6b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d7f4e6bfb8239ac631ac7056cb548e426d3c50d1))



# 8.16.0 (2020-02-08)


### Bug Fixes

* **form:** depends on dom@24.7 ([1a93d9f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/1a93d9f13b3eefef4a75673d0ec0424151b8848b))



# 8.15.0 (2020-02-02)


### Bug Fixes

* **form:** repair DateTimeField ([8b741aa](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8b741aa97b013d8f9658e94ce0f3ebef030d8ba9))



# 8.14.0 (2020-01-28)


### Bug Fixes

* **form:** improve DateTimeField ([0df7be3](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0df7be3e841594075fd211b80aec4b831681b7b5))



# 8.13.0 (2020-01-20)


### Bug Fixes

* **form:** depends on isolated@4.5 ([815fbf0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/815fbf09c0c3a00a90b5e5be23965f7c414dd6d8))



# 8.12.0 (2020-01-20)


### Bug Fixes

* **form:** depends on component@5.5 ([00be476](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/00be476ef3df9676b2c93be57f53872ea8b883a7))



# 8.11.0 (2020-01-20)


### Bug Fixes

* **form:** depends on listener@5.5 ([748e095](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/748e095662b7e035bbc43dd937c7e8e51d30df6d))



# 8.10.0 (2020-01-20)


### Bug Fixes

* **form:** depends on component@5.4 ([5113cb9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5113cb9646b8b9360b7d10ca621dd55131cb7db4))



# 8.9.0 (2020-01-18)


### Bug Fixes

* **form:** repair Form and FormModal ([6b275e2](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6b275e20355c855a5fad17f08250ee8ff40bfe3f))



# 8.8.0 (2019-12-02)


### Bug Fixes

* **form:** update deps ([e9602fd](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/e9602fd50f08e8bfb150d951629303b03c1816d0))



# 8.7.0 (2019-12-02)


### Bug Fixes

* **form:** add lock file ([e2754bd](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/e2754bd257733e0d442698893034ad419596a3a8))



# 8.6.0 (2019-11-11)


### Bug Fixes

* **form:** update dependencies ([4a8c5b4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4a8c5b4c7203a41e715b8abe90eb84d1325131e5))



# 8.5.0 (2019-11-06)


### Bug Fixes

* **form:** increase time before first syncHeight call ([aba768e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/aba768e3ddaff74c446b193b3bfa51071bc3c141))



# 8.4.0 (2019-11-06)


### Bug Fixes

* **form:** increase time before first syncHeight call ([5c6edf5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5c6edf51570748094d2a561ad1d3459c4b78aea1))



# 8.3.0 (2019-11-05)


### Bug Fixes

* **form:** remove debugs and update dependencies ([0e36fa1](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0e36fa12eae53009d09860a927b3bb08a37e2157))



# 8.2.0 (2019-10-29)


### Bug Fixes

* **form:** depends on dom@22.6 ([e03d0ee](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/e03d0eeee4a60352f68c07d4cbca5fa87801aa76))



# 8.1.0 (2019-10-29)


### Bug Fixes

* **form:** depends on dom@22.1 ([615eeac](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/615eeacfa4e2a8b8916b538e96b7d458fd619090))



# 8.0.0 (2019-10-29)


### Bug Fixes

* **form:** rewrite FormModal ([d154ae7](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d154ae7eb5bf481c4ca506e759ec77da9e9ce546))


### BREAKING CHANGES

* **form:** yep



## 7.4.0 (2019-10-14)

* fix(form): depends on dom@20.0 ([47fd5f4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/47fd5f4))



## 7.3.0 (2019-10-08)

* fix(form): remove csstips dependency ([816212f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/816212f))



## 7.2.0 (2019-10-07)

* feat(form): update Field style ([f6c4c05](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f6c4c05))



## 7.1.0 (2019-10-07)

* fix(form): fix const assignement error ([51eb20f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/51eb20f))



## 7.0.0 (2019-10-07)

* feat(form): add BooleanField and DateTimeField ([5230e92](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5230e92))
* feat(form): rewrite Field ([4f2b2ed](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4f2b2ed))
* feat(form): rewrite Form ([0811226](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0811226))


### BREAKING CHANGE

* yes
* yes


## 6.0.0 (2019-09-30)

* feat(form): big improvement ([9fffc7c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9fffc7c))


### BREAKING CHANGE

* yes


## 5.10.0 (2019-09-24)

* fix(form): depends on http@4.2 ([ae61d9b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ae61d9b))



## 5.9.0 (2019-09-24)

* fix(form): drop field message error repeats ([0555b2d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0555b2d))
* fix(form): let the form body scroll ([d714c12](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d714c12))
* fix(form): update dependencies ([ffbada6](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ffbada6))
* feat(form): add FormModal ([483be00](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/483be00))



## 5.8.0 (2019-09-14)

* feat(form): extracts TextareaFieldStyle ([283e173](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/283e173))
* feat(form): improve Field theme customization ([fe5c554](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fe5c554))



## 5.7.0 (2019-09-11)

* fix(form): textareaField forwards "disabled" option to input slot ([4e795f9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4e795f9))



## 5.6.0 (2019-09-06)

* fix(form): error in Datefield ([c2cdb5f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c2cdb5f))



## 5.5.0 (2019-09-06)

* fix(form): depends on validable@5.3 ([f9d7448](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f9d7448))



## 5.4.0 (2019-09-06)

* fix(form): invalid import path ([38cfe39](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/38cfe39))



## 5.3.0 (2019-09-06)

* fix(form): update dependencies ([52ffc84](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/52ffc84))



## 5.2.0 (2019-09-06)

* feat(form): add DateField ([b344f87](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b344f87))
* fix(form): input didnt render on resetForm ([ac0c8ab](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ac0c8ab))
* fix(form): let customize requestOptions + fix submit$ not firing ([46996c8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/46996c8))
* fix(form): use correct theme colors ([d3272a0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d3272a0))



## 5.1.0 (2019-08-28)

* fix(form): filter submit event directly ([683e749](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/683e749))
* fix(form): use correct request category ([64b4138](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/64b4138))
* fix(form): use provided value as default value ([d45d807](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d45d807))
* feat(form): add disabled field state ([f60f556](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f60f556))
* feat(form): update Field styles ([80976f3](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/80976f3))



## 5.0.0 (2019-06-18)

* feat(form): use es6 modules and refactor Form ([7409a9e](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/7409a9e))
* chore(form): remove useless property ([fc3a1ce](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/fc3a1ce))


### BREAKING CHANGE

* yes


## 4.0.0 (2019-06-10)

* fix(form): depend on dom@12 http@3.3 ([40eb53e](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/40eb53e))
* feat(form): dirty the form on submit ([ecc582b](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/ecc582b))
* feat(form): use monostyle csstips and csx directly ([d65292a](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/d65292a))
* chore(form): remove unused files ([73938dd](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/73938dd))


### BREAKING CHANGE

* yep


## 3.1.0 (2019-06-09)

* fix(form): depend on dom@11.3 http@3.2 ([1a22ba7](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/1a22ba7))



## 3.0.0 (2019-05-20)

* feat(form): depend on dom@10.0 http@3.0 ([f1ffa46](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/f1ffa46))


### BREAKING CHANGE

* yes


## 2.11.0 (2019-04-08)

* fix(form): depend on component@4.0 dom@6.0 ([ccc2f96](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/ccc2f96))



## 2.10.0 (2019-04-02)

* fix(form): depend on dom@5.3 ([629b52b](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/629b52b))



## 2.9.0 (2019-04-01)

* fix(form): depend on dom@5.2 listener@3.5 component@3.10 state@3.5 ([374e549](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/374e549))
* feat(form): add Focusable ([86d7ce8](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/86d7ce8))



## 2.8.0 (2019-03-28)

* fix(form): depend on component@3.9 dom@5.0 ([bea7996](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/bea7996))



## 2.7.0 (2019-03-23)

* fix(form): depend on component@3.6 ([e51dbad](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/e51dbad))



## 2.6.0 (2019-03-23)

* fix(form): depend on dom@4.5 ([9192daf](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/9192daf))



## 2.5.0 (2019-03-22)

* fix(form): depend on component@3.3 dom@4.4 ([252a4bf](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/252a4bf))
* fix(form): depend on dom@4.3 ([42c4c81](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/42c4c81))



## 2.4.0 (2019-03-16)

* fix(form): depend on dom@3.13 ([d86d33f](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/d86d33f))



## 2.3.0 (2019-03-16)

* feat(form): add TextareaField and extract FieldStyle ([ace0d93](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/ace0d93))



## 2.2.0 (2019-03-10)

* fix(form): fix focus bug ([c88cc66](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/c88cc66))



## 2.1.0 (2019-03-10)

* fix(form): depend on validable@3.1 ([c0bcf12](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/c0bcf12))



## 2.0.0 (2019-03-10)

* fix(form): update dependencies ([e741597](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/e741597))


### BREAKING CHANGE

* depend on state@3

ISSUES CLOSED: $


## 1.3.0 (2019-03-10)

* fix(form): depend on dom@3.10 ([24cb6ac](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/24cb6ac))



