import pipe from 'ramda/src/pipe.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory';
import { WithField, FieldInput, renderValue } from '@monocycle/form/Field';
import isDate from 'ramda-adjunct/src/isDate.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj';
import { createDate } from '../utilities/createDate.js';
import unless from 'ramda/src/unless.js';
import ifElse from 'ramda/src/ifElse.js';
import isValidDate from 'ramda-adjunct/src/isValidDate.js';
import noop from 'ramda-adjunct/src/noop.js';
import { extend } from '@monocycle/component/lib/utilities/extend.js';





const ten = i => (i < 10 ? '0' : '') + i;

export const toDateString = date => {

  return date.getFullYear() +
    '-' + ten(date.getMonth() + 1) +
    '-' + ten(date.getDate());
};

export const WithDateField = pipe(
  ensurePlainObj,
  mergeDeepRight({
    slots: {
      Input: extend(FieldInput, {
        attrs: {
          type: 'date',
        },
        renderValue: pipe(
          unless(isDate, createDate),
          ifElse(isValidDate, toDateString, noop),
          renderValue,
        )
      })
    },
  }),
  WithField,
);

export const DateField = behaviorToFactory(WithDateField);