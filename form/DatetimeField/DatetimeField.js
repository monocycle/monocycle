import pipe from 'ramda/src/pipe.js';
import map from 'ramda/src/map.js';
import path from 'ramda/src/path.js';
import unless from 'ramda/src/unless.js';
import ifElse from 'ramda/src/ifElse.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithField, FieldInput, renderValue } from '@monocycle/form/Field';
import isValidDate from 'ramda-adjunct/src/isValidDate.js';
import noop from 'ramda-adjunct/src/noop.js';
import isDate from 'ramda-adjunct/src/isDate.js';
import { createDate } from '../utilities/createDate.js';
import { toDatetimeString } from '../utilities/toDatetimeString.js';
import { toIsoString } from '../utilities/toIsoString.js';
import { extend } from '@monocycle/component/lib/utilities/extend.js';





export const WithDatetimeField = pipe(
  WithField.coerce,
  mergeDeepRight({
    type: 'datetime-local',
    slots: {
      Input: extend(FieldInput, {
        attrs: {
          type: 'datetime-local',
        },
        inputEvent: {
          transform: map(pipe(
            path(['target', 'value']),
            createDate,
            toIsoString,
          )),
        },
        renderValue: pipe(
          unless(isDate, createDate),
          ifElse(isValidDate, toDatetimeString, noop),
          renderValue,
        )
      })
    },
  }),
  WithField,
);

export const DatetimeField = behaviorToFactory(WithDatetimeField);