import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import prop from 'ramda/src/prop.js';
import merge from 'ramda/src/merge.js';
import { color } from 'csx/lib/color';
import lensProp from 'ramda/src/lensProp.js';
import lensPath from 'ramda/src/lensPath.js';
import always from 'ramda/src/always.js';
import unless from 'ramda/src/unless.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import { horizontal, center } from 'monostyle/src/flex';
import { newLayer, layerParent, attachToBottom } from 'monostyle/src/layer';
import { rem, em } from 'csx/lib/units';
import { fillParent } from 'monostyle/src/box';
import { toString } from '@monocycle/component/lib/utilities/toString';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj';

export const FieldStyle = pipe(
  ensurePlainObj,
  prop('theme'),
  ensurePlainObj,
  over(lensProp('colors'), merge({
    // default: 'gray',
    primary: color('#117884'),
    invalid: color('#e93f3b'),
    valid: color('#1bc876'),
  })),
  over(lensPath(['field', 'backgroundColor']),
    unless(isNonEmptyString, always('#fcfcfc'))
  ),
  over(lensPath(['field', 'textColor']),
    unless(isNonEmptyString, always('#3e3e3e'))
  ),
  // over(lensPath(['text', 'color']), always('#3e3e3e')),
  (theme) => {

    return [
      layerParent,
      {
        $debugName: 'Field',
        // boxShadow: '0 0.5px 0 0 rgba(0, 0, 0, 0.156), 0 1.5px 0 0 rgba(0, 0, 0, 0.055)',
        // paddingTop: '2.5rem',
        // paddingLeft: '2rem',
        // paddingBottom: '.5rem',
        paddingTop: rem(2.5),
        paddingLeft: rem(2),
        paddingBottom: rem(.5),
        color: toString(theme.field.textColor),
        backgroundColor: toString(theme.field.backgroundColor),
        minHeight: rem(6.4),
        cursor: 'text',
        '&::after': {
          content: '""',
          ...attachToBottom,
          display: 'block',
          width: '0',
          left: '50%',
          height: '3px',
          transition: '.3s ease all',
        },
        '.message': {
          ...attachToBottom,
          left: 'initial',
          color: toString(theme.colors.primary),
          padding: em(.5),
          fontSize: em(.7),
          opacity: 0,
          transition: '.2s ease opacity',
          '.active:not(.clean) > &': {
            opacity: 1,
          }
        },
        '.input': {
          ...fillParent,
          backgroundColor: 'inherit',
          border: 'none',
          outline: 'none',
          // visibility: 'hidden',

        },
        '.label': {
          ...newLayer,
          ...horizontal,
          ...center,
          backgroundColor: toString(theme.field.backgroundColor),
          transition: '.2s ease all',
          paddingLeft: em(1),
          height: '100%'
        },
        '&.active': {
          '&:not(.clean)': {
            '.message': {
              opacity: 1,
            }
          },
          '.label': {
            fontSize: em(.7),
            paddingLeft: em(.5),
            height: em(2)
          },
          // '.input': {
          //   visibility: 'visible'
          // },
        },
        '&.focus::after, &.invalid::after, &.valid::after': {
          left: '0',
          width: '100%',
        },
        '&.focus': {
          '&::after': {
            backgroundColor: toString(theme.colors.primary),
          },
          '.label': {
            color: toString(theme.colors.primary),
          },
          '.message': {
            color: toString(theme.colors.primary),
          },
        },
        '&.valid': {
          '&::after': {
            backgroundColor: toString(theme.colors.success),
          },
          '.label': {
            color: toString(theme.colors.success),
          },
          '.message': {
            color: toString(theme.colors.success),
          },
        },
        '&.invalid': {
          '&::after': {
            backgroundColor: toString(theme.colors.error),
          },
          '.label': {
            color: toString(theme.colors.error),
          },
          '.message': {
            color: toString(theme.colors.error),
          },
        },
        '&.disabled': {
          backgroundColor: toString(theme.colors.disabled)
        },
      }
    ];
  }
);
