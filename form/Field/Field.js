import $ from 'xstream';
import combineObj from 'xs-combine-obj';
import dropRepeats from 'xstream/extra/dropRepeats';
import prop from 'ramda/src/prop.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import defaultTo from 'ramda/src/defaultTo.js';
import merge from 'ramda/src/merge.js';
import __ from 'ramda/src/__.js';
import assoc from 'ramda/src/assoc.js';
import always from 'ramda/src/always.js';
import objOf from 'ramda/src/objOf.js';
import when from 'ramda/src/when.js';
import not from 'ramda/src/not.js';
import equals from 'ramda/src/equals.js';
import propSatisfies from 'ramda/src/propSatisfies.js';
import eqBy from 'ramda/src/eqBy.js';
import converge from 'ramda/src/converge.js';
import map from 'ramda/src/map.js';
import False from 'ramda/src/F.js';
import unless from 'ramda/src/unless.js';
import { isComponent, Component } from '@monocycle/component';
import { pipe } from '@monocycle/component/lib/utilities/pipe.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import isNotUndefined from 'ramda-adjunct/src/isNotUndefined.js';
import isNotEmpty from 'ramda-adjunct/src/isNotEmpty.js';
import { WithStyledView } from '@monocycle/dom/src/StyledView';
import { WithRow } from '@monocycle/dom/src/Box/Row.js';
import { WithTransition } from '@monocycle/state/Transition';
import { fieldStyle } from './Field.style.js';
import { FieldLabel } from './FieldLabel.js';
import { FieldInput } from './FieldInput.js';
import { FieldMessage } from './FieldMessage.js';
import { WithListener, parseFrom } from '@monocycle/listener';
import { makeValidateReducer } from './reducers/validate.js';
import { WithView } from '@monocycle/dom/src/View';
import identity from 'ramda/src/internal/_identity.js';





export const whenNotField = (sinks, sources) => {
  return sources.state.stream
    .compose(dropRepeats(eqBy(pipe(
      ensurePlainObj,
      prop('isField'),
      Boolean
    ))))
    .filter(propSatisfies(not, 'isField'));
};

export const whenOldValue = (sinks, sources) => {
  return whenNotField(sinks, sources)
    .map(() => {
      return sources.state.stream
        .map(prop('oldValue'))
        // .startWith()
        .compose(dropRepeats())
        .map(objOf('value'));

    })
    .flatten();
};


export const WithFieldModel = pipe(
  ensurePlainObj,
  over(lensProp('validate'), unless(isFunction, defaultTo(False))),
  over(lensProp('initWhen'), pipe(
    when(not, always(whenOldValue)),
    parseFrom,
  )),
  when(prop('disabled'), converge(assoc('initWhen'), [
    ({ initWhen }) => pipe(
      initWhen,
      map(pipe(
        ensurePlainObj,
        assoc('disabled', true),
      ))
    ),
    identity,
  ])),
  when(prop('required'), converge(assoc('initWhen'), [
    ({ initWhen }) => pipe(
      initWhen,
      map(pipe(
        ensurePlainObj,
        assoc('required', true),
      ))
    ),
    identity,
  ])),
  when(prop('title'), converge(assoc('initWhen'), [
    ({ initWhen, title }) => pipe(
      initWhen,
      map(pipe(
        ensurePlainObj,
        assoc('title', title),
      ))
    ),
    identity,
  ])),
  ({ validate, initOnly, initWhen }) => {

    const ValidateReducer = makeValidateReducer(validate);

    return pipe(
      WithTransition({
        label: 'initField',
        from: [
          initWhen,
          ({ resetField$, ...sinks }, sources) => {
            return whenOldValue(sinks, sources)
              .map(value => {

                return resetField$.mapTo(value);
              })
              .flatten();
          }
        ],
        reducer: pipe(
          ensurePlainObj,
          ({ value, ...initialState }) => pipe(
            merge(__, initialState),
            ValidateReducer(value),
            merge(__, {
              isField: true,
              touched: false,
              focused: false,
            }),
          )
        ),
      }),
      unless(always(initOnly), pipe(
        WithTransition({
          label: 'validateField',
          from: 'input$',
          reducer: newValue => pipe(
            ValidateReducer(newValue),
            assoc('touched', true),
          ),
        }),
        WithTransition({
          label: 'setFocused',
          from: ({ focus$, blur$ }) => $.merge(
            focus$.mapTo(true),
            blur$.mapTo(false),
          ),
          reducer: focused => pipe(
            ensurePlainObj,
            assoc('focused', focused),
          ),
        }),
      )),
    );
  },
);

export const WithField = pipe(
  WithView.coerce,
  over(lensProp('Style'), unless(isFunction, always(fieldStyle))),
  over(lensProp('type'), unless(isNonEmptyString, always('text'))),
  over(lensProp('slots'), pipe(
    ensurePlainObj,
    over(lensProp('Input'), unless(isFunction, always(FieldInput))),
    over(lensProp('message'), unless(isComponent, FieldMessage)),
    over(lensProp('label'), unless(isComponent, FieldLabel)),
    map(Component),
  )),
  ({
    Style,
    type,
    slots: {
      Input,
      label,
      message
    },
    initWhen,
    validate,
    disabled,
    required,
    title,
    ...viewOptions
  }) => {

    const typeIsHidden = type === 'hidden';

    const withFieldView = pipe(
      WithStyledView({
        name: 'Field',
        styles: Style,
      }),
      WithRow({
        sel: 'label',
        has: [
          Input({
            attrs: {
              type,
            },
          }),
          label,
          message,
        ],
        from: (sinks, sources) => {
          return sources.state.stream
            .map(({ valid, oldValue, disabled, touched, focused, value }) => ({
              sel: '.',
              children: [],
              data: {
                class: {
                  disabled,
                  focus: focused,
                  active: isNotUndefined(value) && isNotEmpty(value) || focused || (!valid && touched),
                  clean: !touched,
                  valid: valid && !equals(oldValue, value),
                  invalid: touched && !valid,
                },
              },
            }));
        },
      }),
      WithView({
        sel: '',
        children: [],
        ...viewOptions
      }),
    );

    const withFieldInputs = pipe(
      WithListener({
        from: $.empty,
        combine: $.merge,
        to: 'resetField$',
      }),
      WithListener({
        from: (sinks, sources) => sources.state.stream
          .map(pipe(prop('valid'), Boolean))
          .compose(dropRepeats()),
        to: 'valid$',
      }),
      WithListener({
        from: (sinks, sources) => sources.state.stream
          .map(pipe(prop('touched'), Boolean))
          .compose(dropRepeats()),
        to: 'touched$',
      }),
      WithListener({
        from: (sinks, sources) => combineObj({
          path: sources.state.stream
            .map(prop('path'))
            // .filter(isNonEmptyArray)
            .compose(dropRepeats()),
          value: sources.state.stream
            .map(prop('value'))
            .compose(dropRepeats()),
        }),
        to: 'value$',
      }),
    );

    return pipe(
      typeIsHidden
        ? WithView()
        : withFieldView,
      withFieldInputs,
      WithFieldModel({
        initOnly: typeIsHidden,
        validate,
        initWhen,
        disabled,
        required,
        title,
      }),
    );
  },
);

WithField.coerce = ensurePlainObj;

export const Field = behaviorToFactory(WithField);