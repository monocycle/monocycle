import { horizontal, center } from 'monostyle/src/flex';
import { newLayer, layerParent, attachToBottom, attachToTop } from 'monostyle/src/layer';
import { rem, em } from 'csx/lib/units';
import { hsl } from 'csx/lib.es2015/color';





const fieldTextColor = 'var(--fieldTextColor)';
const fieldBgColor = 'var(--fieldBgColor)';
const fieldValidColor = 'var(--fieldValidColor, var(--successColor, green))';
const fieldInvalidColor = 'var(--fieldInvalidColor, var(--failureColor, red))';
const fieldDisabledColor = 'var(--fieldDisabledColor, var(--disabledColor, gray))';
const fieldFocusedColor = 'var(--fieldFocusedColor, var(--activeColor, gray))';


export const fieldContainerStyle = {
  color: fieldTextColor,
  backgroundColor: fieldBgColor,
  minHeight: rem(6.4),
  borderBottom: '1px solid ' + hsl(0, 0, .9),
  cursor: 'pointer',
};

export const fieldStyle = [
  layerParent,
  {
    padding: `var(--fieldLabelHeight, ${rem(2.7)}) calc(var(--padX, var(--pad, 1em)) / 2) 0`,
    ...fieldContainerStyle,
    '&::after': {
      content: '""',
      ...attachToBottom,
      display: 'block',
      width: '0',
      left: '50%',
      height: '3px',
      transition: '.3s ease all',
    },
    '.message': {
      ...attachToTop,
      left: 'initial',
      color: fieldTextColor,
      padding: em(.5),
      fontSize: em(.7),
      opacity: 0,
      transition: '.2s ease opacity',
      '.active:not(.clean) > &': {
        opacity: 1,
      },
    },
    '.input': {
      width: '100%',
      backgroundColor: 'inherit',
      border: 'none',
      outline: 'none',
      height: em(2),
      padding: '0',
    },
    '.label': {
      ...newLayer,
      ...horizontal,
      ...center,
      backgroundColor: fieldBgColor,
      transition: '.2s ease all',
      paddingLeft: 'calc(var(--padX, var(--pad, 1em)) / 2)',
      height: '100%',
    },
    '&.active': {
      '&:not(.clean)': {
        '.message': {
          opacity: 1,
        },
      },
      '.label': {
        fontSize: em(.9),
        height: rem(2.7),
        // fontWeight: 'bold',
      },
    },
    '&.focus::after, &.invalid::after, &.valid::after': {
      left: '0',
      width: '100%',
    },
    '&.focus': {
      '&::after': {
        backgroundColor: fieldFocusedColor,
      },
      '.label': {
        color: fieldFocusedColor,
      },
      '.message': {
        color: fieldFocusedColor,
      },
    },
    '&.valid': {
      '&::after': {
        backgroundColor: fieldValidColor,
      },
      '.label': {
        color: fieldValidColor,
      },
      '.message': {
        color: fieldValidColor,
      },
    },
    '&.invalid': {
      '&::after': {
        backgroundColor: fieldInvalidColor,
      },
      '.label': {
        color: fieldInvalidColor,
      },
      '.message': {
        color: fieldInvalidColor,
      },
    },
    '&.disabled': {
      color: fieldDisabledColor,
      '.input': {
        color: fieldDisabledColor,
      },
    },
  },
];
