import $ from 'xstream';
import dropRepeats from 'xstream/extra/dropRepeats';
import prop from 'ramda/src/prop.js';
import map from 'ramda/src/map.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import pickAll from 'ramda/src/pickAll.js';
import always from 'ramda/src/always.js';
import unless from 'ramda/src/unless.js';
import apply from 'ramda/src/apply.js';
import defaultTo from 'ramda/src/defaultTo.js';
import objOf from 'ramda/src/objOf.js';
import applyTo from 'ramda/src/applyTo.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import equals from 'ramda/src/equals.js';
import omit from 'ramda/src/omit.js';
import path from 'ramda/src/path.js';
import { pipe } from '@monocycle/component/lib/utilities/pipe.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { WithEventListener } from '@monocycle/dom/src/EventListener';
import { WithView, coerceViewOptions } from '@monocycle/dom/src/View';
import identity from 'ramda/src/internal/_identity.js';





export const renderValue = pipe(
  identity,
  objOf('value'),
  objOf('props'),
  objOf('data'),
);

export const renderFieldInputWhen = pipe(
  ensurePlainObj,
  over(lensProp('renderValue'), unless(isFunction, always(renderValue))),
  ({
    renderValue,
    ...viewOptions
  }) => (sinks, sources) => {
    return sources.state.stream
      // .filter(prop('isField'))
      .map(prop('value'))
      .compose(dropRepeats())
      .map(pipe(
        defaultTo(''),
        renderValue,
        mergeDeepRight(
          pipe(
            omit(['sel', 'children', 'text', 'key']),
            objOf('data'),
            applyTo,
          )(viewOptions),
        ),
      ));
  }
);

export const WithFieldInput = pipe(
  coerceViewOptions,
  over(lensProp('focusEvent'), ensurePlainObj),
  over(lensProp('blurEvent'), ensurePlainObj),
  over(lensProp('inputEvent'), ensurePlainObj),
  over(lensProp('renderValue'), unless(isFunction, always(renderValue))),
  ({
    focusEvent,
    blurEvent,
    inputEvent,
    renderValue,
    ...viewOptions
  }) => pipe(
    WithView({
      sel: 'input.input',
      from: (sinks, sources) => {

        return $.combine(
          sources.state.stream // value
            .filter(prop('isField'))
            .map(pipe(prop('value'), defaultTo('')))
            .compose(dropRepeats(equals))
            .map(pipe(
              renderValue,
            )),
          sources.state.stream // attrs
            .map(pipe(
              pickAll(['required', 'disabled', 'title']),
            ))
            .compose(dropRepeats(equals))
            .map(pipe(
              objOf('attrs'),
              objOf('data'),
            ))
        ).map(apply(mergeDeepRight));
      },
      ...viewOptions,
    }),
    WithEventListener({
      target: '.input',
      combine: $.merge,
      type: 'focus',
      to: 'focus$',
      ...focusEvent,
    }),
    WithEventListener({
      target: '.input',
      combine: $.merge,
      type: 'blur',
      to: 'blur$',
      ...blurEvent,
    }),
    WithEventListener({
      target: '.input',
      combine: $.merge,
      delay: 0,
      delayType: 'debounce',
      transform: map(path(['target', 'value'])),
      type: 'input',
      to: 'input$',
      ...inputEvent,
    }),
  ),
);

export const FieldInput = behaviorToFactory(WithFieldInput);

WithFieldInput.coerce = FieldInput.coerce = coerceViewOptions;