import { pipe } from '@monocycle/component/lib/utilities/pipe.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithView, coerceViewOptions } from '@monocycle/dom/src/View';
import prop from 'ramda/src/prop.js';
import over from 'ramda/src/over.js';
import unless from 'ramda/src/unless.js';
import isEmpty from 'ramda/src/isEmpty.js';
import objOf from 'ramda/src/objOf.js';
import append from 'ramda/src/append.js';
import lensIndex from 'ramda/src/lensIndex.js';
import toUpper from 'ramda/src/toUpper.js';
import join from 'ramda/src/join.js';
import { ensureString } from '@monocycle/component/lib/utilities/ensureString.js';





export const WithFieldLabel = pipe(
  coerceViewOptions,
  ({ ...viewOptions }) => pipe(
    WithView({
      sel: 'span.label',
      from: (sinks, sources) => {
        return sources.state.stream
          .filter(prop('isField'))
          .map(pipe(
            prop('label'),
            // either(prop('label'), prop('key')),
            ensureString,
            unless(isEmpty, pipe(
              append(':'),
              over(lensIndex(0), toUpper),
              join(''),
            )),
            objOf('children'),
          ));
      },
      ...viewOptions,
    }),
  ),
);

export const withFieldLabel = WithFieldLabel();

export const FieldLabel = behaviorToFactory(WithFieldLabel);

WithFieldLabel.coerce = FieldLabel.coerce = coerceViewOptions;