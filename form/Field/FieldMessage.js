import { pipe } from '@monocycle/component/lib/utilities/pipe.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import isString from 'ramda-adjunct/src/isString.js';
import { WithView, coerceViewOptions } from '@monocycle/dom/src/View';
import dropRepeats from 'xstream/extra/dropRepeats';
import prop from 'ramda/src/prop.js';
import either from 'ramda/src/either.js';
import __ from 'ramda/src/__.js';
import unless from 'ramda/src/unless.js';
import not from 'ramda/src/not.js';
import when from 'ramda/src/when.js';
import isTrue from 'ramda/src/T.js';
import defaultTo from 'ramda/src/defaultTo.js';
import objOf from 'ramda/src/objOf.js';
import identity from 'ramda/src/internal/_identity.js';





export const WhenField = listener => {

  const whenFieldListener = (sinks, sources) => {

    return sources.state.stream
      .map(prop('isField'))
      .filter(isTrue)
      .mapTo(
        listener(sinks, sources),
      )
      .flatten();
  };

  return whenFieldListener;
};

export const updateMessageListener = (sinks, sources) => {
  return sources.state.stream
    .map(prop('error'))
    .compose(dropRepeats())
    .map(when(isString, objOf('message')))
    .map(pipe(
      prop('message'),
      defaultTo(''),
      unless(not, either(
        prop(__, sources.Language),
        identity
      )),
    ));
};

export const WithFieldMessage = pipe(
  coerceViewOptions,
  viewOptions => pipe(
    WithView({
      sel: 'span.message',
      from: WhenField(updateMessageListener),
      ...viewOptions,
    }),
  ),
);

export const FieldMessage = behaviorToFactory(WithFieldMessage);

WithFieldMessage.coerce = FieldMessage.coerce = coerceViewOptions;