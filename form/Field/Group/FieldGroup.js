import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import pipe from 'ramda/src/pipe.js';
import dropRepeats from 'xstream/extra/dropRepeats';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import isNonEmptyArray from 'ramda-adjunct/src/isNonEmptyArray.js';
import { Field, whenOldValue } from '../Field.js';
import unless from 'ramda/src/unless.js';
import assoc from 'ramda/src/assoc.js';
import prop from 'ramda/src/prop.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import always from 'ramda/src/always.js';
import has from 'ramda/src/has.js';
import defaultTo from 'ramda/src/defaultTo.js';
import objOf from 'ramda/src/objOf.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import both from 'ramda/src/both.js';
import reduce from 'ramda/src/reduce.js';
import assocPath from 'ramda/src/assocPath.js';
import pluck from 'ramda/src/pluck.js';
import equals from 'ramda/src/equals.js';
import map from 'ramda/src/map.js';
import propSatisfies from 'ramda/src/propSatisfies.js';
import path from 'ramda/src/path.js';
import any from 'ramda/src/any.js';
import when from 'ramda/src/when.js';
import keys from 'ramda/src/keys.js';
import not from 'ramda/src/not.js';
import converge from 'ramda/src/converge.js';
import concat from 'ramda/src/concat.js';
import filter from 'ramda/src/filter.js';
import isUndefined from 'ramda-adjunct/src/isUndefined.js';
import all from 'ramda/src/all.js';
import { ensureArray } from '@monocycle/component/lib/utilities/ensureArray.js';
import castArray from 'ramda-adjunct/src/ensureArray.js';
import combineObj from 'xs-combine-obj';
import { WithTransition } from '@monocycle/state';
import { WithListener, parseFrom } from '@monocycle/listener';
import { WithIsolated } from '@monocycle/isolated';
import isString from 'ramda-adjunct/src/isString.js';
import identity from 'ramda/src/internal/_identity.js';
import { WithDynamicCollection, collectSinks } from '@monocycle/collection';
import { WithView, coerceViewOptions } from '@monocycle/dom/src/View';
import isArray from 'ramda-adjunct/src/isArray.js';





const castString = unless(isString, x => `${x}`);

export const collectFieldGroupSinks = (instances, sources) => ({
  ...collectSinks(instances, sources),
  value$: instances.pickCombine('value$')
    .compose(sources.Time.debounce(0)),
  valid$: instances.pickCombine('valid$')
    .map(pipe(
      all(Boolean),
    ))
    .compose(sources.Time.debounce(0)),
  touched$: instances.pickCombine('touched$')
    .map(pipe(
      any(Boolean),
    ))
    .compose(sources.Time.debounce(0)),
});

export const fieldGroupItemScope = pipe(
  castString,
  concat('field'),
  objOf('DOM'),
  assoc('*', null)
);

export const WithFieldGroup = pipe(
  coerceViewOptions,
  over(lensProp('kinds'), pipe(
    ensurePlainObj,
    filter(isFunction),
    over(lensProp('default'), defaultTo(Field))
  )),
  over(lensProp('SinksKeys'), unless(isFunction, always(keys))),
  over(lensProp('collectSinks'), unless(isFunction, always(collectFieldGroupSinks))),
  over(lensProp('itemScope'), unless(isFunction, always(fieldGroupItemScope))),
  over(lensProp('initWhen'), pipe(
    when(not, always(whenOldValue)),
    parseFrom,
  )),
  when(has('fields'), converge(assoc('initWhen'), [
    ({ fields, initWhen }) => pipe(
      initWhen,
      map(pipe(
        ensurePlainObj,
        assoc('fields', fields),
      ))
    ),
    identity,
  ])),
  when(prop('disabled'), converge(assoc('initWhen'), [
    ({ initWhen }) => pipe(
      initWhen,
      map(pipe(
        ensurePlainObj,
        assoc('disabled', true),
        over(lensProp('fields'), pipe(
          ensureArray,
          map(pipe(
            ensurePlainObj,
            assoc('disabled', true),
          ))
        )),
      ))
    ),
    identity,
  ])),
  ({
    kinds,
    itemKey,
    itemScope,
    SinksKeys,
    collectSinks,
    initWhen,
    ...viewOptions
  }) => pipe(
    WithDynamicCollection({
      kinds,
      itemKey,
      itemScope,
      collectSinks,
      SinksKeys: pipe(
        SinksKeys,
        concat([
          'value$',
          'valid$',
          'touched$',
        ]),
      ),
    }),
    WithView({
      sel: '',
      children: [],
      ...viewOptions,
    }),
    WithIsolated({
      state: 'fields',
      '*': null,
    }),
    WithListener({
      from: ({ value$, ...sinks }, sources) => {

        return whenOldValue(sinks, sources)
          .map(({ value }) => {
            const valueIsArray = isArray(value);

            return value$
              .map(pipe(
                valueIsArray
                  ? pluck('value')
                  : reduce(
                    (before, { path, value }) => (
                      path
                        ? assocPath(path, value, before)
                        : before
                    ),
                    value
                  ),
                objOf('value')
              ));
          })
          .flatten();
      },
      to: 'value$'
    }),
    WithTransition({
      label: 'setValid',
      from: 'valid$',
      reducer: valid => when(prop('isFieldGroup'), pipe(
        ensurePlainObj,
        assoc('valid', valid),
      )),
    }),
    WithTransition({
      label: 'setTouched',
      from: 'touched$',
      reducer: touched => when(prop('isFieldGroup'), pipe(
        ensurePlainObj,
        assoc('touched', touched),
      )),
    }),
    // WithSinkLogger('value$'),
    WithTransition({
      label: 'setValue',
      from: 'value$',
      reducer: pipe(
        ensurePlainObj,
        prop('value'),
        value => when(prop('isFieldGroup'), pipe(
          ensurePlainObj,
          assoc('value', value),
          assoc('touched', true),
        ))
      ),
    }),
    WithTransition({
      label: 'initFieldGroup',
      from: initWhen,
      reducer: pipe(
        ensurePlainObj,
        over(lensProp('fields'), unless(isUndefined, pipe(
          ensureArray,
          map(ensurePlainObj)
        ))),
        ({ value: initialValue, fields }) => when(
          both(
            isPlainObj,
            propSatisfies(pipe(
              ensureArray,
              all(prop('isField'))
            ), 'fields')
          ),
          pipe(
            ensurePlainObj,
            assoc('isField', true),
            assoc('isFieldGroup', true),
            assoc('value', initialValue),
            assoc('oldValue', initialValue),
            when(
              always(fields),
              assoc('fields', fields)
            ),
            over(lensProp('fields'), pipe(
              ensureArray,
              map(pipe(
                ensurePlainObj,
                over(lensProp('path'), unless(not, castArray)),
                ({ path: valuePath, ...field }) => {

                  const value = isNonEmptyArray(valuePath)
                    ? path(valuePath)(initialValue)
                    : undefined;

                  return ({
                    ...field,
                    path: valuePath,
                    value,
                    oldValue: value,
                    isField: false,
                  });
                }
              )),
            )),
          )
        )
      ),
    }),
    WithListener({
      from: (sinks, sources) => combineObj({
        path: sources.state.stream
          .map(prop('path'))
          .compose(dropRepeats()),
        value: sources.state.stream
          .map(prop('value'))
          .compose(dropRepeats(equals)),
      }),
      to: 'value$',
    }),
  )
);

export const FieldGroup = behaviorToFactory(WithFieldGroup);

WithFieldGroup.coerce = FieldGroup.coerce = coerceViewOptions;

