# State
```mermaid
graph TD;
  Initialized;
```
# State
```mermaid
classDiagram
  Field *-- View
  Field *-- FieldView
  Field o-- FieldInput
  Field o-- FieldLabel
  Field o-- FieldMessage
  Field : initField()
  Field : tbc
```
