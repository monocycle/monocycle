
import pipe from 'ramda/src/pipe.js';
import objOf from 'ramda/src/objOf.js';
import has from 'ramda/src/has.js';
import merge from 'ramda/src/merge.js';
import __ from 'ramda/src/__.js';
import assoc from 'ramda/src/assoc.js';
import unless from 'ramda/src/unless.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj';





export const makeInitFieldReducer = validate => pipe(
  ensurePlainObj,
  ({ value, ...initialState }) => {

    return pipe(
      validate,
      unless(isPlainObj, pipe(
        Boolean,
        objOf('error'),
      )),
      unless(has('value'), assoc('value', value)),
      ({ value, error }) => pipe(
        ensurePlainObj,
        merge(__, {
          ...initialState,
          isField: true,
          error,
          value,
          valid: !error,
          touched: false,
        }),
      ),
    )(value);
  }
);