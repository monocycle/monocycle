import pipe from 'ramda/src/pipe.js';
import objOf from 'ramda/src/objOf.js';
import has from 'ramda/src/has.js';
import merge from 'ramda/src/merge.js';
import __ from 'ramda/src/__.js';
import assoc from 'ramda/src/assoc.js';
import unless from 'ramda/src/unless.js';
import converge from 'ramda/src/converge.js';
import complement from 'ramda/src/complement.js';
import when from 'ramda/src/when.js';
import allPass from 'ramda/src/allPass.js';
import prop from 'ramda/src/prop.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj';
import identity from 'ramda/src/internal/_identity.js';





export const parseValidate = validate => value => pipe(
  validate,
  unless(isPlainObj, pipe(
    Boolean,
    objOf('error'),
  )),
  unless(has('value'), assoc('value', value)),
)(value);

export const makeValidateReducer = pipe(
  parseValidate,
  validate => value => pipe(
    ensurePlainObj,
    merge(__, validate(value)),
    when(
      allPass([
        prop('required'),
        complement(prop('value')),
        complement(prop('error')),
      ]),
      merge(__, {
        error: 'Field is required',
      })
    ),
    converge(assoc('valid'), [
      complement(prop('error')),
      identity,
    ])
  )
);

export const makeValidateReducer1 = validate => newValue => {

  let validated = validate(newValue);

  if (!isPlainObj(validated))
    validated = {
      error: Boolean(validated)
    };

  if (!has('value')(validated))
    validated.value = newValue;

  const { error, value } = validated;

  return pipe(
    ensurePlainObj,
    state => ({
      ...state,
      error,
      value,
      valid: !error,
    })
  );
};