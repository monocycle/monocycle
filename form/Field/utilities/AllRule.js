import pipe from 'ramda/src/pipe.js';
import castArray from 'ramda-adjunct/src/ensureArray.js';
import map from 'ramda/src/map.js';
import { Rule } from './Rule.js';





export const AllRule = pipe(
  castArray,
  map(Rule),
  rules => x => {

    let returned = false;

    rules.some(rule => returned = rule(x));

    return returned;
  }
);
