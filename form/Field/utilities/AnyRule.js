import pipe from 'ramda/src/pipe.js';
import ensureArray from 'ramda-adjunct/src/ensureArray.js';
import map from 'ramda/src/map.js';
import { Rule } from './Rule.js';





export const AnyRule = pipe(
  ensureArray,
  map(Rule),
  rules => x => {

    let error = false;
    let valid;

    rules.some(rule => {

      const returnedError = rule(x);

      error = error || returnedError;

      return valid = !returnedError;
    });

    return valid ? !valid : error;
  }
);
