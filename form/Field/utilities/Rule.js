import unless from 'ramda/src/unless.js';
import over from 'ramda/src/over.js';
import pipe from 'ramda/src/pipe.js';
import lensProp from 'ramda/src/lensProp.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import noop from 'ramda-adjunct/src/noop.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj';
import True from 'ramda/src/T.js';
import always from 'ramda/src/always.js';
import isString from 'ramda-adjunct/src/isString.js';
import isTrue from 'ramda-adjunct/src/isTrue.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import when from 'ramda/src/when.js';
import ifElse from 'ramda/src/ifElse.js';
import objOf from 'ramda/src/objOf.js';





export const Rule = unless(isFunction, pipe(
  ensurePlainObj,
  over(lensProp('validate'), unless(isFunction, always(True))),
  over(lensProp('error'), pipe(
    when(isString, objOf('message')),
    ensurePlainObj,
    over(lensProp('message'), unless(isNonEmptyString, always('Invalid'))),
  )),
  ({ validate, error }) => pipe(
    validate,
    unless(isPlainObj, pipe(
      Boolean,
      ifElse(isTrue,
        noop,
        pipe(
          always(error),
          objOf('error')
        )
      )
    )),
  )
));