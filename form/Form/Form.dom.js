import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { Row } from '@monocycle/dom/src/Box/Row';
import { withFlex } from '@monocycle/dom/src/Flex/index.js';
import { WithIsolated } from '@monocycle/isolated';
import { WithButton } from '@monocycle/dom/src/Button';
import { FieldGroup } from '../Field/Group';
import { WithStyledView } from '@monocycle/dom/src/StyledView';
import { formStyle } from './Form.style.js';
import { WithView } from '@monocycle/dom/src/View';
import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import map from 'ramda/src/map.js';
import lensProp from 'ramda/src/lensProp.js';
import unless from 'ramda/src/unless.js';
import always from 'ramda/src/always.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { Component } from '@monocycle/component';
import { withNoFlex } from '@monocycle/dom/src/NoFlex';
import { withCol } from '@monocycle/dom/src/Box/Col.js';
import { withScrollY } from '@monocycle/dom/src/ScrollY.js';
import { withNoScroll } from '@monocycle/dom/src/NoScroll.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';





export const resetButton = pipe(
  withFlex,
  WithButton({ attrs: { type: 'reset' }, text: 'Cancel' }),
  WithIsolated({
    DOM: 'resetButton',
    '*': null,
  }),
)();

export const submitButton = pipe(
  withFlex,
  WithButton({ attrs: { type: 'submit' }, text: 'Submit' }),
  WithIsolated({
    DOM: 'submitButton',
    '*': null,
  }),
)();

export const formFooter = pipe(
  withFlex,
  WithButton({ attrs: { type: 'submit' }, text: 'Submit' }),
  WithIsolated({
    DOM: 'submitButton',
    '*': null,
  }),
)();

export const WithFormView = pipe(
  FieldGroup.coerce,
  over(lensProp('slots'), pipe(
    ensurePlainObj,
    over(lensProp('submitButton'), unless(isFunction, always(submitButton))),
    over(lensProp('resetButton'), unless(isFunction, always(resetButton))),
    over(lensProp('FieldGroup'), unless(isFunction, always(FieldGroup))),
    over(lensProp('Footer'), unless(isFunction, always(Row))),
    map(Component),
  )),
  mergeDeepRight({
    attrs: {
      novalidate: typeof window !== undefined
    }
  }),
  ({
    kinds,
    fields,
    slots: {
      FieldGroup,
      Footer,
      before,
      submitButton,
      resetButton,
    },
    ...viewOptions
  }) => pipe(
    WithStyledView({
      name: 'Form',
      styles: formStyle,
    }),
    withCol,
    withNoScroll,
    WithView({
      sel: 'form',
      children: [],
      ...viewOptions,
      has: [
        before,
        FieldGroup({
          kinds,
          fields,
        }).map(pipe(
          withFlex,
          withScrollY,
        )),
        Footer([
          resetButton,
          submitButton,
        ]).map(withNoFlex),
      ],
    }),
  ),
);

/* export const WithFormViewOld = pipe(
  WithFields.coerce,
  over(lensProp('slots'), pipe(
    ensurePlainObj,
    over(lensProp('submitButton'), unless(isFunction, always(submitButton))),
    over(lensProp('resetButton'), unless(isFunction, always(resetButton))),
    over(lensProp('Fields'), unless(isFunction, always(Fields))),
    over(lensProp('Footer'), unless(isFunction, always(Row))),
    map(Component),
  )),
  ({
    kinds,
    slots: {
      Fields,
      Footer,
      submitButton,
      resetButton,
    },
    ...viewOptions
  }) => pipe(
    WithStyledView({
      name: 'Form',
      styles: formStyle,
    }),
    withCol,
    withNoScroll,
    WithView({
      sel: 'form',
      children: [],
      ...viewOptions,
      has: [
        Fields({
          kinds,
        }).map(pipe(
          withFlex,
          withScrollY,
        )),
        Footer([
          resetButton,
          submitButton,
        ]).map(withNoFlex),
      ],
    }),
  ),
); */

export const FormView = behaviorToFactory(WithFormView);
