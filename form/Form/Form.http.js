import pipe from 'ramda/src/pipe.js';
import prop from 'ramda/src/prop.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import unless from 'ramda/src/unless.js';
import always from 'ramda/src/always.js';
import eqProps from 'ramda/src/eqProps.js';
import propSatisfies from 'ramda/src/propSatisfies.js';
import objOf from 'ramda/src/objOf.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { WithRequest } from '@monocycle/http/Request';
import dropRepeats from 'xstream/extra/dropRepeats.js';
import isDate from 'ramda-adjunct/src/isDate.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import $ from 'xstream';




export const submitFormWhen = (sinks, sources) => {

  return sources.state.stream
    .filter(propSatisfies(isDate, 'submittedAt'))
    .compose(dropRepeats(eqProps('submittedAt')))
    .map(pipe(
      prop('value'),
      objOf('send'),
    ));
};

export const WithFormRequests = pipe(
  ensurePlainObj,
  over(lensProp('sendWhen'), unless(isFunction, always(submitFormWhen))),
  ({
    sendWhen: from,
    ...requestOptions
  }) => pipe(
    WithRequest({
      combine: $.merge,
      ...requestOptions,
      from,
      to: 'HTTP',
    }),
  )
);

export const withFormRequests = WithFormRequests();