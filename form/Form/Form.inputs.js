import $ from 'xstream';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithEventListener } from '@monocycle/dom/src/EventListener';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { WithListener } from '@monocycle/listener';
import { WithResponse } from '@monocycle/http/Response';
import complement from 'ramda/src/complement.js';
import dropRepeats from 'xstream/extra/dropRepeats';
import pipe from 'ramda/src/pipe.js';
import unless from 'ramda/src/unless.js';
import always from 'ramda/src/always.js';
import prop from 'ramda/src/prop.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import either from 'ramda/src/either.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import isArray from 'ramda-adjunct/src/isArray.js';





export const initFormWhen = (sinks, sources) => {
  return sources.state.stream
    .map(prop('oldValue'))
    .compose(dropRepeats())
    .filter(either(isPlainObj, isArray));
};

export const WithFormInputs = pipe(
  ensurePlainObj,
  over(lensProp('initWhen'), unless(isFunction, always(initFormWhen))),
  over(lensProp('resetWhen'), unless(isFunction, always($.empty))),
  over(lensProp('response'), pipe(
    ensurePlainObj,
  )),
  ({
    initWhen,
    resetWhen,
    response: responseOptions
  }) => pipe(
    WithEventListener({
      type: 'submit',
      options: { preventDefault: true },
      delay: 2000,
      delayType: 'throttle',
      to: 'submitForm$',
    }),
    WithEventListener({
      type: 'reset',
      options: { preventDefault: true },
      combine: $.merge,
      delay: 200,
      delayType: 'debounce',
      to: 'resetForm$',
    }),
    WithListener({
      from: initWhen,
      combine: $.merge,
      to: 'initForm$',
    }),
    WithListener({
      from: resetWhen,
      combine: $.merge,
      to: 'resetForm$',
    }),
    WithListener({
      from: $.empty,
      combine: $.merge,
      to: ['submitForm$'],
    }),
    WithResponse({
      strategy: always(response$ => {
        return response$.take(1)
          .replaceError(pipe(
            prop('response'),
            $.of,
          ));
      }),
      ...responseOptions,
    }),
    WithListener({
      from: (sinks) => sinks.response$
        .filter(prop('error')),
      combine: $.mmerge,
      to: 'submissionFailure$',
    }),
    WithListener({
      from: (sinks) => sinks.response$
        .filter(complement(prop('error'))),
      to: 'submissionSuccess$',
    }),
  ),
);

export const withFormInputs = WithFormInputs();

export const FormInputs = behaviorToFactory(WithFormInputs);
