import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithFieldGroup } from '../Field/Group';
import { WithFormInputs } from './Form.inputs.js';
import { withFormModel } from './Form.state.js';
import { WithFormView } from './Form.dom.js';
import { WithFormRequests } from './Form.http.js';
import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import unless from 'ramda/src/unless.js';
import always from 'ramda/src/always.js';
import lensProp from 'ramda/src/lensProp.js';
import prop from 'ramda/src/prop.js';
import converge from 'ramda/src/converge.js';
import assocPath from 'ramda/src/assocPath.js';
import identity from 'ramda/src/internal/_identity.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';





export const WithForm = pipe(
  WithFieldGroup.coerce,
  over(lensProp('request'), pipe(
    ensurePlainObj,
    over(lensProp('category'), unless(isNonEmptyString, always('submitForm'))),
    converge(assocPath(['response', 'category']), [
      prop('category'),
      identity,
    ]),
  )),
  ({
    initWhen,
    resetWhen,
    request: {
      response,
      ...requestOptions
    },
    ...formViewOptions
  }) => pipe(
    WithFormView(formViewOptions),
    WithFormInputs({
      initWhen,
      resetWhen,
      response
    }),
    WithFormRequests(requestOptions),
    withFormModel,
  ),
);

export const Form = behaviorToFactory(WithForm);
