import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithTransition } from '@monocycle/state/Transition';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import assoc from 'ramda/src/assoc.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import pipe from 'ramda/src/pipe.js';
import converge from 'ramda/src/converge.js';
import equals from 'ramda/src/equals.js';
import when from 'ramda/src/when.js';
import allPass from 'ramda/src/allPass.js';
import prop from 'ramda/src/prop.js';
import propSatisfies from 'ramda/src/propSatisfies.js';
import not from 'ramda/src/not.js';
import map from 'ramda/src/map.js';
import eqProps from 'ramda/src/eqProps.js';
import always from 'ramda/src/always.js';
import isUndefined from 'ramda-adjunct/src/isUndefined.js';
import clone from 'ramda/src/clone.js';
import identity from 'ramda/src/internal/_identity.js';
import dropRepeats from 'xstream/extra/dropRepeats';
import { WithListener } from '@monocycle/listener';
import isNonEmptyArray from 'ramda-adjunct/src/isNonEmptyArray.js';





export const setFieldTouched = pipe(
  ensurePlainObj,
  over(lensProp('fields'), map(pipe(
    ensurePlainObj,
    assoc('touched', true),
    when(propSatisfies(isNonEmptyArray, 'fields'),
      field => setFieldTouched(field),
    )
  )))
);

export const submitFormReducer = pipe(
  ensurePlainObj,
  when(
    allPass([
      prop('isForm'),
      prop('valid'),
      prop('touched'),
      propSatisfies(isUndefined, 'submittedAt'),
      pipe(
        converge(equals, [
          prop('value'),
          prop('oldValue'),
        ]),
        not,
      ),
    ]),
    state => ({
      ...state,
      submittedAt: new Date,
    }),
  ),
  setFieldTouched,
);

export const WithFormModel = pipe(
  ensurePlainObj,
  () => pipe(
    x => x,
    WithTransition({ // resetForm
      label: 'resetForm',
      from: 'resetForm$',
      reducer: () => pipe(
        ensurePlainObj,
        assoc('submittedAt', undefined),
        converge(assoc('oldValue'), [
          pipe(
            prop('oldValue'),
            clone,
          ),
          identity,
        ]),
      ),
    }),
    WithListener('initForm$'),
    WithTransition({ // submitForm
      label: 'submitForm',
      from: 'submitForm$',
      reducer: always(submitFormReducer),
    }),
    WithTransition({ // initForm
      label: 'initForm',
      from: (sinks, sources) => {
        return sources.state.stream
          .filter(allPass([
            prop('isFieldGroup'),
          ]))
          .compose(dropRepeats(eqProps('isFieldGroup')));
      },
      reducer: always(pipe(
        ensurePlainObj,
        assoc('isForm', true),
        assoc('submittedAt', undefined),
      )),
    }),
  ),
);

export const withFormModel = WithFormModel();

export const FormModel = behaviorToFactory(WithFormModel);
