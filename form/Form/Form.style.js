import { scroll } from 'monostyle/src/scroll';





export const formStyle = [
  scroll,
  {
    $debugName: 'Form',
    color: 'var(--formColor)',
    backgroundColor: 'var(--formBgColor)',
  },
];