import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { Form } from '../Form';
import { Row } from '@monocycle/dom/src/Box/Row.js';
import { withPadded } from '@monocycle/dom/src/Padded';
import { WithModal } from '@monocycle/dom/src/Modal';
import { whenVisibleState } from '@monocycle/dom/src/Overlay';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { Name } from '@monocycle/dom/src/Name.js';
import { Component } from '@monocycle/component';
import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import unless from 'ramda/src/unless.js';
import always from 'ramda/src/always.js';
import not from 'ramda/src/not.js';
import map from 'ramda/src/map.js';
import filter from 'ramda/src/filter.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';




export const parseFormModalOptions = pipe(
  ensurePlainObj,
  over(lensProp('visibleWhen'), unless(isFunction, always(whenVisibleState))),
  over(lensProp('slots'), pipe(ensurePlainObj, pipe(
    over(lensProp('Form'), unless(isFunction, always(Form))),
    over(lensProp('Name'), unless(isFunction, always(Name))),
    map(Component),
  ))),
);

export const WithFormModal = pipe(
  WithModal.coerce,
  parseFormModalOptions,
  ({
    slots: {
      Form,
      Name,
    },
    visibleWhen,
    ...modalOptions
  }) => pipe(
    WithModal({
      style: {
        minWidth: '75%',
      },
      ...modalOptions,
      visibleWhen,
      has: [
        Row({
          style: {
            justifyContent: 'center',
            backgroundColor: 'var(--modalHeaderBgColor)',
          },
          has: [
            Name({
              sel: 'h2',
            }),
          ],
        }).map(withPadded),
        Form({
          resetWhen: pipe(
            visibleWhen,
            filter(not),
          ),
        }),
      ],
    }),
  ),
);

export const FormModal = behaviorToFactory(WithFormModal);

FormModal.coerce = WithFormModal.coerce = WithModal.coerce;