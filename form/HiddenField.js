import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithField } from './Field/Field.js';
import { extend } from '@monocycle/component/lib/utilities/extend';





export const WithHiddenField = extend(WithField, {
  type: 'hidden',
});

WithHiddenField.coerce = ensurePlainObj;

export const HiddenField = behaviorToFactory(WithHiddenField);