import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory';
import { WithField } from '../Field/Field.js';
import { extend } from '@monocycle/component/lib/utilities/extend.js';





export const WithNumberField = extend(WithField, {
  type: 'number',
});

export const withNumberField = WithNumberField();

export const NumberField = behaviorToFactory(WithNumberField);

export const numberField = NumberField();