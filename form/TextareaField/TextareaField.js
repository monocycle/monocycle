import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import unless from 'ramda/src/unless.js';
import always from 'ramda/src/always.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { WithField, FieldInput } from '../Field';
import { TextareaFieldStyle } from './TextareaField.style.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory';
import { coerceViewOptions } from '@monocycle/dom/src/View';
import { WithStyledView } from '@monocycle/dom/src/StyledView';
import { renderTextareaFieldValue } from './_utilities/renderValue.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { extend } from '@monocycle/component/lib/utilities/extend.js';





export const WithTextareaField = pipe(
  coerceViewOptions,
  over(lensProp('input'), pipe(
    ensurePlainObj,
    over(lensProp('renderValue'), unless(isFunction, always(renderTextareaFieldValue))),
  )),
  ({
    disabled,
    input: inputOptions,
    ...fieldOptions
  }) => pipe(
    WithField({
      ...fieldOptions,
      disabled,
      slots: {
        Input: extend(FieldInput, {
          sel: 'textarea.input',
          attrs: {
            disabled,
          },
          props: {
            rows: 1,
          },
          ...inputOptions,
        }),
      },
    }),
    WithStyledView({
      name: 'TextareaField',
      sel: 'label',
      styles: TextareaFieldStyle,
    }),
  ),
);

export const TextareaField = behaviorToFactory(WithTextareaField);

WithTextareaField.coerce = TextareaField.coerce = coerceViewOptions;