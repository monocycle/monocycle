
import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj';
import { rem } from 'csx/lib/units';





export const TextareaFieldStyle = pipe(
  ensurePlainObj,
  over(lensProp('colors'), pipe(
    ensurePlainObj,
  )),
  () => [
    {
      $debugName: 'TextareaField',
      '& .input': {
        resize: 'none',
        height: rem(4),
        padding: `${rem(.8)} 0`,
      }
    }
  ]
);