import { syncHeight } from './syncHeight.js';





export const buildHooks = value => {
  // console.log('buildHooks()', { value })
  const syncHeightHandler = event => syncHeight(event.target);

  return {
    insert: vnode => {
      // console.log('buildHooks.insert()', { vnode })

      vnode.elm.addEventListener('input', syncHeightHandler, false);

      vnode.elm.value = value;

      setTimeout(syncHeight.bind(undefined, vnode.elm), 300);
    },
    update: (oldVnode, vnode) => {
      // console.log('buildHooks.update()', { vnode })

      if (vnode.elm.value === value)
        return;

      vnode.elm.value = value;

      setTimeout(syncHeight.bind(undefined, vnode.elm), 300);
    },
    destroy: vnode => {

      vnode.elm.removeEventListener('input', syncHeightHandler);
    },
  };
};
