import pipe from 'ramda/src/pipe.js';
import objOf from 'ramda/src/objOf.js';
import { buildHooks } from './buildHooks.js';





export const renderTextareaFieldValue = pipe(
  buildHooks,
  objOf('hook'),
  objOf('data'),
);