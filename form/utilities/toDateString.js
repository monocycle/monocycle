import { ten } from './ten.js';





export const toDateString = date => {

  return date.getFullYear() +
    '-' + ten(date.getMonth() + 1) +
    '-' + ten(date.getDate());
};