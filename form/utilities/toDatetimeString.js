import { ten } from './ten.js';
import { toDateString } from './toDateString.js';





export const toDatetimeString = date => {

  return toDateString(date) +
    'T' + ten(date.getHours()) +
    ':' + ten(date.getMinutes());
  //  +
  // ':' + ten(date.getSeconds());
};
