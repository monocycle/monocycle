# 6.1.0 (2020-11-09)


### Bug Fixes

* **history:** depends on component@10.1 ([17363a0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/17363a0e71ea7e5724810e190b21d6bd0f049d42))



# 6.0.0 (2020-11-05)


### Bug Fixes

* **history:** make package esm compatible ([8ae00ff](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8ae00ffa875715d1b1278fd6c07e4b7b9b9fe1de))


### BREAKING CHANGES

* **history:** maybe



# 5.10.0 (2020-10-07)


### Bug Fixes

* **history:** update dependencies ([dd0edb1](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/dd0edb1dc0dc4dfd62373ba2a2914a8035f4ba24))



# 5.9.0 (2020-05-19)


### Bug Fixes

* **history:** update deps ([ef30c9d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ef30c9dcd2e28b01aab324cfc8b78d3ee5e1eb4f))



# 5.8.0 (2020-01-20)


### Bug Fixes

* **history:** depends on isolated@4.5 ([030ceb9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/030ceb9c96847a890fac55d9d45592506b9b1637))



# 5.7.0 (2020-01-20)


### Bug Fixes

* **history:** depends on component@5.5 ([210e19b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/210e19b7c4d6963f938b56b9584968c49088e1f4))



# 5.6.0 (2020-01-20)


### Bug Fixes

* **history:** depends on listener@5.5 ([19f3ab5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/19f3ab5054283b6d3026c85d837f59f4ebef27e9))



# 5.5.0 (2020-01-20)


### Bug Fixes

* **history:** depends on component@5.4 ([938eb4e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/938eb4e9f26654a56839da6611353d7ffd68d2e6))



# 5.4.0 (2019-12-02)


### Bug Fixes

* **history:** update deps ([ecda0b4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ecda0b49168f319751e9885a70098f6c190f7e07))



# 5.3.0 (2019-12-02)


### Bug Fixes

* **history:** add lock file ([0f941fb](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0f941fbd0fc966cf7a532ec203023b1e65867b3c))



# 5.2.0 (2019-12-02)


### Bug Fixes

* **history:** add lock file ([0f941fb](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0f941fbd0fc966cf7a532ec203023b1e65867b3c))



# 5.1.0 (2019-12-02)


### Bug Fixes

* **history:** add lock file ([0f941fb](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0f941fbd0fc966cf7a532ec203023b1e65867b3c))



# 5.0.0 (2019-11-11)


### Features

* **history:** hello es modules ([43c2ae9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/43c2ae9e49c441a67e6144ade95d0359f5e6ee6b))


### BREAKING CHANGES

* **history:** yes



# 4.0.0 (2019-10-18)


### Bug Fixes

* **history:** depends on dynamic@2.1 switch@5.0 ([fc9195a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fc9195a6dfb7057da846a20d486709cfe7959a1e))


### BREAKING CHANGES

* **history:** yes



## 3.12.0 (2019-09-06)

* feat(history): add from option to Router ([5dbbc85](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5dbbc85))
* chore(history): add example ([b48f6c5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b48f6c5))



## 3.11.0 (2019-07-10)

* fix(history): prefix relative locations only ([da9c454](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/da9c454))



## 3.10.0 (2019-06-08)

* fix(history): depend on switch@4.1 ([0740a78](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/0740a78))



## 3.9.0 (2019-05-11)

* fix(history): depend on component@4.1 switch@4.0 ([a94860c](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/a94860c))
* fix(history): drop repeated pathname and refactor ([0cad445](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/0cad445))



## 3.8.0 (2019-04-08)

* fix(history): depend on component@4.0 ([bf7ac19](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/bf7ac19))



## 3.7.0 (2019-03-28)

* fix(history): remove debug ([4b4a681](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/4b4a681))



## 3.6.0 (2019-03-24)

* fix(history): remove captureClicks listener from Router ([8d0714b](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/8d0714b))



## 3.5.0 (2019-03-24)

* feat(history): add IsolatedHistory driver behavior ([4fb609f](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/4fb609f))



## 3.4.0 (2019-03-23)

* fix(history): depend on component@3.6 ([fbea124](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/fbea124))



## 3.3.0 (2019-03-23)

* fix(history): depend on component@3.5 switch@3.2 ([78b0175](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/78b0175))



## 3.1.0 (2019-03-23)

* fix(history): depend on component@3.4 switch@3.1 ([5544344](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/5544344))



## 3.1.0 (2019-03-10)

* fix(history): depend on component@3.2 switch@3.0 ([7cd525d](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/7cd525d))



## 3.0.0 (2019-03-03)

* feat(history): depend on component@3.1 and remove symbols dep ([985477e](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/985477e))


### BREAKING CHANGE

* No longer use Component


## 2.1.0 (2019-02-25)

* fix(history): add node_modules to .npmignore ([709c585](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/709c585))



## 2.0.0 (2019-02-04)

* fix(history): update @component ([a102b43](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/a102b43))
* chore(history): add files ([f382382](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/f382382))


### BREAKING CHANGE

* yes


