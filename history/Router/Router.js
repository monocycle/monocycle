export { Case } from '@monocycle/switch';
import { WithSwitch } from '@monocycle/switch';
import { WithDynamic } from '@monocycle/dynamic';
import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import objOf from 'ramda/src/objOf.js';
import unless from 'ramda/src/unless.js';
import lensProp from 'ramda/src/lensProp.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import isUndefined from 'ramda-adjunct/src/isUndefined.js';
import prop from 'ramda/src/prop.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { parseFrom } from '@monocycle/listener';
import dropRepeats from 'xstream/extra/dropRepeats.js';





export const coerce = unless(isPlainObj, objOf('from'));

export const WithRouter = pipe(
  coerce,
  over(lensProp('from'), unless(isUndefined,
    parseFrom
  )),
  ({ from, SinksKeys, ...switchOptions }) => {

    const WithRouterSwitch = switchOptions => WithSwitch({
      ...switchOptions,
      SinksKeys,
      from: (sinks, sources) => {

        return sources.History
          .map(prop('pathname'))
          .compose(dropRepeats());
      }
    });

    if (!from)
      return WithRouterSwitch(switchOptions);

    return WithDynamic({
      SinksKeys,
      from: (sinks, sources) => {


        const stream = from(sinks, sources);

        return stream
          .map(options => {

            return WithRouterSwitch(options);
          });
      },
    });
  }
);

export const Router = behaviorToFactory(WithRouter);

WithRouter.coerce = Router.coerce = coerce;