import pipe from 'ramda/src/pipe.js';
import { WithView, View } from '@monocycle/dom/src/View';
import { Link } from '../Link';
import { Router } from '@monocycle/history/Router';
import { WithIsolated } from '@monocycle/isolated';





const WithPageD = () => WithView([
  View('D'),
]);

const WithPageC = () => pipe(
  WithView([
    View('C'),
    Link({ href: '', text: 'home' }),
    Link({ href: 'd', text: 'page D' }),
    Router({
      default: WithView(''),
      resolve: [
        { resolve: /^\/d/, value: WithPageD() }
      ]
    }),
  ]),

);

const WithPageB = () => pipe(
  WithView([
    View('B'),
    Link({ href: '', text: 'home' }),
    Link({ href: 'c', text: 'page C' }),
    Router({
      default: WithView(''),
      resolve: [
        {
          resolve: /^\/c/, value: pipe(
            WithPageC(),
            WithIsolated({
              History: '/c',
              DOM: 'c',
              '*': null
            }),
          )
        }
      ]
    }),
  ]),
);

const WithPageA = () => pipe(
  WithView([
    View('A'),
    Link({ href: '', text: 'home' }),
    Link({ href: 'b', text: 'page B' }),
    Router({
      default: WithView(''),
      resolve: [
        {
          resolve: /^\/b/, value: pipe(
            WithPageB(),
            WithIsolated({
              History: '/b',
              DOM: 'b',
              '*': null
            })
          )
        }
      ]
    }),
  ]),
);


export const WithApp = () => pipe(
  WithPageA()
  // WithIsolated({
  //   History: '/a',
  //   DOM: 'a',
  //   '*': null
  // })
);