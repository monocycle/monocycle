import prop from 'ramda/src/prop.js';
import isString from 'ramda-adjunct/src/isString.js';
import pipe from 'ramda/src/pipe.js';
import startsWith from 'ramda/src/startsWith.js';
import dropRepeats from 'xstream/extra/dropRepeats.js';


const IsolatedHistorySource = (sink, prefix = '') => {

  // console.debug(`HistorySource(${prefix})`)

  if (!isString(prefix))
    return Object.assign(sink, {
      isHistorySource: true,
    });

  if (prefix && !prefix.startsWith('/'))
    prefix = '/' + prefix;

  const source = Object.assign(sink, {
    isHistorySource: true,
    prefix,
    isolateSink: (sink$, scope) => {

      return sink$
        // .map(x => console.debug(`historySource(${prefix}).sink1(${scope})`, x) || x)
        .map(input => {

          if (typeof input === 'string')
            input = { type: 'push', pathname: input };

          let { pathname } = input;

          pathname = pathname.startsWith('/') || pathname.startsWith('http')
            ? pathname
            : scope + pathname;

          if (pathname.endsWith('/'))
            pathname = pathname.slice(0, -1);

          return {
            ...input,
            pathname
          };
        });
      // .map(x => console.debug(`historySource(${prefix}).sink2(${scope})`, x) || x)
    },
    isolateSource: (source, scope) => {

      if (scope && !scope.startsWith('/'))
        scope = '/' + scope;

      return IsolatedHistorySource(
        source
          .filter(pipe(prop('pathname'), startsWith(scope)))
          .compose(dropRepeats())
          // .map(x => console.debug(`historySource(${prefix}).source1(${scope})`, x) || x)
          .map(({ pathname, ...location }) => ({
            ...location,
            pathname: pathname.slice(scope.length) || '/'
          }))
          // .map(x => console.debug(`historySource(${prefix}).source2(${scope})`, x) || x)
          .remember(),
        scope
      );
    }
  });

  return source;
};

export const withIsolatedHistory = driver => sink$ => IsolatedHistorySource(driver(sink$));