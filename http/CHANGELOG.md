# 8.1.0 (2020-11-09)


### Bug Fixes

* **http:** depends on component@10.1 ([4e90755](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4e90755165438ea71b662ee9b67b0c407e711a4b))



# 8.0.0 (2020-11-05)


### Bug Fixes

* **http:** make package esm compatible ([5fd3e73](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5fd3e73138e03198a541778b8f80190c2f64e539))


### BREAKING CHANGES

* **http:** maybe



# 7.1.0 (2020-10-07)


### Bug Fixes

* **http:** update dependencies ([f43fa28](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f43fa280d18aba49e5418b7f278380f94e150638))



# 7.0.0 (2020-09-26)


### Features

* **http:** stategy is a listener ([d82b692](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d82b692e336d337184111b262bdae4bfeae7c7e3))


### BREAKING CHANGES

* **http:** yes



# 6.14.0 (2020-05-19)


### Bug Fixes

* **http:** update deps ([8f1039d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8f1039d7b62a2928b41b1a4003d0727ac8ff5957))



# 6.13.0 (2020-03-11)


### Bug Fixes

* **http:** remove useless filter in Response ([19327a9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/19327a94cac796a95e5b92f0f69c8518c09b3780))



# 6.12.0 (2020-02-18)


### Bug Fixes

* **http:** update dependencies ([23688cf](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/23688cf77eaf0d3a6ec60c9ff44d586d41af19b5))



# 6.11.0 (2020-01-20)


### Bug Fixes

* **http:** depends on component@5.5 ([ba66bb0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ba66bb0005b728254a0c32b9b73ba180d9455908))



# 6.10.0 (2020-01-20)


### Bug Fixes

* **http:** depends on listener@5.5 ([ea8bb2d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ea8bb2d72cb4ea9a4d9ea2d25546eb7263c9ce58))



# 6.9.0 (2020-01-20)


### Bug Fixes

* **http:** depends on state@4.3 ([5df1960](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5df1960b94eef51c935e5531481718a5d62c010a))



# 6.8.0 (2020-01-20)


### Bug Fixes

* **http:** depends on component@5.4 ([4cf029d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4cf029d37628501f5fec483f990d8ab389328273))



# 6.7.0 (2019-12-14)


### Bug Fixes

* **http:** don't fallback to 'error' in Resource ([7d38218](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/7d38218ab7c2b7fdeb9cf609891beb0f21615423))



# 6.6.0 (2019-12-12)


### Bug Fixes

* **http:** don't expose resource private sinks ([ceddfd6](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ceddfd6642ef04e0916fdf712dd3e0f8e192cee6))



# 6.5.0 (2019-12-11)


### Bug Fixes

* **http:** repair resourceRequestListener ([1727e02](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/1727e02c16f1d90c43f1b03b0935dbe6f82aa294))



# 6.4.0 (2019-12-11)


### Bug Fixes

* **http:** don't expect that a link is a string ([6b87b8d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6b87b8db3f9e0d44ad2dc0dec94a02571b309b90))



# 6.3.0 (2019-12-10)


### Bug Fixes

* **http:** repair fetchDataListener in Resource ([504c10c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/504c10ce47b4cc6336f4e0b3c42b556f3146f5d6))



# 6.2.0 (2019-12-07)


### Features

* **http:** export withResource and resource ([65a53d0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/65a53d0715367aa5d1abb09fd1e6ec7a02b0b1db))



# 6.1.0 (2019-12-07)


### Bug Fixes

* **http:** repair fetchDataListener ([3e16fb7](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3e16fb7cc818e94c12b506b12586db4e67dec78b))



# 6.0.0 (2019-12-05)


### Features

* **http:** rewrite Resource ([0954499](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0954499613fc0813bfb4311680ce752d61a6ef17))


### BREAKING CHANGES

* **http:** yes



# 5.0.0 (2019-12-04)


### Features

* **http:** rewrite Resource ([bed672b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/bed672b7162bd5452ee1cded42956ed1d220e7af))


### BREAKING CHANGES

* **http:** yes



# 4.9.0 (2019-12-03)


### Bug Fixes

* **http:** refactor Resource ([2a3669a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/2a3669a5beb0dfc36e8869d8af9adc6921251999))



# 4.8.0 (2019-12-02)


### Bug Fixes

* **http:** update deps ([2521767](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/252176750aeed27ec4df6a9179b3e29a5c1a67fd))



# 4.7.0 (2019-12-02)


### Bug Fixes

* **http:** add lock file ([a418515](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/a418515c8bddeb9a589daa336b35dfd9dc94d1eb))



# 4.6.0 (2019-12-02)


### Bug Fixes

* **http:** add lock file ([a418515](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/a418515c8bddeb9a589daa336b35dfd9dc94d1eb))



# 4.5.0 (2019-11-26)


### Bug Fixes

* **http:** allow to omit from option ([b98895d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b98895d4b9240bee56c99286f6a8d03284bf9722))



# 4.4.0 (2019-11-11)


### Bug Fixes

* **http:** update dependencies ([7e53ae6](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/7e53ae638b2dc42dbfdb9983c7566f47068b00a3))



# 4.3.0 (2019-10-27)


### Features

* **http:** let customize Resource links ([b43e898](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b43e898591dbae88acf68de154aea64ee6bb47a3))



## 4.2.0 (2019-09-24)

* fix(http): ensure component is passed to Resource ([870bbf6](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/870bbf6))
* fix(http): temporarily fix Response not correctly selecting category ([fd8d034](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fd8d034))
* fix(http): update dependencies ([129dc47](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/129dc47))



## 4.1.0 (2019-09-06)

* feat(http): let customize requestOptions in Resource ([697a5a5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/697a5a5))



## 4.0.0 (2019-06-18)

* fix(http): use mergeDeepRight for merging Request options ([711dae9](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/711dae9))
* feat(http): export es6 modules ([5b697e6](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/5b697e6))


### BREAKING CHANGE

* yes


## 3.3.0 (2019-06-10)

* feat(http): let Resource use Request ([2c75c72](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/2c75c72))



## 3.2.0 (2019-06-08)

* fix(http): let customize 'combine' option and refactor ([2a00df0](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/2a00df0))



## 3.1.0 (2019-05-20)

* fix(http): invalid publish ([4ccf871](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/4ccf871))



## 3.0.0 (2019-05-20)

* feat(http): merge initial options ([e3ff6e7](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/e3ff6e7))


### BREAKING CHANGE

* yes


## 2.0.0 (2019-05-11)

* feat(http): improve Resource and Response, extract Request and add some tests ([4e778fc](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/4e778fc))


### BREAKING CHANGE

* probably


## 1.0.0 (2019-04-09)

* feat(http): extract Response from Resource ([d601239](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/d601239))


### BREAKING CHANGE

* Resource behavior has changed


## 0.3.0 (2019-04-08)

* fix(http): depend on component@4.0 ([41a1d46](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/41a1d46))



## 0.2.0 (2019-04-02)

* fix(http): depend on after@3.7 ([d62b87b](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/d62b87b))
* feat(http): create package and add Resource ([2a2ab51](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/2a2ab51))



