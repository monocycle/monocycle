import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { EmptyObject } from '@monocycle/component/lib/utilities/empty.js';
import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import lensProp from 'ramda/src/lensProp.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import always from 'ramda/src/always.js';
import when from 'ramda/src/when.js';
import not from 'ramda/src/not.js';
import unless from 'ramda/src/unless.js';
import { Stream as $ } from 'xstream';
import { WithListener, parseFrom } from '@monocycle/listener';





export const WithRequest = pipe(
  ensurePlainObj,
  over(lensProp('from'), pipe(
    when(not, always(pipe(
      EmptyObject,
      $.of,
    ))),
    parseFrom,
  )),
  over(lensProp('combine'), unless(isFunction,
    always($.merge),
  )),
  ({ from, combine, ...options }) => {

    return WithListener({
      combine,
      from: (sinks, sources) => {

        return from(sinks, sources)
          .map(mergeDeepRight(options));
      },
      to: 'HTTP'
    });
  }
);

export const Request = behaviorToFactory(WithRequest);