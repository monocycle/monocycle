import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import unless from 'ramda/src/unless.js';
import always from 'ramda/src/always.js';
import propEq from 'ramda/src/propEq.js';
import objOf from 'ramda/src/objOf.js';
import both from 'ramda/src/both.js';
import merge from 'ramda/src/merge.js';
import equals from 'ramda/src/equals.js';
import omit from 'ramda/src/omit.js';
import prop from 'ramda/src/prop.js';
import path from 'ramda/src/path.js';
import ifElse from 'ramda/src/ifElse.js';
import F from 'ramda/src/F.js';
import all from 'ramda/src/all.js';
import isArray from 'ramda-adjunct/src/isArray.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import isString from 'ramda-adjunct/src/isString.js';
import noop from 'ramda-adjunct/src/noop.js';
import isInteger from 'ramda-adjunct/src/isInteger.js';
import isBoolean from 'ramda-adjunct/src/isBoolean.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import when from 'ramda/src/when.js';
import applySpec from 'ramda/src/applySpec.js';
import not from 'ramda/src/not.js';
import identity from 'ramda/src/internal/_identity.js';
import __ from 'ramda/src/__.js';
import dropRepeats from 'xstream/extra/dropRepeats.js';
import { WithAfter } from '@monocycle/after';
import { Stream as $ } from 'xstream';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { WithTransition } from '@monocycle/state';
import { WithListener, parseFrom } from '@monocycle/listener';
import { WithResponse } from './Response';
import { WithRequest } from './Request';
import { Component } from '@monocycle/component';





const isBrowser = typeof window !== 'undefined';

export const extractData = applySpec({
  links: prop('links'),
  data: prop('body'),
});

export const parseDataPath = unless(
  both(isArray, all(isNonEmptyString)),
  always(['body'])
);

export const parseResourceOptions = pipe(
  ensurePlainObj,
  over(lensProp('url'), unless(isNonEmptyString, always(''))),
  over(lensProp('method'), unless(isNonEmptyString, always('get'))),
  over(lensProp('redirects'), unless(isInteger, always(5))),
  over(lensProp('withCredentials'), unless(isBoolean, F)),
  over(lensProp('category'), unless(isNonEmptyString, always('fetchResourceData'))),
  over(lensProp('retries'), unless(isInteger, always(15))),
  over(lensProp('interval'), unless(isInteger, always(6000))),
  over(lensProp('extractData'), unless(isFunction, always(extractData))),
);

export const resourceRequestListener = (sinks, sources) => {

  return sources.state.stream
    .filter(prop('isResource'))
    .map(pipe(
      prop('fetching'),
      when(isString, objOf('url'))
    ))
    .compose(dropRepeats(equals))
    .filter(Boolean);
};

export const WithResource = pipe(
  ensurePlainObj,
  over(lensProp('from'), pipe(
    ifElse(isFunction,
      parseFrom,
      always(pipe(noop, $.of)),
    ),
  )),
  parseResourceOptions,
  ({ category, extractData, interval, retries }) => pipe(
    identity,
    unless(isFunction, Component.empty),

    WithResponse({ // resourceResponse
      category,
      combine: $.merge,
      to: 'resourceResponse$'
    }),

    WithListener({ // fetchData
      from: (sinks, sources) => {
        return sources.state.stream
          .filter(prop('isResource'))
          .filter(pipe(prop('fetching'), not))
          .filter(pipe(prop('receivedAt'), not))
          .map(path(['links', 'self']))
          .compose(dropRepeats())
          .mapTo('self');
      },
      combine: $.merge,
      to: 'fetchData$',
    }),

    WithListener({ // responseError
      from: ({ resourceResponse$ }) => {
        return resourceResponse$
          .filter(prop('error'));
      },
      combine: $.merge,
      to: 'responseError$',
    }),


    WithListener({ // receiveData
      from: ({ resourceResponse$ }) => {
        return resourceResponse$
          .filter(both(
            prop('ok'),
            propEq('type', 'application/json')
          ))
          .map(extractData);
      },
      combine: $.merge,
      to: 'receiveData$',
    }),

    WithRequest({ // resourceRequest
      combine: $.merge,
      category,
      from: (sinks, sources) => {

        return !isBrowser
          ? resourceRequestListener(sinks, sources)
          : sources.Time.periodic(interval)
            .startWith()
            .take(retries)
            .map(() => resourceRequestListener(sinks, sources))
            .flatten();
      },
    }),

    WithTransition({ // initResource
      label: 'initResource',
      from: (sinks, sources) => {
        return sources.state.stream
          .map(prop('isResource'))
          .compose(dropRepeats())
          .filter(not);
      },
      reducer: pipe(
        ensurePlainObj,
        // merge(__),
        () => pipe(
          ensurePlainObj,
          merge(__, {
            isResource: true,
            receivedAt: false,
            fetching: undefined,
            data: undefined,
          }),
        )
      ),
    }),

    WithTransition({ // fetchDataTransition
      label: 'fetchData',
      from: 'fetchData$',
      reducer: linkKey => (state = {}) => ({
        ...state,
        receivedAt: false,
        fetching: prop(linkKey)(state.links),
      })
    }),

    WithTransition({ // receiveDataTransition
      label: 'receiveData',
      from: 'receiveData$',
      reducer: update => when(prop('isResource'), state => ({
        ...state,
        receivedAt: new Date,
        fetching: undefined,
        ...update
      }))
    }),

    WithAfter(omit(['resourceResponse$', 'fetchData$', 'receiveData$'])),
  )
);

export const withResource = WithResource();

export const Resource = behaviorToFactory(WithResource);

export const resource = Resource();
