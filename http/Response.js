import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import unless from 'ramda/src/unless.js';
import when from 'ramda/src/when.js';
import always from 'ramda/src/always.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import isUndefined from 'ramda-adjunct/src/isUndefined.js';
import { Stream as $ } from 'xstream';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { WithListener } from '@monocycle/listener';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';





export const defaultStrategy = always(response$ => {
  return response$.take(1)
    .replaceError($.empty);
});

export const WithResponse = pipe(
  ensurePlainObj,
  over(lensProp('category'), unless(isNonEmptyString,
    always(undefined)
  )),
  over(lensProp('combine'), unless(isFunction,
    always(undefined)
  )),
  over(lensProp('strategy'), unless(isFunction,
    always(defaultStrategy)
  )),
  over(lensProp('to'), when(isUndefined,
    always('response$')
  )),
  ({ to, combine, strategy, category }) => WithListener({
    from: (sinks, sources) => {
      return sources.HTTP.select(category)
        .map(strategy(sinks, sources))
        .flatten();
    },
    combine,
    to
  })
);

export const Response = behaviorToFactory(WithResponse);
