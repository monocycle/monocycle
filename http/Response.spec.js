import test from 'ava';
import { Stream as $ } from 'xstream';
import { Response } from './Response.js';
import keys from 'ramda/src/keys.js';
import Jsverify from 'jsverify';
import { diagramArbitrary, withTime } from 'cyclejs-test-helpers';


test('create empty component', t => {

  t.plan(2);

  const response = Response();

  const sinks = response({
    HTTP: {
      select: (...args) => {

        t.deepEqual(
          args,
          [undefined]
        );

        return $.empty();
      }
    }
  });

  t.deepEqual(
    keys(sinks),
    ['response$']
  );
});

test.only('create response with category', t => {

  return withTime(Time => {

    const property = Jsverify.forall(
      diagramArbitrary,
      diagramArbitrary,
      Jsverify.json,
      (diagramA, diagramB, json) => {

        const response$ = Time.diagram(diagramA)
          .mapTo({
            body: json,
            request: {
              category: 'ga'
            }
          });

        const response = Response({
          category: 'ga',
        });

        const sinks = response({
          HTTP: {
            select: (...args) => {

              t.deepEqual(
                args,
                ['ga']
              );

              return response$.map($.of);
            }
          }
        });

        t.deepEqual(keys(sinks), ['response$']);

        Time.assertEqual(
          sinks.response$,
          response$
            .mapTo({
              body: json,
              request: {
                category: 'ga'
              }
            }),
          t.deepEqual.bind(t)
        );

        return true;
      });

    Jsverify.assert(property, { tests: 50 });

  })();

});
