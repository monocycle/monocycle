# 4.0.0 (2020-11-09)


### Bug Fixes

* **image:** make package esm compatible ([dddf9a0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/dddf9a05f2efbd56d2c599fe4ef1c7d0855d2d6e))


### BREAKING CHANGES

* **image:** maybe



# 3.12.0 (2020-10-07)


### Bug Fixes

* **image:** update dependencies ([6b5f6f6](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6b5f6f6619ad8eda3ea56194510c00ad10c29f87))



# 3.11.0 (2020-05-19)


### Bug Fixes

* **image:** remove type:module ([0009f75](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0009f75a89ded22308704065ef88ff891c658500))
* **image:** update deps ([3f55e14](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3f55e14405cc7d2911aa043577c420468daa0b1e))



# 3.10.0 (2020-05-05)


### Bug Fixes

* **image:** prevent fouc bug ([1eb5731](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/1eb573165074f4244578986a325a3d12043fdea2))



# 3.9.0 (2020-05-04)


### Bug Fixes

* **image:** set viewOptions even in server mode ([c2878b4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c2878b4e5909092024aef3fd89f3084d8e6c5dab))



# 3.8.0 (2020-05-04)


### Bug Fixes

* **image:** update dependencies ([1160ab0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/1160ab0fc0ff620160cea51c58fff1cc87be9450))



# 3.7.0 (2020-05-04)


### Features

* **image:** improve Image ([221afc8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/221afc824e38f1b6550a198f6b7592cd52c98d7a))



# 3.6.0 (2020-03-10)


### Bug Fixes

* **image:** repair Image ([c27a92c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c27a92cb0e6e77fad5d5d94b58005b184c6c6581))



# 3.5.0 (2020-03-06)


### Bug Fixes

* **image:** depends on loader@0.14 ([9cc52ff](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9cc52ff9722013b481a4707d644986c5daad6ebd))



# 3.4.0 (2020-03-05)


### Bug Fixes

* **image:** stop using Fill ([4d7d904](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4d7d90421b62275c16355320cfaf8c91d19de149))



# 3.3.0 (2020-02-10)


### Bug Fixes

* **image:** remove filter ([a4641d4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/a4641d46402948cc7b313f8ee2c9449d33829607))



# 3.2.0 (2020-02-10)


### Bug Fixes

* **image:** url only accepts non empty string ([94e505e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/94e505e02d2c1cbf9e85d2a4d4085d2e3fcd123d))



# 3.1.0 (2020-02-08)


### Bug Fixes

* **image:** depends on dom@24.8 ([ff1ef3c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ff1ef3c5ac8ab1372fa42480c488cc533159be4f))



# 3.0.0 (2020-02-08)


### Features

* **image:** replace urlKey option with extractUrl ([d2db71c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d2db71c012008995fa0a09ae81fc4428dd7cd8bd))


### BREAKING CHANGES

* **image:** yep



# 2.8.0 (2020-01-20)


### Bug Fixes

* **image:** depends on dom@24.6 ([b3b25f0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b3b25f0f80854acdcf856da5a286b5e49a86e404))



# 2.7.0 (2020-01-20)


### Bug Fixes

* **image:** depends on isolated@4.5 ([9808e69](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9808e6903e3881fe6bcd34ee5db8778e1a32aacc))



# 2.6.0 (2020-01-20)


### Bug Fixes

* **image:** depends on component@5.5 ([3aa84f9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3aa84f928138d45977e5cf47019af399f91aca59))



# 2.5.0 (2020-01-20)


### Bug Fixes

* **image:** depends on listener@5.5 ([ac1371c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ac1371ceee109d3e75ee2a91cd294a3331ec01ee))



# 2.4.0 (2020-01-20)


### Bug Fixes

* **image:** depends on component@5.4 ([fbbe9fc](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fbbe9fc8eace1e466ea09b1e189c52d373ce21e8))



# 2.3.0 (2019-12-18)


### Bug Fixes

* **image:** depends on loader@0.7 ([3566480](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/356648058b8497df5eec79fab8c2ad25d01d3f3b))



# 2.2.0 (2019-12-18)


### Bug Fixes

* **image:** repair imageProgress lens ([f7ac8c4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f7ac8c4c2ede457369050ddcf6a61293437fc68d))



# 2.1.0 (2019-12-13)


### Bug Fixes

* **image:** merge viewOptions in noscript image ([d3a8da1](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d3a8da11524fd9945dca0d34f00f19680a52fb63))



# 2.0.0 (2019-12-10)


### Features

* **image:** allow to customize image viewOptions ([aaa43aa](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/aaa43aaee9be0b056d8404b3265f94f4f7d6f961))


### BREAKING CHANGES

* **image:** yes



# 1.2.0 (2019-12-07)


### Bug Fixes

* **image:** repair noscript img ([5f1f7f9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5f1f7f9932694af5f53cea7f4b52713e17989e15))



# 1.1.0 (2019-12-05)


### Bug Fixes

* **image:** progress default to 0 when no blob ([8125ac0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8125ac0393dd0dc3d6a4c127fe5fc6bb2a11ee08))



# 1.0.0 (2019-12-04)


### Features

* **image:** use noscript tag ([41c706a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/41c706a3f18863d37e7e51815162b4ef65457669))


### BREAKING CHANGES

* **image:** yes



# 0.11.0 (2019-12-03)


### Bug Fixes

* **image:** refactor Image ([3ba707c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3ba707cc9a542a36090c5210b4b4e5d1c75d3228))



# 0.10.0 (2019-12-03)


### Bug Fixes

* **image:** repair Image ([e5a41ad](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/e5a41adaa0167fd0ce9b50e66d9a6839d6af32f9))



# 0.9.0 (2019-12-02)


### Bug Fixes

* **image:** update deps ([a6cf6b1](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/a6cf6b1b97f4ebee3be7ecf55371e88517d9ba62))



# 0.8.0 (2019-12-02)


### Bug Fixes

* **image:** add lock file ([4a4c0e8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4a4c0e8a6875cae4734df452bf102f2cde0f20bd))
* **image:** depends on dom@23.8 ([20fea03](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/20fea032c9f9909b0ce0338a25abadc3d84f7a91))



# 0.7.0 (2019-11-29)


### Bug Fixes

* **image:** always display ProgressCircular ([8d403df](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8d403df348a8997793bb30f5cf6fb584cad4d122))



# 0.6.0 (2019-11-29)


### Bug Fixes

* **image:** remove filter ([3d8c71c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3d8c71ca4625b916e113d8a18402c79bbfa874f2))



# 0.5.0 (2019-11-28)


### Bug Fixes

* **image:** make urlListener deterministic ([e6d54f1](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/e6d54f1cb95fffd840f357ee4a4434ad56872ce9))



# 0.4.0 (2019-11-28)


### Bug Fixes

* **image:** depends on dom@23.6 ([4936290](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/4936290d92b6b00d501151b5b210200c22b026ff))



# 0.3.0 (2019-11-27)


### Bug Fixes

* **image:** hang bug ([328de4f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/328de4f6f00e796dd194c37c01346c57784eb13c))



# 0.2.0 (2019-11-26)


### Bug Fixes

* **image:** depends on loader@0.3 ([1785837](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/1785837a9b3b09f0dac5c9cf4863ccc786dd6284))



# 0.1.0 (2019-11-26)


### Features

* **image:** create package ([03fddf0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/03fddf03e7eba9e9dee5b14ce4ce83782a42fa9c))



