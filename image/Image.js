import dropRepeats from 'xstream/extra/dropRepeats.js';
import always from 'ramda/src/always.js';
import defaultTo from 'ramda/src/defaultTo.js';
import prop from 'ramda/src/prop.js';
import objOf from 'ramda/src/objOf.js';
import unless from 'ramda/src/unless.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import isEmpty from 'ramda/src/isEmpty.js';
import map from 'ramda/src/map.js';
import lensProp from 'ramda/src/lensProp.js';
import propSatisfies from 'ramda/src/propSatisfies.js';
import over from 'ramda/src/over.js';
import filter from 'ramda/src/filter.js';
import identity from 'ramda/src/internal/_identity.js';
import isString from 'ramda-adjunct/src/isString.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import isUndefined from 'ramda-adjunct/src/isUndefined.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithView, View, coerceViewOptions } from '@monocycle/dom/src/View';
import { row } from '@monocycle/dom/src/Box/Row.js';
import { withLayerParent } from '@monocycle/dom/src/LayerParent';
import { withLayer } from '@monocycle/dom/src/Layer';
import { withFill } from '@monocycle/dom/src/Fill';
import { WithLoader } from '@monocycle/loader';
import { WithProgressCircular } from '@monocycle/dom/src/ProgressCircular/index.js';
import { WithStyledView } from '@monocycle/dom/src/StyledView';
import helpers from '@monocycle/dom/src/helpers.js';
import { WithIsolated } from '@monocycle/isolated';
import { centerCenter } from 'monostyle/src/flex.js';
import { pipe } from '@monocycle/component/lib/utilities/pipe.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { WithDynamic2 } from '@monocycle/dynamic';
import { ImageStyle } from './Image.style.js';
const { Img } = helpers;





const isBrowser = typeof window !== 'undefined';

const ensureString = pipe(
  identity,
  unless(isString, always(''))
);

const coerceImageOptions = pipe(
  coerceViewOptions,
  over(lensProp('img'), pipe(
    ensurePlainObj,
    over(lensProp('style'), pipe(
      ensurePlainObj,
      mergeDeepRight({
        opacity: '0',
        transition: 'all 1s',
        delayed: {
          opacity: '1'
        }
      })
    )),
  )),
);

export const WithImage = pipe(
  coerceImageOptions,
  over(lensProp('extractUrl'), unless(isFunction, always(prop('url')))),
  over(lensProp('blobKey'), unless(isNonEmptyString, always('blob'))),
  over(lensProp('progressKey'), unless(isNonEmptyString, always('progress'))),
  over(lensProp('img'), ensurePlainObj),
  ({ blobKey, extractUrl, progressKey, img: imgOptions, ...viewOptions }) => {

    const urlListener = (sinks, sources) => {
      return sources.state.stream
        .filter(propSatisfies(isUndefined, blobKey))
        .map(extractUrl)
        .compose(dropRepeats());
    };

    return pipe(
      isBrowser
        ? WithDynamic2({
          from: (sinks, sources) => {
            return sources.state.stream
              .map(prop(blobKey))
              .startWith()
              // .compose(sources.Time.debounce(50))
              .compose(dropRepeats())
              .map(pipe(
                ensureString,
                unless(isEmpty, pipe(
                  objOf('src'),
                  objOf('attrs'),
                  objOf('img'),
                )),
                mergeDeepRight({ img: imgOptions, ...viewOptions }),
              ))
              .map(({ img: imgOptions, ...viewOptions }) => {
                return View({
                  ...viewOptions,
                  has: [
                    !imgOptions ? undefined : Img(imgOptions).map(pipe(
                      withLayer,
                    )),

                    row.map(pipe(
                      withLayer,
                      WithProgressCircular({
                        style: {
                          ...centerCenter,
                          display: 'flex',
                        }
                      }),
                      WithIsolated({
                        state: {
                          get: pipe(
                            prop(progressKey),
                            defaultTo(0)
                          ),
                        },
                        '*': null,
                      }),
                    )),
                  ]
                })(sources);
              })
              .remember();
          }
        })
        : WithView({
          ...viewOptions,
          has: [
            View({
              sel: 'noscript',
              has: Img({
                from: pipe(
                  urlListener,
                  map(pipe(
                    objOf('src'),
                    objOf('attrs'),
                    mergeDeepRight(imgOptions),
                    objOf('data'),
                  )),
                ),
              }).map(pipe(
                withLayer,
              )),
            })
          ]
        }),

      withFill,
      withLayerParent,
      WithStyledView({
        name: 'Image',
        styles: ImageStyle
      }),

      WithLoader({
        urlKey: blobKey,
        from: pipe(
          urlListener,
          filter(isNonEmptyString),
          map(objOf('url')),
        ),
      }),
    );
  }
);

WithImage.coerce = coerceImageOptions;

export const withImage = WithImage();

export const Image = behaviorToFactory(WithImage);

export const image = Image();