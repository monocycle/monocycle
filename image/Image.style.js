import pipe from 'ramda/src/pipe.js';
import prop from 'ramda/src/prop.js';
import mergeDeepRight from 'ramda/src/mergeDeepRight.js';
import lensProp from 'ramda/src/lensProp.js';
import over from 'ramda/src/over.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj';




export const ImageStyle = pipe(
  ensurePlainObj,
  over(lensProp('theme'), pipe(
    prop('image'),
    ensurePlainObj,
    mergeDeepRight({
      colors: {
        background: 'var(--darkerColor, #333)'
      }
    })
  )),
  ({ theme: { colors } }) => ({
    backgroundColor: colors.background,
    overflow: 'hidden',
    '& > noscript': {
      width: '100%',
      height: '100%',
    },
    img: {
      margin: 'auto',
      maxHeight: '100%',
      maxWidth: '100%',
    }
  })
);
