# 5.4.0 (2020-11-09)


### Bug Fixes

* **isolated:** depends on component@10.1 ([b0c10b6](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b0c10b6f45c611cbc642fe83680245fbc680ee9d))



# 5.3.0 (2020-11-05)


### Bug Fixes

* **isolated:** import cjs default on node ([8485e8d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8485e8d5021f8c53cf64d239b8d6a3c354d15230))



# 5.2.0 (2020-11-04)


### Bug Fixes

* **isolated:** correctly destructure isolate ([9043f6b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9043f6bb93350571f706215469ace6dd88530c24))



# 5.1.0 (2020-11-04)


### Bug Fixes

* **isolated:** destructure isolate from cjs module ([0a66a2a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0a66a2aeee093aa874b1859c5333dbcd68163d0d))



# 5.0.0 (2020-11-04)


### Bug Fixes

* **isolated:** make package node esm compatible ([0634fd3](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0634fd3e34b242eb75f127544d7161c06be867a4))


### BREAKING CHANGES

* **isolated:** maybe



# 4.9.0 (2020-10-07)


### Bug Fixes

* **isolated:** depends on component@8.6 ([cfff008](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/cfff008322caae8c73324bc809284e45fcea2273))



# 4.7.0 (2020-05-19)


### Bug Fixes

* **isolated:** remove type:module ([bf16c58](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/bf16c589d865dd1d3d1f1385b6d8e7ec82754f9c))



# 4.6.0 (2020-02-18)


### Bug Fixes

* **isolated:** update dependencies ([6b86c1f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6b86c1f7f942cb08431497b495341da5d34a5375))



# 4.5.0 (2020-01-20)


### Bug Fixes

* **isolated:** always forward properties ([74ae6e5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/74ae6e53d7466cacec69f5ab71579eeab3f9c222))



# 4.4.0 (2020-01-20)


### Bug Fixes

* **isolated:** always forward properties ([067142f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/067142fa0154a0430906b405b6178ae309b9c7aa))



# 4.3.0 (2020-01-20)


### Bug Fixes

* **isolated:** depends on component@5.4 ([5118579](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/511857930f9ba6ec93f53279b790ad3d756b583a))



# 4.2.0 (2019-12-02)


### Bug Fixes

* **isolated:** update dependencies ([f23d0f2](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f23d0f287c915bdea9888468a3097c17a9ab0f99))



# 4.1.0 (2019-12-02)


### Bug Fixes

* **isolated:** add lock file ([b2ee5e5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b2ee5e52e73690e71726f5615b57b5425bfdefff))



# 4.0.0 (2019-11-11)


### Features

* **isolated:** hello es modules ([2a95639](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/2a9563935dfa3eccc955f5d58273bbc5ecc6eed3))


### BREAKING CHANGES

* **isolated:** yes



## 3.5.0 (2019-04-08)

* fix(isolated): depend on component@4.0 ([ccc8df5](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/ccc8df5))



## 3.4.0 (2019-03-23)

* fix(isolated): depend on component@3.6 ([c946102](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/c946102))



## 3.3.0 (2019-03-23)

* fix(isolated): depend on component@3.5 ([e64ab9e](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/e64ab9e))



## 3.1.0 (2019-03-23)

* fix(isolated): depend on component@3.4 ([8964beb](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/8964beb))
* chore(isolated): remove default exports ([ef86861](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/ef86861))



## 3.1.0 (2019-03-09)

* fix(isolated): depend on component@3.2 ([6b5992e](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/6b5992e))



## 3.0.0 (2019-03-03)

* chore(isolated): add dev tasks ([a4d2fa3](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/a4d2fa3))
* fix(isolated): use component@3.1 ([40a49c0](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/40a49c0))


### BREAKING CHANGE

* Isolated now depends on Component


## 2.2.0 (2019-02-25)

* fix(isolated): add node_modules to .npmignore ([3f0dda8](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/3f0dda8))



## 2.1.0 (2019-02-25)

* fix(isolated): update dependencies ([5b95a03](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/5b95a03))



## 2.0.0 (2019-02-03)

* fix(isolated): update @component ([88ca0df](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/88ca0df))
* chore(isolated): add files ([95d2be5](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/95d2be5))


### BREAKING CHANGE

* yes


