import isolateModule from '@cycle/isolate';
import unless from 'ramda/src/unless.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import pipe from 'ramda/src/pipe.js';
import { makeEmptyObject } from '@monocycle/component/lib/utilities/empty.js';
import { assign } from '@monocycle/component/lib/utilities/assign.js';
import { Component } from '@monocycle/component';
import identity from 'ramda/src/internal/_identity.js';



const isolate = isolateModule.default || isolateModule;

export const WithIsolated = scope => {

  return pipe(
    identity,
    unless(isFunction, makeEmptyObject),
    component => {

      return Component(assign(component)(sources => {
        return isolate(component, scope)(sources);
      }));
    },
  );
};

export const withIsolated = WithIsolated();