# 6.1.0 (2020-11-09)


### Bug Fixes

* **listener:** depends on component@10.1 ([6eeddd9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6eeddd97fd6c51c425a24372ffa78276770e9088))



# 6.0.0 (2020-11-08)


### Bug Fixes

* **listener:** make package node esm compatible ([09213d6](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/09213d68ebb2e674368cafa0c0f0253eca80236d))


### BREAKING CHANGES

* **listener:** maybe



# 5.14.0 (2020-11-08)


### Bug Fixes

* **listener:** revert 1055dc81 ([683c732](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/683c732c482f360a5eef4f17a1f1eabe6a0f0c7f))



# 5.12.0 (2020-11-04)


### Bug Fixes

* **listener:** make package node esm compatible ([1055dc8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/1055dc818395a5ff54649426f51bf2611fdf9dc5))



# 5.11.0 (2020-10-07)


### Bug Fixes

* **listener:** depends on component@8.6 ([b56ad9e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/b56ad9e4a5fe96f98fce704cdbc7ca37ce68c3e6))



# 5.10.0 (2020-05-19)


### Bug Fixes

* **listener:** remove type:module ([8765d17](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/8765d174f8d7300b1c2a812e20f1724885f7cee3))



# 5.9.0 (2020-02-18)


### Bug Fixes

* **listener:** update dependencies ([3f7331f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3f7331f5eb480e49a1f9d1c498b52ef3604c4a2f))



# 5.8.0 (2020-02-18)


### Bug Fixes

* **listener:** update dependencies ([3f7331f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3f7331f5eb480e49a1f9d1c498b52ef3604c4a2f))



# 5.7.0 (2020-02-18)


### Bug Fixes

* **listener:** update dependencies ([3f7331f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3f7331f5eb480e49a1f9d1c498b52ef3604c4a2f))



# 5.6.0 (2020-01-20)


### Bug Fixes

* **listener:** always forward properties ([47b5c8e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/47b5c8e311dd3b0b3af5a2ac2c9b9eea954f7e74))



# 5.5.0 (2020-01-20)


### Bug Fixes

* **listener:** always forward properties ([c95ce36](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c95ce3610f51299b94a95811af09e6630cc246f5))



# 5.4.0 (2020-01-20)


### Bug Fixes

* **listener:** depends on component@5.4 ([83a416b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/83a416bdd600e732e95b006c3164a9390d01d35e))



# 5.3.0 (2019-12-12)


### Features

* **listener:** improve error logging ([0420c3c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0420c3c1bd0486c931c7c10ea491d656a1fcbb38))



# 5.2.0 (2019-12-02)


### Bug Fixes

* **listener:** update dependencies ([ad8688b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ad8688b5a96053dcfe931caf438a31506eae3bb2))



# 5.1.0 (2019-12-02)


### Bug Fixes

* **listener:** add lock file ([9de3b7e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/9de3b7ede08573a81f61c305269a51af6ba4670d))



# 5.0.0 (2019-11-11)


### Features

* **listener:** hello es modules ([efe987f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/efe987f7bc850851f1ae1c9a9d6ac1b76f35c312))


### BREAKING CHANGES

* **listener:** yes



# 4.0.0 (2019-11-11)


### Features

* **listener:** hello es modules ([efe987f](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/efe987f7bc850851f1ae1c9a9d6ac1b76f35c312))


### BREAKING CHANGES

* **listener:** yes



## 3.8.0 (2019-05-11)

* fix(listener): depend on component@4.1 ([32e029a](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/32e029a))
* test(listener): add missing sink key test ([6648630](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/6648630))



## 3.7.0 (2019-04-08)

* fix(listener): depend on component@4.0 ([74f094a](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/74f094a))
* chore(listener): format ([eb7f8a9](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/eb7f8a9))



## 3.6.0 (2019-04-07)

* feat(listener): expose a Listener factory ([86d0ad4](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/86d0ad4))



## 3.5.0 (2019-03-31)

* fix(listener): depend on component@3.10 ([6a503f8](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/6a503f8))
* feat(listener): "from" option accepts arrays ([cc5e259](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/cc5e259))
* chore(listener): replace void with undefined ([1f75ee2](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/1f75ee2))



## 3.4.0 (2019-03-27)

* fix(listener): default empty component was buggy ([127a872](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/127a872))
* fix(listener): depend on component@3.8 ([09fec10](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/09fec10))



## 3.3.0 (2019-03-23)

* fix(listener): depend on component@3.5 ([a4c648c](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/a4c648c))
* chore(listener): bump ([0f71d6c](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/0f71d6c))



## 3.1.0 (2019-03-23)

* fix(listener): depend on component@3.4 ([c23cde8](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/c23cde8))
* chore(listener): remove default exports ([f59a0a6](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/f59a0a6))
* chore(listener): use correct identity function ([23985c8](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/23985c8))



## 3.1.0 (2019-03-09)

* fix(listener): depend on component@3.2 ([cc41030](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/cc41030))



## 3.0.0 (2019-03-02)

* feat(listener): upgrade to component@3.1 ([a629d94](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/a629d94))


### BREAKING CHANGE

* "Component" option is removed


## 2.1.0 (2019-02-25)

* fix(listener): add node_modules to .npmignore ([33d8cd8](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/33d8cd8))
* chore(listener): remove unused npm scripts ([64dddf1](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/64dddf1))



## 2.0.0 (2019-02-03)

* fix(listener): update @component ([84934d6](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/84934d6))
* chore(listener): add files ([72f54a2](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/72f54a2))


### BREAKING CHANGE

* yes


