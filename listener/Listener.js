import { Stream as $ } from 'xstream';
import identity from 'ramda/src/internal/_identity.js';
import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import defaultTo from 'ramda/src/defaultTo.js';
import unless from 'ramda/src/unless.js';
import prop from 'ramda/src/prop.js';
import when from 'ramda/src/when.js';
import either from 'ramda/src/either.js';
import always from 'ramda/src/always.js';
import map from 'ramda/src/map.js';
import has from 'ramda/src/has.js';
import lt from 'ramda/src/lt.js';
import juxt from 'ramda/src/juxt.js';
import objOf from 'ramda/src/objOf.js';
import applyTo from 'ramda/src/applyTo.js';
import filter from 'ramda/src/filter.js';
import apply from 'ramda/src/apply.js';
import reduce from 'ramda/src/reduce.js';
import ifElse from 'ramda/src/ifElse.js';
import isEmpty from 'ramda/src/isEmpty.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import castArray from 'ramda-adjunct/src/ensureArray.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import { makeEmptyObject } from '@monocycle/component/lib/utilities/empty.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { Component } from '@monocycle/component';
import { assign } from '@monocycle/component/lib/utilities/assign.js';





const defaultCombine = (before, sink$) => sink$;

export const SafeFrom = pipe(identity, either, applyTo($.empty));

export const parseFrom = pipe(
  castArray,
  map(when(isNonEmptyString, pipe(
    key => object => {
      if (!has(key, object))
        throw new Error(`Unknown '${key}' sink`);
      return prop(key)(object);
    },
  ))),
  filter(isFunction),
  ifElse(isEmpty,
    always($.empty),
    ifElse(pipe(prop('length'), lt(1)),
      froms => pipe(
        juxt(froms),
        apply($.merge),
      ),
      prop(0),
    ),
  ),
);

const coerce = unless(isPlainObj, objOf('from'));

export const WithListener = pipe(
  coerce,
  over(lensProp('from'), pipe(
    parseFrom,
    defaultTo($.empty)
  )),
  over(lensProp('to'), pipe(
    castArray,
    filter(isNonEmptyString)
  )),
  over(lensProp('combine'), unless(isFunction, always(defaultCombine))), // TODO display a warning
  ({ from, to, combine }) => {

    const safeFrom = SafeFrom(from);

    return pipe(
      identity,
      unless(isFunction, makeEmptyObject),
      component => Component(assign(component)(sources => {

        let sinks = component(sources);

        const event$ = safeFrom(sinks, sources);

        if (isEmpty(to))
          return event$.addListener(identity) || sinks;

        return reduce(
          (sinks, to) => {

            return {
              ...sinks,
              [to]: !sinks[to]
                ? event$
                : combine(sinks[to], event$)
            };
          },
          sinks,
          to
        );
      }))
    );
  }
);

export const Listener = behaviorToFactory(WithListener);

WithListener.coerce = Listener.coerce = coerce;
