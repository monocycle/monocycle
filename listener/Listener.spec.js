import test from 'ava';
import { WithListener } from './Listener.js';
import jsc from 'jsverify';
import keys from 'ramda/src/keys.js';
import always from 'ramda/src/always.js';
import { Stream as $ } from 'xstream';
import { diagramArbitrary as diagramArb, withTime } from 'cyclejs-test-helpers';





const testsOptions = { tests: 100 };

test('listens on a source', t => {

  return withTime(Time => {

    const property = jsc.forall(diagramArb, diagramArb, (a, b) => {

      const streamA = Time.diagram(a);
      const streamB = Time.diagram(b);

      const withListener = WithListener({
        from: (sinks, sources) => $.merge(sources.mySource, streamB),
        to: 'mySink'
      });

      const component = withListener();

      const sinks = component({ mySource: streamA });

      t.deepEqual(
        keys(sinks),
        ['mySink']
      );

      Time.assertEqual(
        $.merge(streamA, streamB),
        sinks.mySink,
        t.is.bind(t)
      );
      return true;
    });

    jsc.assert(property, testsOptions);
  })();
});

test('listens on a sink', t => {

  return withTime(Time => {

    const property = jsc.forall(diagramArb, diagramArb, (a, b) => {

      const diagramA = Time.diagram(a);
      const diagramB = Time.diagram(b);

      const withListener = WithListener({
        from: sinks => $.merge(sinks.myPreviousSink, diagramB),
        to: 'mySink'
      });

      const component = withListener(always({
        myPreviousSink: diagramA
      }));

      const sinks = component();
      keys(sinks);

      t.deepEqual(
        keys(sinks),
        ['myPreviousSink', 'mySink']
      );

      Time.assertEqual(
        diagramA,
        sinks.myPreviousSink,
        t.is.bind(t)
      );

      Time.assertEqual(
        $.merge(diagramA, diagramB),
        sinks.mySink,
        t.is.bind(t)
      );
      return true;
    });

    jsc.assert(property, testsOptions);
  })();
});

test('listens on sink by key', t => {

  return withTime(Time => {

    const property = jsc.forall(diagramArb, (a) => {

      const diagramA = Time.diagram(a);

      const withListener = WithListener({
        from: 'myPreviousSink',
        to: 'mySink'
      });

      const component = withListener(always({
        myPreviousSink: diagramA
      }));

      const sinks = component();

      t.deepEqual(
        keys(sinks),
        ['myPreviousSink', 'mySink']
      );

      Time.assertEqual(
        sinks.mySink,
        sinks.myPreviousSink,
        t.is.bind(t)
      );

      Time.assertEqual(
        diagramA,
        sinks.myPreviousSink,
        t.is.bind(t)
      );
      return true;
    });

    jsc.assert(property, testsOptions);
  })();
});

test.skip('throws when missing sink key', t => {

  const withListener = WithListener({
    from: 'missingSinkKey',
    to: 'mySink'
  });

  const component = withListener(always({
    ga: $.of(42)
  }));

  t.throws(component, `Unknown 'missingSinkKey' sink`);
});

test('listens from multiple observables', t => {

  return withTime(Time => {

    const property = jsc.forall(diagramArb, diagramArb, (a, b) => {

      const diagramA = Time.diagram(a);
      const diagramB = Time.diagram(b);

      const withListener = WithListener({
        from: [
          'myPreviousSink',
          () => diagramB,
        ],
        to: 'mySink'
      });

      const component = withListener(always({
        myPreviousSink: diagramA
      }));

      const sinks = component();

      t.deepEqual(
        keys(sinks),
        ['myPreviousSink', 'mySink']
      );

      Time.assertEqual(
        diagramA,
        sinks.myPreviousSink,
        t.is.bind(t)
      );

      Time.assertEqual(
        sinks.mySink,
        $.merge(
          diagramA,
          diagramB,
        ),
        t.is.bind(t)
      );

      return true;
    });

    jsc.assert(property, testsOptions);
  })();
});

test('overrides a sink', t => {

  return withTime(Time => {

    const property = jsc.forall(diagramArb, diagramArb, (a, b) => {

      const diagramA = Time.diagram(a);
      const diagramB = Time.diagram(b);

      const withListener = WithListener({
        from: () => diagramB,
        to: 'mySink'
      });

      const component = withListener(() => ({ mySink: diagramA }));

      const sinks = component();

      t.deepEqual(
        keys(sinks),
        ['mySink']
      );

      Time.assertEqual(
        diagramB,
        sinks.mySink,
        t.is.bind(t)
      );
      return true;
    });

    jsc.assert(property, testsOptions);
  })();
});

test('combines with another sink', t => {

  return withTime(Time => {

    const property = jsc.forall(diagramArb, diagramArb, (a, b) => {

      const diagramA = Time.diagram(a);
      const diagramB = Time.diagram(b);

      const withListener = WithListener({
        from: () => diagramB,
        combine: $.merge,
        to: 'mySink'
      });

      const component = withListener(always({ mySink: diagramA }));

      const sinks = component();

      t.deepEqual(keys(sinks), ['mySink']);

      // assert(equals(keys(sinks), ['mySink']), 'yo')

      Time.assertEqual(
        $.merge(diagramA, diagramB),
        sinks.mySink,
        t.is.bind(t)
      );
      return true;
    });

    jsc.assert(property, testsOptions);
  })();
});