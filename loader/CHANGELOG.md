# 1.0.0 (2020-11-09)


### Bug Fixes

* **loader:** mazke package esm compatible ([3467510](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/3467510efc4999ebe06708c3e16015a963096bfc))


### BREAKING CHANGES

* **loader:** maybe



# 0.16.0 (2020-10-07)


### Bug Fixes

* **loader:** update dependencies ([7c9473d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/7c9473d87fb6e285bf6ef5fb6cb6349b6021519d))



# 0.15.0 (2020-05-19)


### Bug Fixes

* **loader:** remove type:module ([64b78a9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/64b78a9d9f48b8f8c87cb4a7d46a0f1d15f68262))



# 0.14.0 (2020-03-06)


### Bug Fixes

* **loader:** update dependencies ([d82aa20](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d82aa20a9b7181208f1d588d8acb816be358ff90))



# 0.13.0 (2020-03-06)


### Bug Fixes

* **loader:** enable lazy option ([817cc9d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/817cc9d313d0ede8c9962d989758bcab13fb09c6))



# 0.12.0 (2020-02-10)


### Bug Fixes

* **loader:** define a replaceError ([610ffcd](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/610ffcd97cae0c8047bc3df9b0a931e868ff7fee))



# 0.11.0 (2020-01-20)


### Bug Fixes

* **loader:** depends on component@5.5 ([302c02a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/302c02ab05b676e270c1fdd404efc910141559bc))



# 0.10.0 (2020-01-20)


### Bug Fixes

* **loader:** depends on listener@5.5 ([31678e8](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/31678e8c7ddffe1d364d9e8cbb6cf6125ed81931))



# 0.9.0 (2020-01-20)


### Bug Fixes

* **loader:** depends on component@5.4 ([73f1764](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/73f17648877904567b856eec5594fbfb80097cc7))



# 0.8.0 (2020-01-20)


### Bug Fixes

* **loader:** depends on component@5.4 ([73f1764](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/73f17648877904567b856eec5594fbfb80097cc7))



# 0.7.0 (2019-12-18)


### Bug Fixes

* **loader:** set progress false in loaded ([267fda7](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/267fda72481cfd90a9fc58fc0b1d0713051c8f12))



# 0.6.0 (2019-12-02)


### Bug Fixes

* **loader:** update deps ([61a0f30](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/61a0f30a4032190a4fd446b17c8d11cf21cbb26f))



# 0.5.0 (2019-12-02)


### Bug Fixes

* **loader:** add lock file ([92c7101](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/92c71019e08463715951797dffb5883aa42cf26d))



# 0.4.0 (2019-12-02)


### Bug Fixes

* **loader:** add lock file ([92c7101](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/92c71019e08463715951797dffb5883aa42cf26d))



# 0.3.0 (2019-11-26)


### Bug Fixes

* **loader:** add missing dependencies ([842c78a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/842c78abb3d829179868134a1c9f28ffacdf3414))



# 0.2.0 (2019-11-26)


### Features

* **loader:** create package ([6ccd616](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6ccd61673dfe4854b508a94cc211305f3deb5a99))



