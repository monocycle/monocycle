import $ from 'xstream';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import { WithTransition } from '@monocycle/state';
import dropRepeats from 'xstream/extra/dropRepeats.js';
import { WithRequest } from '@monocycle/http/Request.js';
import { WithListener } from '@monocycle/listener';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { Component } from '@monocycle/component';
import pipe from 'ramda/src/pipe.js';
import allPass from 'ramda/src/allPass.js';
import prop from 'ramda/src/prop.js';
import always from 'ramda/src/always.js';
import unless from 'ramda/src/unless.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import equals from 'ramda/src/equals.js';
import propEq from 'ramda/src/propEq.js';
import assoc from 'ramda/src/assoc.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import identity from 'ramda/src/internal/_identity.js';





const withNothing = pipe(
  identity,
  unless(isFunction, always(Component.empty)),
);

const isBrowser = typeof window !== 'undefined';

export const WithLoader = !isBrowser
  ? always(withNothing)
  : pipe(
    ensurePlainObj,
    over(lensProp('urlKey'), unless(isNonEmptyString, always('url'))),
    over(lensProp('from'), unless(isFunction, always('load$'))),
    ({ from, urlKey }) => pipe(

      WithListener({ // loadResponse
        from: (sinks, sources) => {
          return sources.HTTP.select('load')
            .map(response$ => {
              return response$
                .replaceError(always($.of({})));
            })
            .flatten();
        },
        combine: $.merge,
        to: 'loadResponse$'
      }),
      WithListener({ // loading
        from: ({ loadResponse$ }) => {
          return loadResponse$
            .filter(propEq('direction', 'download'))
            .map(prop('percent'))
            .compose(dropRepeats(equals));
        },
        combine: $.merge,
        to: 'loading$'
      }),

      WithListener({ // loaded
        from: ({ loadResponse$ }) => {
          return loadResponse$
            .filter(allPass([
              propEq('statusCode', 200),
            ]))
            .map(prop('body'));
        },
        combine: $.merge,
        to: 'loaded$'
      }),

      WithTransition({ // loading
        from: 'loading$',
        label: 'loading',
        reducer: progress => pipe(
          ensurePlainObj,
          assoc('progress', progress)
        )
      }),

      WithTransition({ // loaded
        from: 'loaded$',
        label: 'loaded',
        reducer: blob => pipe(
          ensurePlainObj,
          assoc('progress', false),
          assoc(urlKey, window.URL.createObjectURL(blob)),
        )
      }),

      WithListener({
        from: $.empty,
        combine: $.merge,
        to: 'load$',
      }),

      WithRequest({
        responseType: 'blob',
        category: 'load',
        method: 'get',
        progress: true,
        lazy: true,
        combine: $.merge,
        from,
      }),
    )
  );

export const withLoader = WithLoader();

export const Loader = behaviorToFactory(WithLoader);

export const loader = Loader();