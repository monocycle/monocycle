# 5.1.0 (2020-11-09)


### Bug Fixes

* **state:** depends on component@10.1 ([e0843aa](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/e0843aa742e6a7649a39b5e68b22febb0a9a7662))



# 5.0.0 (2020-11-05)


### Bug Fixes

* **state:** make package esm compatible ([5aab4ce](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5aab4ceb29cfe64ce43d0521320bfd5707ce8a17))
* **state:** use xstream named export ([ccb753a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ccb753a464974405d8a2f612d164c42032e814e4))


### BREAKING CHANGES

* **state:** maube



# 4.9.0 (2020-10-07)


### Bug Fixes

* **state:** update dependencies ([65db85a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/65db85af9621cfc0c1150076a83e9c0633032d6e))



# 4.8.0 (2020-05-19)


### Bug Fixes

* **state:** remove type:module ([29497e0](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/29497e0f02657b782d3b6d3e5f871fdcd4de7af8))



# 4.7.0 (2020-02-29)


### Bug Fixes

* **state:** call log even if state hasn't changed ([72f33b9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/72f33b9a1c2d114d02b40094ad89c3bf04a47fef))



# 4.6.0 (2020-02-18)


### Bug Fixes

* **state:** update dependencies ([e2ec725](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/e2ec725941f3585bf65a39b9e36f5c48c32b4f33))



# 4.5.0 (2020-01-20)


### Bug Fixes

* **state:** depends on component@5.5 ([dc797f7](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/dc797f7b05d2849e124e52d5cc9e94b393f0a4f0))



# 4.4.0 (2020-01-20)


### Bug Fixes

* **state:** depends on listener@5.5 ([e776608](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/e77660865ad954b9db70cc4ed49d4391f88f7df5))



# 4.3.0 (2020-01-20)


### Bug Fixes

* **state:** depends on component@5.4 ([c5899f5](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/c5899f5080da45fb75fec7b18b927953b4b5b4e5))



# 4.2.0 (2019-12-02)


### Bug Fixes

* **state:** update deps ([5676cc9](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/5676cc9a9309d7612b6bd622d99baa8ac0bcc912))



# 4.1.0 (2019-12-02)


### Bug Fixes

* **state:** add lock file ([0e1be6d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/0e1be6de27f83e3e0a56364305f46aa726047416))



# 4.0.0 (2019-11-11)


### Features

* **state:** hello es modules ([e69e3ea](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/e69e3ea55f63c466e517615eeab8444918896451))


### BREAKING CHANGES

* **state:** yes



## 3.7.0 (2019-05-11)

* feat(state): improve Transition ([9dba0dd](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/9dba0dd))



## 3.6.0 (2019-04-08)

* fix(state): depend on component@4.0 ([02a9a63](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/02a9a63))
* chore(state): use the real identity function ([e769a34](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/e769a34))



## 3.5.0 (2019-03-31)

* fix(state): depend on component@3.10 ([952f05e](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/952f05e))
* feat(state): depend on listener@3.4 component@3.9 ([060f3a1](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/060f3a1))



## 3.4.0 (2019-03-23)

* fix(state): depend on component@3.6 ([52ef199](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/52ef199))



## 3.3.0 (2019-03-23)

* fix(state): depend on component@3.5 ([c3df3c7](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/c3df3c7))



## 3.2.0 (2019-03-23)

* fix(state): depend on component@3.4 listener@3.2 ([66cc473](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/66cc473))
* chore(state): remove default exports ([f4112a0](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/f4112a0))
* chore(state): use correct identity function ([4f80309](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/4f80309))



## 3.1.0 (2019-03-10)

* fix(state): upgrade Transition to component@3 ([60ee228](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/60ee228))



## 3.0.0 (2019-03-09)

* feat(state): depend on component@3.2 listener@3.2 ([302f933](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/302f933))


### BREAKING CHANGE

* depend on component@3


## 2.2.0 (2019-03-08)

* fix(state): invalid "main" in package.json ([b7bdf66](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/b7bdf66))



## 2.1.0 (2019-02-25)

* fix(state): add node_modules to .npmignore ([4634c9c](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/4634c9c))



## 2.0.0 (2019-02-04)

* chore(state): add files ([0384c47](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/0384c47))
* chore(state): update @listener and @symbols ([98e8fbe](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/98e8fbe))
* fix(state): update @component ([fc35b1d](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/fc35b1d))


### BREAKING CHANGE

* yes


