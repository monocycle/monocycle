import { Stream as $ } from 'xstream';
import unless from 'ramda/src/unless.js';
import objOf from 'ramda/src/objOf.js';
import identity from 'ramda/src/internal/_identity.js';
import over from 'ramda/src/over.js';
import equals from 'ramda/src/equals.js';
import lensProp from 'ramda/src/lensProp.js';
import when from 'ramda/src/when.js';
import map from 'ramda/src/map.js';
import partial from 'ramda/src/partial.js';
import __ from 'ramda/src/__.js';
import not from 'ramda/src/not.js';
import always from 'ramda/src/always.js';
import { makeEmptyObject } from '@monocycle/component/lib/utilities/empty.js';
import noop from 'ramda-adjunct/src/noop.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import isUndefined from 'ramda-adjunct/src/isUndefined.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import { WithListener, parseFrom, SafeFrom } from '@monocycle/listener';
import pipe from 'ramda/src/pipe.js';
import { Log } from '@monocycle/component/lib/utilities/log.js';





export const TransitionLog = pipe(
  Log,
  partial(__, [
    '%c',
    ['color: #32b87c'].join(';')
  ])
);

export const WithTransitionLogger = ({ log }) => !log ? identity : reducer => {

  return before => {

    const after = reducer(before);

    log(
      !equals(before, after)
        ? { before, after }
        : ''
    );

    return after;
  };
};

const coerce = pipe(
  when(isUndefined, makeEmptyObject),
  unless(isPlainObj, objOf('reducer')),
);

export const WithTransition = pipe(
  coerce,
  over(lensProp('label'), pipe(
    unless(isNonEmptyString, always('init'))
  )),
  over(lensProp('log'), unless(isFunction,
    always(noop)
  )),
  over(lensProp('to'), unless(isFunction,
    always('state')
  )),
  over(lensProp('combine'), unless(isFunction,
    always($.merge)
  )),
  over(lensProp('reducer'), unless(isFunction, pipe(
    always,
    always
  ))),
  over(lensProp('from'), pipe(
    when(not, always(pipe(noop, $.of))),
    parseFrom,
  )),
  ({ to, combine, label, from, reducer }) => {

    return WithListener({
      from: pipe(
        SafeFrom(from),
        map(pipe(
          reducer,
          WithTransitionLogger({
            log: TransitionLog(label)
          }),
        )),
      ),
      to,
      combine
    });
  },
);

WithTransition.coerce = coerce;