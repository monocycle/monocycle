import test from 'ava';
import jsc from 'jsverify';
import { withState } from '@cycle/state';
import keys from 'ramda/src/keys.js';
import when from 'ramda/src/when.js';
import unless from 'ramda/src/unless.js';
import split from 'ramda/src/split.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import both from 'ramda/src/both.js';
import always from 'ramda/src/always.js';
import isNonEmptyString from 'ramda-adjunct/src/isNonEmptyString.js';
import isInteger from 'ramda-adjunct/src/isInteger.js';
import isNonNegative from 'ramda-adjunct/src/isNonNegative.js';
import ensureArray from 'ramda-adjunct/src/ensureArray.js';
import { WithTransition } from './Transition.js';
import pipe from 'ramda/src/pipe.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { diagramArbitrary, withTime } from 'cyclejs-test-helpers';
import { isComponent } from '@monocycle/component';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';


const replaceAt = (index, replacement, string) => {
  return string.substr(0, index) + replacement + string.substr(index + replacement.length);
};

const generateLetter = (possibles = '', length = 1) => {
  let text = '';

  for (var i = 0; i < length; i++)
    text += possibles.charAt(Math.floor(Math.random() * possibles.length));

  return text;
};

const DiagramArbitrary = pipe(
  ensurePlainObj,
  over(lensProp('possibles'), pipe(
    when(isNonEmptyString, split('')),
    ensureArray
  )),
  over(lensProp('length'), pipe(
    unless(both(isInteger, isNonNegative), always(1)),
  )),
  ({
    possibles,
    length = 1
  } = {}) => {

    if (possibles.length < 1)
      return diagramArbitrary;

    return diagramArbitrary.smap((x) => {

      return x.split('')
        .reduce((before, char, i) => {

          if (char === '-')
            return before;

          return replaceAt(i, generateLetter(possibles.join(''), length), before);
        }, x);
    }, x => {
      return x.replace(new RegExp(possibles.join('|'), 'g'), 'x');//?
    });
  }
);

const diagramArb = DiagramArbitrary({
  possibles: 'abc'
});


const initStateMacro = (t, Spec) => {

  const {
    sources,
    input,
    expectedSinks,
    expectedState
  } = Spec(t);

  return withTime(Time => {
    const withTransition = WithTransition(input);

    const component = withTransition();//?

    t.true(isComponent(component));

    const sinks = withState(component)(sources);

    t.true(isPlainObj(sinks));

    t.deepEqual(keys(sinks), expectedSinks);

    Time.assertEqual(
      sources.state.stream,
      Time.diagram('x', { x: expectedState }),
      t.deepEqual.bind(t)
    );

    return true;
  })();
};

initStateMacro.title = (title = ''/* , input, expected */) => 'Set initial state' + (title ? ' ' + title : '');
initStateMacro.tests = 4;

test('given a value (neither a function nor a plain object)', initStateMacro, t => (t.plan(initStateMacro.tests), {
  input: 42,
  sources: {},
  expectedSinks: ['state'],
  expectedState: 42
}));

test('given a reducer factory', initStateMacro, t => (t.plan(initStateMacro.tests), {
  input: () => {

    const myReducer = (state = { zo: 41 }) => ({
      ...state,
      zo: state.zo + 1
    });

    return myReducer;
  },
  sources: {},
  expectedSinks: ['state'],
  expectedState: { zo: 42 }
}));

test('given an "options" plain object', initStateMacro, t => (t.plan(initStateMacro.tests), {
  input: {
    reducer: { ga: 1, bu: 2 }
  },
  sources: {},
  expectedSinks: ['state'],
  expectedState: { ga: 1, bu: 2 }
}));

const updateStateMacro = (t, options) => {

  const {
    // sources,
    input,
    expectedSinks,
    ExpectedState
  } = options;

  return withTime(Time => {

    const property = jsc.forall(diagramArb, a => {

      const streamA = Time.diagram(a);
      const sources = { mySource: streamA };
      const withTransition = WithTransition(input);
      const component = withTransition();

      t.true(isComponent(component));

      const sinks = withState(component)(sources);

      t.true(isPlainObj(sinks));
      t.deepEqual(keys(sinks), expectedSinks);

      Time.assertEqual(
        sources.state.stream,
        ExpectedState(streamA),
        t.is.bind(t)
      );

      return true;
    });

    jsc.assert(property, { tests: 50, size: 100 });
  })();
};

updateStateMacro.title = (title = ''/* , input, expected */) => 'Updates state' + (title ? ' (' + title + ')' : '');
updateStateMacro.tests = 4;

test(updateStateMacro, {
  input: {
    from: (sinks, sources) => sources.mySource,
    reducer: value => (state = '') => state + value
  },
  expectedSinks: ['state'],
  ExpectedState: stream => stream
    .fold((expected = '', character) => expected + character)
    .drop(1)
});


