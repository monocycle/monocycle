# 7.0.0 (2020-11-09)


### Bug Fixes

* **switch:** make package esm compatible ([d1cc74e](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d1cc74e5f4429bcd6106aa8df701f89c66e87f87))


### BREAKING CHANGES

* **switch:** maybe



# 6.8.0 (2020-10-07)


### Bug Fixes

* **switch:** update dependencies ([87272c3](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/87272c39dba3d34752749a036a31ce4473f719b2))



# 6.7.0 (2020-05-19)


### Bug Fixes

* **switch:** update deps ([cd91f7d](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/cd91f7d88b318bd67c82dbd9ad01d01f4d569afe))



# 6.6.0 (2020-01-20)


### Bug Fixes

* **switch:** update lockfile ([6914e98](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/6914e98d853a136e17c6352662bb42bb02f8b62d))



# 6.5.0 (2020-01-20)


### Bug Fixes

* **switch:** depends on component@5.5 ([bb09de4](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/bb09de4b3a90b000fcc05066c0e7f4565f548ea9))



# 6.4.0 (2020-01-20)


### Bug Fixes

* **switch:** depends on listener@5.5 ([41b7e0c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/41b7e0c6f6c7d52c4def2e435387e0f9272e5f7f))



# 6.3.0 (2020-01-20)


### Bug Fixes

* **switch:** depends on component@5.4 ([201eadc](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/201eadcf4687284e316f244ecd011ab81d81be43))



# 6.2.0 (2019-12-02)


### Bug Fixes

* **switch:** update deps ([13c9c03](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/13c9c03f7e630f81b06699d8abcc951dc5af7269))



# 6.1.0 (2019-12-02)


### Bug Fixes

* **switch:** add lock file ([df3dae3](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/df3dae37c59487a20390a67f5cf362ffe1c6b914))



# 6.0.0 (2019-11-11)


### Features

* **switch:** hello es modules ([d3fc6cf](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/d3fc6cfa673fdc31f90cd0d1831dfd79c071b6ba))


### BREAKING CHANGES

* **switch:** yes



# 5.0.0 (2019-10-18)


### Bug Fixes

* **switch:** use es modules and depends on dynamic@2.1 ([321c99a](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/321c99a21830bc5d40c9475c5b2419f6cb0baee9))


### BREAKING CHANGES

* **switch:** yes



## 4.1.0 (2019-06-08)

* fix(switch): depend on dynamic@1.1 ([fb4b4bb](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/fb4b4bb))



## 4.0.0 (2019-05-11)

* fix(switch): depend on dynamic@1.0 ([a4478ec](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/a4478ec))


### BREAKING CHANGE

* a lot


## 3.5.0 (2019-04-08)

* fix(switch): depend on component@4.0 ([4a13b39](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/4a13b39))



## 3.4.0 (2019-04-02)

* fix(switch): correct invalid main in package.json ([65c7706](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/65c7706))
* fix(switch): update deps and depend on dynamic@0.2 ([5763078](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/5763078))



## 3.3.0 (2019-03-23)

* fix(switch): depend on component@3.6 ([f003966](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/f003966))



## 3.2.0 (2019-03-23)

* fix(switch): depend on component@3.5 after@3.4 ([25bfdd7](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/25bfdd7))
* chore(switch): bump ([8b8b24e](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/8b8b24e))



## 3.0.0 (2019-03-23)

* fix(switch): depend on component@3.4 ([474cbd5](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/474cbd5))



## 3.0.0 (2019-03-10)

* feat(switch): depend on component@3.2 after@3.2 ([fe55775](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/fe55775))


### BREAKING CHANGE

* depend on component@3


## 2.1.0 (2019-02-25)

* fix(switch): add node_modules to .npmignore ([3f6ed34](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/3f6ed34))



## 2.0.0 (2019-02-04)

* fix(switch): update @component ([327522e](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/327522e))
* chore(switch): add files ([879e5cb](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/879e5cb))


### BREAKING CHANGE

* yes


