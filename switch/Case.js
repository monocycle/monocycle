import isFunction from 'ramda-adjunct/src/isFunction.js';
import when from 'ramda/src/when.js';
import unless from 'ramda/src/unless.js';
import always from 'ramda/src/always.js';
import lensProp from 'ramda/src/lensProp.js';
import over from 'ramda/src/over.js';
import defaultTo from 'ramda/src/defaultTo.js';
import T from 'ramda/src/T.js';
import unapply from 'ramda/src/unapply.js';
import isRegExp from 'ramda-adjunct/src/isRegExp.js';
import applySpec from 'ramda/src/applySpec.js';
import equals from 'ramda/src/equals.js';
import prop from 'ramda/src/prop.js';
import ifElse from 'ramda/src/ifElse.js';
import pipe from 'ramda/src/pipe.js';
import lt from 'ramda/src/lt.js';





const match = regex => x => x.match(regex);

export const Case = pipe(
  unapply(ifElse(
    pipe(prop('length'), lt(1)),
    applySpec({
      resolve: prop(0),
      value: prop(1),
    }),
    prop(0)
  )),
  over(lensProp('resolve'), pipe(
    defaultTo(T),
    when(isRegExp, match),
    unless(isFunction, equals)
  )),
  over(lensProp('Value'), pipe(
    unless(isFunction, always(always(undefined))),
  )),
  ({ resolve, value, Value }) => {

    return pipe(
      resolve,
      resolved => resolved && (value || Value(resolved)) || false
    );
  },
);