import test from 'ava';
import { Case } from './Case.js';
import ensureArray from 'ramda-adjunct/src/ensureArray.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import pipe from 'ramda/src/pipe.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import { withFunctionTest } from '@monocycle/component/lib/utilities/WithFactoryMacro.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';


const withCaseTest = pipe(

  test => t => {

    const { testFunction } = t.context;

    const testCaseFunction = testFunction(Case);

    const testCase = pipe(
      ensurePlainObj,
      over(lensProp('args'), ensureArray),
      over(lensProp('caseArgs'), ensureArray),
      ({ args, caseArgs, expected }) => {

        const caseA = testCaseFunction(args);

        t.true(isFunction(caseA));

        const returned = caseA(...caseArgs);

        t.is(returned, expected);

        return returned;
      }
    );

    testCase.nbTests = testFunction.nbTests + 2;

    return test(t, testCase);
  },
  withFunctionTest
);


test('Case: creates a resolve function from a case object', withCaseTest((t, testCase) => {

  t.plan((testCase.nbTests * 3) + 2);

  const isGa = x => x === 'GA';

  testCase({
    args: [],
    caseArgs: [],
    expected: false
  });

  testCase({
    args: {
      resolve: x => {

        t.is(x, 'GA');

        return isGa(x);
      },
      value: 'zo'
    },
    caseArgs: 'GA',
    expected: 'zo'
  });

  testCase({
    args: {
      resolve: x => {

        t.is(x, 'B');

        return x === 'A';
      },
      value: 'bu'
    },
    caseArgs: 'B',
    expected: false
  });
}));

test(`Case: calls 'Value' with resolved value as argument`, withCaseTest((t, testCase) => {

  t.plan((testCase.nbTests * 2) + 3);

  const startsWithG = x => x.match(/^G(.*)/);

  testCase({
    args: {
      resolve: x => {

        t.is(x, 'GA');

        return startsWithG(x);
      },
      Value: matches => {

        t.deepEqual(
          matches,
          startsWithG('GA')
        );

        return 'zo';
      }
    },
    caseArgs: 'GA',
    expected: 'zo'
  });

  testCase({
    args: {
      resolve: x => {

        t.is(x, 'BU');

        return startsWithG(x);
      },
      Value: t.fail
    },
    caseArgs: 'BU',
    expected: false
  });
}));