import { parseFrom } from '@monocycle/listener';
import { Composite, WithComposite } from '@monocycle/component';
import { WithDynamic } from '@monocycle/dynamic';
import { makeEmptyObject } from '@monocycle/component/lib/utilities/empty.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { behaviorToFactory } from '@monocycle/component/lib/utilities/factory.js';
import isTrue from 'ramda-adjunct/src/isTrue.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import ensureArray from 'ramda-adjunct/src/ensureArray.js';
import unless from 'ramda/src/unless.js';
import propSatisfies from 'ramda/src/propSatisfies.js';
import applyTo from 'ramda/src/applyTo.js';
import ifElse from 'ramda/src/ifElse.js';
import always from 'ramda/src/always.js';
import lensProp from 'ramda/src/lensProp.js';
import over from 'ramda/src/over.js';
import map from 'ramda/src/map.js';
import pipe from 'ramda/src/pipe.js';
import { Case as _Case } from './Case.js';
import dropRepeatsModule from 'xstream/extra/dropRepeats.js';





const { default: dropRepeats } = dropRepeatsModule;

export const WithSwitch = pipe(
  ensurePlainObj,
  over(lensProp('from'), parseFrom),
  over(lensProp('merge'), unless(isFunction,
    ifElse(isTrue,
      always(Composite),
      always(false)
    ))),
  over(lensProp('default'), pipe(
    unless(isFunction, always(false)),
  )),
  over(lensProp('Default'), pipe(
    unless(isFunction, always(always(makeEmptyObject))),
  )),
  unless(propSatisfies(isFunction, 'resolve'),
    ({ resolve, merge, ...options }) => {

      const resolvers = pipe(
        ensureArray,
        map(unless(isFunction, _Case))
      )(resolve);

      return {
        ...options,
        resolve: value => {

          if (!merge) {

            let resolved;

            resolvers.some(resolve => {
              const returned = resolve(value);

              return returned && (resolved = returned);
            });

            return resolved;
          }

          const behaviors = resolvers
            .map(applyTo(value))
            .filter(Boolean);

          return component => WithComposite([
            ...behaviors
              .map(behavior => behavior(component))
          ])(component);
        }
      };
    }
  ),
  ({ default: defaultBehavior, Default, resolve, ...rest }) => ({
    ...rest,
    resolve: value => {

      const resolved = resolve(value);

      return resolved || defaultBehavior || Default(value);
    },
  }),
  ({ from, resolve, SinksKeys }) => {

    return WithDynamic({
      SinksKeys,
      from: (sinks, sources) => {

        return pipe(
          from,
          input$ => {

            return input$
              .compose(dropRepeats())
              .map(resolve)
              .remember();
          }
        )(sinks, sources);
      }
    });
  }
);

export const Switch = behaviorToFactory(WithSwitch);

export const Case = _Case;
