/* eslint-disable no-unused-vars */
import test from 'ava';
import { Stream as $ } from 'xstream';
import { WithSwitch, Switch, Case } from './Switch.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import { isComponent } from '@monocycle/component';
import toString from 'ramda/src/toString.js';
import always from 'ramda/src/always.js';
import keys from 'ramda/src/keys.js';





test('Exposes resolved sinks', t => {

  t.plan(3);

  const sources = { mySource: $.of(43) };

  const component = Switch({
    SinksKeys: always(['ga']),
    from: (...args) => {

      t.deepEqual(args, [{}, sources]);

      return sources.mySource;
    },
    resolve: x => {

      t.is(x, 43);

      return x > 42 && (component => sources => ({ ga: $.of('yay') }));
    },
    default: t.fail
  });

  const sinks = component(sources);

  t.deepEqual(keys(sinks), ['ga', 'Ready']);

  return sinks.ga.map(x => {

    t.is(x, 'yay');
  });
});


test('Exposes default sinks', t => {

  t.plan(3);

  const sources = { mySource: $.of(41) };

  const component = Switch({
    SinksKeys: always(['ga']),
    from: (...args) => {

      t.deepEqual(args, [{}, sources]);

      return args[1].mySource;
    },
    resolve: x => {

      t.is(x, 41);

      return x > 42 && (component => sources => ({ ga: $.of('yay') }));
    },
    default: component => {
      // t.is(component, Component.empty);

      // console.log('component', component)
      return sources => ({ ga: $.of('default') });
    }
  });

  const sinks = component(sources);

  t.deepEqual(keys(sinks), ['ga', 'Ready']);

  return sinks.ga.map(x => {

    t.is(x, 'default');
  });
});


test(`Generate 'resolve' function from Case`, t => {

  t.plan(2);

  const sources = { mySource: $.of(100) };

  const { ga } = Switch({
    SinksKeys: always(['ga']),
    from: (...args) => {

      t.deepEqual(args, [{}, sources]);

      return args[1].mySource;

    },
    resolve: Case({
      resolve: x => {

        t.is(x, 100);

        return x > 42;
      },
      value: always(() => ({ ga: $.of('yay') }))
    }),
    default: t.fail
  })(sources);

  return ga.map(x => {

    t.is(x, 'yay');
  });
});


const casesMacro = (t, Spec) => {

  const {
    sources,
    assertions,
    expectedSinks,
    merge,
    default: defaultBehavior
  } = Spec(t);

  const component = WithSwitch({
    SinksKeys: always(['ga', 'bu', 'zo']),
    merge,
    from: (...args) => {

      t.deepEqual(args, [{}, sources]);

      return args[1].mySource;
    },
    resolve: [
      Case({
        resolve: x => {

          assertions.resolve[0](x);

          return x > 42;
        },
        value: component => sources => ({ ga: $.of('yay') })
      }),
      Case({
        resolve: x => {

          assertions.resolve[1](x);

          return x % 2 === 0;
        },
        Value: isEven => {

          return component => sources => ({ bu: $.of('isEven:' + isEven) });
        }
      }),
      Case({
        resolve: x => {

          assertions.resolve[2](x);

          return toString(x).match(/^4(.*)/);
        },
        Value: matches => component => sources => ({ zo: $.of('matches:' + matches[1]) })
      }),
    ],
    Default: value => component => {

      assertions.default(value);

      return defaultBehavior(component);
    }
  })();

  t.true(isComponent(component));

  const sinks = component(sources);

  t.true(isPlainObj(sinks));

  t.deepEqual(keys(sinks), expectedSinks);

  return $.merge(
    sinks.ga.map(assertions.sinks[0]),
    sinks.bu.map(assertions.sinks[1]),
    sinks.zo.map(assertions.sinks[2]),
  );
};

casesMacro.title = (title = ''/* , input, expected */) => `Generate 'resolve' from array of Case` + (title ? ' (' + title + ')' : '');
casesMacro.tests = 4;

test(casesMacro, t => {

  t.plan(casesMacro.tests + 9);

  return {
    sources: { mySource: $.of(41) },
    expectedSinks: ['ga', 'bu', 'zo', 'Ready'],
    assertions: {
      resolve: [
        x => t.is(x, 41),
        x => t.is(x, 41),
        x => t.is(x, 41),
      ],
      default: () => t.fail(),
      sinks: [
        () => t.fail(),
        () => t.fail(),
        x => t.is(x, 'matches:1'),
      ]
    }
  };
});

test('Merges all resolved values', t => {

  t.plan(13);

  const sources = { mySource: $.of(44) };

  const component = Switch({
    SinksKeys: always(['ga', 'bu', 'zo']),
    merge: true,
    from: (...args) => {

      t.deepEqual(args, [{}, sources]);

      return args[1].mySource;
    },
    resolve: [
      Case({
        resolve: x => {

          t.is(x, 44);

          return x > 42;
        },
        value: component => sources => ({
          ga: $.of('yay')
        })
      }),
      Case({
        resolve: x => {

          t.is(x, 44);

          return x % 2 === 0;
        },
        Value: isEven => component => sources => ({
          bu: $.of('isEven:' + isEven)
        })
      }),
      Case({
        resolve: x => {

          t.is(x, 44);

          return toString(x).match(/^4(.*)/);
        },
        Value: matches => component => sources => ({
          zo: $.of('matches:' + matches[1])
        })
      }),
    ],
    default: t.fail.bind(t)
  });

  t.true(isComponent(component));

  const sinks = component(sources);

  t.true(isPlainObj(sinks));

  t.deepEqual(keys(sinks), ['ga', 'bu', 'zo', 'Ready']);

  return $.combine(
    sinks.ga.map(ga => t.is(ga, 'yay')),
    sinks.bu.map(bu => t.is(bu, 'isEven:true')),
    sinks.zo.map(zo => t.is(zo, 'matches:4')),
  );
});

test('Stops iterating other cases when a case resolves', casesMacro, t => {
  t.plan(casesMacro.tests + 3);

  return {
    sources: { mySource: $.of(43) },
    expectedSinks: ['ga', 'bu', 'zo', 'Ready'],
    assertions: {
      resolve: [
        x => t.is(x, 43),
        () => t.fail(),
        () => t.fail(),
      ],
      default: () => t.fail(),
      sinks: [
        x => t.is(x, 'yay'),
        () => t.fail(),
        () => t.fail(),
      ]
    }
  };
});

test('Exposes the default component when no case resolved', casesMacro, t => {
  t.plan(casesMacro.tests + 9);

  return {
    sources: { mySource: $.of(39) },
    expectedSinks: ['ga', 'bu', 'zo', 'Ready'],
    default: () => () => ({ ga: $.of('a'), zo: $.of('b') }),
    assertions: {
      resolve: [
        x => t.is(x, 39),
        x => t.is(x, 39),
        x => t.is(x, 39)
      ],
      default: x => t.is(x, 39),
      sinks: [
        x => t.is(x, 'a'),
        () => t.fail(),
        x => t.is(x, 'b'),
      ]
    }
  };
});
