# 5.0.0 (2020-11-09)


### Bug Fixes

* **symbols:** make package esm compatible ([50083b2](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/50083b2ff8aa21b60ba66dac36c10d984503dd3a))


### BREAKING CHANGES

* **symbols:** maybe



# 4.7.0 (2020-10-07)


### Bug Fixes

* **symbols:** depends on component@8.6 ([fc24093](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/fc24093bb42fefb7c08670a15659e0b8e11fb217))



# 4.6.0 (2020-05-19)


### Bug Fixes

* **symbols:** update deps ([33cd815](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/33cd815f6eeed1e45b18f3558fe738aedee0cc6f))



# 4.5.0 (2020-01-20)


### Bug Fixes

* **symbols:** depends on component@5.5 ([ae88f3c](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/ae88f3ca1a6aacd24977b81b270da6985138fe52))



# 4.4.0 (2020-01-20)


### Bug Fixes

* **symbols:** depends on component@5.4 ([f781b5b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f781b5b105a7ea70368f652fd7895736fb4df2c7))



# 4.3.0 (2019-12-02)


### Bug Fixes

* **symbols:** update deps ([e9938f1](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/e9938f1da48f4384ee5b660c20312e6d6a2afaed))



# 4.2.0 (2019-12-02)


### Bug Fixes

* **symbols:** add lock file ([f7bb053](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f7bb053e05939c39ff1bfe907a704dbee43f172a))



# 4.1.0 (2019-12-02)


### Bug Fixes

* **symbols:** add lock file ([f7bb053](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/f7bb053e05939c39ff1bfe907a704dbee43f172a))



# 4.0.0 (2019-11-11)


### Features

* **symbols:** hello es modules ([726de9b](https://gitlab.com/monocycle/gitlab:monocycle/monocycle/commit/726de9b56331674696195c098ff38daae914cb94))


### BREAKING CHANGES

* **symbols:** yes



## 2.10.0 (2019-04-08)

* fix(symbols): depend on component@4.0 ([23843de](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/23843de))
* chore(symbols): replace void with undefined ([44410db](https://gitlab.com/monocycle/monocycle/tree/master/gitlab:monocycle/monocycle/commit/44410db))



## 2.9.0 (2019-03-23)

* fix(symbols): depend on component@3.6 ([1b0a0fd](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/1b0a0fd))



## 2.8.0 (2019-03-23)

* fix(symbols): depend on component@3.4 ([a48b61d](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/a48b61d))
* chore(symbols): remove default exports ([52a6624](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/52a6624))



## 2.7.0 (2019-03-09)

* fix(symbols): update tests ([007f494](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/007f494))



## 2.6.0 (2019-03-09)

* fix(symbols): depend on component@3.2 ([60885b9](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/60885b9))



## 2.5.0 (2019-03-03)

* fix(symbols): require @monocycle/component instead of ../component ([4fd378e](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/4fd378e))



## 2.4.0 (2019-02-25)

* fix(symbols): bump due to npm offset ([0f43f08](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/0f43f08))



## 2.2.0 (2019-02-25)

* fix(symbols): add node_modules to .npmignore ([ba9e598](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/ba9e598))



## 2.1.0 (2019-02-24)

* feat(symbols): add globalOptions and improve "set" method ([8f516d5](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/8f516d5))



## 2.0.0 (2019-02-03)

* fix(symbols): add @monocycle/component dep and change require accordingly ([5068b7e](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/5068b7e))
* fix(symbols): update @component ([332d5a3](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/332d5a3))
* chore(symbols): add --fix flag to eslint and add watch-test script ([143bdad](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/143bdad))
* chore(symbols): add linter dep and fix errors ([e159ff5](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/e159ff5))
* chore(symbols): add package.json file ([f068086](https://gitlab.com/monocycle/monocycle/tree/master/monocycle/monocycle/commit/f068086))

