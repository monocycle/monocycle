
import pipe from 'ramda/src/pipe.js';
import always from 'ramda/src/always.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import isNotEmpty from 'ramda-adjunct/src/isNotEmpty.js';
import isFalsy from 'ramda-adjunct/src/isFalsy.js';
import over from 'ramda/src/over.js';
import lensProp from 'ramda/src/lensProp.js';
import unless from 'ramda/src/unless.js';
import both from 'ramda/src/both.js';
import when from 'ramda/src/when.js';
import True from 'ramda/src/T.js';
import apply from 'ramda/src/apply.js';
import prop from 'ramda/src/prop.js';
import applyTo from 'ramda/src/applyTo.js';
import __ from 'ramda/src/__.js';





export const Store = pipe(
  ensurePlainObj,
  over(lensProp('assert'), unless(isFunction, always(undefined))),
  over(lensProp('guard'), unless(isFunction, always(True))),
  over(lensProp('data'), ensurePlainObj),
  ({ data, guard, assert }) => {

    const _get = pipe(
      prop,
      applyTo(data),
    );

    const store = {
      get: (key, ...args) => pipe(
        prop,
        applyTo(data),
        when(
          both(isFunction, always(isNotEmpty(args))),
          apply(__, args)
        ),
        when(both(always(assert), isFalsy), () => void assert(key)),
      )(key),
      has: pipe(_get, Boolean),
      set: (key, value, ...rest) => {

        const returned = guard(key, value, ...rest);

        if (returned)
          data[key] = returned === true
            ? value
            : returned;
      },
      hasStore: true
    };

    return store;
  }
);