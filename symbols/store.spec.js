import test from 'ava';
import jsc from 'jsverify';
import { Store } from './store.js';
import identity from 'ramda/src/internal/_identity.js';
import always from 'ramda/src/always.js';
import when from 'ramda/src/when.js';
import either from 'ramda/src/either.js';
import keys from 'ramda/src/keys.js';
import isTrue from 'ramda-adjunct/src/isTrue.js';
import isFalsy from 'ramda-adjunct/src/isFalsy.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';

const thruthyArb = jsc.oneof(
  jsc.number.smap(when(isFalsy, always(42)), identity),
  jsc.fn,
  jsc.dict,
  jsc.nearray,
  jsc.nestring,
  jsc.json.smap(when(either(isFalsy, isTrue), always({ a: 1 })), identity),
);

test('gets, sets values', t => {

  const store = Store();

  t.deepEqual(
    keys(store),
    ['get', 'has', 'set', 'hasStore']
  );

  t.true(isFunction(store.get));
  t.true(isFunction(store.has));
  t.true(isFunction(store.set));
  t.true(store.hasStore);

  const gaSymbol = Symbol('ga');

  t.false(store.has('bu'));
  t.is(store.get('bu'), undefined);
  t.is(store.get(gaSymbol), undefined);

  t.is(store.set(gaSymbol), undefined);
  t.is(store.get(gaSymbol), undefined);
  t.false(store.has(gaSymbol));

  t.is(store.set(gaSymbol, 42), undefined);
  t.is(store.get(gaSymbol), 42);
  t.true(store.has(gaSymbol));

  t.is(store.set(gaSymbol, 'zo'), undefined);
  t.is(store.get(gaSymbol), 'zo');
  t.true(store.has(gaSymbol));

  t.is(store.set(gaSymbol), undefined);
  t.is(store.get(gaSymbol), undefined);
  t.false(store.has(gaSymbol));
});

test('gets, sets a value (with default data)', t => {

  const store = Store({
    data: { bu: 41 },
  });

  t.is(store.get('bu'), 41);
  t.is(store.set('bu', 42));
  t.is(store.get('bu'), 42);
});

test('guard allows setting value', t => {

  t.plan(5);

  const store = Store({
    data: { bu: 41 },
    guard: (...args) => {

      t.is(args.length, 4);
      t.deepEqual(args, ['bu', 42, 43, 44]);

      return true;
    }
  });

  t.is(store.get('bu'), 41);
  t.is(store.set('bu', 42, 43, 44));
  t.is(store.get('bu'), 42);
});

test('guard prevents setting value', t => {

  t.plan(400);
  const property = jsc.forall(jsc.falsy, a => {

    const store = Store({
      guard: always(a)
    });

    t.is(store.get('bu'), undefined);
    t.is(store.set('bu', 42), undefined);
    t.is(store.set('bu', 43), undefined);
    t.is(store.get('bu'), undefined);

    return true;
  });

  jsc.assert(property, {
    tests: 100,
    size: 100
  });
});

test('guard allows setting overriden value (non-true)', t => {

  t.plan(500);
  const property = jsc.forall(thruthyArb, a => {

    const store = Store({
      data: { bu: 41 },
      guard: (...args) => {

        t.is(args.length, 4);
        t.deepEqual(args, ['bu', 42, 43, 44]);

        return a;
      }
    });

    t.is(store.get('bu'), 41);
    t.is(store.set('bu', 42, 43, 44));
    t.is(store.get('bu'), a);

    return true;
  });

  jsc.assert(property, {
    tests: 100,
    size: 100
  });
});

test('auto apply a function "value" when arguments', t => {

  const property = jsc.forall(jsc.nearray(thruthyArb), jsc.nearray(thruthyArb), (argsA, argsB) => {

    const store = Store();
    const calls = [];

    const f = (...args) => {

      calls.push(args);
      return args;
    };

    store.set('zo', f);

    t.is(store.get('zo'), f);

    const zoA = store.get('zo', ...argsA);

    t.deepEqual(zoA, argsA);
    t.deepEqual(calls, [argsA]);

    const zoB = store.get('zo', ...argsB);

    t.deepEqual(zoB, argsB);
    t.deepEqual(calls, [argsA, argsB]);

    return true;
  });

  jsc.assert(property, {
    tests: 100,
  });
});

test('calls assert when asked item is not found', t => {

  const calls = [];

  const store = Store({
    assert: (...args) => {

      calls.push(args);
    }
  });

  store.get('ga');
  store.get('bu');
  store.get('zo', {});

  t.deepEqual(
    calls,
    [
      ['ga'],
      ['bu'],
      ['zo'],
    ]
  );
});