import { Store } from './store.js';
import isFunction from 'ramda-adjunct/src/isFunction.js';
import isBoolean from 'ramda-adjunct/src/isBoolean.js';
import isPlainObj from 'ramda-adjunct/src/isPlainObj.js';
import isArray from 'ramda-adjunct/src/isArray.js';
import merge from 'ramda/src/merge.js';
import unless from 'ramda/src/unless.js';
import concat from 'ramda/src/concat.js';
import pipe from 'ramda/src/pipe.js';
import __ from 'ramda/src/__.js';
import always from 'ramda/src/always.js';
import keys from 'ramda/src/keys.js';
import assoc from 'ramda/src/assoc.js';
import arrayOf from 'ramda/src/of.js';
import tryCatch from 'ramda/src/tryCatch.js';
import objOf from 'ramda/src/objOf.js';
import over from 'ramda/src/over.js';
import lt from 'ramda/src/lt.js';
import map from 'ramda/src/map.js';
import apply from 'ramda/src/apply.js';
import ifElse from 'ramda/src/ifElse.js';
import prop from 'ramda/src/prop.js';
import when from 'ramda/src/when.js';
import filter from 'ramda/src/filter.js';
import both from 'ramda/src/both.js';
import propSatisfies from 'ramda/src/propSatisfies.js';
import lensProp from 'ramda/src/lensProp.js';
import { ensurePlainObj } from '@monocycle/component/lib/utilities/ensurePlainObj.js';
import { coerce } from '@monocycle/component/lib/utilities/coerce.js';





export const WithSymbols = pipe(
  ensurePlainObj,
  over(lensProp('strict'), unless(isBoolean, always(true))),
  over(lensProp('mergeOptions'), unless(isFunction, always(merge))),
  over(lensProp('globalOptions'), ensurePlainObj),
  ({ mergeOptions, globalOptions, strict }) => {

    const assert = !strict
      ? undefined
      : key => {
        throw new Error(`Unknown '${key}' symbol`);
      };

    return Component => {

      const BehaviorFactory = (factory, defaultOptions) => {

        const makeBehavior = (options, ...rest) => pipe(
          unless(isFunction, always(coerce))(factory.coerce),
          mergeOptions.bind(undefined, ensurePlainObj(defaultOptions)),
          assoc('Component', Component),
          arrayOf,
          concat(__, rest),
          apply(factory)
        )(options);

        return Object.assign(makeBehavior, factory);
      };

      const set = (key, value, options = {}) => {

        if (isPlainObj(key))
          return keys(key).map(k => Component.set(
            k.startsWith('With')
              ? k.slice(4)
              : k,
            key[k]
          ));

        if (Component.has(key))
          return;

        try {

          const create = pipe(
            BehaviorFactory,
            Behavior => Object.assign(Behavior, {
              make: tryCatch(
                (...args) => Behavior(...args)(),
                err => {
                  throw new Error(`Cannot create ${key} ⇐ ${err.stack}]`);
                }
              )
            })
          );

          return pipe(
            unless(isArray, pipe(
              arrayOf,
              concat(__, [options]),
              arrayOf
            )),
            // ensureArray,
            map(pipe(
              when(isArray, ([factory, options]) => ({
                factory,
                options
              })),
              unless(isPlainObj, objOf('factory')),
              over(lensProp('factory'), unless(isFunction, Component.get)),
              over(lensProp('options'), ensurePlainObj)
            )),
            filter(both(
              propSatisfies(isFunction, 'factory'),
              propSatisfies(isPlainObj, 'options'),
            )),
            map(({ factory, options }) => create(factory, merge(options, globalOptions))),
            ifElse(propSatisfies(lt(1), 'length'),
              behaviorFactories => create(({ has, ...options }) => component => Component([
                component,
                ...behaviorFactories.map(tryCatch(
                  (behaviorFactory, i, arr) => behaviorFactory.make({
                    ...options,
                    has: i === arr.length - 1
                      ? has
                      : undefined
                  }),
                  err => {
                    throw new Error(`Cannot create '${key}' composite ⇐ ${err.stack}]`);
                  }
                ))
              ])),
              prop(0)
            ),
          )(value);
        } catch (err) {
          throw new Error(`Cannot set '${key}' symbol ⇐ ${err.stack}`);
        }
      };

      return Object.assign(
        Component,
        Store({
          guard: set,
          assert
        }),
        {
          hasSymbols: true
        }
      );
    };
  }
);